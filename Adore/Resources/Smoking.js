{
    "-1": {
        "en": "Not Specified",
        "zh": "未填"
    },
    "1": {
        "en": "No",
        "zh": "不吸烟"
    },
    "2": {
        "en": "Seldomly",
        "zh": "很少"
    },
    "3": {
        "en": "Sometimes",
        "zh": "偶尔"
    },
    "4": {
        "en": "Often",
        "zh": "经常"
    },
    "5": {
        "en": "Frequent",
        "zh": "频繁"
    }
}