{
    "-1": {
        "en": "Not Specified",
        "zh": "未填"
    },
    "1": {
        "en": "Citizen",
        "zh": "公民"
    },
    "2": {
        "en": "Green Card",
        "zh": "绿卡"
    },
    "3": {
        "en": "Permanent resident",
        "zh": "永久居民"
    },
    "4": {
        "en": "Study Visa",
        "zh": "学习签证"
    },
    "5": {
        "en": "Work Visa",
        "zh": "工作签证"
    },
    "6": {
        "en": "Other",
        "zh": "其它"
    }
}