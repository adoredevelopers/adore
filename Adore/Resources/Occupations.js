{
    "-1": {
        "en": "Not Specified",
        "zh": "未填"
    },
    "1": {
        "en":"Accountants",
        "zh":"会计"
    },
    "2": {
        "en":"Actuaries",
        "zh":"精算师"
    },
    "3": {
        "en":"Administrative",
        "zh":"行政"
    },
    "4": {
        "en":"Advertising",
        "zh":"广告"
    },
    "5": {
        "en":"Aerospace Engineers",
        "zh":"航空航天工程师"
    },
    "6": {
        "en":"Agricultural Engineers",
        "zh":"农业工程"
    },
    "7": {
        "en":"Agricultural Representatives, Consultants and Specialists",
        "zh":"农业专家"
    },
    "8": {
        "en":"Air Traffic Controllers",
        "zh":"空管"
    },
    "9": {
        "en":"Aircraft Mechanics",
        "zh":"航空技师"
    },
    "10": {
        "en":"Airworthiness Inspectors",
        "zh":"航空器适航监察员"
    },
    "11": {
        "en":"Animal Health Technologists",
        "zh":"兽医"
    },
    "12": {
        "en":"Animation Painters",
        "zh":"动漫家"
    },
    "13": {
        "en":"Anthropologists",
        "zh":"人类学家"
    },
    "14": {
        "en":"Appraisers",
        "zh":"估价师"
    },
    "15": {
        "en":"Archaeologists",
        "zh":"考古学家"
    },
    "16": {
        "en":"Architects",
        "zh":"建筑师"
    },
    "17": {
        "en":"Archivists",
        "zh":"档案专业人员"
    },
    "18": {
        "en":"Astronomers",
        "zh":"天文学家"
    },
    "19": {
        "en":"Audiologists",
        "zh":"听力学家"
    },
    "20": {
        "en":"Audio and Video Recording Technicians",
        "zh":"音频与视频录入技师"
    },
    "21": {
        "en":"Avionics Inspectors, Mechanics and Technicians",
        "zh":"电子设备专家"
    },
    "22": {
        "en":"Bakers",
        "zh":"面包师"
    },
    "23": {
        "en":"Barbers",
        "zh":"理发师"
    },
    "24": {
        "en":"Biological Technicians",
        "zh":"生物学家"
    },
    "25": {
        "en":"Biomedical Engineers",
        "zh":"生物工程师"
    },
    "26": {
        "en":"Blacksmiths",
        "zh":"铁匠"
    },
    "27": {
        "en":"Boilermakers",
        "zh":"锅炉制造工"
    },
    "28": {
        "en":"Bookkeepers",
        "zh":"簿记员"
    },
    "29": {
        "en":"Broadcast Technicians",
        "zh":"播音技师"
    },
    "30": {
        "en":"Cabinetmakers",
        "zh":"家具木工"
    },
    "31": {
        "en":"Cable Television Service Technicians",
        "zh":"有线电视服务与维护技术员"
    },
    "32": {
        "en":"Carvers",
        "zh":"雕刻匠"
    },
    "33": {
        "en":"Chefs ",
        "zh":"大厨"
    },
    "34": {
        "en":"Chemical Engineers",
        "zh":"化学工程师"
    },
    "35": {
        "en":"Chemists",
        "zh":"配药师"
    },
    "36": {
        "en":"Civil Engineering",
        "zh":"土木工程"
    },
    "37": {
        "en":"Computer Hardware Engineers",
        "zh":"计算机硬件工程师"
    },
    "38": {
        "en":"Conservators",
        "zh":"保护区管理员"
    },
    "39": {
        "en":"Cooks",
        "zh":"炊事员"
    },
    "40": {
        "en":"Copywriters",
        "zh":"文案人员"
    },
    "41": {
        "en":"Curatorial Assistants",
        "zh":"馆长助理"
    },
    "42": {
        "en":"Dental Technicians",
        "zh":"牙科技术师"
    },
    "43": {
        "en":"Denturists",
        "zh":"牙托技师"
    },
    "44": {
        "en":"Directors",
        "zh":"董事"
    },
    "45": {
        "en":"Driver's Licence Examiners",
        "zh":"驾照考官"
    },
    "46": {
        "en":"Driving Instructors",
        "zh":"驾照教官"
    },
    "47": {
        "en":"Economists",
        "zh":"经济学家"
    },
    "48": {
        "en":"Editors",
        "zh":"编辑"
    },
    "49": {
        "en":"Electrical Mechanics",
        "zh":"电子技师"
    },
    "51": {
        "en":"Executive Chefs",
        "zh":"特级厨师"
    },
    "52": {
        "en":"Fashion Designers",
        "zh":"时装设计师"
    },
    "53": {
        "en":"Financial and Investment Analysts",
        "zh":"金融投资分析师"
    },
    "54": {
        "en":"Financial Planners",
        "zh":"财经管理"
    },
    "55": {
        "en":"Firefighters",
        "zh":"消防员"
    },
    "56": {
        "en":"Historians",
        "zh":"历史学家"
    },
    "57": {
        "en":"Industrial Engineering",
        "zh":"制造工程师"
    },
    "58": {
        "en":"Insurance Clerks",
        "zh":"保险管理员"
    },
    "59": {
        "en":"Interpreters",
        "zh":"翻译"
    },
    "60": {
        "en":"Jewellers and Related Workers",
        "zh":"珠宝商"
    },
    "61": {
        "en":"Journalists",
        "zh":"记者"
    },
    "62": {
        "en":"Landscape Architects",
        "zh":"景观设计师"
    },
    "63": {
        "en":"Legal Assistants and Paralegals",
        "zh":"法律助理"
    },
    "64": {
        "en":"Librarians",
        "zh":"图书管理员"
    },
    "65": {
        "en":"Locksmiths",
        "zh":"开锁匠"
    },
    "66": {
        "en":"Machinists",
        "zh":"机械师"
    },
    "67": {
        "en":"Mechanical Engineers",
        "zh":"机械工程师"
    },
    "68": {
        "en":"Medical Technologists",
        "zh":"医学技师"
    },
    "69": {
        "en":"Meteorologists",
        "zh":"气象学家"
    },
    "70": {
        "en":"Orthopaedic Technologists",
        "zh":"整形外科技师"
    },
    "71": {
        "en":"Patent Agents",
        "zh":"专利师"
    },
    "73": {
        "en":"Photographers",
        "zh":"照相师"
    },
    "74": {
        "en":"Physicists",
        "zh":"物理学家"
    },
    "75": {
        "en":"Potters",
        "zh":"陶工"
    },
    "76": {
        "en":"Psychologists",
        "zh":"心理学家"
    },
    "77": {
        "en":"Psychometricians",
        "zh":"精神治疗医师"
    },
    "78": {
        "en":"Railway Locomotive Engineers",
        "zh":"火车技师"
    },
    "79": {
        "en":"Teacher",
        "zh":"老师"
    },
    "80": {
        "en":"Seamstresses",
        "zh":"裁缝"
    },
    "81": {
        "en":"Sociologists",
        "zh":"社会学家"
    },
    "82": {
        "en":"Software Engineers",
        "zh":"软件工程师"
    },
    "83": {
        "en":"Taxidermists",
        "zh":"剥制师"
    },
    "85": {
        "en":"Upholsterers",
        "zh":"室内装潢商"
    },
    "86": {
        "en":"Doctor",
        "zh":"医生"
    },
    "87": {
        "en":"Lawer",
        "zh":"律师"
    },
    "88": {
        "en":"Driver",
        "zh":"司机"
    },
    "999": {
        "en":"Other",
        "zh":"其它"
    }
}