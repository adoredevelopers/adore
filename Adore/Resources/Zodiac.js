{
    "1": {
        "en": "Aries",
        "zh": "白羊座"
    },
    "2": {
        "en": "Taurus",
        "zh": "金牛座"
    },
    "3": {
        "en": "Gemini",
        "zh": "双子座"
    },
    "4": {
        "en": "Cancer",
        "zh": "巨蟹座"
    },
    "5": {
        "en": "Leo",
        "zh": "狮子座"
    },
    "6": {
        "en": "Virgo",
        "zh": "处女座"
    },
    "7": {
        "en": "Libra",
        "zh": "天秤座"
    },
    "8": {
        "en": "Scorpio",
        "zh": "天蝎座"
    },
    "9": {
        "en": "Sagittarius",
        "zh": "射手座"
    },
    "10": {
        "en": "Capricorn",
        "zh": "摩羯座"
    },
    "11": {
        "en": "Aquarius",
        "zh": "水瓶座"
    },
    "12": {
        "en": "Pisces",
        "zh": "双鱼座"
    }
}