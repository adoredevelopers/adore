{
    "-1": {
        "en": "Not Specified",
        "zh": "未填"
    },
    "1": {
        "en": "Self Taught",
        "zh": "自学成才"
    },
    "2": {
        "en": "Secondary",
        "zh": "高中"
    },
    "3": {
        "en": "Post Secondary",
        "zh": "大专"
    },
    "4": {
        "en": "Bachelor",
        "zh": "本科"
    },
    "5": {
        "en": "Master",
        "zh": "研究生"
    },
    "6": {
        "en": "PhD.",
        "zh": "博士生"
    },
    "7": {
        "en": "Post Doc",
        "zh": "博士后"
    }
}