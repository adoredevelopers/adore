{
    "-1": {
        "en": "Not Specified",
        "zh": "未填"
    },
    "1": {
        "en": "No",
        "zh": "滴酒不沾"
    },
    "2": {
        "en": "Ocassional",
        "zh": "偶尔喝喝"
    },
    "3": {
        "en": "Casual Drinker",
        "zh": "社交聚会"
    },
    "4": {
        "en": "Moderate",
        "zh": "半斤八两"
    },
    "5": {
        "en": "A Way Of Life",
        "zh": "千杯不倒"
    }
}