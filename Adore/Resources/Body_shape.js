{
    "-1": {
        "en": "Not Specified",
        "zh": "未填"
    },
    "1": {
        "en": "Cute 'n' Tiny",
        "zh": "小巧"
    },
    "2": {
        "en": "Slim",
        "zh": "纤细"
    },
    "3": {
        "en": "Slender",
        "zh": "苗条"
    },
    "4": {
        "en": "Fit",
        "zh": "匀称"
    },
    "5": {
        "en": "Athletic",
        "zh": "运动型"
    },
    "6": {
        "en": "Typical",
        "zh": "正常"
    },
    "7": {
        "en": "Built",
        "zh": "结实"
    },
    "8": {
        "en": "Muscular",
        "zh": "健美型"
    },
    "9": {
        "en": "Brawny",
        "zh": "肌肉型"
    },
    "10": {
        "en": "Curvy",
        "zh": "凹凸有致"
    },
    "11": {
        "en": "Chubby",
        "zh": "有点小肉"
    },
    "12": {
        "en": "Rubenesque",
        "zh": "丰满"
    },
    "13": {
        "en": "Very Huggy",
        "zh": "大抱枕"
    }
}
