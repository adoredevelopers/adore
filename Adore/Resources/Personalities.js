{
    "-1": {
        "en": "Not Specified",
        "zh": "未填"
    },
    "1": {
        "en":"INTJ-A (Analysts - Assertive Architect): Imaginative and strategic thinkers, with a plan for everything.",
        "zh":"INTJ-A ()"
    },
    "2": {
        "en":"INTJ-T (Analysts - Turbulent Architect): Imaginative and strategic thinkers, with a plan for everything.",
        "zh":"INTJ-T ()"
    },
    "3": {
        "en":"INTP-A (Analysts - Assertive Logician): Innovative inventors with an unquenchable thirst for knowledge.",
        "zh":"INTP-A ()"
    },
    "4": {
        "en":"INTP-T (Analysts - Turbulent Logician): Innovative inventors with an unquenchable thirst for knowledge.",
        "zh":"INTP-T ()"
    },
    "5": {
        "en":"ENTJ-A (Analysts - Assertive Commander): Bold, imaginative and strong-willed leaders, always finding a way, or making one.",
        "zh":"ENTJ-A ()"
    },
    "6": {
        "en":"ENTJ-T (Analysts - Turbulent Commander): Bold, imaginative and strong-willed leaders, always finding a way, or making one.",
        "zh":"ENTJ-T ()"
    },
    "7": {
        "en":"ENTP-A (Analysts - Assertive Debater): Smart and curious thinkers who cannot resist an intellectual challenge.",
        "zh":"ENTP-A ()"
    },
    "8": {
        "en":"ENTP-T (Analysts - Turbulent Debater): Smart and curious thinkers who cannot resist an intellectual challenge.",
        "zh":"ENTP-T ()"
    },
    "9": {
        "en":"INFJ-A (Diplomats - Assertive Advocate): Quite and mystical, yet very inspiring and tireless idealists.",
        "zh":"INFJ-A ()"
    },
    "10": {
        "en":"INFJ-T (Diplomats - Turbulent Advocate): Quite and mystical, yet very inspiring and tireless idealists.",
        "zh":"INFJ-T ()"
    },
    "11": {
        "en":"INFP-A (Diplomats - Assertive Mediator): Poetic, kind and altruistic people, always eager to help a good cause.",
        "zh":"INFP-A ()"
    },
    "12": {
        "en":"INFP-T (Diplomats - Turbulent Mediator): Poetic, kind and altruistic people, always eager to help a good cause.",
        "zh":"INFP-T ()"
    },
    "13": {
        "en":"ENFJ-A (Diplomats - Assertive Protagonist): Charismatic and inspiring leaders, able to mesmerize their listeners.",
        "zh":"ENFJ-A ()"
    },
    "14": {
        "en":"ENFJ-T (Diplomats - Turbulent Protagonist): Charismatic and inspiring leaders, able to mesmerize their listeners.",
        "zh":"ENFJ-T ()"
    },
    "15": {
        "en":"ENFP-A (Diplomats - Assertive Campaigner): Enthusiastic, creative and sociable free spirits, who can always find a reason to smile.",
        "zh":"ENFP-A ()"
    },
    "16": {
        "en":"ENFP-T (Diplomats - Turbulent Campaigner): Enthusiastic, creative and sociable free spirits, who can always find a reason to smile.",
        "zh":"ENFP-T ()"
    },
    "17": {
        "en":"ISTJ-A (Sentinels - Assertive Logistician): Practical and fast-minded individuals, whose reliability cannot be doubted.",
        "zh":"ISTJ-A ()"
    },
    "18": {
        "en":"ISTJ-T (Sentinels - Turbulent Logistician): Practical and fast-minded individuals, whose reliability cannot be doubted.",
        "zh":"ISTJ-T ()"
    },
    "19": {
        "en":"ISFJ-A (Sentinels - Assertive Defender): Very dedicated and warm protectors, always ready to defend their loved ones.",
        "zh":"ISFJ-A ()"
    },
    "20": {
        "en":"ISFJ-T (Sentinels - Turbulent Defender): Very dedicated and warm protectors, always ready to defend their loved ones.",
        "zh":"ISFJ-T ()"
    },
    "21": {
        "en":"ESTJ-A (Sentinels - Assertive Executive): Excellent administrators, unsurpassed at managing things, or people.",
        "zh":"ESTJ-A ()"
    },
    "22": {
        "en":"ESTJ-T (Sentinels - Turbulent Executive): Excellent administrators, unsurpassed at managing things, or people.",
        "zh":"ESTJ-T ()"
    },
    "23": {
        "en":"ESFJ-A (Sentinels - Assertive Consul): Extraordinarily caring, social and popular people, always eager to help.",
        "zh":"ESFJ-A ()"
    },
    "24": {
        "en":"ESFJ-T (Sentinels - Turbulent Consul): Extraordinarily caring, social and popular people, always eager to help.",
        "zh":"ESFJ-T ()"
    },
    "25": {
        "en":"ISTP-A (Explorers - Assertive Virtuoso): Bold and practical experimenters, masters of all kinds of tools.",
        "zh":"ISTP-A ()"
    },
    "26": {
        "en":"ISTP-T (Explorers - Turbulent Virtuoso): Bold and practical experimenters, masters of all kinds of tools.",
        "zh":"ISTP-T ()"
    },
    "27": {
        "en":"ISFP-A (Explorers - Assertive Adventurer): Flexible and charming artists, always ready to explore and experience somthing new.",
        "zh":"ISFP-A ()"
    },
    "28": {
        "en":"ISFP-T (Explorers - Turbulent Adventurer): Flexible and charming artists, always ready to explore and experience somthing new.",
        "zh":"ISFP-T ()"
    },
    "29": {
        "en":"ESTP-A (Explorers - Assertive Extrepreneur): Smart, energetic and very perceptive people, who truly enjoy living on the edge.",
        "zh":"ESTP-A ()"
    },
    "30": {
        "en":"ESTP-T (Explorers - Turbulent Extrepreneur): Smart, energetic and very perceptive people, who truly enjoy living on the edge.",
        "zh":"ESTP-T ()"
    },
    "31": {
        "en":"ESFP-A (Explorers - Assertive Entertainer): Spontaneous, energetic and enthusiastic people, life is never boring around them.",
        "zh":"ESFP-A ()"
    },
    "32": {
        "en":"ESFP-T (Explorers - AsTurbulentsertive Entertainer): Spontaneous, energetic and enthusiastic people, life is never boring around them.",
        "zh":"ESFP-T ()"
    }
}
