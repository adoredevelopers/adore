{
	"1": {
		"en":"English",
		"zh":"英文"
	},
	"2": {
		"en":"Mandarin",
		"zh":"国语"
	},
	"3": {
		"en":"Cantonese",
		"zh":"粤语（繁体字）"
	},
	"4": {
		"en":"French",
		"zh":"法语"
	},
	"5": {
		"en":"Afrikaans",
		"zh":"南非洲的荷兰语"
	},
	"6": {
		"en":"Albanian",
		"zh":"阿尔巴尼亚语"
	},
	"7": {
		"en":"Arabic",
		"zh":"阿拉伯语"
	},
	"8": {
		"en":"Armenian",
		"zh":"亚美尼亚语"
	},
	"9": {
		"en":"Basque",
		"zh":"巴斯克语"
	},
	"10": {
		"en":"Bengali",
		"zh":"孟加拉语"
	},
	"11": {
		"en":"Bulgarian",
		"zh":"保加利亚语"
	},
	"12": {
		"en":"Catalan",
		"zh":"加泰罗尼亚语"
	},
	"13": {
		"en":"Cambodian",
		"zh":"柬埔寨语"
	},
	"14": {
		"en":"Croatian",
		"zh":"克罗地亚人语"
	},
	"15": {
		"en":"Czech",
		"zh":"捷克语"
	},
	"16": {
		"en":"Danish",
		"zh":"丹麦语"
	},
	"17": {
		"en":"Dutch",
		"zh":"荷兰语"
	},
	"18": {
		"en":"Estonian",
		"zh":"爱沙尼亚语"
	},
	"19": {
		"en":"Fiji",
		"zh":"斐济语"
	},
	"20": {
		"en":"Finnish",
		"zh":"芬兰语"
	},
	"21": {
		"en":"Georgian",
		"zh":"乔治亚语"
	},
	"22": {
		"en":"German",
		"zh":"德语"
	},
	"23": {
		"en":"Greek",
		"zh":"希腊语"
	},
	"24": {
		"en":"Gujarati",
		"zh":"古吉拉特语"
	},
	"25": {
		"en":"Hebrew",
		"zh":"希伯来语"
	},
	"26": {
		"en":"Hindi",
		"zh":"北印度语"
	},
	"27": {
		"en":"Hungarian",
		"zh":"匈牙利语"
	},
	"28": {
		"en":"Icelandic",
		"zh":"冰岛语"
	},
	"29": {
		"en":"Indonesian",
		"zh":"印尼语群"
	},
	"30": {
		"en":"Irish",
		"zh":"爱尔兰语"
	},
	"31": {
		"en":"Italian",
		"zh":"意大利语"
	},
	"32": {
		"en":"Japanese",
		"zh":"日语"
	},
	"33": {
		"en":"Javanese",
		"zh":"爪哇语"
	},
	"34": {
		"en":"Korean",
		"zh":"韩语"
	},
	"35": {
		"en":"Latin",
		"zh":"拉丁语"
	},
	"36": {
		"en":"Latvian",
		"zh":"拉脱维亚语"
	},
	"37": {
		"en":"Lithuanian",
		"zh":"立陶宛语"
	},
	"38": {
		"en":"Macedonian",
		"zh":"马其顿语"
	},
	"39": {
		"en":"Malay",
		"zh":"马来语"
	},
	"40": {
		"en":"Malayalam",
		"zh":"印度西南部语"
	},
	"41": {
		"en":"Maltese",
		"zh":"马尔他语"
	},
	"42": {
		"en":"Maori",
		"zh":"毛利语"
	},
	"43": {
		"en":"Marathi",
		"zh":"马拉地语"
	},
	"44": {
		"en":"Mongolian",
		"zh":"蒙古语"
	},
	"45": {
		"en":"Nepali",
		"zh":"尼泊尔语"
	},
	"46": {
		"en":"Norwegian",
		"zh":"挪威语"
	},
	"47": {
		"en":"Persian",
		"zh":"波斯语"
	},
	"48": {
		"en":"Polish",
		"zh":"波兰语"
	},
	"49": {
		"en":"Portuguese",
		"zh":"葡萄牙语"
	},
	"50": {
		"en":"Punjabi",
		"zh":"旁遮普语"
	},
	"51": {
		"en":"Quechua",
		"zh":"盖丘亚语"
	},
	"52": {
		"en":"Romanian",
		"zh":"罗马尼亚语"
	},
	"53": {
		"en":"Russian",
		"zh":"俄语"
	},
	"54": {
		"en":"Samoan",
		"zh":"萨摩亚语"
	},
	"55": {
		"en":"Serbian",
		"zh":"塞尔维亚语"
	},
	"56": {
		"en":"Slovak",
		"zh":"斯洛伐克人语"
	},
	"57": {
		"en":"Slovenian",
		"zh":"斯洛文尼亚语"
	},
	"58": {
		"en":"Spanish",
		"zh":"西班牙语"
	},
	"59": {
		"en":"Swahili",
		"zh":"斯瓦希里语"
	},
	"60": {
		"en":"Swedish",
		"zh":"瑞典语"
	},
	"61": {
		"en":"Tamil",
		"zh":"坦米尔语"
	},
	"62": {
		"en":"Tatar",
		"zh":"鞑靼语"
	},
	"63": {
		"en":"Telugu",
		"zh":"泰卢固语"
	},
	"64": {
		"en":"Thai",
		"zh":"泰国语"
	},
	"65": {
		"en":"Tibetan",
		"zh":"藏语"
	},
	"66": {
		"en":"Tonga",
		"zh":"汤加语"
	},
	"67": {
		"en":"Turkish",
		"zh":"土耳其语"
	},
	"68": {
		"en":"Ukrainian",
		"zh":"乌克兰语"
	},
	"69": {
		"en":"Urdu",
		"zh":"乌尔都语"
	},
	"70": {
		"en":"Uzbek",
		"zh":"乌兹别克语"
	},
	"71": {
		"en":"Vietnamese",
		"zh":"土耳其语"
	},
	"72": {
		"en":"Welsh",
		"zh":"威尔士语"
	},
	"73": {
		"en":"Xhosa",
		"zh":"班图语"
	},
    "999": {
        "en":"Other",
        "zh":"其它"
    }
}