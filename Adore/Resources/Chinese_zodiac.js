{
    "0": {
        "en": "Monkey",
        "zh": "猴"
    },
    "1": {
        "en": "Rooster",
        "zh": "鸡"
    },
    "2": {
        "en": "Dog",
        "zh": "狗"
    },
    "3": {
        "en": "Pig",
        "zh": "猪"
    },
    "4": {
        "en": "Rat",
        "zh": "鼠"
    },
    "5": {
        "en": "Ox",
        "zh": "牛"
    },
    "6": {
        "en": "Tiger",
        "zh": "虎"
    },
    "7": {
        "en": "Rabbit",
        "zh": "兔"
    },
    "8": {
        "en": "Dragon",
        "zh": "龙"
    },
    "9": {
        "en": "Snake",
        "zh": "蛇"
    },
    "10": {
        "en": "Horse",
        "zh": "马"
    },
    "11": {
        "en": "Goat",
        "zh": "羊"
    }
}