{
    "-1": {
        "en": "Not Specified",
        "zh": "未填"
    },
    "25": {
        "en": "less than $25K annually",
        "zh": "年薪少于 15万"
    },
    "50": {
        "en": "$25K - $50K annually",
        "zh": "年薪 15万 - 25万"
    },
    "75": {
        "en": "$50K - $75K annually",
        "zh": "年薪 25万 - 40万"
    },
    "100": {
        "en": "$75K - $100K annually",
        "zh": "年薪 40万 - 55万"
    },
    "150": {
        "en": "$100K - $150K annually",
        "zh": "年薪 55万 - 80万"
    },
    "300": {
        "en": "$150K to $300K annually",
        "zh": "年薪 80万 - 150万"
    },
    "999": {
        "en": "more than $300K annually",
        "zh": "年薪超过 150万"
    }
}
