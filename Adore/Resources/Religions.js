{
    "-1": {
        "en": "Not Specified",
        "zh": "未填"
    },
    "1": {
        "en": "Nonreligious",
        "zh": "无"
    },
    "2": {
        "en": "Atheism",
        "zh": "无神论"
    },
    "3": {
        "en": "Buddhism",
        "zh": "佛教"
    },
    "4": {
        "en": "Christians",
        "zh": "基督教"
    },
    "5": {
        "en": "Muslims",
        "zh": "穆斯林"
    },
    "6": {
        "en": "Hinduism",
        "zh": "印度教"
    },
    "7": {
        "en": "Islam",
        "zh": "伊斯兰教"
    },
    "8": {
        "en": "Judaism",
        "zh": "犹太教"
    },
    "9": {
        "en": "Shinto",
        "zh": "神道教"
    },
    "999": {
        "en": "Other",
        "zh": "其它"
    }
}