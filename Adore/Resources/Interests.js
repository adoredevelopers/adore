{
	"1": {
		"en":"Aircraft Spotting",
		"zh":"航观"
	},
	"2": {
		"en":"Acting",
		"zh":"演戏"
	},
	"3": {
		"en":"Aeromodeling",
		"zh":"航模"
	},
	"4": {
		"en":"Animals/pets/dogs",
		"zh":"宠物"
	},
	"5": {
		"en":"Archery",
		"zh":"射箭"
	},
	"6": {
		"en":"Arts",
		"zh":"美术"
	},
	"7": {
		"en":"Aquarium",
		"zh":"水族"
	},
	"8": {
		"en":"Astrology",
		"zh":"占星"
	},
	"9": {
		"en":"Astronomy",
		"zh":"天文"
	},
	"10": {
		"en":"Badminton",
		"zh":"羽毛球"
	},
	"11": {
		"en":"Baseball",
		"zh":"棒球"
	},
	"12": {
		"en":"Base Jumping",
		"zh":"低空跳伞"
	},
	"13": {
		"en":"Basketball",
		"zh":"篮球"
	},
	"14": {
		"en":"Beach tanning",
		"zh":"沙滩日晒"
	},
	"15": {
		"en":"Beachcombing",
		"zh":"沙滩冲浪"
	},
	"16": {
		"en":"Bicycling",
		"zh":"自行车"
	},
	"17": {
		"en":"Bird watching",
		"zh":"观鸟"
	},
	"18": {
		"en":"Board Games",
		"zh":"桌游"
	},
	"19": {
		"en":"Boating",
		"zh":"划船"
	},
	"20": {
		"en":"Body Building",
		"zh":"健身"
	},
	"21": {
		"en":"Bonsai Tree",
		"zh":"盆栽"
	},
	"22": {
		"en":"Boomerangs",
		"zh":"回旋镖"
	},
	"23": {
		"en":"Bowling",
		"zh":"保龄球"
	},
	"24": {
		"en":"Cake Decorating",
		"zh":"糕点装饰"
	},
	"25": {
		"en":"Calligraphy",
		"zh":"书法"
	},
	"26": {
		"en":"Camping",
		"zh":"野营"
	},
	"27": {
		"en":"Canoeing",
		"zh":"独木舟"
	},
	"28": {
		"en":"Cartooning",
		"zh":"画漫画"
	},
	"29": {
		"en":"Car Racing",
		"zh":"飙车"
	},
	"30": {
		"en":"Casino Gambling",
		"zh":"赌场"
	},
	"31": {
		"en":"Ceramics",
		"zh":"陶术"
	},
	"32": {
		"en":"Cheerleading",
		"zh":"啦啦队"
	},
	"33": {
		"en":"Church/church activities",
		"zh":"教会"
	},
	"34": {
		"en":"Coin Collecting",
		"zh":"硬币收集"
	},
	"35": {
		"en":"Collecting",
		"zh":"收集"
	},
	"36": {
		"en":"Collecting Antiques",
		"zh":"股东"
	},
	"37": {
		"en":"Cooking",
		"zh":"烹饪"
	},
	"38": {
		"en":"Cosplay",
		"zh":"角色扮演"
	},
	"39": {
		"en":"Crafts",
		"zh":"手工"
	},
	"40": {
		"en":"Dancing",
		"zh":"跳舞"
	},
	"41": {
		"en":"Darts",
		"zh":"飞镖"
	},
	"42": {
		"en":"Dominoes",
		"zh":"多米诺"
	},
	"43": {
		"en":"Drawing",
		"zh":"画画"
	},
	"44": {
		"en":"Eating out",
		"zh":"吃货"
	},
	"45": {
		"en":"Fishing",
		"zh":"钓鱼"
	},
	"46": {
		"en":"Football",
		"zh":"橄榄球"
	},
	"47": {
		"en":"Gardening",
		"zh":"园艺"
	},
	"48": {
		"en":"Golf",
		"zh":"高尔夫"
	},
	"49": {
		"en":"Go Kart Racing",
		"zh":"卡丁车"
	},
	"50": {
		"en":"Guitar",
		"zh":"吉他"
	},
	"51": {
		"en":"Hiking",
		"zh":"徒步旅行"
	},
	"52": {
		"en":"Horse riding",
		"zh":"骑马"
	},
	"53": {
		"en":"Hunting",
		"zh":"打猎"
	},
	"54": {
		"en":"Iceskating",
		"zh":"滑冰"
	},
	"55": {
		"en":"Jump Roping",
		"zh":"跳绳"
	},
	"56": {
		"en":"Kayaking",
		"zh":"皮船"
	},
	"57": {
		"en":"Kites",
		"zh":"风筝"
	},
	"58": {
		"en":"Legos",
		"zh":"乐高"
	},
	"59": {
		"en":"Listening to music",
		"zh":"听音乐"
	},
	"60": {
		"en":"Magic",
		"zh":"魔术"
	},
	"61": {
		"en":"Marksmanship",
		"zh":"射击"
	},
	"62": {
		"en":"Martial Arts",
		"zh":"武术"
	},
	"63": {
		"en":"Models",
		"zh":"模型"
	},
	"64": {
		"en":"Motorcycles",
		"zh":"摩托党"
	},
	"65": {
		"en":"Mountain Climbing",
		"zh":"爬山"
	},
	"66": {
		"en":"Origami",
		"zh":"折纸"
	},
	"67": {
		"en":"Painting",
		"zh":"油画"
	},
	"68": {
		"en":"Paintball",
		"zh":"彩蛋射击"
	},
	"69": {
		"en":"Parachuting",
		"zh":"跳伞"
	},
	"70": {
		"en":"Parkour",
		"zh":"跑酷"
	},
	"71": {
		"en":"Photography",
		"zh":"摄影"
	},
	"72": {
		"en":"Piano",
		"zh":"钢琴"
	},
	"73": {
		"en":"Rafting",
		"zh":"漂流"
	},
	"74": {
		"en":"Reading",
		"zh":"阅读"
	},
	"75": {
		"en":"Robotics",
		"zh":"机器人"
	},
	"76": {
		"en":"Rockets",
		"zh":"火箭"
	},
	"77": {
		"en":"Running",
		"zh":"跑"
	},
	"78": {
		"en":"Skiing",
		"zh":"滑雪"
	},
	"79": {
		"en":"Shopping",
		"zh":"逛街"
	},
	"80": {
		"en":"Skateboarding",
		"zh":"滑雪板"
	},
	"81": {
		"en":"Sketching",
		"zh":"素描"
	},
	"82": {
		"en":"Sky Diving",
		"zh":"高空跳伞"
	},
	"83": {
		"en":"Sleeping",
		"zh":"睡"
	},
	"84": {
		"en":"Snorkeling",
		"zh":"潜水"
	},
	"85": {
		"en":"Soccer",
		"zh":"足球"
	},
	"86": {
		"en":"Pool",
		"zh":"台球"
	},
	"87": {
		"en":"Socializing",
		"zh":"交友"
	},
	"88": {
		"en":"Stamp Collecting",
		"zh":"集邮"
	},
	"89": {
		"en":"Surfing",
		"zh":"冲浪"
	},
	"90": {
		"en":"Karaoke",
		"zh":"卡拉OK"
	},
	"91": {
		"en":"Swimming",
		"zh":"游泳"
	},
	"92": {
		"en":"Table Tennies",
		"zh":"乒乓球"
	},
	"93": {
		"en":"Tea Tasting",
		"zh":"品茶"
	},
	"94": {
		"en":"Tennis",
		"zh":"网球"
	},
	"95": {
		"en":"Traveling",
		"zh":"旅游"
	},
	"96": {
		"en":"Treasure Hunting",
		"zh":"寻宝"
	},
	"97": {
		"en":"TV watching",
		"zh":"看电视"
	},
	"98": {
		"en":"Ultimate Frisbee",
		"zh":"飞碟"
	},
	"99": {
		"en":"Video Games",
		"zh":"电游"
	},
	"100": {
		"en":"Walking",
		"zh":"走"
	},
	"101": {
		"en":"Wine Tasting",
		"zh":"品酒"
	},
	"102": {
		"en":"Writing",
		"zh":"写作"
	},
	"103": {
		"en":"Yoga",
		"zh":"瑜伽"
	}
}