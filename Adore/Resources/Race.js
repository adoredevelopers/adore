{
    "-1": {
        "en": "Not Specified",
        "zh": "未填"
    },
    "1": {
        "en": "Chinese / Ethnic Chinese",
        "zh": "华人 / 华裔"
    },
    "2": {
        "en": "Cambodian",
        "zh": "柬埔寨"
    },
    "3": {
        "en": "Filipino",
        "zh": "菲律宾"
    },
    "4": {
        "en": "Japanese",
        "zh": "日本"
    },
    "5": {
        "en": "Korean",
        "zh": "韩国"
    },
    "6": {
        "en": "Laotian",
        "zh": "老挝"
    },
    "7": {
        "en": "Malaysian",
        "zh": "马来西亚"
    },
    "8": {
        "en": "Thai",
        "zh": "泰国"
    },
    "9": {
        "en": "Vietnamese",
        "zh": "越南"
    },
    "10": {
        "en": "Asian",
        "zh": "亚裔"
    },
    "11": {
        "en": "Korean",
        "zh": "韩国"
    },
    "12": {
        "en": "Black",
        "zh": "黑人"
    },
    "13": {
        "en": "Hispanic / Latin",
        "zh": "西班牙 / 拉丁"
    },
    "14": {
        "en": "Indian",
        "zh": "印度"
    },
    "15": {
        "en": "Middle Eastern",
        "zh": "中东"
    },
    "16": {
        "en": "Native American",
        "zh": "北美土著"
    },
    "17": {
        "en": "White / Caucasian",
        "zh": "白人 / 高加索人"
    },
    "18": {
        "en": "Mixed",
        "zh": "混血"
    },
    "19": {
        "en": "Other",
        "zh": "其他"
    }
}