//
//  AppDelegate.h
//  Adore
//
//  Created by Chao Lu on 2013-09-08.
//  Copyright (c) 2013 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCLAlertView.h"
#import "MMDrawerController.h"
#import "ADLeftDrawerViewController.h"
#import "ADNavigationController.h"
#import "ADLogoutRequest.h"
#import "ADBackgroundJobManager.h"


typedef NS_ENUM(NSInteger, ADCenterDrawerViewControllerType)
{
    ADCenterDrawerViewControllerTypeNone,
    ADCenterDrawerViewControllerTypeLogin,
    ADCenterDrawerViewControllerTypeNearbyUsers,
    //    ADCenterDrawerViewControllerTypeNearbyEvents,
    ADCenterDrawerViewControllerTypeAdore,
    ADCenterDrawerViewControllerTypeChat,
    ADCenterDrawerViewControllerTypeSetting,
    ADCenterDrawerViewControllerTypeViewProfile
};



@interface AppDelegate : UIResponder <UIApplicationDelegate>


@property (nonatomic, assign) ADCenterDrawerViewControllerType centerDrawerType;
@property (nonatomic, strong) ADNavigationController *centerController;
@property (nonatomic, strong) MMDrawerController *drawerController;
@property (nonatomic, strong) ADLeftDrawerViewController *leftDrawerViewController;

@property (nonatomic, strong) ADNavigationController *viewProfileController;
@property (nonatomic, strong) ADNavigationController *nearbyNavigationController;
@property (nonatomic, strong) ADNavigationController *settingNavigationController;

@property (nonatomic, strong) ADLogoutRequest *logoutRequest;

@property (nonatomic, strong) ADUserManager *userManager;
@property (nonatomic ,strong) ADChatManager *chatManager;
@property (nonatomic, strong) ADBackgroundJobManager *backgroundJobManager;


@property (nonatomic, strong) UIWindow *window;

@property (strong, nonatomic) UIView *blurView;

@property (strong, nonatomic) SCLAlertView *alertView;


- (void)logoutCurrentUser;
- (void)logoutMultiDeviceUser;

- (void)showLeftDrawer;
- (void)showLoginView;
- (void)showPostLoginView;
- (void)showNearbyUsersView;
//- (void)showNearbyEventsView;
- (void)showAdoreView;
- (void)showPendingUserView;
- (void)showChatView;
- (void)showSettingView;
- (void)showViewProfileView;

@end
