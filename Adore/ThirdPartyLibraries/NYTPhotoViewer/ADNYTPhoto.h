//
//  ADNTYPhoto.h
//  Adore
//
//  Created by Wang on 2015-04-29.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NYTPhoto.h"

@protocol ADNTYPhotoDelegate;



@interface ADNYTPhoto : NSObject <NYTPhoto>


@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIImage *placeholderImage;
@property (nonatomic, strong) NSAttributedString *attributedCaptionTitle;
@property (nonatomic, strong) NSAttributedString *attributedCaptionSummary;
@property (nonatomic, strong) NSAttributedString *attributedCaptionCredit;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) BOOL selected;
@property (nonatomic, strong) NSString *user_id;
@property (nonatomic, strong) NSString *url;


@end