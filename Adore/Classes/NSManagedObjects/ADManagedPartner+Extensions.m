//
//  ADManagedPartner+Extensions.m
//  Adore
//
//  Created by Kevin Wang on 2015-06-18.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADManagedPartner+Extensions.h"
#import "ADCouponObject.h"
#import "ADBusinessPartnerObject.h"
#import "NSString+ThumbnailURL.h"
#import "ADNYTPhoto.h"



@implementation ADManagedPartner (Extensions)

#pragma mark -
#pragma mark - Helper methods

- (void)updateWithBusinessPartnerObject:(ADBusinessPartnerObject *)partner
{
    self.partner_id = partner.partner_id;
    self.name = partner.name;
    self.legal_name = partner.legal_name;
    
    self.address = partner.address;
    self.postal_code = partner.postal_code;
    self.phone_number = partner.phone_number;
    self.homepage = partner.phone_number;
    
    self.number_of_recommendations = @(partner.number_of_recommendations);
    self.popularity = @(partner.popularity);
    self.coupon_discount = @(partner.coupon_discount);
    self.coupon_discount_message = partner.coupon_discount_message;
    self.partner_level = partner.partner_level;
    
    self.partner_images = [NSKeyedArchiver archivedDataWithRootObject:partner.partner_images];
    self.activity_tags = [NSKeyedArchiver archivedDataWithRootObject:partner.activity_tags];
}


#pragma mark -
#pragma mark - Images related

- (NSArray *)arrayOfNYTPhotos
{
    NSMutableArray *photos = [NSMutableArray array];
    NSArray *partnerImagesArray = [self partnerImagesArray];
    
    for (NSString *url in partnerImagesArray) {
        [photos addObject:[self NYTPhotoWithUrl:url]];
    }
    return photos.copy;
}

- (ADNYTPhoto *)NYTPhotoWithUrl:(NSString *)url
{
    ADNYTPhoto *photo = [[ADNYTPhoto alloc] init];
    photo.url = url;
    photo.placeholderImage = [UIImage imageNamed:@"profile_image_placeholder_150x150"];
    
    NSString *nameString = self.name ? self.name : @"";
    
    photo.attributedCaptionTitle = [[NSAttributedString alloc] initWithString:nameString attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    return photo;
}

- (NSArray *)partnerImagesArray
{
    NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:self.partner_images];
    return array;
}

- (NSString *)partnerImageAtIndex:(NSInteger)index
{
    NSArray *array = [self partnerImagesArray];
    if (index < array.count) {
        return array[index];
    }
    return nil;
}

- (NSString *)partnerImageThumbnailUrlAtIndex:(NSInteger)index
{
    return [[self partnerImageAtIndex:index] thumbnailImageUrl];
}


//- (UIImage *)partnerTypeImage
//{
//    return [self activityImageAtIndex:0];
//}
//
//- (NSArray *)activityTagsArray
//{
//    NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:self.activity_tags];
//    return array;
//}
//
//- (UIImage *)activityImageAtIndex:(NSInteger)index
//{
//    if (index >= self.activityTagsArray.count) {
//        return nil;
//    }
//    NSNumber *number = self.activityTagsArray[index];
//    ADPartnerActivityType type = [self activityTypeForNumber:number];
//    return [self imageForActivityType:type];
//}
//
//- (ADPartnerActivityType)activityTypeForNumber:(NSNumber *)number
//{
//    switch ([number integerValue]) {
//        case 1:
//            return ADPartnerActivityTypeBoardGame;
//        case 2:
//            return ADPartnerActivityTypeDessert;
//        case 3:
//            return ADPartnerActivityTypeTea;
//        case 4:
//            return ADPartnerActivityTypeCoffee;
//        case 5:
//            return ADPartnerActivityTypeFood;
//        case 6:
//            return ADPartnerActivityTypeDrinks;
//        case 7:
//            return ADPartnerActivityTypeFroyo;
//        case 8:
//            return ADPartnerActivityTypeBowling;
//        case 9:
//            return ADPartnerActivityTypeDart;
//        case 10:
//            return ADPartnerActivityTypeGoKart;
//        case 11:
//            return ADPartnerActivityTypeGolf;
//        case 12:
//            return ADPartnerActivityTypeKaraoke;
//        default:
//            return ADPartnerActivityTypeInvalid;
//    }
//}
//
//- (UIImage *)imageForActivityType:(ADPartnerActivityType)type
//{
//    switch (type) {
//        case ADPartnerActivityTypeBoardGame:
//            return [UIImage imageNamed:@"coupon_dice"];
//
//        case ADPartnerActivityTypeDessert:
//        case ADPartnerActivityTypeTea:
//        case ADPartnerActivityTypeCoffee:
//
//        case ADPartnerActivityTypeFood:
//            return [UIImage imageNamed:@"coupon_dinner"];
//
//        case ADPartnerActivityTypeDrinks:
//            return [UIImage imageNamed:@"coupon_drink"];
//
//        case ADPartnerActivityTypeFroyo:
//            return [UIImage imageNamed:@"coupon_ice_cream"];
//
//        case ADPartnerActivityTypeBowling:
//            return [UIImage imageNamed:@"coupon_bowling"];
//
//        case ADPartnerActivityTypeDart:
//            return [UIImage imageNamed:@"coupon_dart"];
//
//        case ADPartnerActivityTypeGoKart:
//
//        case ADPartnerActivityTypeGolf:
//            return [UIImage imageNamed:@"coupon_golf"];
//
//        case ADPartnerActivityTypeKaraoke:
//            return [UIImage imageNamed:@"coupon_karaoke"];
//
//        case ADPartnerActivityTypeInvalid:
//            return nil;
//    }
//    return nil;
//}


@end
