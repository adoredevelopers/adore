//
//  ADManagedUser.h
//  
//
//  Created by Wang on 2015-08-03.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ADManagedUser : NSManagedObject

@property (nonatomic, retain) NSDate * birthday;
@property (nonatomic, retain) NSNumber * body_shape;
@property (nonatomic, retain) NSNumber * c_zodiac;
@property (nonatomic, retain) NSData * display_image_urls;
@property (nonatomic, retain) NSNumber * drinking;
@property (nonatomic, retain) NSNumber * education_level;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * employer;
@property (nonatomic, retain) NSString * fullname;
@property (nonatomic, retain) NSString * gender;
@property (nonatomic, retain) NSNumber * height;
@property (nonatomic, retain) NSNumber * martial_status;
@property (nonatomic, retain) NSNumber * income;
@property (nonatomic, retain) NSData * interest_data;
@property (nonatomic, retain) NSNumber * isPendingUser;
@property (nonatomic, retain) NSNumber * isRead;
@property (nonatomic, retain) NSData * language_data;
@property (nonatomic, retain) NSNumber * last_updated_timestamp;
@property (nonatomic, retain) NSString * motto;
@property (nonatomic, retain) NSString * nickname;
@property (nonatomic, retain) NSNumber * occupation;
@property (nonatomic, retain) NSNumber * personality;
@property (nonatomic, retain) NSString * postgrad_edu_institution;
@property (nonatomic, retain) NSNumber * race;
@property (nonatomic, retain) NSString * related_to_pending_user_email;
@property (nonatomic, retain) NSNumber * relationship;
@property (nonatomic, retain) NSDate * relationship_created_date;
@property (nonatomic, retain) NSNumber * religion;
@property (nonatomic, retain) NSNumber * residential_status;
@property (nonatomic, retain) NSNumber * smoking;
@property (nonatomic, retain) NSString * undergrad_edu_institution;
@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSNumber * weight;
@property (nonatomic, retain) NSNumber * zodiac;
@property (nonatomic, retain) NSString * birth_city;
@property (nonatomic, retain) NSString * birth_country;
@property (nonatomic, retain) NSString * birth_province;
@property (nonatomic, retain) NSString * residence_city;
@property (nonatomic, retain) NSString * residence_province;
@property (nonatomic, retain) NSString * residence_country;
@property (nonatomic, retain) NSNumber * face_validation_result;

@end
