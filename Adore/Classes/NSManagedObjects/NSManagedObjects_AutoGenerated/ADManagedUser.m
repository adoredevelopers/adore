//
//  ADManagedUser.m
//  
//
//  Created by Wang on 2015-08-03.
//
//

#import "ADManagedUser.h"


@implementation ADManagedUser

@dynamic birthday;
@dynamic body_shape;
@dynamic c_zodiac;
@dynamic display_image_urls;
@dynamic drinking;
@dynamic education_level;
@dynamic email;
@dynamic employer;
@dynamic fullname;
@dynamic gender;
@dynamic height;
@dynamic martial_status;
@dynamic income;
@dynamic interest_data;
@dynamic isPendingUser;
@dynamic isRead;
@dynamic language_data;
@dynamic last_updated_timestamp;
@dynamic motto;
@dynamic nickname;
@dynamic occupation;
@dynamic personality;
@dynamic postgrad_edu_institution;
@dynamic race;
@dynamic related_to_pending_user_email;
@dynamic relationship;
@dynamic relationship_created_date;
@dynamic religion;
@dynamic residential_status;
@dynamic smoking;
@dynamic undergrad_edu_institution;
@dynamic userId;
@dynamic weight;
@dynamic zodiac;
@dynamic birth_city;
@dynamic birth_country;
@dynamic birth_province;
@dynamic residence_city;
@dynamic residence_province;
@dynamic residence_country;
@dynamic face_validation_result;

@end
