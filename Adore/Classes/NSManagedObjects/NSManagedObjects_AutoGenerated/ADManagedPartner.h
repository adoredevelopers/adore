//
//  ADManagedPartner.h
//  Adore
//
//  Created by Kevin Wang on 2015-06-18.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ADManagedCoupon;

@interface ADManagedPartner : NSManagedObject

@property (nonatomic, retain) NSData * activity_tags;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSNumber * coupon_discount;
@property (nonatomic, retain) NSString * coupon_discount_message;
@property (nonatomic, retain) NSString * enterprise_id;
@property (nonatomic, retain) NSString * homepage;
@property (nonatomic, retain) NSString * legal_name;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * number_of_recommendations;
@property (nonatomic, retain) NSString * partner_id;
@property (nonatomic, retain) NSData * partner_images;
@property (nonatomic, retain) NSString * partner_level;
@property (nonatomic, retain) NSString * phone_number;
@property (nonatomic, retain) NSNumber * popularity;
@property (nonatomic, retain) NSString * postal_code;
@property (nonatomic, retain) NSSet *coupons;
@end

@interface ADManagedPartner (CoreDataGeneratedAccessors)

- (void)addCouponsObject:(ADManagedCoupon *)value;
- (void)removeCouponsObject:(ADManagedCoupon *)value;
- (void)addCoupons:(NSSet *)values;
- (void)removeCoupons:(NSSet *)values;

@end
