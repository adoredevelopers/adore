//
//  ADManagedConversation.h
//  Adore
//
//  Created by Kevin Wang on 2015-06-18.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ADManagedChatMessage;

@interface ADManagedConversation : NSManagedObject

@property (nonatomic, retain) NSString * user_email;
@property (nonatomic, retain) NSString * message_owner_id;
@property (nonatomic, retain) ADManagedChatMessage *last_message;
@property (nonatomic, retain) NSSet *messages;
@end

@interface ADManagedConversation (CoreDataGeneratedAccessors)

- (void)addMessagesObject:(ADManagedChatMessage *)value;
- (void)removeMessagesObject:(ADManagedChatMessage *)value;
- (void)addMessages:(NSSet *)values;
- (void)removeMessages:(NSSet *)values;

@end
