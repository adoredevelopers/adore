//
//  ADManagedCoupon.h
//  Adore
//
//  Created by Kevin Wang on 2015-06-18.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ADManagedPartner;

@interface ADManagedCoupon : NSManagedObject

@property (nonatomic, retain) NSString * activity_tag;
@property (nonatomic, retain) NSString * connection_user_email;
@property (nonatomic, retain) NSString * coupon_code;
@property (nonatomic, retain) NSString * coupon_status;
@property (nonatomic, retain) NSDate * creation_date;
@property (nonatomic, retain) NSDate * expiration_date;
@property (nonatomic, retain) NSString * partner_id;
@property (nonatomic, retain) NSDate * read_date;
@property (nonatomic, retain) NSDate * redeem_date;
@property (nonatomic, retain) ADManagedPartner *partner;

@end
