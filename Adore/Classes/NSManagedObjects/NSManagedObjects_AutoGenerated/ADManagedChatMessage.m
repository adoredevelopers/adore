//
//  ADManagedChatMessage.m
//  Adore
//
//  Created by Kevin Wang on 2015-06-18.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADManagedChatMessage.h"
#import "ADManagedConversation.h"
#import "ADUserManager.h"
#import "ADManagedUser+Extensions.h"
#import "ADChatManager.h"



@implementation ADManagedChatMessage

@dynamic isIncoming;
@dynamic message;
@dynamic mode;
@dynamic read_date;
@dynamic sent_date;
@dynamic server_date;
@dynamic conversation;
@dynamic last_message_conversation;
@dynamic message_owner_id;



#pragma mark -
#pragma mark - JSQMessageData protocol

- (NSString *)senderId
{
    if (self.isIncoming) {
        return self.conversation.user_email;
    }
    else {
        return [ADUserManager sharedInstance].currentUserProfile.email;
    }
}

- (NSString *)senderDisplayName
{
    return @"1";
}

- (NSDate *)date
{
    return self.sent_date;
}

- (NSString *)text
{
    return self.message;
}

- (NSUInteger)messageHash
{
    return self.hash;
}

- (BOOL)isMediaMessage
{
    return NO;
}

@end
