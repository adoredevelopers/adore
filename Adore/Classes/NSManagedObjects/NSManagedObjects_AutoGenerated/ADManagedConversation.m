//
//  ADManagedConversation.m
//  Adore
//
//  Created by Kevin Wang on 2015-06-18.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADManagedConversation.h"
#import "ADManagedChatMessage.h"


@implementation ADManagedConversation

@dynamic user_email;
@dynamic message_owner_id;
@dynamic last_message;
@dynamic messages;

@end
