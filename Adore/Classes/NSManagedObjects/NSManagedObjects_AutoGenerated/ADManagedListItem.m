//
//  ADManagedListItem.m
//  Adore
//
//  Created by Wang on 2015-04-12.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADManagedListItem.h"


@implementation ADManagedListItem

@dynamic lid;
@dynamic cn_string;
@dynamic en_string;


- (NSString *)valueForLanguageCode:(NSString *)languageCode
{
    if ([languageCode isEqualToString:@"zh"]) {
        return self.cn_string;
    } else if ([languageCode isEqualToString:@"cn"]) {
        return self.cn_string;
    } else {
        return self.en_string;
    }
}


@end
