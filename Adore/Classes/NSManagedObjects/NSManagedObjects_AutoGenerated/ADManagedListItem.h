//
//  ADManagedListItem.h
//  Adore
//
//  Created by Wang on 2015-04-12.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ADManagedListItem : NSManagedObject


@property (nonatomic, retain) NSNumber * lid;
@property (nonatomic, retain) NSString * cn_string;
@property (nonatomic, retain) NSString * en_string;

- (NSString *)valueForLanguageCode:(NSString *)languageCode;


@end
