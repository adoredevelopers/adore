//
//  ADManagedCoupon.m
//  Adore
//
//  Created by Kevin Wang on 2015-06-18.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADManagedCoupon.h"
#import "ADManagedPartner.h"


@implementation ADManagedCoupon

@dynamic activity_tag;
@dynamic connection_user_email;
@dynamic coupon_code;
@dynamic coupon_status;
@dynamic creation_date;
@dynamic expiration_date;
@dynamic partner_id;
@dynamic read_date;
@dynamic redeem_date;
@dynamic partner;

@end
