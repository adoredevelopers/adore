//
//  ADManagedChatMessage.h
//  Adore
//
//  Created by Kevin Wang on 2015-06-18.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "JSQMessageData.h"

@class ADManagedConversation;



@interface ADManagedChatMessage : NSManagedObject <JSQMessageData>

@property (nonatomic, retain) NSNumber * isIncoming;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSString * mode;
@property (nonatomic, retain) NSDate * read_date;
@property (nonatomic, retain) NSDate * sent_date;
@property (nonatomic, retain) NSDate * server_date;
@property (nonatomic, retain) NSString * message_owner_id;
@property (nonatomic, retain) ADManagedConversation *conversation;
@property (nonatomic, retain) ADManagedConversation *last_message_conversation;


#pragma mark - JSQMessageData protocol

@property (nonatomic, copy, readonly) NSString *senderId;
@property (nonatomic, copy, readonly) NSString *senderDisplayName;
@property (nonatomic, copy, readonly) NSDate *date;
@property (nonatomic, copy, readonly) NSString *text;
@property (nonatomic, assign, readonly) BOOL isMediaMessage;


@end
