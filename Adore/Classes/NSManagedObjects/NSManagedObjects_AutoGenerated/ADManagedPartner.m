//
//  ADManagedPartner.m
//  Adore
//
//  Created by Kevin Wang on 2015-06-18.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADManagedPartner.h"
#import "ADManagedCoupon.h"


@implementation ADManagedPartner

@dynamic activity_tags;
@dynamic address;
@dynamic coupon_discount;
@dynamic coupon_discount_message;
@dynamic enterprise_id;
@dynamic homepage;
@dynamic legal_name;
@dynamic name;
@dynamic number_of_recommendations;
@dynamic partner_id;
@dynamic partner_images;
@dynamic partner_level;
@dynamic phone_number;
@dynamic popularity;
@dynamic postal_code;
@dynamic coupons;

@end
