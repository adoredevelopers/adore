//
//  ADManagedCoupon+Extensions.m
//  Adore
//
//  Created by Kevin Wang on 2015-06-18.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADManagedCoupon+Extensions.h"
#import "ADCouponObject.h"
#import "NSDate+Utils.h"
#import "ADNYTPhoto.h"
#import "ADManagedPartner+Extensions.h"



@implementation ADManagedCoupon (Extensions)


#pragma mark -
#pragma mark - Helper methods

- (void)updateWithCouponObject:(ADCouponObject *)coupon parnter:(ADManagedPartner *)partner
{
    self.partner_id = coupon.partner_id;
    self.coupon_code = coupon.coupon_code;
    self.activity_tag = coupon.coupon_activity_tag;
    self.connection_user_email = coupon.connection_user;
    self.coupon_status = coupon.coupon_status;
    self.creation_date = coupon.creation_date;
    self.expiration_date = coupon.expiration_date;
    self.redeem_date = coupon.redeem_date;
    
    self.partner = partner;
}

- (NSAttributedString *)expireAttributedString
{
    NSDate *today = [NSDate date];
    
    if ([[today dateComponet] compare:self.expiration_date] == NSOrderedSame) {
        return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Expires today", nil)];
    }
    else {
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        
        NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                            fromDate:[today dateComponet]
                                                              toDate:self.expiration_date
                                                             options:0];
        NSString *dayString = [NSString stringWithFormat:NSLocalizedString(@"Expires in %ld days", nil), components.day];
        if (components.day < 7) {
            return [[NSAttributedString alloc] initWithString:dayString attributes:@{ NSForegroundColorAttributeName : [UIColor redColor] }];
        }
        else {
            return [[NSAttributedString alloc] initWithString:dayString];
        }
    }
    return nil;
}

- (BOOL)expired
{
    NSDate *today = [NSDate date];
    return [today compare:self.expiration_date] == NSOrderedDescending;
}

- (BOOL)activated
{
    return [self.coupon_status isEqualToString:kAD_COUPON_STATUS_UNUSED] == NO;
}

- (BOOL)redeemed
{
    return [self.coupon_status isEqualToString:kAD_COUPON_STATUS_REDEEMED];
}


#pragma mark -
#pragma mark - QR Image

- (UIImage *)couponQRImage
{
    NSData *stringData = [self.coupon_code dataUsingEncoding:NSUTF8StringEncoding];
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"M" forKey:@"inputCorrectionLevel"];
    CIImage *outputImage = qrFilter.outputImage;
    CGAffineTransform transform = CGAffineTransformMakeScale(8.0f, 8.0f);
    CIImage *transformedImage = [outputImage imageByApplyingTransform:transform];
    UIImage *qrCodeImage = [UIImage imageWithCIImage:transformedImage];
    
    return qrCodeImage;
}

- (ADNYTPhoto *)couponNYTPhoto
{
    ADNYTPhoto *photo = [[ADNYTPhoto alloc] init];
    photo.image = self.couponQRImage;
    return photo;
}


@end
