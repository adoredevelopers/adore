//
//  ADManagedUser+Extensions.m
//  Adore
//
//  Created by Kevin Wang on 2015-06-18.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADManagedUser+Extensions.h"
#import "ADUserObject.h"
#import "NSString+ThumbnailURL.h"
#import "NSDate+Utils.h"
#import "ADNYTPhoto.h"
#import "ADDetailListCoreDataHelper.h"
#import "ADUserManager.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ADUserDefaultHelper.h"
#import "NSMutableDictionary+SetNonNilObject.h"
#import "ADNSURLCreator.h"
#import "NSDateFormatter+Utils.h"
#import "ADAddressObject.h"



@implementation ADManagedUser (Extensions)

#pragma mark -
#pragma mark - Helper methods

- (void)updateWithUserObject:(ADUserObject *)userObject
{
    self.userId = userObject.userId;
    self.email = userObject.email;
    self.motto = userObject.motto;
    self.nickname = userObject.nickname;
    self.fullname = userObject.fullname;
    self.gender = userObject.gender;
    self.birthday = userObject.birthday;
    self.c_zodiac = userObject.c_zodiac;
    self.zodiac = userObject.zodiac;
    self.weight = userObject.weight;
    self.height = userObject.height;
    self.martial_status = userObject.martial_status;
    self.body_shape = userObject.body_shape;
    self.race = userObject.race;
    self.residential_status = userObject.residential_status;
    self.residence_country = userObject.residence_country;
    self.residence_province = userObject.residence_province;
    self.residence_city = userObject.residence_city;
    self.birth_country = userObject.birth_country;
    self.birth_province = userObject.birth_province;
    self.birth_city = userObject.birth_city;
    
    self.religion = userObject.religion;
    self.drinking = userObject.drinking;
    self.smoking = userObject.smoking;
    self.undergrad_edu_institution = userObject.undergrad_edu_institution;
    self.postgrad_edu_institution = userObject.postgrad_edu_institution;
    self.employer = userObject.employer;
    self.occupation = userObject.occupation;
    self.income = userObject.income;
    self.education_level = userObject.education_level;
    self.personality = userObject.personality;
    
    self.display_image_urls = [NSKeyedArchiver archivedDataWithRootObject:userObject.displayImageUrlArray];
    self.interest_data = [NSKeyedArchiver archivedDataWithRootObject:userObject.interestArray];
    self.language_data = [NSKeyedArchiver archivedDataWithRootObject:userObject.languageArray];
    
    self.last_updated_timestamp = userObject.last_updated_timestamp;
}

- (NSInteger)age
{
    return [self.birthday age];
}

- (ADUserRelationshipType)relationType
{
    return [self.relationship integerValue];
}

- (NSString *)canonicalSearchTerm
{
    return self.nickname.lowercaseString;
}

- (void)setInterestArray:(NSArray *)array
{
    self.interest_data = [NSKeyedArchiver archivedDataWithRootObject:array];
}

- (NSArray *)interestArray
{
    NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:self.interest_data];
    return array;
}

- (void)setLanguageArray:(NSArray *)array
{
    self.language_data = [NSKeyedArchiver archivedDataWithRootObject:array];
}

- (NSArray *)languageArray
{
    NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:self.language_data];
    return array;
}

// list shows up in settings.
- (NSArray *)profileUserDetailItemArray
{
    NSMutableArray *tempDetailsArray = @[].mutableCopy;
    [tempDetailsArray addObject:[ADUserDetailItem headerBasicInfo]];
    [tempDetailsArray addObject:[ADUserDetailItem emailType]];
    [tempDetailsArray addObject:[ADUserDetailItem nameType]];
    [tempDetailsArray addObject:[ADUserDetailItem nicknameType]];
    [tempDetailsArray addObject:[ADUserDetailItem mottoType]];
    [tempDetailsArray addObject:[ADUserDetailItem genderType]];
    [tempDetailsArray addObject:[ADUserDetailItem birthdayType]];
    [tempDetailsArray addObject:[ADUserDetailItem c_zodiacType]];
    [tempDetailsArray addObject:[ADUserDetailItem zodiacType]];
    [tempDetailsArray addObject:[ADUserDetailItem raceType]];
    [tempDetailsArray addObject:[ADUserDetailItem heightType]];
    [tempDetailsArray addObject:[ADUserDetailItem weightType]];
    [tempDetailsArray addObject:[ADUserDetailItem martialStatusType]];
    [tempDetailsArray addObject:[ADUserDetailItem bodyShapeType]];
    [tempDetailsArray addObject:[ADUserDetailItem residentialStatusType]];
    [tempDetailsArray addObject:[ADUserDetailItem birthPlaceType]];
    [tempDetailsArray addObject:[ADUserDetailItem currentResidenceType]];
    
    [tempDetailsArray addObject:[ADUserDetailItem headerDetails]];
    [tempDetailsArray addObject:[ADUserDetailItem drinkingType]];
    [tempDetailsArray addObject:[ADUserDetailItem smokingType]];
    [tempDetailsArray addObject:[ADUserDetailItem religionType]];
    [tempDetailsArray addObject:[ADUserDetailItem languageType]];
    [tempDetailsArray addObject:[ADUserDetailItem interestType]];
    [tempDetailsArray addObject:[ADUserDetailItem personalityType]];
    
    [tempDetailsArray addObject:[ADUserDetailItem headerWorkEdu]];
    [tempDetailsArray addObject:[ADUserDetailItem educationType]];
    [tempDetailsArray addObject:[ADUserDetailItem undergradEduType]];
    [tempDetailsArray addObject:[ADUserDetailItem postgradEduType]];
    [tempDetailsArray addObject:[ADUserDetailItem employerType]];
    [tempDetailsArray addObject:[ADUserDetailItem occupationType]];
    [tempDetailsArray addObject:[ADUserDetailItem incomeLevelType]];
    
    return tempDetailsArray.copy;
}

- (NSArray *)editableItemArray
{
    NSMutableArray *tempDetailsArray = @[].mutableCopy;
    [tempDetailsArray addObject:[ADUserDetailItem headerBasicInfo]];
    [tempDetailsArray addObject:[ADUserDetailItem nameType]];
    [tempDetailsArray addObject:[ADUserDetailItem nicknameType]];
    [tempDetailsArray addObject:[ADUserDetailItem mottoType]];
    [tempDetailsArray addObject:[ADUserDetailItem birthdayType]];
    [tempDetailsArray addObject:[ADUserDetailItem raceType]];
    [tempDetailsArray addObject:[ADUserDetailItem heightType]];
    [tempDetailsArray addObject:[ADUserDetailItem weightType]];
    [tempDetailsArray addObject:[ADUserDetailItem martialStatusType]];
    [tempDetailsArray addObject:[ADUserDetailItem bodyShapeType]];
    [tempDetailsArray addObject:[ADUserDetailItem residentialStatusType]];
    [tempDetailsArray addObject:[ADUserDetailItem birthPlaceType]];
    [tempDetailsArray addObject:[ADUserDetailItem currentResidenceType]];

    [tempDetailsArray addObject:[ADUserDetailItem headerDetails]];
    [tempDetailsArray addObject:[ADUserDetailItem drinkingType]];
    [tempDetailsArray addObject:[ADUserDetailItem smokingType]];
    [tempDetailsArray addObject:[ADUserDetailItem religionType]];
    [tempDetailsArray addObject:[ADUserDetailItem languageType]];
    [tempDetailsArray addObject:[ADUserDetailItem interestType]];
    [tempDetailsArray addObject:[ADUserDetailItem personalityType]];
    
    [tempDetailsArray addObject:[ADUserDetailItem headerWorkEdu]];
    [tempDetailsArray addObject:[ADUserDetailItem educationType]];
    [tempDetailsArray addObject:[ADUserDetailItem undergradEduType]];
    [tempDetailsArray addObject:[ADUserDetailItem postgradEduType]];
    [tempDetailsArray addObject:[ADUserDetailItem employerType]];
    [tempDetailsArray addObject:[ADUserDetailItem occupationType]];
    [tempDetailsArray addObject:[ADUserDetailItem incomeLevelType]];
    
    return tempDetailsArray.copy;
}

// list shows up in adores.
- (NSArray *)userDetailItemArray
{
    NSMutableArray *tempDetailsArray = @[].mutableCopy;
    
    NSMutableArray *basicInfoSection = @[].mutableCopy;
    if (self.fullname != nil && self.fullname.length > 0) {
        [basicInfoSection addObject:[ADUserDetailItem nameType]];
    }
    if (self.motto != nil && self.motto.length > 0) {
        [basicInfoSection addObject:[ADUserDetailItem mottoType]];
    }
    if (self.gender != nil && self.gender.length > 0) {
        [basicInfoSection addObject:[ADUserDetailItem genderType]];
    }
    if (self.birthday != nil) {
        [basicInfoSection addObject:[ADUserDetailItem birthdayType]];
    }
    if (self.c_zodiac != nil) {
        [basicInfoSection addObject:[ADUserDetailItem c_zodiacType]];
    }
    if (self.zodiac != nil) {
        [basicInfoSection addObject:[ADUserDetailItem zodiacType]];
    }
    if (self.race != nil) {
        [basicInfoSection addObject:[ADUserDetailItem raceType]];
    }
    if (self.height != nil) {
        [basicInfoSection addObject:[ADUserDetailItem heightType]];
    }
    if (self.weight != nil) {
        [basicInfoSection addObject:[ADUserDetailItem weightType]];
    }
    if (self.martial_status != nil) {
        [basicInfoSection addObject:[ADUserDetailItem martialStatusType]];
    }
    if (self.body_shape != nil) {
        [basicInfoSection addObject:[ADUserDetailItem bodyShapeType]];
    }
    if (self.residential_status != nil) {
        [basicInfoSection addObject:[ADUserDetailItem residentialStatusType]];
    }
    if ([[self birthPlaceString] length] > 0) {
        [basicInfoSection addObject:[ADUserDetailItem birthPlaceType]];
    }
    if ([[self currentResidenceString] length] > 0) {
        [basicInfoSection addObject:[ADUserDetailItem currentResidenceType]];
    }
    if (basicInfoSection.count > 0) {
        [tempDetailsArray addObject:[ADUserDetailItem headerBasicInfo]];
        [tempDetailsArray addObjectsFromArray:basicInfoSection];
    }
    
    NSMutableArray *detailSection = @[].mutableCopy;
    if (self.drinking != nil) {
        [detailSection addObject:[ADUserDetailItem drinkingType]];
    }
    if (self.smoking != nil) {
        [detailSection addObject:[ADUserDetailItem smokingType]];
    }
    if (self.religion != nil) {
        [detailSection addObject:[ADUserDetailItem religionType]];
    }
    if (self.personality != nil) {
        [detailSection addObject:[ADUserDetailItem personalityType]];
    }
    if (self.interestArray != nil && self.interestArray.count > 0) {
        [detailSection addObject:[ADUserDetailItem interestType]];
    }
    if (self.languageArray != nil && self.languageArray.count > 0) {
        [detailSection addObject:[ADUserDetailItem languageType]];
    }
    if (detailSection.count > 0) {
        [tempDetailsArray addObject:[ADUserDetailItem headerDetails]];
        [tempDetailsArray addObjectsFromArray:detailSection];
    }
    
    NSMutableArray *workSection = @[].mutableCopy;
    if (self.education_level != nil) {
        [workSection addObject:[ADUserDetailItem educationType]];
    }
    if (self.undergrad_edu_institution != nil && [self.undergrad_edu_institution length] > 0) {
        [workSection addObject:[ADUserDetailItem undergradEduType]];
    }
    if (self.postgrad_edu_institution != nil && [self.postgrad_edu_institution length] > 0) {
        [workSection addObject:[ADUserDetailItem postgradEduType]];
    }
    if (self.employer != nil && [self.employer length] > 0) {
        [workSection addObject:[ADUserDetailItem employerType]];
    }
    if (self.occupation != nil) {
        [workSection addObject:[ADUserDetailItem occupationType]];
    }
    if ([self.income integerValue] != 0) {
        [workSection addObject:[ADUserDetailItem incomeLevelType]];
    }
    if (workSection.count > 0) {
        [tempDetailsArray addObject:[ADUserDetailItem headerWorkEdu]];
        [tempDetailsArray addObjectsFromArray:workSection];
    }
    return tempDetailsArray.copy;
}

// list shows up in nearby users. (TODO:Only show partial data).
- (NSArray *)nearbyUserDetailItemArray
{
    return [self userDetailItemArray];
}

- (NSDictionary *)profileJsonDictionary
{
    NSDateFormatter *dateFormatter = [NSDateFormatter ADDefaultFormatter];
    
    NSMutableDictionary *dict = @{}.mutableCopy;
    
    [dict setObject:self.nickname forKey:@"nickname" defaultValue:@""];
    [dict setObject:self.fullname forKey:@"full_name" defaultValue:@""];
    [dict setObject:self.gender forKey:@"gender" defaultValue:@""];
    [dict setObject:[dateFormatter stringFromDate:self.birthday] forKey:@"birthday" defaultValue:@""];
    [dict setObject:self.motto forKey:@"motto" defaultValue:@""];
    [dict setObject:self.weight forKey:@"weight" defaultValue:@""];
    [dict setObject:self.height forKey:@"height" defaultValue:@""];
    [dict setObject:self.martial_status forKey:@"martial_status" defaultValue:@""];
    [dict setObject:self.body_shape forKey:@"body_shape" defaultValue:@""];
    [dict setObject:self.smoking forKey:@"smoking" defaultValue:@""];
    [dict setObject:self.drinking forKey:@"drinking" defaultValue:@""];
    [dict setObject:self.religion forKey:@"religion" defaultValue:@""];
    [dict setObject:self.personality forKey:@"personality" defaultValue:@""];
    [dict setObject:self.languageArray forKey:@"language" defaultValue:@[]];
    [dict setObject:self.interestArray forKey:@"interest" defaultValue:@[]];
    [dict setObject:self.education_level forKey:@"education_level" defaultValue:@""];
    [dict setObject:self.undergrad_edu_institution forKey:@"undergrad_edu_institutions" defaultValue:@""];
    [dict setObject:self.postgrad_edu_institution forKey:@"postgrad_edu_institutions" defaultValue:@""];
    [dict setObject:self.employer forKey:@"employer" defaultValue:@""];
    [dict setObject:self.occupation forKey:@"occupation" defaultValue:@""];
    [dict setObject:self.income forKey:@"income" defaultValue:@""];
    [dict setObject:self.race forKey:@"race" defaultValue:@""];
    [dict setObject:self.residential_status forKey:@"residential_status" defaultValue:@""];
    
    NSMutableDictionary *residence_place = [NSMutableDictionary dictionary];
    [residence_place setObject:self.residence_country forKey:@"residence_country" defaultValue:@""];
    [residence_place setObject:self.residence_province forKey:@"residence_province" defaultValue:@""];
    [residence_place setObject:self.residence_city forKey:@"residence_city" defaultValue:@""];
    [dict setObject:residence_place forKey:@"residence_place"];

    NSMutableDictionary *birth_place = [NSMutableDictionary dictionary];
    [birth_place setObject:self.birth_country forKey:@"birth_country" defaultValue:@""];
    [birth_place setObject:self.birth_province forKey:@"birth_province" defaultValue:@""];
    [birth_place setObject:self.birth_city forKey:@"birth_city" defaultValue:@""];
    [dict setObject:birth_place forKey:@"birth_place"];
    
    return dict;
}

- (NSString *)displayImageJsonString
{
    NSString *imageBaseURL = [ADNSURLCreator profileImageBaseUrl];
    NSMutableArray *tempImageArray = @[].mutableCopy;
    for (NSString *url in self.profileImageArray) {
        if ([url hasPrefix:imageBaseURL]) {
            [tempImageArray addObject:[url stringByReplacingOccurrencesOfString:imageBaseURL withString:@"" options:NSAnchoredSearch range:NSMakeRange(0, [imageBaseURL length])]];
        }
        else {
            [tempImageArray addObject:url];
        }
    }
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:tempImageArray options:0 error:nil];
    NSString *json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return json;
}


#pragma mark -
#pragma mark - Profile image helpers

- (NSArray *)profileImageArray
{
    NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:self.display_image_urls];
    return array;
}

- (NSString *)profilePictureUrl
{
    return self.profileImageArray.firstObject;
}

- (NSString *)profilePictureThumbnailUrl
{
    return [self.profilePictureUrl thumbnailImageUrl];
}

- (NSString *)userPhotoThumbnailUrlAtIndex:(NSUInteger)index
{
    if (index < self.profileImageArray.count) {
        return [self.profileImageArray[index] thumbnailImageUrl];
    }
    return @"";
}

- (ADNYTPhoto *)NYTPhotoWithUrl:(NSString *)url
{
    ADNYTPhoto *photo = [[ADNYTPhoto alloc] init];
    photo.url = url;
    photo.placeholderImage = [UIImage imageNamed:@"profile_image_placeholder_150x150"];
    photo.user_id = self.email;
    NSString *nicknameString = self.nickname ? self.nickname : @"";
    
    ADDetailListCoreDataHelper *detailListCoreDataHelper = [ADUserManager sharedInstance].detailListCoreDataHelper;
    NSString *zodiacString = nil;
    if (self.zodiac != nil) {
        zodiacString = [detailListCoreDataHelper stringValueOfId:self.zodiac
                                                            type:ADUserDetailItemTypeZodiac];
    }
    if (zodiacString == nil) {
        zodiacString = @"";
    }
    
    NSString *summaryString = [NSString stringWithFormat:@"%ld, %@", (long)self.age, zodiacString];
    NSString *mottoString = self.motto ? self.motto : @"";
    photo.attributedCaptionTitle = [[NSAttributedString alloc] initWithString:nicknameString attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    photo.attributedCaptionSummary = [[NSAttributedString alloc] initWithString:summaryString attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
    photo.attributedCaptionCredit = [[NSAttributedString alloc] initWithString:mottoString attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    return photo;
}

- (ADNYTPhoto *)NYTPhotoForUserProfile
{
    return [self NYTPhotoWithUrl:self.profilePictureUrl];
}

- (NSArray *)arrayOfNYTPhotos
{
    NSMutableArray *photos = [NSMutableArray array];
    NSArray *profileImageArray = [self profileImageArray];
    
    for (NSString *url in profileImageArray) {
        [photos addObject:[self NYTPhotoWithUrl:url]];
    }
    return photos.copy;
}

- (void)addUserPhoto:(NSString *)urlString
{
    // Can have a max of 8 profile images
    if (self.profileImageArray.count < 8) {
        NSMutableArray *tempArray = self.profileImageArray.mutableCopy;
        [tempArray addObject:urlString];
        self.display_image_urls = [NSKeyedArchiver archivedDataWithRootObject:tempArray];
        [self saveUserProfileImageIntoUserDefault];
    }
}

- (void)removeUserPhotoAtIndex:(NSUInteger)index
{
    if (index < self.profileImageArray.count) {
        NSMutableArray *tempArray = self.profileImageArray.mutableCopy;
        NSString *urlToBeRemoved = tempArray[index];
        // remove image from cache
        [[SDImageCache sharedImageCache] removeImageForKey:urlToBeRemoved];
        
        [tempArray removeObjectAtIndex:index];
        
        self.display_image_urls = [NSKeyedArchiver archivedDataWithRootObject:tempArray];
        [self saveUserProfileImageIntoUserDefault];
    }
}

- (void)moveUserPhotoFromIndex:(NSUInteger)fromIndex toIndex:(NSInteger)toIndex
{
    if (toIndex < self.profileImageArray.count
        && fromIndex < self.profileImageArray.count) {
        NSMutableArray *tempArray = self.profileImageArray.mutableCopy;
        NSString *urlToBeMoved = tempArray[fromIndex];
        
        [tempArray removeObjectAtIndex:fromIndex];
        [tempArray insertObject:urlToBeMoved atIndex:toIndex];
        
        self.display_image_urls = [NSKeyedArchiver archivedDataWithRootObject:tempArray];
        [self saveUserProfileImageIntoUserDefault];
    }
}

- (void)saveUserProfileImageIntoUserDefault
{
    [ADUserDefaultHelper setUserProfileImageUrl:self.profilePictureThumbnailUrl];
}


#pragma mark -
#pragma mark - Profile detail helpers

- (NSString *)currentResidenceString
{
    return [ADAddressObject addressStringForCountry:self.residence_country province:self.residence_province city:self.residence_city];
}

- (NSString *)birthPlaceString
{
    return [ADAddressObject addressStringForCountry:self.birth_country province:self.birth_province city:self.birth_city];
}


@end
