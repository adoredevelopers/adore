//
//  ADManagedCoupon+Extensions.h
//  Adore
//
//  Created by Kevin Wang on 2015-06-18.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADManagedCoupon.h"

@class ADCouponObject;
@class ADNYTPhoto;
@class ADManagedPartner;

static NSString * const kAD_COUPON_STATUS_UNUSED = @"Unused";

static NSString * const kAD_COUPON_STATUS_REDEEMED = @"Redeemed";

static NSString * const kAD_COUPON_STATUS_ACTIVATED = @"Activated";



@interface ADManagedCoupon (Extensions)


- (void)updateWithCouponObject:(ADCouponObject *)coupon parnter:(ADManagedPartner *)partner;
- (NSAttributedString *)expireAttributedString;
- (BOOL)expired;
- (BOOL)activated;
- (BOOL)redeemed;

- (UIImage *)couponQRImage;
- (ADNYTPhoto *)couponNYTPhoto;


@end
