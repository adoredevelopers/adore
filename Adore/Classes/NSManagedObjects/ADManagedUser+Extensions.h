//
//  ADManagedUser+Extensions.h
//  Adore
//
//  Created by Kevin Wang on 2015-06-18.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADManagedUser.h"

@class ADNYTPhoto;
@class ADUserObject;


@interface ADManagedUser (Extensions)

@property (nonatomic, retain, readonly) NSArray * profileImageArray;
@property (nonatomic, retain) NSArray * languageArray;
@property (nonatomic, retain) NSArray * interestArray;


@property (nonatomic, strong, readonly) NSArray * profileUserDetailItemArray;       // used in Settings
@property (nonatomic, strong, readonly) NSArray * editableItemArray;                // used in edit profile
@property (nonatomic, strong, readonly) NSArray * userDetailItemArray;              // used in Adores
@property (nonatomic, strong, readonly) NSArray * nearbyUserDetailItemArray;       // used in nearby users. Only show partial data.

@property (nonatomic, assign, readonly) NSInteger age;
@property (nonatomic, assign, readonly) ADUserRelationshipType relationType;
@property (nonatomic, strong, readonly) NSString * canonicalSearchTerm;

@property (nonatomic, strong, readonly) NSString * profilePictureUrl;
@property (nonatomic, strong, readonly) NSString * profilePictureThumbnailUrl;
@property (nonatomic, strong, readonly) ADNYTPhoto *NYTPhotoForUserProfile;
@property (nonatomic, strong, readonly) NSArray * arrayOfNYTPhotos;

@property (nonatomic, strong, readonly) NSDictionary *profileJsonDictionary;
@property (nonatomic, strong, readonly) NSString *displayImageJsonString;


- (void)updateWithUserObject:(ADUserObject *)userObject;

- (NSString *)userPhotoThumbnailUrlAtIndex:(NSUInteger)index;
- (void)addUserPhoto:(NSString *)urlString;
- (void)removeUserPhotoAtIndex:(NSUInteger)index;
- (void)moveUserPhotoFromIndex:(NSUInteger)fromIndex toIndex:(NSInteger)toIndex;

- (NSString *)currentResidenceString;
- (NSString *)birthPlaceString;

@end
