//
//  ADManagedPartner+Extensions.h
//  Adore
//
//  Created by Kevin Wang on 2015-06-18.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADManagedPartner.h"

@class ADBusinessPartnerObject;

typedef NS_ENUM(NSInteger, ADPartnerActivityType)
{
    ADPartnerActivityTypeInvalid = 0,
    ADPartnerActivityTypeBoardGame,
    ADPartnerActivityTypeDessert,
    ADPartnerActivityTypeTea,
    ADPartnerActivityTypeCoffee,
    ADPartnerActivityTypeFood,
    ADPartnerActivityTypeDrinks,
    ADPartnerActivityTypeFroyo,
    ADPartnerActivityTypeBowling,
    ADPartnerActivityTypeDart,
    ADPartnerActivityTypeGoKart,
    ADPartnerActivityTypeGolf,
    ADPartnerActivityTypeKaraoke
};


@interface ADManagedPartner (Extensions)


- (void)updateWithBusinessPartnerObject:(ADBusinessPartnerObject *)partner;

- (NSArray *)arrayOfNYTPhotos;
- (NSArray *)partnerImagesArray;
- (NSString *)partnerImageAtIndex:(NSInteger)index;
- (NSString *)partnerImageThumbnailUrlAtIndex:(NSInteger)index;


@end
