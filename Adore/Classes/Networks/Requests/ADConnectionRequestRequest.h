//
//  ADConnectionRequestRequest.h
//  Adore
//
//  Created by Wang on 2014-06-19.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADBaseRequest.h"

typedef void (^ConnectionRequestCompletionBlock) (BOOL response, BOOL approved, NSError *error);

typedef NS_ENUM(NSInteger, ADRequestUserConnectionType) {
    ADRequestUserConnectionTypeAdd,
	ADRequestUserConnectionTypeReject,
	ADRequestUserConnectionTypeBlock,
	ADRequestUserConnectionTypeUnblock,
    ADRequestUserConnectionTypeDelete
};



@interface ADConnectionRequestRequest : ADBaseRequest


- (instancetype)initWithTargetId:(NSString *)targetId Action:(ADRequestUserConnectionType)action CompletionHandler:(ConnectionRequestCompletionBlock)completion;


@end
