//
//  ADUserCouponRequest.m
//  Adore
//
//  Created by Chao Lu on 2015-01-08.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADUserCouponRequest.h"
#import "ADUserManager.h"
#import "ADCouponObject.h"
#import "ADBusinessPartnerObject.h"
#import "NSDictionary+NonNilObject.h"
#import "ADNSURLCreator.h"
#import "ADJSONResponse.h"



@implementation ADUserCouponRequest

- (instancetype)initWithCompletionHandler:(UserCouponRequestCompletionBlock)completion
{
    self = [super init];
    
    if (self) {
        self.sessionTask = [self.session dataTaskWithURL:[ADNSURLCreator getUserCouponsURL]
                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                           NSMutableArray *userCoupons = nil;
                                           NSMutableArray *businessPartners = nil;

                                           NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                           if (httpResponse.statusCode == 200) {
                                               ADJSONResponse *jsonResponse = [[ADJSONResponse alloc] initWithData:data responseType:@"get user coupons"];
                                               if (jsonResponse.success == NO) {
                                                   error = [self errorWithErrorMessage:jsonResponse.message];
                                               }
                                               else {
                                                   NSDictionary *dataDict = [jsonResponse dataAsDictionary];
                                                   
                                                   userCoupons = @[].mutableCopy;
                                                   for (NSDictionary *couponDict in dataDict[@"coupons"]) {
                                                       ADCouponObject *coupon = [[ADCouponObject alloc] initWithDictionary:couponDict];
                                                       [userCoupons addObject:coupon];
                                                   }
                                                   
                                                   businessPartners = @[].mutableCopy;
                                                   for (NSDictionary *partnerDict in dataDict[@"partners"]) {
                                                       ADBusinessPartnerObject *partner = [[ADBusinessPartnerObject alloc] initWithDictionary:partnerDict];
                                                       [businessPartners addObject:partner];
                                                   }
                                               }
                                           }
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               if (error) {
                                                   DDLogError(@"%@", error.localizedDescription);
                                               }
                                               completion(userCoupons.copy, businessPartners.copy, nil);
                                           });
                                       }];
        [self.sessionTask resume];
    }
    
    return self;
}

@end
