//
//  ADNearbyUserRequest.h
//  Adore
//
//  Created by Wang on 2014-03-13.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADNearbyUserFilterViewController.h"
#import "ADBaseRequest.h"

typedef void (^NearbyUserRequestCompletionBlock) (NSArray* list, NSError *error);



@interface ADNearbyUserRequest : ADBaseRequest


- (instancetype)initWithMode:(NSString*)mode BatchSize:(NSInteger)batchSize Gender:(ADGenderFilter)gender MinAge:(NSInteger)minAge MaxAge:(NSInteger)maxAge MaxDistance:(NSInteger)maxDistance CompletionHandler:(NearbyUserRequestCompletionBlock)completion;


@end
