//
//  ADUserConnectionRequest.h
//  Adore
//
//  Created by Wang on 2014-06-19.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADBaseRequest.h"

typedef void (^UserConnectionRequestCompletionBlock) (NSArray* list, NSError *error);



@interface ADUserConnectionRequest : ADBaseRequest


- (instancetype)initWithCompletionHandler:(UserConnectionRequestCompletionBlock)completion;
- (instancetype)initWithList:(NSArray *)list completionHandler:(UserConnectionRequestCompletionBlock)completion;


@end
