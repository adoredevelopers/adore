//
//  ADUpdateLastActivityRequest.m
//  Adore
//
//  Created by Wang on 2014-09-26.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADUpdateLastActivityRequest.h"
#import "ADUserManager.h"
#import "NSDictionary+NonNilObject.h"
#import "ADNSURLCreator.h"
#import "ADJSONResponse.h"



@implementation ADUpdateLastActivityRequest

- (instancetype)initWithCompletionHandler:(UpdateLastActivityCompletionBlock)completion
{
    self = [super init];
    
    if (self) {
        CLLocation *currentLocation = self.userManager.locationHelper.currentLocation;
        if (currentLocation == nil || self.userManager.sessionTicket == nil) {
            return nil;
        }
        NSURL *url = [ADNSURLCreator updateLastActivity:currentLocation];
        
        self.sessionTask = [self.session dataTaskWithURL:url
                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                           NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                           if (httpResponse.statusCode == 200) {
                                               ADJSONResponse *jsonResponse = [[ADJSONResponse alloc] initWithData:data responseType:@"update last activity"];
                                               if (jsonResponse.success == NO) {
                                                   error = [self errorWithErrorMessage:jsonResponse.message];
                                               }
                                           }
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               if (error) {
                                                   DDLogError(@"%@", error.localizedDescription);
                                               }
                                               completion(YES, error);
                                           });
                                       }];
        [self.sessionTask resume];
    }
    
    return self;
}

@end
