//
//  ADNearbyUserRequest.m
//  Adore
//
//  Created by Wang on 2014-03-13.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADNearbyUserRequest.h"
#import "ADUserManager.h"
#import "ADNearbyUserObject.h"
#import "NSDictionary+NonNilObject.h"
#import "ADNSURLCreator.h"
#import "ADJSONResponse.h"



@implementation ADNearbyUserRequest

- (instancetype)initWithMode:(NSString*)mode
                   BatchSize:(NSInteger)batchSize
                      Gender:(ADGenderFilter)gender
                      MinAge:(NSInteger)minAge
                      MaxAge:(NSInteger)maxAge
                 MaxDistance:(NSInteger)maxDistance
           CompletionHandler:(NearbyUserRequestCompletionBlock)completion
{
    self = [super init];
    
    if (self) {
        CLLocation *currentLocation = self.userManager.locationHelper.currentLocation;
        
//        if (self.userManager.sessionTicket == nil) {
//            return nil;
//        }
        if (currentLocation == nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSError *error = [NSError errorWithDomain:NSPOSIXErrorDomain
                                                     code:0
                                                 userInfo:@{ NSLocalizedFailureReasonErrorKey : NSLocalizedString(@"Location service is not enabled.", nil),
                                                             NSLocalizedDescriptionKey : NSLocalizedString(@"You can turn Location Services on at Settings > Privacy > Location Services.", nil)
                                                             }];
                DDLogError(@"%@", error.localizedDescription);
                completion(nil, error);
            });
            return nil;
        }
        NSString *genderString = nil;
        switch (gender) {
            case ADGenderFilterAll: {
                genderString = @"";
                break;
            }
            case ADGenderFilterFemale: {
                genderString = @"Female";
                break;
            }
            case ADGenderFilterMale: {
                genderString = @"Male";
                break;
            }
        }
        
        NSURL *url = [ADNSURLCreator nearbyUserURLWithMode:mode BatchSize:batchSize Gender:genderString MinAge:minAge MaxAge:maxAge MaxDistance:maxDistance CurrentLocation:currentLocation];
        self.sessionTask = [self.session dataTaskWithURL:url
                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                           NSMutableArray *nearbyUserArray = nil;
                                           NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                           if (httpResponse.statusCode == 200) {
                                               ADJSONResponse *jsonResponse = [[ADJSONResponse alloc] initWithData:data responseType:@"nearby user"];
                                               if (jsonResponse.success == NO) {
                                                   error = [self errorWithErrorMessage:jsonResponse.message];
                                               }
                                               else {
                                                   nearbyUserArray = @[].mutableCopy;
                                                   for (NSDictionary *dict in [jsonResponse dataAsArray]) {
                                                       ADNearbyUserObject *nearbyUser = [[ADNearbyUserObject alloc] initWithDictionary:dict];
                                                       [nearbyUserArray addObject:nearbyUser];
                                                   }
                                               }
                                           }
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               if (error) {
                                                   DDLogError(@"%@", error.localizedDescription);
                                               }
                                               completion(nearbyUserArray, error);
                                           });
                                       }];
        [self.sessionTask resume];
    }
    
    return self;
}

@end
