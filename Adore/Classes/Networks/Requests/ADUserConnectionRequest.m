//
//  ADUserConnectionRequest.m
//  Adore
//
//  Created by Wang on 2014-06-19.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADUserConnectionRequest.h"
#import "ADUserManager.h"
#import "ADUserConnectionObject.h"
#import "NSDictionary+NonNilObject.h"
#import "NSDictionary+JSONObject.h"
#import "ADManagedUser+Extensions.h"
#import "ADNSURLCreator.h"
#import "ADJSONResponse.h"



@implementation ADUserConnectionRequest

- (instancetype)initWithCompletionHandler:(UserConnectionRequestCompletionBlock)completion
{
	self = [super init];
	
	if (self) {
		self.sessionTask = [self.session dataTaskWithURL:[ADNSURLCreator getUserConnectionsURL]
                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                           NSMutableArray *connectionsArray = nil;
                                           NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                           if (httpResponse.statusCode == 200) {
                                               ADJSONResponse *jsonResponse = [[ADJSONResponse alloc] initWithData:data responseType:@"get user connections"];
                                               if (jsonResponse.success == NO) {
                                                   error = [self errorWithErrorMessage:jsonResponse.message];
                                               }
                                               else {
                                                   connectionsArray = @[].mutableCopy;
                                                   for (NSDictionary *dict in [jsonResponse dataAsArray]) {
                                                       ADUserConnectionObject *connection = [[ADUserConnectionObject alloc] initWithDictionary:dict];
                                                       [connectionsArray addObject:connection];
                                                   }
                                               }
                                           }
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               if (error) {
                                                   DDLogError(@"%@", error.localizedDescription);
                                               }
                                               completion(connectionsArray, error);
                                           });
                                       }];
		[self.sessionTask resume];
	}
	
	return self;	
}

- (instancetype)initWithList:(NSArray *)list completionHandler:(UserConnectionRequestCompletionBlock)completion
{
    self = [super init];
    
    if (self) {
        NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[ADNSURLCreator updateConnectionsProfileURL]];

        NSMutableDictionary *currentList = @{}.mutableCopy;
        for (ADManagedUser *user in list) {
            if (user.last_updated_timestamp && user.email) {
                [currentList setObject:user.last_updated_timestamp forKey:user.email];
            }
        }
        if (currentList.count == 0) {
            return nil;
        }
        
        NSData *body = [@{ @"user_timestamps" : currentList } JSONDataObject];
        [urlRequest setHTTPMethod:@"POST"];
        [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [urlRequest setHTTPBody:body];
        
        self.sessionTask = [self.session dataTaskWithRequest:urlRequest
                                           completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                               NSMutableArray *connectionsArray = nil;
                                               NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                               if (httpResponse.statusCode == 200) {
                                                   ADJSONResponse *jsonResponse = [[ADJSONResponse alloc] initWithData:data responseType:@"get user connections by list"];
                                                   if (jsonResponse.success == NO) {
                                                       error = [self errorWithErrorMessage:jsonResponse.message];
                                                   }
                                                   else {
                                                       DDLogDebug(@"updatedUsers %@", [jsonResponse dataAsArray]);
                                                       connectionsArray = @[].mutableCopy;
                                                       for (NSDictionary *dict in [jsonResponse dataAsArray]) {
                                                           ADUserConnectionObject *connection = [[ADUserConnectionObject alloc] initWithDictionary:dict];
                                                           [connectionsArray addObject:connection];
                                                       }
                                                   }
                                               }
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   if (error) {
                                                       DDLogError(@"%@", error.localizedDescription);
                                                   }
                                                   completion(connectionsArray, error);
                                               });
                                           }];
        [self.sessionTask resume];
    }
    
    return self;
}

@end
