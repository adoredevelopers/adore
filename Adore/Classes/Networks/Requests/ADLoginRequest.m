//
//  ADLoginRequest.m
//  Adore
//
//  Created by Wang on 2014-04-18.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADLoginRequest.h"
#import "ADUserManager.h"
#import "ADLoginObject.h"
#import "NSString+Security.h"
#import "NSDictionary+JSONObject.h"
#import "NSDictionary+NonNilObject.h"
#import "ADUserDefaultHelper.h"
#import "ADNSUrlCreator.h"
#import "ADJSONResponse.h"



@implementation ADLoginRequest


- (instancetype)initWithUsername:(NSString *)username
                        Password:(NSString *)password
               CompletionHandler:(LoginRequestCompletionBlock)completion
{
	self = [super init];
	if (self) {
		NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[ADNSURLCreator loginURL]];

        NSDictionary *dict = @{@"email": username, @"password_hash": [password SHA256], @"notification_token": [ADUserDefaultHelper notificationToken]};
		[urlRequest setHTTPBody:[dict queryDataObject]];
		[urlRequest setHTTPMethod:@"POST"];
        
		self.sessionTask = [self.session dataTaskWithRequest:urlRequest
                                           completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                               ADLoginObject *loginObj = nil;
                                               NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                               if (httpResponse.statusCode == 200) {
                                                   ADJSONResponse *jsonResponse = [[ADJSONResponse alloc] initWithData:data responseType:@"login"];
                                                   if (jsonResponse.success == NO) {
                                                       error = [self errorWithErrorMessage:jsonResponse.message];
                                                   }
                                                   else {
                                                       loginObj = [[ADLoginObject alloc] initWithDictionary:[jsonResponse dataAsDictionary]];
                                                   }
                                               }
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   if (error) {
                                                       DDLogError(@"%@", error.localizedDescription);
                                                   }
                                                   completion(loginObj, error);
                                               });
		}];
		[self.sessionTask resume];
	}
	return self;
}


@end
