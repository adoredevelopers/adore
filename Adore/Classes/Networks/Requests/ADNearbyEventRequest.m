//
//  ADNearbyEventRequest.m
//  Adore
//
//  Created by Chao Lu on 2016-03-05.
//  Copyright © 2016 AdoreMobile Inc. All rights reserved.
//

#import "ADNearbyEventRequest.h"
#import "ADUserManager.h"
#import "ADNSURLCreator.h"
#import "ADJSONResponse.h"
#import "ADNearbyEventObject.h"

@implementation ADNearbyEventRequest


- (instancetype)initWithMode:(NSString*)mode BatchSize:(NSInteger)batchSize MaxDistance:(NSInteger)maxDistance CompletionHandler:(NearbyEventRequestCompletionBlock)completion
{
    self = [super init];
    
    if (self) {
        CLLocation *currentLocation = self.userManager.locationHelper.currentLocation;
        
        //        if (self.userManager.sessionTicket == nil) {
        //            return nil;
        //        }
        if (currentLocation == nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSError *error = [NSError errorWithDomain:NSPOSIXErrorDomain
                                                     code:0
                                                 userInfo:@{ NSLocalizedFailureReasonErrorKey : NSLocalizedString(@"Location service is not enabled.", nil),
                                                             NSLocalizedDescriptionKey : NSLocalizedString(@"You can turn Location Services on at Settings > Privacy > Location Services.", nil)
                                                             }];
                DDLogError(@"%@", error.localizedDescription);
                completion(nil, error);
            });
            return nil;
        }
        
        NSURL *url = [ADNSURLCreator nearbyEventURLWithMode:mode BatchSize:batchSize MaxDistance:maxDistance CurrentLocation:currentLocation];
        self.sessionTask = [self.session dataTaskWithURL:url
                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                           NSMutableArray *nearbyEventArray = nil;
                                           NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                           if (httpResponse.statusCode == 200) {
                                               ADJSONResponse *jsonResponse = [[ADJSONResponse alloc] initWithData:data responseType:@"nearby event"];
                                               if (jsonResponse.success == NO) {
                                                   error = [self errorWithErrorMessage:jsonResponse.message];
                                               }
                                               else {
                                                   nearbyEventArray = @[].mutableCopy;
                                                   for (NSDictionary *dict in [jsonResponse dataAsArray]) {
                                                       ADNearbyEventObject *nearbyEvent = [[ADNearbyEventObject alloc] initWithDictionary:dict];
                                                       [nearbyEventArray addObject:nearbyEvent];
                                                   }
                                               }
                                           }
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               if (error) {
                                                   DDLogError(@"%@", error.localizedDescription);
                                               }
                                               completion(nearbyEventArray, error);
                                           });
                                       }];
        [self.sessionTask resume];
    }
    
    return self;

}


@end
