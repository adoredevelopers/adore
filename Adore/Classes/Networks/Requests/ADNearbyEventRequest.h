//
//  ADNearbyEventRequest.h
//  Adore
//
//  Created by Chao Lu on 2016-03-05.
//  Copyright © 2016 AdoreMobile Inc. All rights reserved.
//

#import "ADBaseRequest.h"


typedef void (^NearbyEventRequestCompletionBlock) (NSArray* list, NSError *error);


@interface ADNearbyEventRequest : ADBaseRequest


- (instancetype)initWithMode:(NSString*)mode
                   BatchSize:(NSInteger)batchSize
                 MaxDistance:(NSInteger)maxDistance
           CompletionHandler:(NearbyEventRequestCompletionBlock)completion;


@end
