//
//  ADUpdateLastActivityRequest.h
//  Adore
//
//  Created by Wang on 2014-09-26.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADBaseRequest.h"

typedef void (^UpdateLastActivityCompletionBlock) (BOOL success, NSError *error);



@interface ADUpdateLastActivityRequest : ADBaseRequest


- (instancetype)initWithCompletionHandler:(UpdateLastActivityCompletionBlock)completion;


@end
