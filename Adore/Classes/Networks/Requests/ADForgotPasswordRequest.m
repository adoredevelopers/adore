//
//  ADForgotPasswordRequest.m
//  Adore
//
//  Created by Wang on 2015-03-15.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADForgotPasswordRequest.h"
#import "NSString+Security.h"
#import "NSDictionary+JSONObject.h"
#import "NSDictionary+NonNilObject.h"
#import "ADNSURLCreator.h"
#import "ADJSONResponse.h"



@implementation ADForgotPasswordRequest

- (instancetype)initWithEmail:(NSString *)email
            CompletionHandler:(ForgotPasswordRequestCompletionBlock)completion
{
    self = [super init];
    
    if (self) {
        NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[ADNSURLCreator forgotPasswordURLWithEmail:email]];
        self.sessionTask = [self.session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if (httpResponse.statusCode == 200) {
                ADJSONResponse *jsonResponse = [[ADJSONResponse alloc] initWithData:data responseType:@"forgot password"];
                if (jsonResponse.success == NO) {
                    error = [self errorWithErrorMessage:jsonResponse.message];
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (error) {
                    DDLogError(@"%@", error.localizedDescription);
                }
                completion(error == nil, error);
            });
        }];
        
        [self.sessionTask resume];
    }
    return self;
}


@end
