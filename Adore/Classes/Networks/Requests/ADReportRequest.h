//
//  ADReportRequest.h
//  Adore
//
//  Created by Wang on 2015-07-07.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADBaseRequest.h"

@class ADReportObject;

typedef void (^ReportRequestCompletionBlock) (BOOL success, NSError *error);



@interface ADReportRequest : ADBaseRequest


- (instancetype)initWithReport:(ADReportObject *)reportObject CompletionHandler:(ReportRequestCompletionBlock)completion;


@end
