//
//  ADBaseRequest.h
//  Adore
//
//  Created by Wang on 2014-09-30.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADUserManager;



@interface ADBaseRequest : NSObject


@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSURLSessionTask *sessionTask;
@property (nonatomic, strong) ADUserManager *userManager;

- (void)cancel;
- (NSError *)errorWithErrorMessage:(NSString *)errorMessage;


@end
