//
//  ADLoginRequest.h
//  Adore
//
//  Created by Chao Lu on 2016-01-18.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADBaseRequest.h"
#import "ADJSONResponse.h"

@class ADLoginObject;

typedef void (^CheckSessionTicketRequestCompletionBlock) (ADJSONResponse *resp, NSError *error);

@interface ADCheckSessionTicketRequest : ADBaseRequest

- (instancetype)initWithCompletionHandler:(CheckSessionTicketRequestCompletionBlock)completion;

@end
