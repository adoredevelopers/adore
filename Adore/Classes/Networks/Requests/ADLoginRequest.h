//
//  ADLoginRequest.h
//  Adore
//
//  Created by Wang on 2014-04-18.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADBaseRequest.h"

@class ADLoginObject;

typedef void (^LoginRequestCompletionBlock) (ADLoginObject* obj, NSError *error);



@interface ADLoginRequest : ADBaseRequest


- (instancetype)initWithUsername:(NSString *)username Password:(NSString *)password CompletionHandler:(LoginRequestCompletionBlock)completion;


@end
