//
//  ADUserCouponRequest.h
//  Adore
//
//  Created by Chao Lu on 2015-01-08.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADBaseRequest.h"

typedef void (^UserCouponRequestCompletionBlock) (NSArray *couponArray, NSArray *partnerArray, NSError *error);



@interface ADUserCouponRequest : ADBaseRequest


- (instancetype)initWithCompletionHandler:(UserCouponRequestCompletionBlock)completion;


@end
