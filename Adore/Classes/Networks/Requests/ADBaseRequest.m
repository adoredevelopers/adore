//
//  ADBaseRequest.m
//  Adore
//
//  Created by Wang on 2014-09-30.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADBaseRequest.h"
#import "ADUserManager.h"
#import "AppDelegate.h"



@implementation ADBaseRequest

- (instancetype)init
{
    self = [super init];
    if (self) {
        _userManager = [ADUserManager sharedInstance];
        _session = [NSURLSession sharedSession];
    }
    return self;
}

- (void)cancel
{
    [self.sessionTask cancel];
    self.sessionTask = nil;
}

- (NSError *)errorWithErrorMessage:(NSString *)errorMessage
{
    NSError *error = nil;
    if (errorMessage) {
        error = [NSError errorWithDomain:NSPOSIXErrorDomain code:0 userInfo:@{ NSLocalizedDescriptionKey : errorMessage }];
    }
    
    return error;
}

@end
