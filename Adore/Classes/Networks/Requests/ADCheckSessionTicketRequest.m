//
//  ADLogoutRequest.m
//  Adore
//
//  Created by Chao Lu on 2016-01-18.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADCheckSessionTicketRequest.h"
#import "ADNSURLCreator.h"
#import "ADJSONResponse.h"

@implementation ADCheckSessionTicketRequest

- (instancetype)initWithCompletionHandler:(CheckSessionTicketRequestCompletionBlock)completion
{
    self = [super init];
    
    if (self) {
        self.sessionTask = [self.session dataTaskWithURL:[ADNSURLCreator checkSessionTicketURL]
                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                           ADJSONResponse *jsonResponse = nil;
                                           if (error == nil) {
                                               NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                               if (httpResponse.statusCode == 200) {
                                                   jsonResponse = [[ADJSONResponse alloc] initWithData:data responseType:@"check session ticket"];
                                                   if (jsonResponse.success == NO) {
                                                       error = [self errorWithErrorMessage:jsonResponse.message];
                                                   }

                                               }
                                           }
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               if (error) {
                                                   DDLogError(@"%@", error.localizedDescription);
                                               }
                                               completion(jsonResponse, nil);
                                           });
                                       }];
        [self.sessionTask resume];
    }
    
    return self;
}

@end
