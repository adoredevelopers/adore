//
//  ADRegisterRequest.m
//  Adore
//
//  Created by Wang on 2014-04-23.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADRegisterRequest.h"
#import "ADLoginObject.h"
#import "ADRegisterObject.h"
#import "NSDictionary+NonNilObject.h"
#import "ADNSURLCreator.h"
#import "ADJSONResponse.h"



@implementation ADRegisterRequest

- (instancetype)initWithRegisterObject:(ADRegisterObject *)registerObject
                     CompletionHandler:(RegisterRequestCompletionBlock)completion
{
	self = [super init];
	if (self) {
		NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[ADNSURLCreator registerURL]];
		
		NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", kAD_HTTP_MULTIPART_BOUNDARY];
		[urlRequest addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
		NSData *body = [registerObject JSONData];
		[urlRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
		[urlRequest setHTTPBody:body];
        [urlRequest setHTTPMethod:@"POST"];

		self.sessionTask = [self.session uploadTaskWithRequest:urlRequest
                                                      fromData:body
                                             completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                 ADLoginObject *loginObj = nil;
                                                 NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                                 if (httpResponse.statusCode == 200) {
                                                     ADJSONResponse *jsonResponse = [[ADJSONResponse alloc] initWithData:data responseType:@"register"];
                                                     if (jsonResponse.success == NO) {
                                                         error = [self errorWithErrorMessage:jsonResponse.message];
                                                     }
                                                     else {
                                                         loginObj = [[ADLoginObject alloc] initWithDictionary:[jsonResponse dataAsDictionary]];
                                                     }
                                                 }
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     if (error) {
                                                         DDLogError(@"%@", error.localizedDescription);
                                                     }
                                                     completion(loginObj, error);
                                                 });
        }];
		[self.sessionTask resume];
	}
	return self;
}

@end
