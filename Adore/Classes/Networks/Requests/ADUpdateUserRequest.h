//
//  ADUpdateUserRequest.h
//  Adore
//
//  Created by Wang on 2015-03-12.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADBaseRequest.h"

@class ADManagedUser;

typedef void (^UpdateUserRequestCompletionBlock) (BOOL response, NSError *error);



@interface ADUpdateUserRequest : ADBaseRequest


- (instancetype)initWithUser:(ADManagedUser *)user tempImages:(NSDictionary *)tempImages CompletionHandler:(UpdateUserRequestCompletionBlock)completion;


@end
