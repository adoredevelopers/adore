//
//  ADLogoutRequest.m
//  Adore
//
//  Created by Kevin Wang on 2015-06-05.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADLogoutRequest.h"
#import "ADNSURLCreator.h"
#import "ADUserManager.h"



@implementation ADLogoutRequest


- (instancetype)initWithCompletionHandler:(LogoutRequestCompletionBlock)completion
{
    self = [super init];
    
    if (self) {
        self.sessionTask = [self.session dataTaskWithURL:[ADNSURLCreator logoutURL]
                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                           if (error == nil) {
                                               NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                               if (httpResponse.statusCode == 200) {
                                                   DDLogDebug(@"logout:\n%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                                               }
                                           }
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               completion(YES, nil);
                                           });
        }];
        [self.sessionTask resume];
    }
    
    return self;
}


@end
