//
//  ADPendingConnectionsRequest.h
//  Adore
//
//  Created by Wang on 2014-07-09.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADBaseRequest.h"

typedef void (^PendingConnectionsRequestCompletionBlock) (NSArray* list, NSError *error);



@interface ADPendingConnectionsRequest : ADBaseRequest


- (instancetype)initWithCompletionHandler:(PendingConnectionsRequestCompletionBlock)completion;


@end
