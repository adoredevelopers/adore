//
//  ADReportRequest.m
//  Adore
//
//  Created by Wang on 2015-07-07.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADReportRequest.h"
#import "ADReportObject.h"
#import "ADNSURLCreator.h"



@implementation ADReportRequest


- (instancetype)initWithReport:(ADReportObject *)reportObject CompletionHandler:(ReportRequestCompletionBlock)completion
{
    self = [super init];
    
    if (self) {
        NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[ADNSURLCreator reportURL]];
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", kAD_HTTP_MULTIPART_BOUNDARY];
        [urlRequest addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSData *body = [reportObject JSONData];
        [urlRequest setHTTPMethod:@"POST"];
        [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [urlRequest setHTTPBody:body];
        
        self.sessionTask = [self.session dataTaskWithRequest:urlRequest
                                           completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                               if (error == nil) {
                                                   NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                                   if (httpResponse.statusCode == 200) {
                                                       DDLogDebug(@"report:\n%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                                                   }
                                               }
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   completion(YES, nil);
                                               });
                                           }];
        [self.sessionTask resume];
    }
    
    return self;
}

@end