//
//  ADConnectionRequest.m
//  Adore
//
//  Created by Wang on 2014-06-19.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADConnectionRequestRequest.h"
#import "ADUserManager.h"
#import "ADUserObject.h"
#import "NSDictionary+JSONObject.h"
#import "NSDictionary+NonNilObject.h"
#import "ADNSURLCreator.h"
#import "ADJSONResponse.h"



@implementation ADConnectionRequestRequest

- (instancetype)initWithTargetId:(NSString *)targetId Action:(ADRequestUserConnectionType)action CompletionHandler:(ConnectionRequestCompletionBlock)completion
{
	self = [super init];
	if (self) {
		NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[self urlFor:action]];
        
        __block NSString *actionString = [self actionStringFor:action];
		NSDictionary *requestDict = @{ actionString : targetId };
		[urlRequest setHTTPBody:[requestDict queryDataObject]];
        [urlRequest setHTTPMethod:@"POST"];

		self.sessionTask = [self.session dataTaskWithRequest:urlRequest
                                           completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                               BOOL approved = NO;
                                               NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                               if (httpResponse.statusCode == 200) {
                                                   ADJSONResponse *jsonResponse = [[ADJSONResponse alloc] initWithData:data responseType:@"connection request"];
                                                   if (jsonResponse.success == NO) {
                                                       error = [self errorWithErrorMessage:jsonResponse.message];
                                                   }
                                                   else {
                                                       approved = [jsonResponse.jsonDictionary boolForKey:@"approved"];
                                                   }
                                               }
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   if (error) {
                                                       DDLogError(@"%@", error.localizedDescription);
                                                   }
                                                   completion(error == nil, approved, error);
                                               });
                                           }];
		[self.sessionTask resume];
	}
	
	return self;
}

- (NSString *)actionStringFor:(ADRequestUserConnectionType)action
{
	switch (action)
	{
        case ADRequestUserConnectionTypeAdd:
            return @"added_user_id";
		case ADRequestUserConnectionTypeReject:
			return @"rejected_user_id";
		case ADRequestUserConnectionTypeBlock:
			return @"blocked_user_id";
		case ADRequestUserConnectionTypeUnblock:
			return @"unblocked_user_id";
        case ADRequestUserConnectionTypeDelete:
            return @"deleted_user_id";
	}
    return nil;
}

- (NSURL *)urlFor:(ADRequestUserConnectionType)action
{
    switch (action)
    {
        case ADRequestUserConnectionTypeAdd:
            return [ADNSURLCreator addConnectionURL];
        case ADRequestUserConnectionTypeReject:
            return [ADNSURLCreator rejectConnectionURL];
        case ADRequestUserConnectionTypeBlock:
            return [ADNSURLCreator blockConnectionURL];
        case ADRequestUserConnectionTypeUnblock:
            return [ADNSURLCreator unblockConnectionURL];
        case ADRequestUserConnectionTypeDelete:
            return [ADNSURLCreator deleteConnectionURL];
    }
    return nil;
}

@end
