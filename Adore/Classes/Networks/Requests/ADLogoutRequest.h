//
//  ADLogoutRequest.h
//  Adore
//
//  Created by Kevin Wang on 2015-06-05.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADBaseRequest.h"

typedef void (^LogoutRequestCompletionBlock) (BOOL success, NSError *error);



@interface ADLogoutRequest : ADBaseRequest


- (instancetype)initWithCompletionHandler:(LogoutRequestCompletionBlock)completion;


@end
