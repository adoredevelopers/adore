//
//  ADUpdateUserRequest.m
//  Adore
//
//  Created by Wang on 2015-03-12.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADUpdateUserRequest.h"
#import "ADUserManager.h"
#import "ADManagedUser+Extensions.h"
#import "NSDictionary+JSONObject.h"
#import "NSDictionary+NonNilObject.h"
#import "ADUserObject.h"
#import "ADUserCoreDataHelper.h"
#import "ADNSURLCreator.h"
#import "ADJSONResponse.h"



@implementation ADUpdateUserRequest


- (instancetype)initWithUser:(ADManagedUser *)user tempImages:(NSDictionary *)tempImages CompletionHandler:(UpdateUserRequestCompletionBlock)completion
{
    self = [super init];
    
    if (self) {
        NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[ADNSURLCreator updateUserProfileURL]];
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", kAD_HTTP_MULTIPART_BOUNDARY];
        [urlRequest addValue:contentType forHTTPHeaderField: @"Content-Type"];

        NSDictionary *jsonDictionary = [user profileJsonDictionary];
        NSMutableDictionary *userProfileDict = @{}.mutableCopy;
        userProfileDict[@"profile"] = [jsonDictionary JSONString];
        userProfileDict[@"display_image_urls"] = [user displayImageJsonString];
        
        for (NSString* imageUrl in user.profileImageArray) {
            if ([imageUrl hasPrefix:KAD_IMAGE_URL_PREFIX]) {
                UIImage *image = tempImages[imageUrl];
                if (image && imageUrl) {
                    [userProfileDict setObject:image forKey:imageUrl];
                }
            }
        }
        
        NSData *body = [userProfileDict multiUploadsDataObject];
        [urlRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
        [urlRequest setHTTPBody:body];
        [urlRequest setHTTPMethod:@"POST"];

        self.sessionTask = [self.session uploadTaskWithRequest:urlRequest
                                                      fromData:body
                                             completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                 NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                                 if (httpResponse.statusCode == 200) {
                                                     ADJSONResponse *jsonResponse = [[ADJSONResponse alloc] initWithData:data responseType:@"update user profile"];
                                                     if (jsonResponse.success == NO) {
                                                         error = [self errorWithErrorMessage:jsonResponse.message];
                                                     }
                                                     else {
                                                         NSDictionary *dataDict = [jsonResponse dataAsDictionary];
                                                         ADUserObject *userObject = [[ADUserObject alloc] initWithDictionary:dataDict];
                                                         // User id and email are missing from the response
                                                         userObject.userId = user.userId;
                                                         userObject.email = user.email;
                                                         [self.userManager.userCoreDataHelper saveUserWithEmail:user.email withUserObject:userObject withRelationshipType:ADUserRelationshipTypeInvalid updateProfileOnly:YES isRead:NO save:YES];
                                                         
                                                         // By default, the face validation result is always true, so that users can use all functionalities
                                                         [self.userManager.userCoreDataHelper updateFaceValidationResult:YES];
                                                     }
                                                 }
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     if (error) {
                                                         DDLogError(@"%@", error.localizedDescription);
                                                     }
                                                     completion(error == nil, error);
                                                 });
                                             }];
        [self.sessionTask resume];
    }
    
    return self;
}


@end
