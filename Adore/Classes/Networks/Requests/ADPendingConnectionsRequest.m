//
//  ADPendingConnectionsRequest.m
//  Adore
//
//  Created by Wang on 2014-07-09.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADPendingConnectionsRequest.h"
#import "ADUserManager.h"
#import "ADManagedUser+Extensions.h"
#import "NSDictionary+NonNilObject.h"
#import "ADPendingRequestObject.h"
#import "ADNSURLCreator.h"
#import "ADJSONResponse.h"



@implementation ADPendingConnectionsRequest


- (instancetype)initWithCompletionHandler:(PendingConnectionsRequestCompletionBlock)completion
{
	self = [super init];
	
	if (self) {
		self.sessionTask = [self.session dataTaskWithURL:[ADNSURLCreator getPendingRequestsURL]
                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                           NSMutableArray *pendingArray = nil;
                                           NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                           if (httpResponse.statusCode == 200) {
                                               ADJSONResponse *jsonResponse = [[ADJSONResponse alloc] initWithData:data responseType:@"pending connections"];
                                               if (jsonResponse.success == NO) {
                                                   error = [self errorWithErrorMessage:jsonResponse.message];
                                               }
                                               else {
                                                   pendingArray = @[].mutableCopy;
                                                   for (NSDictionary *dict in [jsonResponse dataAsArray]) {
                                                       ADPendingRequestObject *pendingRequest = [[ADPendingRequestObject alloc] initWithDictionary:dict];
                                                       [pendingArray addObject:pendingRequest];
                                                   }
                                               }
                                           }
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               if (error) {
                                                   DDLogError(@"%@", error.localizedDescription);
                                               }
                                               completion(pendingArray, error);
                                           });
                                       }];
		[self.sessionTask resume];
	}
	
	return self;
}

@end
