//
//  ADForgotPasswordRequest.h
//  Adore
//
//  Created by Wang on 2015-03-15.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADBaseRequest.h"

typedef void (^ForgotPasswordRequestCompletionBlock) (BOOL response, NSError *error);



@interface ADForgotPasswordRequest : ADBaseRequest


- (instancetype)initWithEmail:(NSString *)email CompletionHandler:(ForgotPasswordRequestCompletionBlock)completion;


@end
