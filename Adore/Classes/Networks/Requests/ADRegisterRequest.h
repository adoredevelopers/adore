//
//  ADRegisterRequest.h
//  Adore
//
//  Created by Wang on 2014-04-23.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADBaseRequest.h"

@class ADLoginObject;
@class ADRegisterObject;

typedef void (^RegisterRequestCompletionBlock) (ADLoginObject* obj, NSError *error);



@interface ADRegisterRequest : ADBaseRequest


- (instancetype)initWithRegisterObject:(ADRegisterObject *)registerObject CompletionHandler:(RegisterRequestCompletionBlock)completion;


@end
