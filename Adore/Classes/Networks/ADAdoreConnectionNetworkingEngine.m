//
//  ADAdoreConnectionNetworkingEngine.m
//  Adore
//
//  Created by Wang on 2015-01-20.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADAdoreConnectionNetworkingEngine.h"
#import "ADConnectionRequestRequest.h"
#import "ADConnectionObject.h"
#import "ADUserManager.h"
#import "ADChatManager.h"
#import "ADManagedUser+Extensions.h"
#import "ADUserCoreDataHelper.h"
#import "NSObject+Notification.h"



@interface ADAdoreConnectionNetworkingEngine ()


@property (nonatomic, strong) ADConnectionRequestRequest *addUserRequest;
@property (nonatomic, strong) ADConnectionRequestRequest *rejectUserRequest;

@property (nonatomic, strong) ADConnectionRequestRequest *blockUserRequest;
@property (nonatomic, strong) ADConnectionRequestRequest *deleteUserRequest;


@end



@implementation ADAdoreConnectionNetworkingEngine

#pragma mark -
#pragma mark - Create requests

- (void)startAddingUserRequestToUser:(ADManagedUser *)user completionBlock:(ADConnectionNetworkCompletionBlock)completionBlock
{
    ConnectionRequestCompletionBlock networkingCompletionBlock = ^(BOOL response, BOOL approved, NSError *error) {
        completionBlock(response, approved, error);
    };
    self.addUserRequest = [[ADConnectionRequestRequest alloc] initWithTargetId:user.email
                                                                        Action:ADRequestUserConnectionTypeAdd
                                                             CompletionHandler:networkingCompletionBlock];
}

- (void)startRejectingUserRequestWithUser:(ADManagedUser *)user completionBlock:(ADConnectionNetworkCompletionBlock)completionBlock
{
    ConnectionRequestCompletionBlock networkingCompletionBlock = ^(BOOL response, BOOL approved, NSError *error) {
        if (response) {
            [self.userManager.userCoreDataHelper rejectPendingUser:user];
        }
        completionBlock(response, approved, error);
    };
    self.rejectUserRequest = [[ADConnectionRequestRequest alloc] initWithTargetId:user.email
                                                                           Action:ADRequestUserConnectionTypeReject
                                                                CompletionHandler:networkingCompletionBlock];
}

- (void)startDeletingUser:(ADManagedUser *)user completionBlock:(ADConnectionNetworkCompletionBlock)completionBlock
{
    ConnectionRequestCompletionBlock networkingCompletionBlock = ^(BOOL response, BOOL approved, NSError *error) {
        if (response) {
            [self.chatManager deleteAllMessagesByEmail:user.email];
            [self.userManager.userCoreDataHelper deleteUser:user];
        }
        completionBlock(response, approved, error);
    };
    self.deleteUserRequest = [[ADConnectionRequestRequest alloc] initWithTargetId:user.email
                                                                           Action:ADRequestUserConnectionTypeDelete
                                                                CompletionHandler:networkingCompletionBlock];
}

- (void)startBlockingUser:(NSString *)email completionBlock:(ADConnectionNetworkCompletionBlock)completionBlock
{
    ConnectionRequestCompletionBlock networkingCompletionBlock = ^(BOOL response, BOOL approved,NSError *error) {
        completionBlock(response, approved, error);
    };
    self.blockUserRequest = [[ADConnectionRequestRequest alloc] initWithTargetId:email
                                                                          Action:ADRequestUserConnectionTypeBlock
                                                               CompletionHandler:networkingCompletionBlock];
}

- (void)startUnblockingUser:(NSString *)email completionBlock:(ADConnectionNetworkCompletionBlock)completionBlock
{
    ConnectionRequestCompletionBlock networkingCompletionBlock = ^(BOOL response, BOOL approved, NSError *error) {
        completionBlock(response, approved, error);
    };
    self.blockUserRequest = [[ADConnectionRequestRequest alloc] initWithTargetId:email
                                                                          Action:ADRequestUserConnectionTypeUnblock
                                                               CompletionHandler:networkingCompletionBlock];
}


#pragma mark -
#pragma mark - Cancel requests

- (void)cancelAllRequests
{
    [self cancelAddUserRequest];
    [self cancelRejectUserRequest];
    [self cancelBlockUserRequest];
    [self cancelDeleteUserRequest];
}

- (void)cancelAddUserRequest
{
    [self.addUserRequest cancel];
    self.addUserRequest = nil;
}

- (void)cancelRejectUserRequest
{
    [self.rejectUserRequest cancel];
    self.rejectUserRequest = nil;
}

- (void)cancelBlockUserRequest
{
    [self.blockUserRequest cancel];
    self.blockUserRequest = nil;
}

- (void)cancelDeleteUserRequest
{
    [self.deleteUserRequest cancel];
    self.deleteUserRequest = nil;
}

@end
