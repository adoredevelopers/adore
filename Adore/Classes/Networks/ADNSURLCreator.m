//
//  ADNSURLCreator.m
//  Adore
//
//  Created by Wang on 2015-06-05.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADNSURLCreator.h"
#import "ADUserManager.h"

static NSString * const kAD_URL_PROFILE_IMAGE_BASE = @"/uploads/profile_images/";

static NSString * const kAD_URL_PARTNER_IMAGE_BASE = @"/uploads/partner_images/";


// Urls
static NSString * const kAD_URL_LOGIN = @"%@/login";

static NSString * const kAD_URL_REGISTER = @"%@/register";

static NSString * const kAD_URL_FORGOT_PASSWORD = @"%@/reset_user_password?email=%@";

static NSString * const kAD_URL_NEARBY_USER = @"%@/get_nearby_users?mode=%@&batch_size=%ld&longitude=%.6f&latitude=%.6f&max_distance=%ld&min_age=%ld&max_age=%ld&gender=%@&token=%@";

static NSString * const kAD_URL_NEARBY_EVENT = @"%@/get_nearby_events?mode=%@&batch_size=%ld&longitude=%.6f&latitude=%.6f&max_distance=%ld&token=%@";

static NSString * const kAD_URL_CONNECTION_ADD = @"%@/add_user_connection?token=%@";

static NSString * const kAD_URL_CONNECTION_REJECT = @"%@/reject_user_connection?token=%@";

static NSString * const kAD_URL_CONNECTION_BLOCK = @"%@/block_user_connection?token=%@";

static NSString * const kAD_URL_CONNECTION_UNBLOCK = @"%@/unblock_user_connection?token=%@";

static NSString * const kAD_URL_CONNECTION_DELETE = @"%@/delete_user_connection?token=%@";

static NSString * const kAD_URL_GET_USER_CONNECTIONS = @"%@/get_user_connections?token=%@";

static NSString * const kAD_URL_UPDATE_USER_CONNECTIONS_PROFILE = @"%@/get_users_with_last_updated_timestamp?token=%@";

static NSString * const kAD_URL_GET_USER_PENDING_CONNECTIONS = @"%@/get_user_pending_connections?token=%@";

static NSString * const kAD_URL_GET_USER_COUPONS = @"%@/get_user_coupons_and_partners?token=%@";

static NSString * const kAD_URL_UPDATE_LAST_ACTIVITY = @"%@/update_user_coordinate_and_last_activity?longitude=%.6f&latitude=%.6f&token=%@";

static NSString * const kAD_URL_UPDATE_USER_PROFILE = @"%@/update_user?token=%@";

static NSString * const kAD_URL_LOGOUT = @"%@/logout?token=%@";

static NSString * const kAD_URL_REPORT = @"%@/report_user?token=%@";

static NSString * const kAD_URL_CHECK_SESSION_TICKET = @"%@/check_token?token=%@";



@implementation ADNSURLCreator


#pragma mark -
#pragma mark - Private methods

+ (NSString *)sessionTicket
{
    return [ADUserManager sharedInstance].sessionTicket;
}

+ (NSString *)serverURLString
{
    NSString *url = [NSString stringWithFormat:@"%@:%@", kAD_SERVER_HOST, kAD_SERVER_PORT];
    return url;
}

+ (NSURL *)urlWithString:(NSString *)urlString
{
    DDLogInfo(@"%@", urlString);
    return [NSURL URLWithString:urlString];
}


#pragma mark -
#pragma mark - Public methods

+ (NSString *)profileImageBaseUrl
{
    return [NSString stringWithFormat:@"%@%@", [self serverURLString], kAD_URL_PROFILE_IMAGE_BASE];
}

+ (NSString *)partnerImageBaseUrl
{
    return [NSString stringWithFormat:@"%@%@", [self serverURLString], kAD_URL_PARTNER_IMAGE_BASE];
}

+ (NSURL *)loginURL
{
    NSString *url = [NSString stringWithFormat:kAD_URL_LOGIN, [self serverURLString]];
    return [self urlWithString:url];
}

+ (NSURL *)registerURL
{
    NSString *url = [NSString stringWithFormat:kAD_URL_REGISTER, [self serverURLString]];
    return [self urlWithString:url];
}

+ (NSURL *)forgotPasswordURLWithEmail:(NSString *)email
{
    NSString *url = [NSString stringWithFormat:kAD_URL_FORGOT_PASSWORD, [self serverURLString], email];
    return [self urlWithString:url];
}

+ (NSURL *)nearbyUserURLWithMode:(NSString*)mode BatchSize:(NSInteger)batchSize Gender:(NSString *)genderString MinAge:(NSInteger)minAge MaxAge:(NSInteger)maxAge MaxDistance:(NSInteger)maxDistance CurrentLocation:(CLLocation *)currentLocation
{
    NSString *url = [NSString stringWithFormat:kAD_URL_NEARBY_USER, [self serverURLString], mode, (long)batchSize, currentLocation.coordinate.longitude, currentLocation.coordinate.latitude, (long)maxDistance*1000, (long)minAge, (long)maxAge, genderString, [self sessionTicket]];
    return [self urlWithString:url];
}

+ (NSURL *)nearbyEventURLWithMode:(NSString *)mode BatchSize:(NSInteger)batchSize MaxDistance:(NSInteger)maxDistance CurrentLocation:(CLLocation *)currentLocation
{
    NSString *url = [NSString stringWithFormat:kAD_URL_NEARBY_EVENT, [self serverURLString], mode, (long)batchSize, currentLocation.coordinate.longitude, currentLocation.coordinate.latitude, (long)maxDistance*1000, [self sessionTicket]];
    return [self urlWithString:url];
}

+ (NSURL *)addConnectionURL
{
    NSString *url = [NSString stringWithFormat:kAD_URL_CONNECTION_ADD, [self serverURLString], [self sessionTicket]];
    return [self urlWithString:url];
}

+ (NSURL *)rejectConnectionURL
{
    NSString *url = [NSString stringWithFormat:kAD_URL_CONNECTION_REJECT, [self serverURLString], [self sessionTicket]];
    return [self urlWithString:url];
}

+ (NSURL *)blockConnectionURL
{
    NSString *url = [NSString stringWithFormat:kAD_URL_CONNECTION_BLOCK, [self serverURLString], [self sessionTicket]];
    return [self urlWithString:url];
}

+ (NSURL *)unblockConnectionURL
{
    NSString *url = [NSString stringWithFormat:kAD_URL_CONNECTION_UNBLOCK, [self serverURLString], [self sessionTicket]];
    return [self urlWithString:url];
}

+ (NSURL *)deleteConnectionURL
{
    NSString *url = [NSString stringWithFormat:kAD_URL_CONNECTION_DELETE, [self serverURLString], [self sessionTicket]];
    return [self urlWithString:url];
}

+ (NSURL *)getUserConnectionsURL
{
    NSString *url = [NSString stringWithFormat:kAD_URL_GET_USER_CONNECTIONS, [self serverURLString], [self sessionTicket]];
    return [self urlWithString:url];
}

+ (NSURL *)updateConnectionsProfileURL
{
    NSString *url = [NSString stringWithFormat:kAD_URL_UPDATE_USER_CONNECTIONS_PROFILE, [self serverURLString], [self sessionTicket]];
    return [self urlWithString:url];
}

+ (NSURL *)getPendingRequestsURL
{
    NSString *url = [NSString stringWithFormat:kAD_URL_GET_USER_PENDING_CONNECTIONS, [self serverURLString], [self sessionTicket]];
    return [self urlWithString:url];
}

+ (NSURL *)getUserCouponsURL
{
    NSString *url = [NSString stringWithFormat:kAD_URL_GET_USER_COUPONS, [self serverURLString], [self sessionTicket]];
    return [self urlWithString:url];
}

+ (NSURL *)updateUserProfileURL
{
    NSString *url = [NSString stringWithFormat:kAD_URL_UPDATE_USER_PROFILE, [self serverURLString], [self sessionTicket]];
    return [self urlWithString:url];
}

+ (NSURL *)updateLastActivity:(CLLocation *)location
{
    NSString *url = [NSString stringWithFormat:kAD_URL_UPDATE_LAST_ACTIVITY, [self serverURLString], location.coordinate.longitude, location.coordinate.latitude, [self sessionTicket]];
    return [self urlWithString:url];
}

+ (NSURL *)logoutURL
{
    NSString *url = [NSString stringWithFormat:kAD_URL_LOGOUT, [self serverURLString], [self sessionTicket]];
    return [self urlWithString:url];
}

+ (NSURL *)reportURL
{
    NSString *url = [NSString stringWithFormat:kAD_URL_REPORT, [self serverURLString], [self sessionTicket]];
    return [self urlWithString:url];
}

+ (NSURL *)checkSessionTicketURL
{
    NSString *url = [NSString stringWithFormat:kAD_URL_CHECK_SESSION_TICKET, [self serverURLString], [self sessionTicket]];
    return [self urlWithString:url];
}

@end
