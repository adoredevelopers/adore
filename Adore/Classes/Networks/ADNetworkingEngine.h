//
//  ADNetworkingEngine.h
//  Adore
//
//  Created by Wang on 2015-01-20.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADChatManager;
@class ADUserManager;

@interface ADNetworkingEngine : NSObject


@property (nonatomic, strong) ADUserManager *userManager;
@property (nonatomic, strong) ADChatManager *chatManager;


@end
