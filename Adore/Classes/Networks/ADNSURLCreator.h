//
//  ADNSURLCreator.h
//  Adore
//
//  Created by Wang on 2015-06-05.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>



@interface ADNSURLCreator : NSObject


+ (NSString *)profileImageBaseUrl;
+ (NSString *)partnerImageBaseUrl;

+ (NSURL *)loginURL;
+ (NSURL *)registerURL;
+ (NSURL *)forgotPasswordURLWithEmail:(NSString *)email;
+ (NSURL *)nearbyUserURLWithMode:(NSString *)mode BatchSize:(NSInteger)batchSize Gender:(NSString *)genderString MinAge:(NSInteger)minAge MaxAge:(NSInteger)maxAge MaxDistance:(NSInteger)maxDistance CurrentLocation:(CLLocation *)currentLocation;
+ (NSURL *)nearbyEventURLWithMode:(NSString *)mode BatchSize:(NSInteger)batchSize MaxDistance:(NSInteger)maxDistance CurrentLocation:(CLLocation *)currentLocation;
+ (NSURL *)addConnectionURL;
+ (NSURL *)rejectConnectionURL;
+ (NSURL *)blockConnectionURL;
+ (NSURL *)unblockConnectionURL;
+ (NSURL *)deleteConnectionURL;
+ (NSURL *)getUserConnectionsURL;
+ (NSURL *)updateConnectionsProfileURL;
+ (NSURL *)getPendingRequestsURL;
+ (NSURL *)getUserCouponsURL;
+ (NSURL *)updateUserProfileURL;
+ (NSURL *)updateLastActivity:(CLLocation *)location;
+ (NSURL *)logoutURL;
+ (NSURL *)reportURL;
+ (NSURL *)checkSessionTicketURL;


@end
