//
//  ADJSONResponse.h
//  Adore
//
//  Created by Wang on 2015-07-04.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ADJSONResponse : NSObject


@property (nonatomic, assign, readonly) BOOL success;
@property (nonatomic, strong, readonly) NSString *message;
@property (nonatomic, strong, readonly) NSDictionary *jsonDictionary;


- (instancetype)initWithData:(NSData *)data;
- (instancetype)initWithData:(NSData *)data responseType:(NSString *)responseType;



- (NSDictionary *)dataAsDictionary;
- (NSArray *)dataAsArray;


@end
