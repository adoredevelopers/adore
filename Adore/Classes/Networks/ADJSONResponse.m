//
//  ADJSONResponse.m
//  Adore
//
//  Created by Wang on 2015-07-04.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADJSONResponse.h"
#import "NSDictionary+NonNilObject.h"



@interface ADJSONResponse ()


@end



@implementation ADJSONResponse

- (instancetype)initWithData:(NSData *)data
{
    self = [self initWithData:data responseType:@"response json"];
    return self;
}

- (instancetype)initWithData:(NSData *)data responseType:(NSString *)responseType
{
    self = [super init];
    if (self) {
        NSError *error = nil;
        _jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        DDLogDebug(@"-------------------------------------------------------------");
        DDLogDebug(@"%@:\n%@", responseType, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        
        _success = [_jsonDictionary boolForKey:@"success"];
        _message = [_jsonDictionary stringObjectForKey:@"message"];
    }
    
    return self;
}



- (NSDictionary *)dataAsDictionary
{
    return [self.jsonDictionary dictionaryObjectForKey:@"data"];
}

- (NSArray *)dataAsArray
{
    return [self.jsonDictionary arrayObjectForKey:@"data"];
}


@end
