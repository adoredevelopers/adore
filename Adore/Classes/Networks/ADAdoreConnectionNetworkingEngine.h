//
//  ADAdoreConnectionNetworkingEngine.h
//  Adore
//
//  Created by Wang on 2015-01-20.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADNetworkingEngine.h"

@class ADManagedUser;

typedef void (^ADConnectionNetworkCompletionBlock) (BOOL response, BOOL approved, NSError *error);



@interface ADAdoreConnectionNetworkingEngine : ADNetworkingEngine


- (void)startAddingUserRequestToUser:(ADManagedUser *)user completionBlock:(ADConnectionNetworkCompletionBlock)completionBlock;
- (void)startRejectingUserRequestWithUser:(ADManagedUser *)user completionBlock:(ADConnectionNetworkCompletionBlock)completionBlock;
- (void)startBlockingUser:(NSString *)email completionBlock:(ADConnectionNetworkCompletionBlock)completionBlock;
- (void)startUnblockingUser:(NSString *)email completionBlock:(ADConnectionNetworkCompletionBlock)completionBlock;
- (void)startDeletingUser:(ADManagedUser *)user completionBlock:(ADConnectionNetworkCompletionBlock)completionBlock;


@end
