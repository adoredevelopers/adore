//
//  ADNetworkingEngine.m
//  Adore
//
//  Created by Wang on 2015-01-20.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADNetworkingEngine.h"
#import "ADUserManager.h"
#import "ADChatManager.h"



@implementation ADNetworkingEngine

#pragma mark -
#pragma mark - Memory management

- (void)dealloc
{
    [self cancelAllRequests];
}


#pragma mark -
#pragma mark - Cancel requests

- (void)cancelAllRequests
{
    
}


#pragma mark -
#pragma mark - Lazy initializers

- (ADUserManager *)userManager
{
    if (_userManager == nil) {
        _userManager = [ADUserManager sharedInstance];
    }
    return _userManager;
}

- (ADChatManager *)chatManager
{
    if (_chatManager == nil) {
        _chatManager = [ADChatManager sharedInstance];
    }
    return _chatManager;
}


@end
