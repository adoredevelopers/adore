//
//  ADImageTextTableViewCell.h
//  Adore
//
//  Created by Wang on 2014-12-10.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TTTAttributedLabel/TTTAttributedLabel.h>

@class ADManagedUser;
@class CustomBadge;

@interface ADImageTextTableViewCell : UITableViewCell <TTTAttributedLabelDelegate>

@property (nonatomic, weak) IBOutlet UIImageView *iconView;
@property (nonatomic, weak) IBOutlet TTTAttributedLabel *label;
@property (nonatomic, weak) IBOutlet UILabel *detailLabel;
@property (nonatomic, weak) IBOutlet UIImageView *accessoryImageView;

@property (nonatomic, strong) CustomBadge *badgeView;

@property (nonatomic, strong) ADManagedUser *userObject;

- (void)updateViewWithUserObject:(ADManagedUser *)user showBadge:(BOOL)showBadge;

- (void)updateWithImage:(UIImage *)image text:(NSString *)text;
- (void)updateWithImage:(UIImage *)image text:(NSString *)text detailText:(NSString *)detailText;

- (void)updateWithImage:(UIImage *)image text:(NSString *)text badge:(NSInteger)badge;
- (void)updateWithImage:(UIImage *)image text:(NSString *)text detailText:(NSString *)detailText badge:(NSInteger)badge;

@end
