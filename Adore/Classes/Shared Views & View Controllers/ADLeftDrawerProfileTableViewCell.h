//
//  ADLeftDrawerProfileTableViewCell.h
//  Adore
//
//  Created by Wang on 2015-09-11.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ADLeftDrawerProfileTableViewCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UIImageView *profileImageView;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *editLabel;

- (void)startPulses;
- (void)stopPulses;

@end
