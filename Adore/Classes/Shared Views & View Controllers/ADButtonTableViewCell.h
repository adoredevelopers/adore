//
//  ADButtonTableViewCell.h
//  Adore
//
//  Created by Wang on 2014-12-14.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADButtonTableViewCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UILabel *label;
@property (nonatomic, weak) IBOutlet UIImageView *iconView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imageViewHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imageViewWidthConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imageViewPaddingConstraint;

+ (CGFloat)rowHeight;

- (void)updateWithImage:(UIImage *)image text:(NSString *)text;

@end
