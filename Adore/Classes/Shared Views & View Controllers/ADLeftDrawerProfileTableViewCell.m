//
//  ADLeftDrawerProfileTableViewCell.m
//  Adore
//
//  Created by Wang on 2015-09-11.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADLeftDrawerProfileTableViewCell.h"



@implementation ADLeftDrawerProfileTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
    if ([self respondsToSelector:@selector(setSeparatorInset:)]) {
        [self setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [self setLayoutMargins:UIEdgeInsetsZero]; // iOS 8+ property
    }

    self.profileImageView.backgroundColor = [UIColor whiteColor];
    self.profileImageView.clipsToBounds = YES;
    self.profileImageView.layer.cornerRadius = 50;
    self.profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.profileImageView.layer.borderWidth = 1.5;
    
    self.editLabel.text = NSLocalizedString(@"Tap to view/edit", nil);
}

- (void)startPulses {
    CABasicAnimation *pulseAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pulseAnimation.duration = .4;
    pulseAnimation.toValue = [NSNumber numberWithFloat:1.1];
    pulseAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    pulseAnimation.autoreverses = YES;
    pulseAnimation.repeatCount = FLT_MAX;
    [self.profileImageView.layer addAnimation:pulseAnimation forKey:nil];
}

- (void)stopPulses {
    [self.profileImageView.layer removeAllAnimations];
}

@end
