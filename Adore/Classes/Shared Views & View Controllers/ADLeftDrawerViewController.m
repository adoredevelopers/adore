//
//  ADLeftDrawerViewController.m
//  Adore
//
//  Created by Wang on 2015-04-16.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADLeftDrawerViewController.h"
#import "AppDelegate.h"
#import "ADUserManager.h"
#import "ADUserCoreDataHelper.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ADManagedUser+Extensions.h"
#import "ADChatManager.h"
#import "BBBadgeBarButtonItem.h"
#import "CustomBadge.h"
#import "ADLeftDrawerProfileTableViewCell.h"

typedef NS_ENUM (NSInteger, ADLeftDrawerSectionType)
{
    ADLeftDrawerSectionTypeProfile,
    ADLeftDrawerSectionTypeFeature,
    ADLeftDrawerSectionTypeCount
};

@interface ADLeftDrawerViewController () <UITableViewDataSource, UITableViewDelegate>


@property (nonatomic, strong) UITableView *tableView;


@end



@implementation ADLeftDrawerViewController

#pragma mark -
#pragma mark - View lifecycles

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self refreshNSFetchedResultsControllers];
    
    [self showMessageButtonView];
    
    self.view.backgroundColor = [self viewBackgroundColor];
    
    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;

    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 280, self.view.bounds.size.height) style:UITableViewStyleGrouped];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"left_drawer_bg"]];
//    [(UIImageView *)self.tableView.backgroundView sd_setImageWithURL:[NSURL URLWithString:self.userManager.currentUserProfile.profilePictureUrl] placeholderImage:[UIImage imageNamed:@"left_drawer_bg"] options:SDWebImageRetryFailed];
    UIBlurEffect * effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView * blurView = [[UIVisualEffectView alloc] initWithEffect:effect];
    blurView.alpha = 0.3f;
    blurView.frame = self.tableView.backgroundView.frame;
    [self.tableView.backgroundView addSubview:blurView];
//    self.tableView.backgroundColor = [self viewBackgroundColor];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.contentInset = UIEdgeInsetsMake(44,0,0,0);
    [self.view addSubview:self.tableView];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ADLeftDrawerProfileTableViewCell" bundle:nil] forCellReuseIdentifier:@"ProfileCell"];
    
    // Leave this line here. We want the table view to highlight the current selected mode
    self.selectionMode = self.selectionMode;
    
    
    
    /* Register Other Device SocketIO Connection Notification */
    [[NSNotificationCenter defaultCenter] addObserverForName:kAD_NOTIFICATION_SOCKET_CONNECTION_FROM_ANOTHER_DEVICE
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                                                      [appDelegate logoutCurrentUser];
                                                  }];
}


//- (void)viewWillAppear:(BOOL)animated
//{
//    [self.tableView reloadData];
//}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self setSelectionMode:_selectionMode];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    ADLeftDrawerProfileTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    if (self.selectionMode == ADLeftDrawerSelectionModeProfile) {
        [cell startPulses];
    } else {
        [cell stopPulses];
    }
}


#pragma mark -
#pragma mark - Helper methods

- (void)refreshNSFetchedResultsControllers
{
    self.pendingUsersController = [self.userManager.userCoreDataHelper pendingUsersFetchedResultsControllerWithDelegate:self];
    self.unreadMessagesController = [self.chatManager unreadMessagesFetchedResultsControllerWithDelegate:self];
    self.unreadConnectionsController = [self.userManager.userCoreDataHelper unreadConnectedUsersFetchedResultsControllerWithDelegate:self];
    
    [self.tableView reloadData];
}

- (UIColor *)viewBackgroundColor
{
    return [UIColor colorWithRed:100.0/255.0 green:100.0/255.0 blue:100.0/255.0 alpha:0.7];
}

- (UIColor *)cellBackgroundColor
{
    return [UIColor clearColor];
}

- (UIColor *)cellHighlightColor
{
    return [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:0.7];
}

- (UIView *)cellHighlightView
{
    UIView *selectionColorView = [[UIView alloc] init];
    selectionColorView.backgroundColor = [self cellHighlightColor];
    return selectionColorView;
}

- (UIColor *)cellContentColor
{
    return [UIColor whiteColor];
}

- (void)messageButtonClicked:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate showPendingUserView];
}

- (void)showMessageButtonView
{
    NSInteger count = self.pendingUsersController.fetchedObjects.count;
    if (count > 0) {
        UIButton *button = [[UIButton alloc] init];
        button.tintColor = [UIColor whiteColor];
        [button setImage:[[UIImage imageNamed:@"heart"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        [button sizeToFit];
        [button addTarget:self action:@selector(messageButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        BBBadgeBarButtonItem *messageButton = [[BBBadgeBarButtonItem alloc] initWithCustomUIButton:button];
        messageButton.badgeValue = [NSString stringWithFormat:@"%ld", (long)count];
        messageButton.badgeOriginX = 20;
        self.navigationItem.leftBarButtonItem = messageButton;
    }
    else {
        self.navigationItem.leftBarButtonItem = nil;
    }
}

- (void)setSelectionMode:(ADLeftDrawerSelectionMode)selectionMode
{
    _selectionMode = selectionMode;
    
//    if (selectionMode == ADLeftDrawerSelectionModeProfile) {
//        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
//    }
//    else {
//        [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES];
//    }
//
//    if (selectionMode == ADLeftDrawerSelectionModeNearbyUsers) {
//        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] animated:YES scrollPosition:UITableViewScrollPositionNone];
//    }
//    else {
//        [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] animated:YES];
//    }
//    if (selectionMode == ADLeftDrawerSelectionModeNearbyEvents) {
//        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1] animated:YES scrollPosition:UITableViewScrollPositionNone];
//    }
//    else {
//        [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1] animated:YES];
//    }
//    if (selectionMode == ADLeftDrawerSelectionModeAdore) {
//        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1] animated:YES scrollPosition:UITableViewScrollPositionNone];
//    }
//    else {
//        [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1] animated:YES];
//    }
//    if (selectionMode == ADLeftDrawerSelectionModeChat) {
//        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:1] animated:YES scrollPosition:UITableViewScrollPositionNone];
//    }
//    else {
//        [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:1] animated:YES];
//    }
//    if (selectionMode == ADLeftDrawerSelectionModeSettings) {
//        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:1] animated:YES scrollPosition:UITableViewScrollPositionNone];
//    }
//    else {
//        [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:1] animated:YES];
//    }
    if (selectionMode == ADLeftDrawerSelectionModeProfile) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
    else {
        [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES];
    }
    
    if (selectionMode == ADLeftDrawerSelectionModeNearbyUsers) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
    else {
        [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] animated:YES];
    }
    if (selectionMode == ADLeftDrawerSelectionModeAdore) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1] animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
    else {
        [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1] animated:YES];
    }
    if (selectionMode == ADLeftDrawerSelectionModeChat) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1] animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
    else {
        [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1] animated:YES];
    }
    if (selectionMode == ADLeftDrawerSelectionModeSettings) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:1] animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
    else {
        [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:1] animated:YES];
    }
}

- (void)reloadProfileCell
{
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:ADLeftDrawerSectionTypeProfile]] withRowAnimation:UITableViewRowAnimationNone];
}


#pragma mark -
#pragma mark - NSFetchedResultsControllerDelegate methods

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if (controller == self.pendingUsersController) {
        [self showMessageButtonView];
    }
    else {
        [self.tableView reloadData];
    }
}


#pragma mark -
#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ADLeftDrawerSectionTypeCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == ADLeftDrawerSectionTypeProfile) {
        return 1;
    }
    else if (section == ADLeftDrawerSectionTypeFeature) {
        return 4;//5;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == ADLeftDrawerSectionTypeProfile && indexPath.row == 0) {
        return 160;
    }
    return 60;
}

- (CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == ADLeftDrawerSectionTypeProfile) {
        return 20.0;
    }
    return 0.0;
}

- (CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == ADLeftDrawerSectionTypeProfile) {
        return 10.0;
    }
    return 0.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == ADLeftDrawerSectionTypeProfile && indexPath.row == 0) {
        ADLeftDrawerProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileCell"];
        
        ADManagedUser *profile = self.userManager.currentUserProfile;
        [cell.profileImageView sd_setImageWithURL:[NSURL URLWithString:[profile profilePictureUrl]]
                                 placeholderImage:[UIImage imageNamed:@"profile_image_placeholder_72x72"] options:SDWebImageRetryFailed];
        cell.nameLabel.text = profile.nickname;
        cell.backgroundColor = [UIColor clearColor];
//        [cell setSelectedBackgroundView:[self cellHighlightView]];
        UIView *profileCellBackgroundView = [[UIView alloc] init];
        [profileCellBackgroundView setBackgroundColor:[UIColor clearColor]];
        [cell setSelectedBackgroundView: profileCellBackgroundView];
        
        return cell;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell"];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero]; // iOS 8+ property
        }
        cell.backgroundColor = [self cellBackgroundColor];
        cell.imageView.contentMode = UIViewContentModeCenter;
        cell.imageView.tintColor = [self cellContentColor];
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [self cellContentColor];
        cell.accessoryView = nil;
    }
    
    if (indexPath.section == ADLeftDrawerSectionTypeFeature) {
        switch (indexPath.row) {
//            case 0:
//            {
//                cell.imageView.image = [[UIImage imageNamed:@"globe"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//                cell.textLabel.text = NSLocalizedString(@"Nearby Users", nil);
//                break;
//            }
//            case 1:
//            {
//                cell.imageView.image = [[UIImage imageNamed:@"location"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//                cell.textLabel.text = NSLocalizedString(@"Nearby Events", nil);
//                break;
//            }
//            case 2:
//            {
//                cell.imageView.image = [[UIImage imageNamed:@"heart"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//                cell.textLabel.text = NSLocalizedString(@"Adore", nil);
//                NSInteger count = self.unreadConnectionsController.fetchedObjects.count;
//                if (count > 0) {
//                    CustomBadge *badge = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%ld", (long)count]];
//                    cell.accessoryView = badge;
//                }
//                else {
//                    cell.accessoryView = nil;
//                }
//                break;
//            }
//            case 3:
//            {
//                cell.imageView.image = [[UIImage imageNamed:@"chat"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//                cell.textLabel.text = NSLocalizedString(@"Chat", nil);
//                NSInteger count = self.unreadMessagesController.fetchedObjects.count;
//                if (count > 0) {
//                    CustomBadge *badge = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%ld", (long)count]];
//                    cell.accessoryView = badge;
//                }
//                else {
//                    cell.accessoryView = nil;
//                }
//                break;
//            }
//            case 4:
//            {
//                cell.imageView.image = [[UIImage imageNamed:@"gear"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//                cell.textLabel.text = NSLocalizedString(@"Settings", nil);
//                break;
//            }
            case 0:
            {
                cell.imageView.image = [[UIImage imageNamed:@"globe"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.textLabel.text = NSLocalizedString(@"Nearby Users", nil);
                break;
            }
            case 1:
            {
                cell.imageView.image = [[UIImage imageNamed:@"heart"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.textLabel.text = NSLocalizedString(@"Adore", nil);
                NSInteger count = self.unreadConnectionsController.fetchedObjects.count;
                if (count > 0) {
                    CustomBadge *badge = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%ld", (long)count]];
                    cell.accessoryView = badge;
                }
                else {
                    cell.accessoryView = nil;
                }
                break;
            }
            case 2:
            {
                cell.imageView.image = [[UIImage imageNamed:@"chat"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.textLabel.text = NSLocalizedString(@"Chat", nil);
                NSInteger count = 0;
                count = self.unreadMessagesController.fetchedObjects.count;
                if (count > 0) {
                    CustomBadge *badge = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%ld", (long)count]];
                    cell.accessoryView = badge;
                }
                else {
                    cell.accessoryView = nil;
                }
                break;
            }
            case 3:
            {
                cell.imageView.image = [[UIImage imageNamed:@"gear"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.textLabel.text = NSLocalizedString(@"Settings", nil);
                break;
            }
        }
    }
    
    [cell setSelectedBackgroundView:[self cellHighlightView]];
    
    return cell;
}

#pragma mark -
#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    if (indexPath.section == ADLeftDrawerSectionTypeProfile) {
        if (indexPath.row == 0) {
            [appDelegate showViewProfileView];
        }
    }
    else if (indexPath.section == ADLeftDrawerSectionTypeFeature) {
        switch (indexPath.row) {
//            case 0:
//                [appDelegate showNearbyUsersView];
//                break;
//            case 1:
//                [appDelegate showNearbyEventsView];
//                break;
//            case 2:
//                [appDelegate showAdoreView];
//                break;
//            case 3:
//                [appDelegate showChatView];
//                break;
//            case 4:
//                [appDelegate showSettingView];
                //                break;
            case 0:
                [appDelegate showNearbyUsersView];
                break;
            case 1:
                [appDelegate showAdoreView];
                break;
            case 2:
                [appDelegate showChatView];
                break;
            case 3:
                [appDelegate showSettingView];
                break;
        }
    }
}

@end
