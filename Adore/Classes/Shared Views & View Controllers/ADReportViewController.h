//
//  ADReportViewController.h
//  Adore
//
//  Created by Wang on 2015-07-07.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADViewController.h"



@interface ADReportViewController : ADViewController


- (void)setReportedUserId:(NSString *)userId;
- (void)setReportedImageUrl:(NSString *)url;


@end