//
//  ADLeftDrawerViewController.h
//  Adore
//
//  Created by Wang on 2015-04-16.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADViewController.h"

typedef NS_ENUM(NSInteger, ADLeftDrawerSelectionMode)
{
    ADLeftDrawerSelectionModeNone,
    ADLeftDrawerSelectionModeProfile,
    ADLeftDrawerSelectionModeNearbyUsers,
//    ADLeftDrawerSelectionModeNearbyEvents,
    ADLeftDrawerSelectionModeAdore,
    ADLeftDrawerSelectionModeChat,
    ADLeftDrawerSelectionModeSettings
};



@interface ADLeftDrawerViewController : ADViewController


@property (nonatomic, assign) ADLeftDrawerSelectionMode selectionMode;

- (void)refreshNSFetchedResultsControllers;
- (void)reloadProfileCell;

@end
