//
//  ADTextViewInputViewController.h
//  Adore
//
//  Created by Kevin Wang on 2015-07-10.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//


#import "ADViewController.h"

@protocol ADTextViewInputDelegate;



@interface ADTextViewInputViewController : ADViewController


@property (weak, nonatomic) id<ADTextViewInputDelegate> delegate;
@property (strong, nonatomic) NSString *startingString;

@property (assign, nonatomic) BOOL showEditButton;
@property (assign, nonatomic) UIDataDetectorTypes dataDetectorTypes;
@property (assign, nonatomic) NSUInteger maxAllowableCharacters;

- (instancetype)initWithTitle:(NSString *)title delegate:(id<ADTextViewInputDelegate>)delegate startingString:(NSString *)startingString showEditButton:(BOOL)showEditButton;


@end



@protocol ADTextViewInputDelegate <NSObject>

@optional
- (void)textViewInputFinishedWithString:(NSString *)string;
- (void)textViewInputBackButtonTappedWithString:(NSString *)string;

@end
