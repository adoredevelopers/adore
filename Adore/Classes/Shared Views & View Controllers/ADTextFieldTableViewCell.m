//
//  ADTextFieldTableViewCell.m
//  Adore
//
//  Created by Wang on 2015-04-30.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADTextFieldTableViewCell.h"
#import "JVFloatLabeledTextField.h"
#import "ADManagedUser+Extensions.h"



@interface ADTextFieldTableViewCell ()

@property (nonatomic, weak) IBOutlet UIView *separatorView;

@end



@implementation ADTextFieldTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.textField.font = [UIFont systemFontOfSize:16];
    self.textField.floatingLabelFont = [UIFont boldSystemFontOfSize:11];
    self.textField.floatingLabelTextColor = [[UIApplication sharedApplication].delegate window].tintColor;
    self.textField.floatingLabelActiveTextColor = self.tintColor;
    self.textField.clearButtonMode = UITextFieldViewModeWhileEditing;

    self.textField.translatesAutoresizingMaskIntoConstraints = NO;
    self.textField.keepBaseline = 1;
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    [toolbar sizeToFit];
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil) style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyboard:)];
    doneButton.tintColor = [Constants femaleTintColor];
    toolbar.items = @[space, doneButton];
    self.textField.inputAccessoryView = toolbar;
}

+ (instancetype)textFieldCellForTableView:(UITableView *)tableView type:(ADUserDetailItemType)type userData:(ADManagedUser *)managedUser delegate:(id<UITextFieldDelegate>)delegate
{
    ADTextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_TEXT_FIELD_TABLE_VIEW_CELL_IDENTIFIER];
    cell.textField.delegate = delegate;
    cell.textField.tag = type;
    
    switch (type) {
        case ADUserDetailItemTypeName:
            [cell setPlaceholdText:NSLocalizedString(@"Name", nil)];
            [cell setTextFieldText:managedUser.fullname];
            break;
        case ADUserDetailItemTypeMotto:
            [cell setPlaceholdText:NSLocalizedString(@"Motto", nil)];
            [cell setTextFieldText:managedUser.motto];
            break;
        case ADUserDetailItemTypeNickname:
            [cell setPlaceholdText:NSLocalizedString(@"Nickname", nil)];
            [cell setTextFieldText:managedUser.nickname];
            break;
        case ADUserDetailItemTypeUndergradEdu:
            [cell setPlaceholdText:NSLocalizedString(@"Undergrad Education", nil)];
            [cell setTextFieldText:managedUser.undergrad_edu_institution];
            break;
        case ADUserDetailItemTypePostgradEdu:
            [cell setPlaceholdText:NSLocalizedString(@"Postgrad Education", nil)];
            [cell setTextFieldText:managedUser.postgrad_edu_institution];
            break;
        case ADUserDetailItemTypeEmployer:
            [cell setPlaceholdText:NSLocalizedString(@"Employer (Company)", nil)];
            [cell setTextFieldText:managedUser.employer];
            break;

        default:
        {
            break;
        }
    }
    return cell;
}

+ (instancetype)textFieldCellForTableView:(UITableView *)tableView
                                     text:(NSString *)text
                          placeholderText:(NSString *)placeholder
                                      tag:(NSInteger)tag
                                 delegate:(id<UITextFieldDelegate>)delegate
{
    ADTextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_TEXT_FIELD_TABLE_VIEW_CELL_IDENTIFIER];
    cell.separatorView.hidden = YES;
    cell.textField.delegate = delegate;
    cell.textField.tag = tag;
    [cell setTextFieldText:text];
    [cell setPlaceholdText:placeholder];
    cell.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{ NSForegroundColorAttributeName:[UIColor colorWithWhite:0.4 alpha:1] }];
    return cell;
}

- (void)dismissKeyboard:(id)sender
{
    [self.textField resignFirstResponder];
}

- (void)setPlaceholdText:(NSString *)placeholder
{
    [self.textField setPlaceholder:placeholder floatingTitle:placeholder];
}

- (void)setTextFieldText:(NSString *)text
{
    self.textField.text = text;
}

- (CGFloat)cellHeight
{
    return 44;
}


@end
