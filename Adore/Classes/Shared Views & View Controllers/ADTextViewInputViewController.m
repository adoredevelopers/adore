//
//  ADTextViewInputViewController.m
//  Adore
//
//  Created by Kevin Wang on 2015-07-10.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADTextViewInputViewController.h"



@interface ADTextViewInputViewController () <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

@property (assign, nonatomic) CGSize keyboardSize;

@property (nonatomic, weak) IBOutlet UILabel *charactersRemainingLabel;

@end



@implementation ADTextViewInputViewController


#pragma mark -
#pragma mark - Initializer

- (void)commonInit
{
    _dataDetectorTypes = UIDataDetectorTypeNone;
    _showEditButton = YES;
    _keyboardSize = CGSizeZero;
    _maxAllowableCharacters = NSUIntegerMax;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithTitle:(NSString *)title delegate:(id<ADTextViewInputDelegate>)delegate startingString:(NSString *)startingString showEditButton:(BOOL)showEditButton
{
    self = [super initWithNibName:@"ADTextViewInputViewController" bundle:nil];
    if (self) {
        [self commonInit];
        
        self.title = title;
        _delegate = delegate;
        _startingString = startingString;
        _showEditButton = showEditButton;
    }
    return self;
}


#pragma mark -
#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    if (self.dataDetectorTypes != UIDataDetectorTypeNone) {
        self.textView.editable = NO;
        self.textView.dataDetectorTypes = self.dataDetectorTypes;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textViewTapped:)];
        [self.textView addGestureRecognizer:tap];
    }
    
    self.textView.text = self.startingString;
    self.textView.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (self.showEditButton) {
        [self setRightButtonToEdit];
    }
    if (self.maxAllowableCharacters < NSUIntegerMax) {
        [self changeRemainingLabel:self.startingString.length];
    }
    else {
        self.charactersRemainingLabel.text = @"";
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self addKeyboardNotifications];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    /**
     *	if we are not showing the edit button, we assume that this is a view which will let the user type and then hit back to save
     *  their changes, so we might as well show the keyboard as they come in here instead of making them tap.
     */
    if (self.showEditButton == NO) {
        [self.textView becomeFirstResponder];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeKeyboardNotifications];
}

- (void)didMoveToParentViewController:(UIViewController *)parent
{
    if (![parent isEqual:self.parentViewController]) {
        if ([self.delegate respondsToSelector:@selector(textViewInputBackButtonTappedWithString:)]) {
            [self.delegate textViewInputBackButtonTappedWithString:self.textView.text];
        }
    }
}


#pragma mark -
#pragma mark - Helpers

- (void)setRightButtonToEdit
{
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(startEditing)];
    self.navigationItem.rightBarButtonItem = editButton;
}

- (void)setRightButtonToDone
{
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(doneEditing)];
    self.navigationItem.rightBarButtonItem = doneButton;
}

- (void)updateBottomConstraint
{
    if (self.textView.editable) {
        self.bottomConstraint.constant = self.keyboardSize.height;
    }
    else {
        self.bottomConstraint.constant = 0.0;
    }
}

-(void)colorRemainingText: (NSUInteger)charsLeft
{
    if (charsLeft <= self.maxAllowableCharacters * 0.1) {
        self.charactersRemainingLabel.textColor = [UIColor redColor];
    }
    else if (charsLeft <= self.maxAllowableCharacters * 0.2) {
        self.charactersRemainingLabel.textColor = [UIColor orangeColor];
    }
    else {
        self.charactersRemainingLabel.textColor = [UIColor blackColor];
    }
}

-(void)changeRemainingLabel:(NSUInteger)length
{
    NSUInteger charsLeft = self.maxAllowableCharacters  - length;
    self.charactersRemainingLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Characters Remaining: %ld", nil), (long)charsLeft];
    [self colorRemainingText:charsLeft];
}


#pragma mark -
#pragma mark - Action Methods

- (void)startEditing
{
    if (self.showEditButton) {
        [self setRightButtonToDone];
    }
    
    self.textView.editable = YES;
    self.textView.dataDetectorTypes = UIDataDetectorTypeNone;
    
    [self.textView becomeFirstResponder];
}

- (void)doneEditing
{
    if (self.showEditButton) {
        [self setRightButtonToEdit];
    }
    
    self.textView.editable = NO;
    self.textView.dataDetectorTypes = self.dataDetectorTypes;
    
    [self.textView resignFirstResponder];
    
    if ([self.delegate respondsToSelector:@selector(textViewInputFinishedWithString:)]) {
        [self.delegate textViewInputFinishedWithString:self.textView.text];
    }
}


#pragma mark -
#pragma mark - UIGestureRecognizers

- (void)textViewTapped:(UITapGestureRecognizer *)tap
{
    [self startEditing];
}


#pragma mark -
#pragma mark - Keyboard Notifications and Handling

- (void)addKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)removeKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - Keyboard Handling

- (void)keyboardWillShow:(NSNotification *)notification
{
    // get some information about the keyboard from the notification
    NSTimeInterval animationDuration;
    NSUInteger animationCurve;
    CGRect keyboardFrame;
    
    NSDictionary *userInfo = notification.userInfo;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardFrame];
    
    self.keyboardSize = [self.view convertRect:keyboardFrame fromView:nil].size;
    
    [self updateBottomConstraint];
    
    [UIView animateWithDuration:animationDuration
                          delay:0.0
                        options:animationCurve
                     animations:^{
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    // get some information about the keyboard from the notification
    NSTimeInterval animationDuration;
    NSUInteger animationCurve;
    CGRect keyboardFrame;
    
    NSDictionary *userInfo = notification.userInfo;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardFrame];
    
    [self updateBottomConstraint];
    
    // adjust the scrollview now that the keyboard is hidden.
    [UIView animateWithDuration:animationDuration
                          delay:0.0
                        options:animationCurve
                     animations:^{
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

#pragma mark -
#pragma mark - UITextViewDelegate Methods

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (self.maxAllowableCharacters < NSUIntegerMax) {
        NSUInteger newLength = [textView.text length] + [text length] - range.length;
        return newLength <= self.maxAllowableCharacters;
    }
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView
{
    if (self.maxAllowableCharacters < NSUIntegerMax) {
        [self changeRemainingLabel:textView.text.length];
    }
}
@end
