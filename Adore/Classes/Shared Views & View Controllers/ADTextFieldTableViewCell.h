//
//  ADTextFieldTableViewCell.h
//  Adore
//
//  Created by Wang on 2015-04-30.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADUserDetailItem.h"

@class ADManagedUser;
@class JVFloatLabeledTextField;

static NSString * kAD_TEXT_FIELD_TABLE_VIEW_CELL_IDENTIFIER = @"TextFieldTableViewCell";



@interface ADTextFieldTableViewCell : UITableViewCell


@property (nonatomic, weak) IBOutlet JVFloatLabeledTextField *textField;

+ (instancetype)textFieldCellForTableView:(UITableView *)tableView
                                     type:(ADUserDetailItemType)type
                                 userData:(ADManagedUser *)managedUser
                                 delegate:(id<UITextFieldDelegate>)delegate;

+ (instancetype)textFieldCellForTableView:(UITableView *)tableView
                                     text:(NSString *)text
                          placeholderText:(NSString *)placeholder
                                      tag:(NSInteger)tag
                                 delegate:(id<UITextFieldDelegate>)delegate;

- (void)setPlaceholdText:(NSString *)placeholder;
- (void)setTextFieldText:(NSString *)text;

- (CGFloat)cellHeight;


@end
