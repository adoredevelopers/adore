//
//  ADViewController.m
//  Adore
//
//  Created by Wang on 2014-12-10.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADViewController.h"
#import "AppDelegate.h"
#import "ADChatManager.h"
#import "ADUserManager.h"
#import "ADUserCoreDataHelper.h"
#import "SCLAlertView.h"
#import "MBProgressHUD.h"
#import "ADUserManager.h"
#import "ADChatManager.h"
#import "BBBadgeBarButtonItem.h"
#ifdef DEV
#import "Adore_DEV-Swift.h"
#else
#import "Adore-Swift.h"
#endif



@interface ADViewController ()

@end



@implementation ADViewController

#pragma mark -
#pragma mark - Lazy Instantiators

- (ADUserManager *)userManager
{
    if (_userManager == nil) {
        _userManager = [ADUserManager sharedInstance];
    }
    return _userManager;
}

- (ADChatManager *)chatManager
{
    if (_chatManager == nil) {
        _chatManager = [ADChatManager sharedInstance];
    }
    return _chatManager;
}


#pragma mark -
#pragma mark - View lifecycles

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self registerNotificationReceiver];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateDrawerBadge];
}

- (void)dealloc
{
    [self deregisterNotificationReceiver];
}


#pragma mark -
#pragma mark - Helper methods

- (IBAction)showLeftDrawer:(id)sender
{
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate showLeftDrawer];
}

- (void)updateDrawerBadge
{
    if (self.navigationItem.leftBarButtonItem.tag == 399) {
        NSUInteger total = [self.userManager badgeNumberCount];
        
        _drawerButton = [[UIButton alloc] init];
        _drawerButton.tintColor = [UIColor whiteColor];
        [_drawerButton setImage:[[UIImage imageNamed:@"triple_bars"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        [_drawerButton sizeToFit];
        [_drawerButton addTarget:self action:@selector(showLeftDrawer:) forControlEvents:UIControlEventTouchUpInside];
        
        BBBadgeBarButtonItem *messageButton = [[BBBadgeBarButtonItem alloc] initWithCustomUIButton:_drawerButton];
        
        if (total > 0) {
            messageButton.badgeValue = [NSString stringWithFormat:@"%lu", (unsigned long)total];
        }
        else {
            messageButton.badgeValue = nil;
        }
        messageButton.badgeOriginX = 20;
        messageButton.tag = 399;
        self.navigationItem.leftBarButtonItem = messageButton;
    }
}

- (void)registerNotificationReceiver
{
    
}

- (void)deregisterNotificationReceiver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark -
#pragma mark - Keyboard Helper Methods

- (void)registerKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)deregisterKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)keyboardWillShow:(NSNotification *)notification
{
}

-(void)keyboardWillHide:(NSNotification *)notification
{
}

- (void)showLoadingIndicator:(NSString *)message
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = message;
}

- (void)dismissLoadingIndicator
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (SCLAlertView *)showToastMessage:(NSString *)message
{
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    alert.showAnimationType = SCLAlertViewShowAnimationFadeIn;
    alert.shouldDismissOnTapOutside = YES;
    
    [alert showInfo:self
              title:nil
           subTitle:message
   closeButtonTitle:nil
           duration:2.0f];
    
    return alert;
}

- (void)showErrorMessage:(NSString *)errorMessage
{
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    alert.showAnimationType = SCLAlertViewShowAnimationFadeIn;
    
    [alert showError:NSLocalizedString(@"Error", nil)
            subTitle:errorMessage
    closeButtonTitle:NSLocalizedString(@"OK", nil)
            duration:0.0f];
}

- (void)showSuccessWithTitle:(NSString *)title message:(NSString *)message
{
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    alert.showAnimationType = SCLAlertViewShowAnimationFadeIn;
    
    [alert showSuccess:title
              subTitle:message
      closeButtonTitle:NSLocalizedString(@"OK", nil)
              duration:0.0f];
}

- (SCLAlertView *)showMessageWithTitle:(NSString *)title message:(NSString *)message
{
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    alert.showAnimationType = SCLAlertViewShowAnimationFadeIn;
    
    [alert showInfo:title
           subTitle:message
   closeButtonTitle:NSLocalizedString(@"OK", nil)
           duration:0.0f];
    
    return alert;
}

@end
