//
//  ADViewController.h
//  Adore
//
//  Created by Wang on 2014-12-10.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCLAlertView.h"
//#import "MPCoachMarks.h"


@class ADUserManager;
@class ADChatManager;

@interface ADViewController : UIViewController <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) ADChatManager *chatManager;
@property (nonatomic, strong) ADUserManager *userManager;

@property (nonatomic, strong) UIButton *drawerButton;

@property (nonatomic, strong) NSFetchedResultsController *pendingUsersController;
@property (nonatomic, strong) NSFetchedResultsController *connectedUsersController;
@property (nonatomic, strong) NSFetchedResultsController *unreadConnectionsController;
@property (nonatomic, strong) NSFetchedResultsController *couponsController;

@property (nonatomic, strong) NSFetchedResultsController *unreadMessagesController;
@property (nonatomic, strong) NSFetchedResultsController *recentChatsController;


- (void)registerNotificationReceiver;
- (void)deregisterNotificationReceiver;

- (void)registerKeyboardNotification;
- (void)deregisterKeyboardNotification;

- (IBAction)showLeftDrawer:(id)sender;
- (void)updateDrawerBadge;


- (void)showLoadingIndicator:(NSString *)message;
- (void)dismissLoadingIndicator;

- (SCLAlertView *)showToastMessage:(NSString *)message;
- (void)showErrorMessage:(NSString *)errorMessage;
- (void)showSuccessWithTitle:(NSString *)title message:(NSString *)message;
- (SCLAlertView *)showMessageWithTitle:(NSString *)title message:(NSString *)message;

- (void)refreshContent;


@end
