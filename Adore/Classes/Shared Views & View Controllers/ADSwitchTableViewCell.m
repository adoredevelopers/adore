//
//  ADSwitchTableViewCell.m
//  Adore
//
//  Created by Wang on 2015-01-20.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADSwitchTableViewCell.h"



@implementation ADSwitchTableViewCell


- (void)updateWithText:(NSString *)text switchOn:(BOOL)switchOn
{
    self.label.text = text;
    self.switchControl.on = switchOn;
}


@end
