//
//  ADButtonTableViewCell.m
//  Adore
//
//  Created by Wang on 2014-12-14.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADButtonTableViewCell.h"

@implementation ADButtonTableViewCell

+ (CGFloat)rowHeight
{
    return 44;
}

- (void)updateWithImage:(UIImage *)image text:(NSString *)text
{
    self.iconView.image = image;
    self.label.text = text;
    self.label.textColor = self.tintColor;
    
    if (image) {
        self.imageViewHeightConstraint.constant = 30;
        self.imageViewWidthConstraint.constant = 30;
        self.imageViewPaddingConstraint.constant = 8;
    }
    else {
        self.imageViewHeightConstraint.constant = 0;
        self.imageViewWidthConstraint.constant = 0;
        self.imageViewPaddingConstraint.constant = 0;
    }
}


@end
