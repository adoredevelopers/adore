//
//  ADProfileImageCollectionViewCell.m
//  Adore
//
//  Created by Wang on 2014-12-15.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADProfileImageCollectionViewCell.h"
#import "SDWebImage/UIImageView+WebCache.h"



@implementation ADProfileImageCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.userDetailsPhotoImageView.layer.cornerRadius = 4;
    self.userDetailsPhotoImageView.clipsToBounds = YES;
}

- (void)updateViewWithImageUrl:(NSString *)imageUrl AtIndex:(NSUInteger)index editing:(BOOL)editing
{
    [self.userDetailsPhotoImageView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"profile_image_placeholder_72x72"] options:SDWebImageRetryFailed | SDWebImageLowPriority];
    self.userDetailsPhotoImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    self.userDetailsPhotoImageView.tag = index;
    self.deleteButton.hidden = (editing == NO);
    self.deleteButton.tag = index;
}

- (void)updateViewWithImage:(UIImage *)image editing:(BOOL)editing
{
   	self.userDetailsPhotoImageView.image = image;
    self.userDetailsPhotoImageView.contentMode = UIViewContentModeCenter;
    self.deleteButton.hidden = (editing == NO);
}

@end
