//
//  ADProfileImageCollectionViewCell.h
//  Adore
//
//  Created by Wang on 2014-12-15.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADProfileImageCollectionViewCell : UICollectionViewCell


@property (nonatomic, weak) IBOutlet UIImageView *userDetailsPhotoImageView;
@property (nonatomic, weak) IBOutlet UIButton *deleteButton;

- (void)updateViewWithImageUrl:(NSString *)imageUrl AtIndex:(NSUInteger)index editing:(BOOL)editing;
- (void)updateViewWithImage:(UIImage *)image editing:(BOOL)editing;


@end
