//
//  ADProfileImageCollectionTableViewCell.m
//  Adore
//
//  Created by Wang on 2014-12-15.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADProfileImageCollectionTableViewCell.h"

#import "ADManagedUser+Extensions.h"
#import "ADProfileImageCollectionViewCell.h"
#import "UICollectionView+Draggable.h"
#import "NSObject+Notification.h"



@interface ADProfileImageCollectionTableViewCell () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource_Draggable>

@end



@implementation ADProfileImageCollectionTableViewCell

#pragma mark -
#pragma mark - Initilizer

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        _isEditing = NO;
    }
    
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _collectionView.dataSource = self;
    
    UINib *cellNib = [UINib nibWithNibName:@"ADProfileImageCollectionViewCell" bundle:nil];
    [_collectionView registerNib:cellNib forCellWithReuseIdentifier:@"ADProfileImageCollectionViewCell"];
}


#pragma mark -
#pragma mark - Helper Methods

- (void)deleteButtonClicked:(UIButton *)sender
{
    // You need to have at least one image
    if (self.userObject.profileImageArray.count <= 1) {
        return;
    }
    [self postNotificationOnMainThread:kAD_NOTIFICATION_USER_PROFILE_IMAGE_UPDATED];
    [self.userObject removeUserPhotoAtIndex:sender.tag];
    [self.collectionView reloadData];
}

- (NSInteger)numberOfItemsInPhotoCell
{
    NSInteger count = self.userObject.profileImageArray.count;
    
    // show add button at the end if it is in editing mode
    if (self.isEditing) {
        count++;
    }
    return MIN(count, 8);
}

- (CGFloat)cellHeight
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat cellWidth = (int)((screenWidth - 25) / 4);
    
    if ([self numberOfItemsInPhotoCell] <= 4) {
        return cellWidth + 10;
    }
    return cellWidth * 2 + 15;
}


#pragma mark -
#pragma mark - UICollectionViewDataSource Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self numberOfItemsInPhotoCell];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ADProfileImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ADProfileImageCollectionViewCell" forIndexPath:indexPath];
    
    if (indexPath.row < self.userObject.profileImageArray.count) {
        [cell updateViewWithImageUrl:[self.userObject userPhotoThumbnailUrlAtIndex:indexPath.row] AtIndex:indexPath.row editing:self.isEditing];
        if (self.isEditing) {
            [cell.deleteButton addTarget:self action:@selector(deleteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        }
        else {
            [cell.deleteButton removeTarget:self action:@selector(deleteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    // + button
    else if (indexPath.row == self.userObject.profileImageArray.count) {
        [cell updateViewWithImage:[[UIImage imageNamed:@"small_camera"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] editing:NO];
        cell.userDetailsPhotoImageView.tintColor = [UIColor darkGrayColor];
    }
    return cell;
}

#pragma mark -
#pragma mark - UICollectionViewDraggable Methods

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL canMove = YES;
    if (indexPath.row >= self.userObject.profileImageArray.count) {
        canMove = NO;
    }
    return canMove;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    BOOL canMove = YES;
    if (toIndexPath.row >= self.userObject.profileImageArray.count) {
        canMove = NO;
    }
    
    return canMove;
}

- (void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    if (fromIndexPath.row != toIndexPath.row) {
        [self.delegate setUserProfileImagesOrderingUpdated:YES];
    }
    [self.userObject moveUserPhotoFromIndex:fromIndexPath.row toIndex:toIndexPath.row];
}

- (CGAffineTransform)collectionView:(UICollectionView *)collectionView transformForDraggingItemAtIndexPath:(NSIndexPath *)indexPath duration:(NSTimeInterval *)duration
{
    CGAffineTransform scaleTransform = CGAffineTransformMakeScale(1.25, 1.25);
    
    return scaleTransform;
}

@end
