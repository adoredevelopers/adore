//
//  ADProfileImageScrollableCollectionTableViewCell.m
//  Adore
//
//  Created by Wang on 2015-04-30.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADProfileImageScrollableCollectionTableViewCell.h"
#import "ADProfileImageCollectionViewCell.h"
#import "ADManagedUser+Extensions.h"
#import "ADManagedPartner+Extensions.h"



@interface ADProfileImageScrollableCollectionTableViewCell() <UICollectionViewDataSource, UICollectionViewDelegate>


@end



@implementation ADProfileImageScrollableCollectionTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.collectionView.dataSource = self;

    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat cellWidth = (int)((screenWidth - 25) / 4);
    CGFloat cellHeight = cellWidth;
    layout.itemSize = CGSizeMake(cellWidth, cellHeight);
    //layout.sectionInset = UIEdgeInsetsMake(0, 2.5, 0, 2.5);
    self.collectionView.collectionViewLayout = layout;
    
    UINib *cellNib = [UINib nibWithNibName:@"ADProfileImageCollectionViewCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"ADProfileImageCollectionViewCell"];
}

- (void)setUserObject:(ADManagedUser *)userObject
{
    _userObject = userObject;
    [self.collectionView reloadData];
}

- (void)setPartner:(ADManagedPartner *)partner
{
    _partner = partner;
    [self.collectionView reloadData];
}


#pragma mark -
#pragma mark - Helper Methods

- (NSInteger)numberOfItemsInPhotoCell
{
    if (self.userObject) {
        NSInteger count = self.userObject.profileImageArray.count;
        count--;
        return MIN(count, 8);
    }
    else if (self.partner) {
        return self.partner.partnerImagesArray.count;
    }
    return 0;
}

- (CGFloat)cellHeight
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat cellWidth = (int)((screenWidth - 25) / 4);
    
    return cellWidth + 10;
}


#pragma mark -
#pragma mark - UICollectionViewDataSource Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self numberOfItemsInPhotoCell];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ADProfileImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ADProfileImageCollectionViewCell" forIndexPath:indexPath];
    
    if (self.userObject) {
        if (indexPath.row < self.userObject.profileImageArray.count) {
            [cell updateViewWithImageUrl:[self.userObject userPhotoThumbnailUrlAtIndex:indexPath.row+1] AtIndex:indexPath.row editing:NO];
        }
    }
    else if (self.partner) {
        if (indexPath.row < self.partner.partnerImagesArray.count) {
            [cell updateViewWithImageUrl:[self.partner partnerImageAtIndex:indexPath.row] AtIndex:indexPath.row editing:NO];
        }
    }
    return cell;
}


@end
