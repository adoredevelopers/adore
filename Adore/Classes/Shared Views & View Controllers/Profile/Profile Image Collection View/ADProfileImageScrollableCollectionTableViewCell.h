//
//  ADProfileImageScrollableCollectionTableViewCell.h
//  Adore
//
//  Created by Wang on 2015-04-30.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ADManagedUser;
@class ADManagedPartner;

static NSString * kAD_USER_DETAIL_PROFILE_IMAGE_SCROLLABLE_COLLECTION_VIEW_CELL_IDENTIFIER = @"UserDetailScrollablePhotoCell";



@interface ADProfileImageScrollableCollectionTableViewCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) ADManagedUser *userObject;
@property (nonatomic, strong) ADManagedPartner *partner;


- (CGFloat)cellHeight;


@end
