//
//  ADProfileImageCollectionTableViewCell.h
//  Adore
//
//  Created by Wang on 2014-12-15.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ADManagedUser;

static NSString * kAD_USER_DETAIL_PROFILE_IMAGE_COLLECTION_VIEW_CELL_IDENTIFIER = @"UserDetailPhotoCell";



@protocol ADProfileImageCollectionTableViewCellDelegate <NSObject>

@required
- (void)setUserProfileImagesOrderingUpdated:(BOOL)isUpdated;

@end



@interface ADProfileImageCollectionTableViewCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) ADManagedUser *userObject;

@property (nonatomic, assign) BOOL isEditing;

@property (nonatomic, assign) id delegate;

- (CGFloat)cellHeight;

@end
