//
//  ADProfileCoverTableViewCell.h
//  Adore
//
//  Created by Wang on 2015-04-29.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ADManagedUser;

static NSString * kAD_USER_DETAIL_PROFILE_COVER_IDENTIFIER = @"UserDetailProfileCover";



@interface ADProfileCoverTableViewCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UIImageView *profileImageView;
@property (nonatomic, weak) IBOutlet UIButton *button;

@property (nonatomic, strong) ADManagedUser *userObject;


- (void)setupButton;
- (CGFloat)cellHeight;


@end
