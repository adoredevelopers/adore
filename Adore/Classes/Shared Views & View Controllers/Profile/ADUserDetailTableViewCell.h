//
//  ADUserDetailTableViewCell.h
//  Adore
//
//  Created by Kevin Wang on 2014-11-16.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADUserDetailItem.h"

@class ADManagedUser;

static NSString * kAD_USER_DETAIL_TABLE_VIEW_CELL_IDENTIFIER = @"UserDetailDetailCell";



@interface ADUserDetailTableViewCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *detailLabel;


+ (instancetype)userDetailCellForTableView:(UITableView *)tableView type:(ADUserDetailItemType)type userData:(ADManagedUser *)managedUser;

- (CGFloat)cellHeight;


@end
