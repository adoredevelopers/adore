//
//  ADUserDetailSectionHeaderCell.m
//  Adore
//
//  Created by Wang on 2015-04-08.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADUserDetailSectionHeaderCell.h"



@implementation ADUserDetailSectionHeaderCell


+ (instancetype)userDetailSectionHeaderCellForTableView:(UITableView *)tableView type:(ADUserDetailItemType)type
{
    ADUserDetailSectionHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_USER_DETAIL_SECTION_HEADER_IDENTIFIER];

    if (type == ADUserDetailItemTypeHeaderBasicInfo) {
        cell.titleLabel.text = NSLocalizedString(@"Basic Info", nil);
    }
    else if (type == ADUserDetailItemTypeHeaderDetails) {
        cell.titleLabel.text = NSLocalizedString(@"Details", nil);
    }
    else if (type == ADUserDetailItemTypeHeaderWorkEdu) {
        cell.titleLabel.text = NSLocalizedString(@"Work & Education", nil);
    }
    return cell;
}

- (CGFloat)cellHeight
{
    return 44;
}

@end
