//
//  ADUserDetailSectionHeaderCell.h
//  Adore
//
//  Created by Wang on 2015-04-08.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADUserDetailItem.h"

static NSString * kAD_USER_DETAIL_SECTION_HEADER_IDENTIFIER = @"UserDetailSectionHeader";



@interface ADUserDetailSectionHeaderCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UILabel *titleLabel;


+ (instancetype)userDetailSectionHeaderCellForTableView:(UITableView *)tableView type:(ADUserDetailItemType)type;

- (CGFloat)cellHeight;


@end
