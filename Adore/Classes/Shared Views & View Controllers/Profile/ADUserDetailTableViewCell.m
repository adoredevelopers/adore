//
//  ADUserDetailTableViewCell.m
//  Adore
//
//  Created by Kevin Wang on 2014-11-16.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADUserDetailTableViewCell.h"
#import "ADManagedUser+Extensions.h"
#import "ADDetailListCoreDataHelper.h"
#import  "ADUserManager.h"



@implementation ADUserDetailTableViewCell


+ (instancetype)userDetailCellForTableView:(UITableView *)tableView type:(ADUserDetailItemType)type userData:(ADManagedUser *)managedUser
{
    ADUserDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_USER_DETAIL_TABLE_VIEW_CELL_IDENTIFIER];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;

    ADDetailListCoreDataHelper *detailListCoreDataHelper = [ADUserManager sharedInstance].detailListCoreDataHelper;
    switch (type) {
        case ADUserDetailItemTypeEmail: {
            cell.titleLabel.text = NSLocalizedString(@"Email", nil);
            cell.detailLabel.text = managedUser.email;
            break;
        }
        case ADUserDetailItemTypeName: {
            cell.titleLabel.text = NSLocalizedString(@"Name", nil);
            cell.detailLabel.text = managedUser.fullname;
            break;
        }
        case ADUserDetailItemTypeNickname: {
            cell.titleLabel.text = NSLocalizedString(@"Nickname", nil);
            cell.detailLabel.text = managedUser.nickname;
            break;
        }
        case ADUserDetailItemTypeMotto: {
            cell.titleLabel.text = NSLocalizedString(@"Motto", nil);
            cell.detailLabel.text = managedUser.motto;
            break;
        }
        case ADUserDetailItemTypeGender: {
            cell.titleLabel.text = NSLocalizedString(@"Gender", nil);
            cell.detailLabel.text = managedUser.gender;
            break;
        }
        case ADUserDetailItemTypeZodiac: {
            cell.titleLabel.text = NSLocalizedString(@"Zodiac", nil);
            NSString *zodiacNonLocalizedString = [detailListCoreDataHelper stringNonLocalizedValueOfId:managedUser.zodiac type:ADUserDetailItemTypeZodiac];
            NSString *zodiacString = [detailListCoreDataHelper stringValueOfId:managedUser.zodiac
                                                                          type:ADUserDetailItemTypeZodiac];
            NSTextAttachment *zodiacIconAttachment = [[NSTextAttachment alloc] init];
            zodiacIconAttachment.bounds = CGRectMake(0, [UIFont systemFontOfSize:17.0].descender, 22.0, 22.0);
            zodiacIconAttachment.image = [UIImage imageNamed:[zodiacNonLocalizedString lowercaseString]];
            NSAttributedString *zodiacAttachmentString = [NSAttributedString attributedStringWithAttachment:zodiacIconAttachment];
            NSMutableAttributedString *zodiacAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@", zodiacString, @" "]];
            [zodiacAttrString appendAttributedString:zodiacAttachmentString];
            cell.detailLabel.attributedText = zodiacAttrString;
            break;
        }
        case ADUserDetailItemTypeC_Zodiac: {
            cell.titleLabel.text = NSLocalizedString(@"Chinese Zodiac", nil);
            cell.detailLabel.text = [detailListCoreDataHelper stringValueOfId:managedUser.c_zodiac
                                                                         type:ADUserDetailItemTypeC_Zodiac];
            break;
        }
        case ADUserDetailItemTypeBirthday: {
            cell.titleLabel.text = NSLocalizedString(@"Birthday", nil);
            cell.detailLabel.text = [NSDateFormatter localizedStringFromDate:managedUser.birthday dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
            break;
        }
        case ADUserDetailItemTypeHeight: {
            cell.titleLabel.text = NSLocalizedString(@"Height", nil);
            cell.detailLabel.text = [detailListCoreDataHelper stringValueOfId:managedUser.height
                                                                         type:ADUserDetailItemTypeHeight];
            break;
        }
        case ADUserDetailItemTypeWeight: {
            cell.titleLabel.text = NSLocalizedString(@"Weight", nil);
            cell.detailLabel.text = [detailListCoreDataHelper stringValueOfId:managedUser.weight
                                                                         type:ADUserDetailItemTypeWeight];
            break;
        }
        case ADUserDetailItemTypeMartialStatus: {
            cell.titleLabel.text = NSLocalizedString(@"Martial Status", nil);
            cell.detailLabel.text = [detailListCoreDataHelper stringValueOfId:managedUser.martial_status
                                                                         type:ADUserDetailItemTypeMartialStatus];
            break;
        }
        case ADUserDetailItemTypeBodyShape: {
            cell.titleLabel.text = NSLocalizedString(@"Body Shape", nil);
            cell.detailLabel.text = [detailListCoreDataHelper stringValueOfId:managedUser.body_shape
                                                                         type:ADUserDetailItemTypeBodyShape];
            break;
        }
        case ADUserDetailItemTypePersonality: {
            cell.titleLabel.text = NSLocalizedString(@"Personality", nil);
            cell.detailLabel.text = [detailListCoreDataHelper stringValueOfId:managedUser.personality
                                                                         type:ADUserDetailItemTypePersonality];
            break;
        }
        case ADUserDetailItemTypeInterest: {
            cell.titleLabel.text = NSLocalizedString(@"Interests", nil);
            cell.detailLabel.text = [detailListCoreDataHelper stringValueOfArray:managedUser.interestArray
                                                                            type:ADUserDetailItemTypeInterest];
            break;
        }
        case ADUserDetailItemTypeLanguage: {
            cell.titleLabel.text = NSLocalizedString(@"Languages", nil);
            cell.detailLabel.text = [detailListCoreDataHelper stringValueOfArray:managedUser.languageArray
                                                                            type:ADUserDetailItemTypeLanguage];
            break;
        }
        case ADUserDetailItemTypeReligion: {
            cell.titleLabel.text = NSLocalizedString(@"Religion", nil);
            cell.detailLabel.text = [detailListCoreDataHelper stringValueOfId:managedUser.religion
                                                                         type:ADUserDetailItemTypeReligion];
            break;
        }
        case ADUserDetailItemTypeDrinking: {
            cell.titleLabel.text = NSLocalizedString(@"Drinking", nil);
            cell.detailLabel.text = [detailListCoreDataHelper stringValueOfId:managedUser.drinking
                                                                         type:ADUserDetailItemTypeDrinking];
            break;
        }
        case ADUserDetailItemTypeSmoking: {
            cell.titleLabel.text = NSLocalizedString(@"Smoking", nil);
            cell.detailLabel.text = [detailListCoreDataHelper stringValueOfId:managedUser.smoking
                                                                         type:ADUserDetailItemTypeSmoking];
            break;
        }
        case ADUserDetailItemTypeEducation: {
            cell.titleLabel.text = NSLocalizedString(@"Education Level", nil);
            cell.detailLabel.text = [detailListCoreDataHelper stringValueOfId:managedUser.education_level
                                                                         type:ADUserDetailItemTypeEducation];
            break;
        }
        case ADUserDetailItemTypeUndergradEdu: {
            cell.titleLabel.text = NSLocalizedString(@"Undergrad Education", nil);
            cell.detailLabel.text = managedUser.undergrad_edu_institution;
            break;
        }
        case ADUserDetailItemTypePostgradEdu: {
            cell.titleLabel.text = NSLocalizedString(@"Postgrad Education", nil);
            cell.detailLabel.text = managedUser.postgrad_edu_institution;
            break;
        }
        case ADUserDetailItemTypeEmployer: {
            cell.titleLabel.text = NSLocalizedString(@"Employer (Company)", nil);
            cell.detailLabel.text = managedUser.employer;
            break;
        }
        case ADUserDetailItemTypeOccupation: {
            cell.titleLabel.text = NSLocalizedString(@"Occupation", nil);
            cell.detailLabel.text = [detailListCoreDataHelper stringValueOfId:managedUser.occupation
                                                                         type:ADUserDetailItemTypeOccupation];
            break;
        }
        case ADUserDetailItemTypeIncome: {
            cell.titleLabel.text = NSLocalizedString(@"Income", nil);
            cell.detailLabel.text = [detailListCoreDataHelper stringValueOfId:managedUser.income
                                                                         type:ADUserDetailItemTypeIncome];
            break;
        }
        case ADUserDetailItemTypeRace: {
            cell.titleLabel.text = NSLocalizedString(@"Race", nil);
            cell.detailLabel.text = [detailListCoreDataHelper stringValueOfId:managedUser.race
                                                                         type:ADUserDetailItemTypeRace];
            break;
        }
        case ADUserDetailItemTypeResidentialStatus: {
            cell.titleLabel.text = NSLocalizedString(@"Residential Status", nil);
            cell.detailLabel.text = [detailListCoreDataHelper stringValueOfId:managedUser.residential_status
                                                                         type:ADUserDetailItemTypeResidentialStatus];
            break;
        }
        case ADUserDetailItemTypeBirthPlace: {
            cell.titleLabel.text = NSLocalizedString(@"Birth Place", nil);
            cell.detailLabel.text = [managedUser birthPlaceString];
            break;
        }
        case ADUserDetailItemTypeCurrentResidence: {
            cell.titleLabel.text = NSLocalizedString(@"Current Residence", nil);
            cell.detailLabel.text = [managedUser currentResidenceString];
            break;
        }
        case ADUserDetailItemTypeHeaderBasicInfo:
        case ADUserDetailItemTypeHeaderDetails:
        case ADUserDetailItemTypeHeaderWorkEdu:
        case ADUserDetailItemTypeProfileImage: {
            cell.titleLabel.text = nil;
            cell.detailLabel.text = nil;
            break;
        }
    }
    return cell;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    self.detailLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.detailLabel.frame);
}

- (CGFloat)cellHeight
{
    [self layoutIfNeeded];
    CGFloat height = [self.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    return MAX(height + 1, 44);
}


@end
