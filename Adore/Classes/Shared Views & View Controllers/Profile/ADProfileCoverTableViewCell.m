//
//  ADProfileCoverTableViewCell.m
//  Adore
//
//  Created by Wang on 2015-04-29.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADProfileCoverTableViewCell.h"
#import "ADManagedUser+Extensions.h"
#import "SDWebImage/UIImageView+WebCache.h"


@implementation ADProfileCoverTableViewCell


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.button.layer.cornerRadius = 30;
}

- (void)setUserObject:(ADManagedUser *)userObject
{
    _userObject = userObject;
    [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:userObject.profilePictureUrl] placeholderImage:[UIImage imageNamed:@"profile_image_placeholder_150x150"] options:SDWebImageRetryFailed]; // todo: create 600x600 profile image placeholder file
}

- (void)setupButton
{
    self.button.hidden = NO;
    
    ADUserRelationshipType relationship = [self.userObject.relationship integerValue];
    switch (relationship) {
        case ADUserRelationshipTypeInvalid:
        {
            self.button.tintColor = self.window.tintColor;
            [self.button setImage:[[UIImage imageNamed:@"heart"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            break;
        }
        case ADUserRelationshipTypeRequested:
        case ADUserRelationshipTypeRejected:
        {
            self.button.tintColor = Constants.maleTintColor;
            [self.button setImage:[[UIImage imageNamed:@"heart_hour_glass"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            break;
        }
        case ADUserRelationshipTypeConnected:
        case ADUserRelationshipTypeBlocked:
        {
            self.button.tintColor = self.window.tintColor;
            [self.button setImage:[[UIImage imageNamed:@"chat"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            break;
        }
        case ADUserRelationshipTypeSelf:
        {
            [self.button setImage:nil forState:UIControlStateNormal];
            self.button.hidden = YES;
            break;
        }
    }
}

- (CGFloat)cellHeight
{
    return 300;
}

@end
