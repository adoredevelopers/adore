//
//  ADReportViewController.m
//  Adore
//
//  Created by Wang on 2015-07-07.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADReportViewController.h"
#import "ADReportObject.h"
#import "ADReportRequest.h"
#import "ADDetailListCoreDataHelper.h"
#import "ADReportRequest.h"
#import "ADTextViewInputViewController.h"



@interface ADReportViewController () <UITableViewDataSource, UITableViewDelegate, ADTextViewInputDelegate>


@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) ADReportObject *reportObject;
@property (nonatomic, strong) NSDictionary *reasonsDict;
@property (nonatomic, strong) NSArray *reasonsKeys;
@property (nonatomic, strong) NSString *languageCode;

@property (nonatomic, strong) ADReportRequest *request;


@end


@implementation ADReportViewController


#pragma mark -
#pragma mark - Initializer

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        ADDetailListCoreDataHelper *helper = [[ADDetailListCoreDataHelper alloc] init];
        _languageCode = [helper currentLanguageCode];
        _reportObject = [[ADReportObject alloc] init];
        _reasonsDict = [helper loadListFromFile:@"Report_reason"];
        _reasonsKeys = [[_reasonsDict allKeys] sortedArrayUsingComparator:^NSComparisonResult(NSString *obj1, NSString *obj2) {
            NSInteger int1 = [obj1 integerValue];
            NSInteger int2 = [obj2 integerValue];
            if (int1 < int2) {
                return NSOrderedAscending;
            }
            return NSOrderedDescending;
        }];
    }
    return self;
}


#pragma mark -
#pragma mark - View lifecycles

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
    UIBarButtonItem *send = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Send", nil) style:UIBarButtonItemStyleDone target:self action:@selector(send)];
    self.navigationItem.leftBarButtonItem = cancel;
    self.navigationItem.rightBarButtonItem = send;
    
    [self.navigationController.navigationBar setTranslucent:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
}


#pragma mark -
#pragma mark - Helper methods

- (void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)send
{
    if (self.reportObject.reason == nil) {
        [self showMessageWithTitle:nil message:NSLocalizedString(@"Please pick a reason.", nil)];
        return;
    }
    [self showLoadingIndicator:nil];
    
    self.request = [[ADReportRequest alloc] initWithReport:self.reportObject CompletionHandler:^(BOOL success, NSError *error) {
        if (success) {
            [self dismissLoadingIndicator];
            [self cancel];
        }
        else {
            [self showErrorMessage:error.localizedDescription];
        }
    }];
}

- (void)showCommentsEditor
{
    ADTextViewInputViewController *textView = [[ADTextViewInputViewController alloc] initWithTitle:NSLocalizedString(@"Comments", nil) delegate:self startingString:self.reportObject.comments showEditButton:NO];
    [self.navigationController pushViewController:textView animated:YES];
}

- (void)setReportedUserId:(NSString *)userId
{
    self.reportObject.user_id = userId;
}

- (void)setReportedImageUrl:(NSString *)url
{
    self.reportObject.image_url = url;
}


#pragma mark -
#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return self.reasonsKeys.count;
    }
    else if (section == 1) {
        return 1;
    }
    
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return NSLocalizedString(@"Reason", nil);
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell"];
    }

    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.textLabel.text = nil;
    cell.detailTextLabel.text = nil;
    
    if (indexPath.section == 0) {
        NSString *key = self.reasonsKeys[indexPath.row];
        NSDictionary *item = self.reasonsDict[key];

        cell.textLabel.text = item[self.languageCode];
        if ([key isEqualToString:self.reportObject.reason]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    }
    else if (indexPath.section == 1) {
        cell.textLabel.text = NSLocalizedString(@"Comments", nil);
        cell.detailTextLabel.text = self.reportObject.comments;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    return cell;
}


#pragma mark -
#pragma mark - UITableViewDataDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        self.reportObject.reason = self.reasonsKeys[indexPath.row];
        [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else if (indexPath.section == 1) {
        [self showCommentsEditor];
    }
}


#pragma mark -
#pragma mark - ADTextViewInputDelegate methods

- (void)textViewInputBackButtonTappedWithString:(NSString *)string
{
    self.reportObject.comments = string;
}


@end
