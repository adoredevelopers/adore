//
//  ADSwitchTableViewCell.h
//  Adore
//
//  Created by Wang on 2015-01-20.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ADSwitchTableViewCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UILabel *label;
@property (nonatomic, weak) IBOutlet UISwitch *switchControl;


- (void)updateWithText:(NSString *)text switchOn:(BOOL)switchOn;

@end
