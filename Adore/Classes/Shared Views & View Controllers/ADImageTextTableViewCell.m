//
//  ADImageTextTableViewCell.m
//  Adore
//
//  Created by Wang on 2014-12-10.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADImageTextTableViewCell.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ADManagedUser+Extensions.h"
#import "CustomBadge.h"
#import "AppDelegate.h"


@implementation ADImageTextTableViewCell

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.accessoryType = UITableViewCellAccessoryNone;
}

- (void)showBadgeViewWithString:(NSString *)string
{
    if (self.badgeView == nil) {
        self.badgeView = [CustomBadge customBadgeWithString:string withScale:0.7];
        CGPoint point = CGPointMake(CGRectGetMaxX(self.iconView.frame), CGRectGetMinY(self.iconView.frame));
        self.badgeView.center = point;
        [self.contentView addSubview:self.badgeView];
    }
    else {
        self.badgeView.badgeText = string;
    }
}

- (void)hideBadge
{
    [self.badgeView removeFromSuperview];
    self.badgeView = nil;
}

- (void)updateViewWithUserObject:(ADManagedUser *)user showBadge:(BOOL)showBadge
{
    self.userObject = user;
    self.label.text = user.nickname;
    self.detailLabel.text = nil;
    [self.iconView sd_setImageWithURL:[NSURL URLWithString:user.profilePictureThumbnailUrl] placeholderImage:[UIImage imageNamed:@"profile_image_placeholder_72x72"] options:SDWebImageRetryFailed | SDWebImageLowPriority];
    
    CustomBadge *badge = nil;
    if (user.isRead == NO) {
        badge = [CustomBadge customBadgeWithString:NSLocalizedString(@"New", nil)];
    }
    if (user.relationType == ADUserRelationshipTypeBlocked) {
        badge = [CustomBadge customBadgeWithString:NSLocalizedString(@"Blocked", nil)];
    }
    self.accessoryView = badge;
    
//    if (showBadge) {
//        [self showBadgeViewWithString:nil];
//    }
//    else {
//        [self hideBadge];
//    }
}

- (void)updateWithImage:(UIImage *)image text:(NSString *)text
{
    [self updateWithImage:image text:text detailText:nil];
}

- (void)updateWithImage:(UIImage *)image text:(NSString *)text detailText:(NSString *)detailText
{
    self.label.enabledTextCheckingTypes = NSTextCheckingTypePhoneNumber|NSTextCheckingTypeAddress|NSTextCheckingTypeLink;
    [self.label setText:text];
    self.detailLabel.text = detailText;
    self.iconView.image = image;
}

- (void)updateWithImage:(UIImage *)image text:(NSString *)text badge:(NSInteger)badge
{
    [self updateWithImage:image text:text detailText:nil badge:badge];
}

- (void)updateWithImage:(UIImage *)image text:(NSString *)text detailText:(NSString *)detailText badge:(NSInteger)badge
{
    [self updateWithImage:image text:text detailText:detailText];
    [self showBadgeViewWithString:[@(badge) stringValue]];
}

#pragma mark
#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    NSLog(@"linkClick");
    [[UIApplication sharedApplication] openURL:url];
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithAddress:(NSDictionary *)addressComponents
{
    NSLog(@"addressClick");
    NSLog(@"detailAdd:%@,lontitude:%f,latitude:%f",addressComponents[@"detailAdd"],
          [addressComponents[@"lontitude"] floatValue],
          [addressComponents[@"latitude"] floatValue]);
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithPhoneNumber:(NSString *)phoneNumber
{
    NSLog(@"phoneClick");
    NSString *num = [NSString stringWithFormat:@"tel:%@",phoneNumber];
    //    NSString *num = [NSString stringWithFormat:@"telprompt://%@",number];
    //而这个方法则打电话前先弹框  是否打电话 然后打完电话之后回到程序中 网上说这个方法可能不合法 无法通过审核
    
    UIApplication *application = [UIApplication sharedApplication];
    AppDelegate *delegate = (AppDelegate *)application.delegate;
    UIWindow *window = delegate.window;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:[NSString stringWithFormat:@"是否拨打 %@",phoneNumber]
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"取消"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * _Nonnull action) {}];
    
    UIAlertAction *actionDone = [UIAlertAction actionWithTitle:@"确定"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:num]]; //拨号
                                                       }];
    [alert addAction:actionCancel];
    [alert addAction:actionDone];
    
    [window.rootViewController presentViewController:alert animated:YES completion:nil];
}

@end
