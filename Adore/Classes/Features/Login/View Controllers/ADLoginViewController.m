//
//  ADLoginViewController.m
//  Adore
//
//  Created by Wang on 2014-04-18.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADLoginViewController.h"
#import "ADLoginRequest.h"
#import "ADLoginObject.h"
#import "ADUserObject.h"
#import "ADUserManager.h"
#import "ADChatManager.h"
#import "ADUserDefaultHelper.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ADUserManager.h"
#import "ADUserCoreDataHelper.h"
#import "ADForgotPasswordRequest.h"
#import "AppDelegate.h"
#import "SCLAlertView.h"



@interface ADLoginViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UIImageView *userProfileImageView;
@property (nonatomic, weak) IBOutlet UIButton *loginButton;
@property (nonatomic, weak) IBOutlet UIButton *registerButton;
@property (nonatomic, weak) IBOutlet UIButton *forgotPasswordButton;
@property (nonatomic, weak) IBOutlet UIButton *backgroundButton;

@property (nonatomic, weak) IBOutlet UITextField *usernameTextField;
@property (nonatomic, weak) IBOutlet UITextField *passwordTextField;

@property (nonatomic, strong) ADLoginRequest *loginRequest;
@property (nonatomic, strong) ADForgotPasswordRequest *forgotPasswordRequest;

@property (nonatomic, strong) NSString *defaultUserName;

@end



@implementation ADLoginViewController

#pragma mark
#pragma mark - View lifecycles

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    self.defaultUserName = [ADUserDefaultHelper username];
    self.usernameTextField.text = self.defaultUserName;
    
    [self.loginButton setTitle:NSLocalizedString(@"Login", nil) forState:UIControlStateNormal];
    [self.loginButton.layer setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.9].CGColor];
    self.loginButton.layer.cornerRadius = 6;
    self.loginButton.clipsToBounds = YES;
    [self.registerButton setTitle:NSLocalizedString(@"Sign Up", nil) forState:UIControlStateNormal];
    [self.registerButton.layer setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.9].CGColor];
    self.registerButton.layer.cornerRadius = 6;
    self.registerButton.clipsToBounds = YES;
    [self.forgotPasswordButton setTitle:NSLocalizedString(@"Forgot Password?", nil) forState:UIControlStateNormal];
    [self.forgotPasswordButton.layer setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.9].CGColor];
    self.forgotPasswordButton.layer.cornerRadius = 6;
    self.forgotPasswordButton.clipsToBounds = YES;
    self.usernameTextField.placeholder = NSLocalizedString(@"Email address", nil);
    self.passwordTextField.placeholder = NSLocalizedString(@"Password", nil);
    self.userProfileImageView.layer.cornerRadius = 4;
    self.userProfileImageView.clipsToBounds = YES;
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_bg"]];
    backgroundView.frame = self.view.frame;
    backgroundView.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:backgroundView];
    [self.view sendSubviewToBack:backgroundView];
    
    [self setupProfileImageView:self.usernameTextField.text];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}


#pragma mark
#pragma mark - Helper methods

- (void)setupProfileImageView:(NSString *)url
{
    if ([self.defaultUserName isEqualToString:url]) {
        NSString *imageUrl = [ADUserDefaultHelper userProfileImageUrl];
        [self.userProfileImageView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"login_icon"] options:SDWebImageRetryFailed];
    }
    else {
        self.userProfileImageView.image = [UIImage imageNamed:@"login_icon"];
    }
}

- (IBAction)loginButtonClicked:(id)sender
{
	if (self.usernameTextField.text.length == 0 || self.passwordTextField.text.length == 0) {
		return;
	}
    [self showLoadingIndicator:nil];
    
    [self.usernameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    
    __weak __typeof(self) weakSelf = self;
	LoginRequestCompletionBlock completionBlock = ^(ADLoginObject* obj, NSError *error) {
        [weakSelf dismissLoadingIndicator];
        weakSelf.loginRequest = nil;

		if (obj == nil) {
			if (error != nil) {
                [weakSelf showErrorMessage:error.localizedDescription];
			}
			return;
		}
		if (obj.token.length > 0) {
            [ADUserDefaultHelper setLoggedIn:YES];
            
            [self.userManager updateCurrentUser:obj.profile sessionTicket:obj.token needsFetching:YES];
            [self.chatManager connectToChatService];
            
            AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            
            [delegate.leftDrawerViewController refreshNSFetchedResultsControllers];
            
            [delegate showPostLoginView];
		}
	};
	self.loginRequest = [[ADLoginRequest alloc] initWithUsername:self.usernameTextField.text Password:self.passwordTextField.text CompletionHandler:completionBlock];
}

- (IBAction)forgotPasswordButtonClicked:(id)sender
{
    __weak __typeof(self) weakSelf = self;

    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    alert.showAnimationType = SCLAlertViewShowAnimationFadeIn;
    
    UITextField *textField = [alert addTextField:NSLocalizedString(@"Enter your email", nil)];
    [alert addButton:NSLocalizedString(@"Submit", nil)
     validationBlock:^BOOL{
        return textField.text.length > 0;
     }
         actionBlock:^(void) {             
             [weakSelf showLoadingIndicator:nil];
             ForgotPasswordRequestCompletionBlock completionBlock = ^(BOOL success, NSError *error) {
                 [weakSelf dismissLoadingIndicator];
                 if (success) {
                     [weakSelf showSuccessWithTitle:NSLocalizedString(@"Link Sent", nil)
                                        message:NSLocalizedString(@"An email containing a link has been sent to your email address.", nil)];
                 }
                 else {
                     [weakSelf showErrorMessage:error.localizedDescription];
                 }
             };
             weakSelf.forgotPasswordRequest = [[ADForgotPasswordRequest alloc] initWithEmail:textField.text CompletionHandler:completionBlock];
         }];
    
    [alert showEdit:self
              title:NSLocalizedString(@"Forgot Password?", nil)
           subTitle:NSLocalizedString(@"Enter the email address you used to create the account, and an email will be sent with a link you can use to reset your password.", nil)
   closeButtonTitle:NSLocalizedString(@"Cancel", nil)
           duration:0.0f];
}

- (IBAction)backgroundButtonClicked:(id)sender
{
	[self.usernameTextField resignFirstResponder];
	[self.passwordTextField resignFirstResponder];
}


#pragma mark -
#pragma mark - Text field delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.usernameTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    else if (textField == self.passwordTextField) {
        [self.view endEditing:YES];
        [self loginButtonClicked:nil];
    }
	return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *updatedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == self.usernameTextField) {
        [self setupProfileImageView:updatedString];
    }
    
    return YES;
}

@end
