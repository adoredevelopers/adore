//
//  ADEULAViewController.m
//  Adore
//
//  Created by Wang on 2015-07-16.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADEULAViewController.h"
#import "MBProgressHUD.h"



@interface ADEULAViewController () <UIWebViewDelegate>


@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *agreeButton;


@end



@implementation ADEULAViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"User Agreement", nil);
    
    if (self.viewOnly) {
        self.navigationItem.rightBarButtonItem = nil;
    }
    else {
        self.navigationItem.rightBarButtonItem.title = NSLocalizedString(@"I Agree", nil);
    }
    
    NSURL *url = [NSURL URLWithString:@"http://static.adoreapp.love/static/mobile-eula.htm"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self.agreeButton setEnabled:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];    
}

@end
