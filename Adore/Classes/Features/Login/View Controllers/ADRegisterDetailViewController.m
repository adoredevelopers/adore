//
//  ADRegisterDetailViewController.m
//  Adore
//
//  Created by Wang on 2014-05-11.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADRegisterDetailViewController.h"
#import "ADUserManager.h"
#import "ADRegisterRequest.h"
#import "ADRegisterObject.h"
#import "ADLoginObject.h"
#import "ADChatManager.h"
#import "ADUserDefaultHelper.h"
#import "ADUserObject.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "TDDatePickerController.h"
#import "UIImage+Resize.h"
#import "NSDate+Utils.h"
#import "AppDelegate.h"
#import "ADTextFieldTableViewCell.h"
#import "JVFloatLabeledTextField.h"
#import "SCLAlertView.h"
#import "ADAddressObject.h"

typedef NS_ENUM(NSInteger, ADRegisterDetailRow) {
    ADRegisterDetailRowPhoto = 0,
    ADRegisterDetailRowGender = 1,
    ADRegisterDetailRowNickname = 2,
    ADRegisterDetailRowBirthday = 3,
    ADRegisterDetailRowCurrentResidence = 4,
};



@interface ADRegisterDetailViewController () <UINavigationControllerDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, TDDatePickerControllerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) ADRegisterObject *registerObject;
@property (nonatomic, strong) ADRegisterRequest *registerRequest;

@property (nonatomic, strong) TDDatePickerController *datePickerView;

@property (nonatomic, weak) JVFloatLabeledTextField *emailTextField;
@property (nonatomic, weak) JVFloatLabeledTextField *passwordTextField;
@property (nonatomic, weak) JVFloatLabeledTextField *nicknameTextField;

@end



@implementation ADRegisterDetailViewController

#pragma mark
#pragma mark - View lifecycles

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    self.title = NSLocalizedString(@"Adore", nil);
    
    self.navigationItem.rightBarButtonItem.title = NSLocalizedString(@"Register", nil);
    
    self.registerObject = [[ADRegisterObject alloc] init];    
    self.datePickerView = [[TDDatePickerController alloc] initWithNibName:@"TDDatePickerController"
                                                                   bundle:nil];
    self.datePickerView.delegate = self;
    
    UINib *textFieldCellNib = [UINib nibWithNibName:@"ADTextFieldTableViewCell" bundle:nil];
    [self.tableView registerNib:textFieldCellNib forCellReuseIdentifier:kAD_TEXT_FIELD_TABLE_VIEW_CELL_IDENTIFIER];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerKeyboardNotification];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self deregisterKeyboardNotification];
    [super viewWillDisappear:animated];
}

-(void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets;
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height), 0.0);
    } else {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width), 0.0);
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
    [UIView commitAnimations];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.tableView.scrollIndicatorInsets = self.tableView.contentInset;
    
    [UIView commitAnimations];
}


#pragma mark
#pragma mark - Button actions

- (IBAction)registerButtonClicked:(id)sender
{
    // avoid fat finger
    if (self.registerRequest) {
        return;
    }
    
    [self hideKeyboard];
    
    // registration fields validation
    
    NSString *errorMessage = [self.registerObject validationResult];
    if (errorMessage) {
        [self showErrorMessage:errorMessage];
        return;
    }
    
    [self showLoadingIndicator:NSLocalizedString(@"Registering...", nil)];
    
    __weak __typeof(self) weakSelf = self;
	RegisterRequestCompletionBlock completionBlock = ^(ADLoginObject* obj, NSError *error) {
        weakSelf.registerRequest = nil;
        [weakSelf dismissLoadingIndicator];
        if (error != nil) {
            [weakSelf showErrorMessage:error.localizedDescription];
            return;
        }
        if (obj.token.length > 0) {
            [ADUserDefaultHelper setLoggedIn:YES];
            
            [self.userManager updateCurrentUser:obj.profile sessionTicket:obj.token needsFetching:NO];
            [self.chatManager connectToChatService];

            AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [delegate showPostLoginView];
        }
	};
    self.registerObject.notification_token = [ADUserDefaultHelper notificationToken];

	self.registerRequest = [[ADRegisterRequest alloc] initWithRegisterObject:self.registerObject CompletionHandler:completionBlock];
}

- (IBAction)profileImageUploadButtonClicked:(id)sender
{
    // Dismiss Keyboard
    [[self view] endEditing:YES];
    
	BOOL photoLibraryAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
	BOOL cameraAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
	if (photoLibraryAvailable && cameraAvailable) {
		UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:NSLocalizedString(@"Take Photo", nil), NSLocalizedString(@"Choose Existing Photo", nil), nil];
		[actionSheet showInView:self.view];
	}
    else if (photoLibraryAvailable) {
        [self showPhotoLibraryPicker];
    }
    else if (cameraAvailable) {
        [self showCameraPicker];
    }
	else {
        [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_NO_IMAGE_SOURCE];
        [self showErrorMessage:NSLocalizedString(@"No source is available.", nil)];
	}
}

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (void)showBirthdayPicker
{
    [self presentSemiModalViewController:self.datePickerView];
    
    if (self.registerObject.birthday) {
        self.datePickerView.datePicker.date = self.registerObject.birthday;
    }
    else {
        self.datePickerView.datePicker.date = [NSDate appUserMaximumBirthday];
    }
    self.datePickerView.datePicker.backgroundColor = [UIColor whiteColor];
}

- (void)showAddressPicker
{
    __weak __typeof(self) weakSelf = self;
    
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    alert.showAnimationType = SCLAlertViewShowAnimationFadeIn;
    
    UITextField *textField = [alert addTextField:NSLocalizedString(@"", nil)];
    textField.text = [self.registerObject residentialAddress];
    
    [alert addButton:NSLocalizedString(@"Done", nil)
     validationBlock:^BOOL{
         return textField.text.length > 0;
     }
         actionBlock:^(void) {
             [weakSelf showLoadingIndicator:nil];
             
             CLGeocoder *clGeo = [[CLGeocoder alloc] init];
             CLGeocodeCompletionHandler completionBlock = ^(NSArray *placemarks, NSError *error) {
                 [weakSelf dismissLoadingIndicator];
                 CLPlacemark *place = [placemarks firstObject];
                 if (place != nil) {
                     ADAddressObject *address = [ADAddressObject addressObjectWithDictionary:place.addressDictionary];
                     SCLAlertView *confirmAlert = [[SCLAlertView alloc] initWithNewWindow];
                     
                     [confirmAlert addButton:NSLocalizedString(@"OK", nil)
                                 actionBlock:^(void) {
                                     [weakSelf.registerObject updateWithAddressObject:address];
                                     [weakSelf.tableView reloadData];
                                 }];
                     
                     [confirmAlert showInfo:weakSelf
                                      title:[address addressString]
                                   subTitle:NSLocalizedString(@"Do you want to use this address as your current residence?", nil)
                           closeButtonTitle:NSLocalizedString(@"Cancel", nil)
                                   duration:0.0f];
                 }
                 else {
                     [self showErrorMessage:NSLocalizedString(@"We couldn't find any address for you. Please try something else.", nil)];
                 }
             };
             [clGeo geocodeAddressString:textField.text completionHandler:completionBlock];
         }];
    
    [alert showEdit:self
              title:NSLocalizedString(@"", nil)
           subTitle:NSLocalizedString(@"Search for an address or a postal code", nil)
   closeButtonTitle:NSLocalizedString(@"Cancel", nil)
           duration:0.0f];
}

- (void)showPhotoLibraryPicker
{
    [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_IMAGE_PHOTO_LIBRARY];

    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.navigationBar.translucent = NO;
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)showCameraPicker
{
    [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_IMAGE_CAMERA];

    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.navigationBar.translucent = NO;
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)segmentedControlValueChanged:(UISegmentedControl *)sender
{
    if (self.registerObject.sex == nil) {
        [self showMessageWithTitle:nil message:NSLocalizedString(@"Please confirm your gender as you cannot change it later.", nil)];
    }
    
    if (sender.selectedSegmentIndex == 0) {
        self.registerObject.sex = @"Female";
    }
    else {
        self.registerObject.sex = @"Male";
    }
}


#pragma mark -
#pragma mark - Image Picker Helper

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	UIImage *img = [info objectForKey:UIImagePickerControllerEditedImage];
	if (img == nil) {
		img = [info objectForKey:UIImagePickerControllerOriginalImage];
	}
    img = [img scaleToSizeKeepAspect:CGSizeMake(640, 640)];
    img = [img compressedImage];
    self.registerObject.photo = img;
    [self.tableView reloadData];
	[self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark - TDDatePickerControllerDelegate Methods

-(void)datePickerSetDate:(TDDatePickerController*)viewController
{
    [self dismissSemiModalViewController:viewController];

    self.registerObject.birthday = viewController.datePicker.date;
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:ADRegisterDetailRowBirthday inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(void)datePickerClearDate:(TDDatePickerController*)viewController {
    [self dismissSemiModalViewController:viewController];
    
    self.registerObject.birthday = nil;
}

-(void)datePickerCancel:(TDDatePickerController*)viewController
{
    [self dismissSemiModalViewController:viewController];
}


#pragma mark -
#pragma mark - UITextFieldDelegate Methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *updatedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField.tag == 1) {
        self.registerObject.email = updatedString;
    }
    else if (textField.tag == 2) {
        self.registerObject.password = updatedString;
    }
    else if (textField.tag == 3) {
        self.registerObject.nickname = updatedString;
    }
    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == 1) {
        [self.passwordTextField becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
    return YES;
}


#pragma mark -
#pragma mark - Action Sheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:NSLocalizedString(@"Take Photo", nil)]) {
        [self showCameraPicker];
    }
    else if ([buttonTitle isEqualToString:NSLocalizedString(@"Choose Existing Photo", nil)]){
        [self showPhotoLibraryPicker];
    }
}


#pragma mark -
#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 2;
    }
    else if (section == 1) {
        return 5;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        if (indexPath.row == ADRegisterDetailRowPhoto) {
            return 270;
        }
    }
	return tableView.rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            ADTextFieldTableViewCell *cell = [ADTextFieldTableViewCell textFieldCellForTableView:tableView text:self.registerObject.email placeholderText:NSLocalizedString(@"Email", nil) tag:1 delegate:self];
            cell.textField.autocorrectionType = UITextAutocorrectionTypeNo;
            cell.textField.secureTextEntry = NO;
            cell.textField.clearsOnBeginEditing = NO;
            cell.textField.keyboardType = UIKeyboardTypeEmailAddress;
            cell.textField.returnKeyType = UIReturnKeyNext;
            cell.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            self.emailTextField = cell.textField;
            return cell;
        }
        else if (indexPath.row == 1) {
            ADTextFieldTableViewCell *cell = [ADTextFieldTableViewCell textFieldCellForTableView:tableView text:self.registerObject.password placeholderText:NSLocalizedString(@"Password", nil) tag:2 delegate:self];
            cell.textField.autocorrectionType = UITextAutocorrectionTypeNo;
            cell.textField.secureTextEntry = YES;
            cell.textField.clearsOnBeginEditing = YES;
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.textField.returnKeyType = UIReturnKeyDefault;
            cell.textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
            self.passwordTextField = cell.textField;
            return cell;
        }
    }
    else if (indexPath.section == 1) {
        switch (indexPath.row) {
            case ADRegisterDetailRowPhoto:
                return [self profileImageCellForTableView:tableView];
                
            case ADRegisterDetailRowGender:
                return [self sexCellForTableView:tableView];
                
            case ADRegisterDetailRowNickname:
            {
                ADTextFieldTableViewCell *cell = [ADTextFieldTableViewCell textFieldCellForTableView:tableView text:self.registerObject.nickname placeholderText:NSLocalizedString(@"Nickname", nil) tag:3 delegate:self];
                cell.textField.autocorrectionType = UITextAutocorrectionTypeNo;
                cell.textField.secureTextEntry = NO;
                cell.textField.clearsOnBeginEditing = NO;
                cell.textField.keyboardType = UIKeyboardTypeDefault;
                cell.textField.returnKeyType = UIReturnKeyDefault;
                cell.textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
                self.nicknameTextField = cell.textField;
                return cell;
            }
                
            case ADRegisterDetailRowBirthday:
                return [self birthdayCellForTableView:tableView];
                
            case ADRegisterDetailRowCurrentResidence:
            {
                return [self residencePlaceCellForTableView:tableView];
            }
        }
    }
	return nil;
}

- (UITableViewCell *)profileImageCellForTableView:(UITableView *)tableView
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileImageCell"];
    
    UIButton *profileImageButton = (UIButton *)[cell viewWithTag:35];
    
    profileImageButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
    profileImageButton.layer.borderWidth = 2.0f;
    profileImageButton.layer.cornerRadius = profileImageButton.frame.size.height / 2;
    profileImageButton.clipsToBounds = YES;
    profileImageButton.tintColor = [UIColor darkGrayColor];
    UIImage *image = self.registerObject.photo;
    if (image == nil) {
        image = [[UIImage imageNamed:@"camera"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    [profileImageButton setImage:image forState:UIControlStateNormal];
    [profileImageButton addTarget:self action:@selector(profileImageUploadButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

	return cell;
}

- (UITableViewCell *)sexCellForTableView:(UITableView *)tableView
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SexCell"];
    
    UISegmentedControl *genderSegmentedControl = (UISegmentedControl *)[cell viewWithTag:40];
    [genderSegmentedControl setTitle:NSLocalizedString(@"Female", nil) forSegmentAtIndex:0];
    [genderSegmentedControl setTitle:NSLocalizedString(@"Male", nil) forSegmentAtIndex:1];
    if ([self.registerObject.sex isEqualToString:@"Male"]) {
        genderSegmentedControl.selectedSegmentIndex = 1;
    }
    else if ([self.registerObject.sex isEqualToString:@"Female"]) {
        genderSegmentedControl.selectedSegmentIndex = 0;
    }
    else {
        genderSegmentedControl.selectedSegmentIndex = UISegmentedControlNoSegment;
    }
    [genderSegmentedControl addTarget:self action:@selector(segmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    return cell;
}

- (UITableViewCell *)birthdayCellForTableView:(UITableView *)tableView
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BirthdayCell"];
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"BirthdayCell"];
        cell.textLabel.text = NSLocalizedString(@"Birthday", nil);
        cell.textLabel.font = [UIFont systemFontOfSize:16.0];
        cell.detailTextLabel.textColor = [UIColor blackColor];
	}

    cell.detailTextLabel.text = [NSDateFormatter localizedStringFromDate:self.registerObject.birthday dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
	return cell;
}

- (UITableViewCell *)residencePlaceCellForTableView:(UITableView *)tableView
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ResidencePlaceCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"ResidencePlaceCell"];
        cell.textLabel.text = NSLocalizedString(@"Current Residence", nil);
        cell.textLabel.font = [UIFont systemFontOfSize:16.0];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    
    cell.detailTextLabel.text = self.registerObject.residentialAddress;
    return cell;
}


#pragma mark -
#pragma mark - Table View Controller Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self hideKeyboard];

    if (indexPath.section == 1 && indexPath.row == ADRegisterDetailRowPhoto) {
        [self profileImageUploadButtonClicked:nil];
    }
    else if (indexPath.section == 1 && indexPath.row == ADRegisterDetailRowBirthday) {
        [self showBirthdayPicker];
    }
    else if (indexPath.section == 1 && indexPath.row == ADRegisterDetailRowCurrentResidence) {
        [self showAddressPicker];
    }
}


@end
