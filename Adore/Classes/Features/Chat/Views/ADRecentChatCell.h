//
//  ADRecentChatCell.h
//  Adore
//
//  Created by Wang on 2014-04-22.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ADManagedConversation;



@interface ADRecentChatCell : UITableViewCell


- (void)updateWithConversation:(ADManagedConversation *)conversation badgeCount:(NSInteger)badgeCount;


@end