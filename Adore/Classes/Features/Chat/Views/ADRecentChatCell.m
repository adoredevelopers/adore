//
//  ADRecentChatCell.m
//  Adore
//
//  Created by Wang on 2014-04-22.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADRecentChatCell.h"
#import "ADManagedChatMessage.h"
#import "ADManagedConversation.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ADManagedUser+Extensions.h"
#import "ADUserManager.h"
#import "ADChatManager.h"
#import "NSDate+Utils.h"
#import "ADUserCoreDataHelper.h"
//#import "CustomBadge.h"
#import "M13BadgeView.h"



@interface ADRecentChatCell ()

@property (nonatomic, weak) IBOutlet UIImageView *profileImageView;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *lastMessageLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
//@property (nonatomic, strong) IBOutlet CustomBadge *badgeView;
@property (nonatomic, strong) M13BadgeView *recentChatMessageCountBadgeView;
@property (nonatomic, strong) IBOutlet UIView *badgeParentWrapperView;

@end



@implementation ADRecentChatCell


- (void)updateWithConversation:(ADManagedConversation *)conversation badgeCount:(NSInteger)badgeCount
{
    ADManagedUser *user = [[ADUserManager sharedInstance].userCoreDataHelper loadUserObjectFromCoreDataByEmail:conversation.user_email];
    self.nameLabel.text = user.nickname;
    self.lastMessageLabel.text = conversation.last_message.message;
    self.dateLabel.text = [conversation.last_message.sent_date toShortFormatString];
    [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:[user profilePictureThumbnailUrl]] placeholderImage:[UIImage imageNamed:@"profile_image_placeholder_72x72"] options:SDWebImageRetryFailed | SDWebImageLowPriority];
    
//    [self.badgeView removeFromSuperview];
    [self.recentChatMessageCountBadgeView removeFromSuperview];
    if (badgeCount > 0) {
//        self.badgeView = [CustomBadge customBadgeRoundStyleWithString:[@(badgeCount) stringValue] withScale:0.8];
        
//        CGPoint point = CGPointMake(CGRectGetMaxX(self.dateLabel.frame) - CGRectGetWidth(self.badgeView.frame)/2, CGRectGetMaxY(self.dateLabel.frame) + 15);
//        self.badgeView.center = point;
//        [self.contentView addSubview:self.badgeView];
        
//        self.chatMessageBadgeView = [[UIView alloc] init];
        _recentChatMessageCountBadgeView = [[M13BadgeView alloc] initWithFrame:CGRectMake(0, 0, 18.0, 18.0)];
        _recentChatMessageCountBadgeView.hidesWhenZero = YES;
        _recentChatMessageCountBadgeView.font = [UIFont systemFontOfSize:12.0];
        _recentChatMessageCountBadgeView.borderWidth = 0.0;
        _recentChatMessageCountBadgeView.maximumWidth = 40.0;
        _recentChatMessageCountBadgeView.horizontalAlignment = M13BadgeViewHorizontalAlignmentCenter;
        _recentChatMessageCountBadgeView.verticalAlignment = M13BadgeViewVerticalAlignmentBottom;
        _recentChatMessageCountBadgeView.alignmentShift = CGSizeMake(0, 10.0);
        _recentChatMessageCountBadgeView.text = [NSString stringWithFormat:@"%ld", (long)badgeCount];
        [self.badgeParentWrapperView addSubview:_recentChatMessageCountBadgeView];
    }
}


@end
