//
//  ADChatBundle.h
//  Adore
//
//  Created by Wang on 2014-12-09.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADManagedUser;
@class ADUserManager;
@class JSQMessagesBubbleImage;
@class JSQMessagesAvatarImage;

@protocol ADChatBundleDelegate;



@interface ADChatBundle : NSObject

@property (strong, nonatomic, readonly) JSQMessagesBubbleImage *outgoingBubbleImageData;
@property (strong, nonatomic, readonly) JSQMessagesBubbleImage *incomingBubbleImageData;
@property (weak, nonatomic) id<ADChatBundleDelegate> delegate;

- (instancetype)initWithUsers:(NSArray *)users;

- (NSString *)chatTitle;
- (NSString *)chatTargetId;
- (ADManagedUser *)sender;
- (ADManagedUser *)userByEmail:(NSString *)email;
- (JSQMessagesAvatarImage *)avatarByEmail:(NSString *)email;

@end



@protocol ADChatBundleDelegate <NSObject>

- (void)chatBundleUpdated:(id)sender;

@end