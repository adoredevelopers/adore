//
//  ADChatBundle.m
//  Adore
//
//  Created by Wang on 2014-12-09.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADChatBundle.h"
#import <JSQMessagesViewController/JSQMessages.h>
#import "SDWebImage/UIImageView+WebCache.h"
#import "ADManagedUser+Extensions.h"
#import "ADUserManager.h"
#import "Constants.h"

@interface ADChatBundle ()

@property (nonatomic, strong) ADUserManager *userManager;

@property (strong, nonatomic) NSMutableDictionary *avatars;
@property (strong, nonatomic) NSMutableDictionary *users;


@end


@implementation ADChatBundle

- (instancetype)initWithUsers:(NSArray *)users
{
    self = [super init];
    if (self)
    {
        _userManager = [ADUserManager sharedInstance];
        JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
//        _outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
//        _incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleGreenColor]];
        _incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
        _outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[Constants femaleTintColor]];
        
        _users = @{}.mutableCopy;
        _avatars = @{}.mutableCopy;
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        for (ADManagedUser *user in users) {
            JSQMessagesAvatarImage *placeholderAvatar = [JSQMessagesAvatarImageFactory avatarImageWithPlaceholder:[UIImage imageNamed:@"profile_image_placeholder_72x72"] diameter:kAD_CHAT_AVATAR_IMAGE_SIZE];
            [_avatars setObject:placeholderAvatar forKey:user.email];
            
            [manager downloadImageWithURL:[NSURL URLWithString:[user profilePictureThumbnailUrl]]
                                  options:0
                                 progress:nil
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (image) {
                                        JSQMessagesAvatarImage *userAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:image diameter:kAD_CHAT_AVATAR_IMAGE_SIZE];
                                        [_avatars setObject:userAvatar forKey:user.email];
                                        if ([self.delegate respondsToSelector:@selector(chatBundleUpdated:)]) {
                                            [self.delegate chatBundleUpdated:self];
                                        }
                                    }}];
            [_users setObject:user forKey:user.email];
        }
    }
    
    return self;
}

- (NSString *)chatTitle
{
    for (ADManagedUser *user in [self.users allValues])
    {
        if (user != [self sender]) {
            return user.nickname;
        }
    }
    return nil;
}

// We currently only support chat between two pepople. If in the future we need to support group chat, this is the group chat id.
// it is used to grab chat history from CoreData
- (NSString *)chatTargetId
{
    for (ADManagedUser *user in [self.users allValues])
    {
        if (user != [self sender]) {
            return user.email;
        }
    }
    return nil;
}

- (ADManagedUser *)sender
{
    return self.userManager.currentUserProfile;
}

- (ADManagedUser *)userByEmail:(NSString *)email
{
    return self.users[email];
}

- (JSQMessagesAvatarImage *)avatarByEmail:(NSString *)email
{
    return self.avatars[email];
}

@end
