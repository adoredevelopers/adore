//
//  ADChatViewController.m
//  Adore
//
//  Created by Wang on 2014-12-09.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADChatViewController.h"
#import "ADUserConnectionTableViewController.h"
#import "ADManagedChatMessage.h"
#import "ADManagedConversation.h"
#import "ADUserManager.h"
#import "ADUserCoreDataHelper.h"
#import "ADManagedUser+Extensions.h"
#import <JSQMessagesViewController/JSQMessages.h>
#import "ADChatBundle.h"
#import "NSObject+Notification.h"
#import "ADNearbyUserDetailsViewController.h"
#import "ADUserConnectionTableViewController.h"
#import "ADMessagesToolbarButtonFactory.h"
#import "WBEmoticonInputView.h"
#import "YYKit.h"
#import "WBModel.h"
#import "WBStatusHelper.h"
#import "UIImage+APResize.h"

typedef void (^HandleSendChatMessageResponseBlock)(NSString *response);



@interface ADChatViewController () <YYTextViewDelegate, YYTextKeyboardObserver, WBStatusComposeEmoticonViewDelegate>

@end



@implementation ADChatViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _userManager = [ADUserManager sharedInstance];
        _chatManager = [ADChatManager sharedInstance];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /* ================================================================
     * ADChatViewController is a subclass of JSQMessagesViewController,
     * not ADViewController,
     * thus registerNotificationReceiver & deregisterNotificationReceiver is not called from super class,
     * needs to do it here.
     * ====================
     */
    [self registerNotificationReceiver];
    
    self.automaticallyAdjustsScrollViewInsets = NO;

    self.collectionView.collectionViewLayout.messageBubbleTextViewFrameInsets = UIEdgeInsetsMake(3.0f, 0.0f, 0.0f, 6.0f);
    self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeMake(kAD_CHAT_AVATAR_IMAGE_SIZE, kAD_CHAT_AVATAR_IMAGE_SIZE);
    self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeMake(kAD_CHAT_AVATAR_IMAGE_SIZE, kAD_CHAT_AVATAR_IMAGE_SIZE);
    
    // Setup
    self.title = [self.chatManager isConnectedToChatService] ? self.chatBundle.chatTitle : @"Disconnected"; // Initially show disconnected till SocketIO connects to server
    self.senderId = self.chatBundle.sender.email;
    self.senderDisplayName = self.chatBundle.sender.nickname;    
    self.inputToolbar.contentView.leftBarButtonItem = [ADMessagesToolbarButtonFactory defaultEmoticonButtonItem];
    
    self.emoticonGroups = [WBStatusHelper emoticonGroups];
    
    // Load messages
    self.messages = [[self.chatManager loadChatMessageByEmail:[self.chatBundle chatTargetId] beforeDate:nil] mutableCopy];
    self.showLoadEarlierMessagesHeader = (self.messages.count == kAD_CHAT_SERVICE_BATCH_SIZE);
    
    NSFetchedResultsController *unreadMessagesController = [self.chatManager unreadMessagesFetchedResultsControllerWithEmail:[self.chatBundle chatTargetId] delegate:nil];
    [self addMessagesToCurrentMessages:unreadMessagesController.fetchedObjects];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"sent_date" ascending:YES];
    [self.messages sortUsingDescriptors:@[sortDescriptor]];
    [self.chatManager markMessagesRead:self.messages withDate:nil];
    
    [self finishReceivingMessage];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.automaticallyScrollsToMostRecentMessage = YES;
}

- (void)dealloc
{
    [self deregisterNotificationReceiver];
}


#pragma mark -
#pragma mark - Helper Methods

- (void)registerNotificationReceiver
{
    __weak __typeof(self) weakSelf = self;
    [[NSNotificationCenter defaultCenter] addObserverForName:kAD_NOTIFICATION_SOCKET_CONNECTED
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      weakSelf.title = self.chatBundle.chatTitle;
                                                  }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kAD_NOTIFICATION_SOCKET_DISCONNECTED
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      weakSelf.title = NSLocalizedString(@"Disconnected", nil);
                                                  }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kAD_NOTIFICATION_SOCKET_CONNECTING
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      weakSelf.title = NSLocalizedString(@"Connecting...", nil);
                                                  }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewMessageNotification:) name:kAD_NOTIFICATION_NEW_MESSAGE_RECEIVED object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMessageSentNotification:) name:kAD_NOTIFICATION_MESSAGE_SENT object:nil];
}

- (void)deregisterNotificationReceiver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)handleNewMessageNotification:(NSNotification *)notification
{
    [JSQSystemSoundPlayer jsq_playMessageReceivedSound];

    ADManagedChatMessage *message = notification.userInfo[@"message"];
    [self addMessagesToCurrentMessages:@[message]];
    [self finishReceivingMessageAnimated:YES];
}

- (void)handleMessageSentNotification:(NSNotification *)notification
{
    [self.collectionView.collectionViewLayout invalidateLayoutWithContext:[JSQMessagesCollectionViewFlowLayoutInvalidationContext context]];
    [self.collectionView reloadData];
}

- (void)showUserProfileView:(NSString *)email
{
    // If we are from Nearby user detail and user connection view, pop.
    if (self.parentViewClass == [ADNearbyUserDetailsViewController class]
        || self.parentViewClass == [ADUserConnectionTableViewController class]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        UIStoryboard *adoreStoryBoard = [UIStoryboard storyboardWithName:@"ADAdoreStoryboard" bundle:nil];
        ADUserConnectionTableViewController *userConnectionVC = [adoreStoryBoard instantiateViewControllerWithIdentifier:@"ADUserConnectionTableViewController"];
        userConnectionVC.userObject = [self.userManager.userCoreDataHelper loadUserObjectFromCoreDataByEmail:email];
        userConnectionVC.parentViewClass = [self class];
        [self.navigationController pushViewController:userConnectionVC animated:YES];
    }
}

- (void)addMessagesToCurrentMessages:(NSArray *)messages
{
    for (ADManagedChatMessage *msg in messages) {
        if ([self.messages containsObject:msg] == NO) {
            [self.messages addObject:msg];
        }
    }
}


#pragma mark -
#pragma mark - ADChatBundleDelegate

- (void)chatBundleUpdated:(id)sender
{
    [self.collectionView reloadData];
}


#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
    [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    ADManagedChatMessage *message = [self.chatManager sendMessageThroughSocket:text toUser:self.chatBundle.chatTargetId];
    [self.messages addObject:message];
    
    [self finishSendingMessage];
}

- (void)didPressAccessoryButton:(UIButton *)sender
{
    UITextView *textView = self.inputToolbar.contentView.textView;
    if (textView.isFirstResponder) {
        if (textView.inputView) {
            self.inputToolbar.contentView.leftBarButtonItem = [ADMessagesToolbarButtonFactory defaultEmoticonButtonItem];
            textView.inputView = nil;
            [textView reloadInputViews];
        } else {
            self.inputToolbar.contentView.leftBarButtonItem = [ADMessagesToolbarButtonFactory defaultKeyboardButtonItem];
            WBEmoticonInputView *v = [WBEmoticonInputView sharedView];
            v.delegate = self;
            textView.inputView = v;
            [textView reloadInputViews];
        }
    } else {
        self.inputToolbar.contentView.leftBarButtonItem = [ADMessagesToolbarButtonFactory defaultKeyboardButtonItem];
        WBEmoticonInputView *v = [WBEmoticonInputView sharedView];
        v.delegate = self;
        textView.inputView = v;
        [textView reloadInputViews];
        
        [textView becomeFirstResponder];
    }
}

#pragma mark - JSQMessages CollectionView DataSource

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ADManagedChatMessage *message = [self.messages objectAtIndex:indexPath.item];
    if ([[self.navigationController visibleViewController] isEqual:self]) {
        [self.chatManager markMessageRead:message withDate:nil];
    }
    return message;
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ADManagedChatMessage *message = [self.messages objectAtIndex:indexPath.item];
    if (message.isIncoming) {
        return self.chatBundle.incomingBubbleImageData;
    }
    return self.chatBundle.outgoingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ADManagedChatMessage *message = [self.messages objectAtIndex:indexPath.item];
    if (message.isIncoming) {
        return [self.chatBundle avatarByEmail:message.conversation.user_email];
    }
    return [self.chatBundle avatarByEmail:self.chatBundle.sender.email];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item % 5 == 0) {
        ADManagedChatMessage *message = [self.messages objectAtIndex:indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    ADManagedChatMessage *message = [self.messages objectAtIndex:indexPath.item];
    if (!message.isIncoming) {
        if ([self.chatManager isSendingMessage:message]) {
            return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Sending", nil)];
        }
        else if (message.server_date == nil) {
            return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Failed", nil)];
        }
    }
    return nil;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 14.f;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.messages count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    UIActivityIndicatorView *aiv = (UIActivityIndicatorView *)[cell.messageBubbleContainerView viewWithTag:150];
    [aiv removeFromSuperview];
    UIImageView *iv = (UIImageView *)[cell.messageBubbleContainerView viewWithTag:151];
    [iv removeFromSuperview];
    
    
    ADManagedChatMessage *message = self.messages[indexPath.item];

    if (message.isMediaMessage == NO) {
        if (message.isIncoming) {
            cell.textView.textColor = [UIColor darkGrayColor];
        }
        else {
            cell.textView.textColor = [UIColor whiteColor];
            cell.messageBubbleImageView.alpha = 0.8;
        }
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
        
//        cell.textView.attributedText = [self parseTextForEmoticonAttributedString:cell.textView.text];
    }
    
    if (message.isIncoming == NO) {
        if ([self.chatManager isSendingMessage:message]) {
            UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            activityIndicator.tag = 150;
            activityIndicator.hidesWhenStopped = YES;
            activityIndicator.center = CGPointMake(-15.0, 20);
            [activityIndicator startAnimating];
            [cell.messageBubbleContainerView addSubview:activityIndicator];
        }
        else if (message.server_date == nil) {
            // TO DO: shown warning view should be tappable, allowing user to resent the message
            UIImageView *warningView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"exclamation_mark"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
            warningView.tag = 151;
            warningView.frame = CGRectMake(-30.0, 0, 28.0, 42.0);
            warningView.tintColor = [UIColor redColor];
            [cell.messageBubbleContainerView addSubview:warningView];
        }
    }
    
    return cell;
}


#pragma mark - JSQMessages collection view flow layout delegate

#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item % 5 == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
{
    NSDate *date = ((ADManagedChatMessage *)[self.messages firstObject]).sent_date;
    NSArray *earlierMessages = [self.chatManager loadChatMessageByEmail:[self.chatBundle chatTargetId] beforeDate:date];
    
    if ([earlierMessages count] > 0) {
        [self.messages replaceObjectsInRange:NSMakeRange(0, 0) withObjectsFromArray:earlierMessages];
    }
    if ([earlierMessages count] < kAD_CHAT_SERVICE_BATCH_SIZE) {
        self.showLoadEarlierMessagesHeader = NO;
    }
    
    self.automaticallyScrollsToMostRecentMessage = NO;
    [self finishReceivingMessage];
    self.automaticallyScrollsToMostRecentMessage = YES;
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    [self.inputToolbar.contentView.textView resignFirstResponder];
    
    ADManagedChatMessage *message = [self.messages objectAtIndex:indexPath.item];
    if (message.isIncoming) {
        [self showUserProfileView:message.conversation.user_email];
    }
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation
{
    [self.inputToolbar.contentView.textView resignFirstResponder];
}

//#pragma mark @protocol YYTextViewDelegate
//- (void)textViewDidChange:(YYTextView *)textView {
//    // do not touch
//}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark @protocol YYTextKeyboardObserver
- (void)keyboardChangedWithTransition:(YYTextKeyboardTransition)transition {
//    CGRect toFrame = [[YYTextKeyboardManager defaultManager] convertRect:transition.toFrame toView:self.view];
//    if (transition.animationDuration == 0) {
//        _toolbar.bottom = CGRectGetMinY(toFrame);
//    } else {
//        [UIView animateWithDuration:transition.animationDuration delay:0 options:transition.animationOption | UIViewAnimationOptionBeginFromCurrentState animations:^{
//            _toolbar.bottom = CGRectGetMinY(toFrame);
//        } completion:NULL];
//    }
}

#pragma mark @protocol WBStatusComposeEmoticonView
- (void)emoticonInputDidTapText:(NSString *)text {
    if (text.length) {
        UITextView *textView = self.inputToolbar.contentView.textView;
        [textView replaceRange:textView.selectedTextRange withText:text];
    }
}

- (void)emoticonInputDidTapEmoticon:(WBEmoticon *)emoticon {
    NSString *text = emoticon.chs;
    if (text.length) {
        UITextView *textView = self.inputToolbar.contentView.textView;
        UIFont *textFont = [textView font];
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:textView.attributedText];
        NSString *emoticonImagePath = [[WBStatusHelper emoticonBundle] pathForScaledResource:emoticon.png ofType:nil inDirectory:emoticon.group.groupID];
        NSTextAttachment * emoticonAttachment = [[NSTextAttachment alloc] init];
        emoticonAttachment.image = [[UIImage alloc] initWithContentsOfFile:emoticonImagePath];
        NSMutableAttributedString *emoticonImageString = [NSAttributedString attributedStringWithAttachment:emoticonAttachment].mutableCopy;
        [emoticonImageString addAttribute:NSFontAttributeName value:textFont.fontName range:NSMakeRange(0, text.length)];
        [attributedText appendAttributedString:emoticonImageString];
        textView.attributedText = attributedText;
//        [textView replaceRange:textView.selectedTextRange withText:text];
    }
}

- (void)emoticonInputDidTapBackspace {
    UITextView *textView = self.inputToolbar.contentView.textView;
    [textView deleteBackward];
}

- (NSAttributedString *)parseTextForEmoticonAttributedString:(NSString *)text {
    // parsing emoticons
    NSMutableAttributedString *emoticonAttributeString = [[NSMutableAttributedString alloc] initWithString:text];
    NSRegularExpression * re = [NSRegularExpression regularExpressionWithPattern:@"\\[:[0-9]+:[-_a-zA-Z0-9\u4E00-\u9FA5]+:\\]" options:kNilOptions error:NULL];
    NSArray *matchResultArray = [re matchesInString:text options:0 range:NSMakeRange(0, text.length)];
    NSMutableArray *emoticonDictArray = [NSMutableArray arrayWithCapacity:matchResultArray.count];
    for (NSTextCheckingResult *match in matchResultArray) {
        NSRange range = [match range];
        NSString *subStr = [text substringWithRange:range];
        WBEmoticon *emoticon = [WBStatusHelper emoticonFromCode:subStr withEmoticonGroups:self.emoticonGroups];
        if (emoticon != nil) {
            NSTextAttachment *emoticonTextAttachment = [[NSTextAttachment alloc] init];
            UIImage *emoticonImage = [WBStatusHelper emoticonImageNamed:emoticon];
            emoticonTextAttachment.image = [UIImage imageWithImage:emoticonImage scaledToFillToSize:CGSizeMake(20.0, 20.0)];
            emoticonTextAttachment.bounds = CGRectMake(0, [UIFont systemFontOfSize:17.0].descender, emoticonTextAttachment.image.size.width, emoticonTextAttachment.image.size.height);
            NSAttributedString *emoticonAttributeString = [NSAttributedString attributedStringWithAttachment:emoticonTextAttachment];
            NSMutableDictionary *emoticonDict = [NSMutableDictionary dictionaryWithCapacity:2];
            [emoticonDict setObject:emoticonAttributeString forKey:@"image"];
            [emoticonDict setObject:[NSValue valueWithRange:range] forKey:@"range"];
            [emoticonDictArray addObject:emoticonDict];
        }
    }
    
    // replacing emoticons
    for (int i = (int)emoticonDictArray.count-1; i >= 0; i--) {
        NSRange range;
        [emoticonDictArray[i][@"range"] getValue:&range];
        [emoticonAttributeString replaceCharactersInRange:range withAttributedString:emoticonDictArray[i][@"image"]];
    }
    [emoticonAttributeString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17.0] range:NSMakeRange(0, emoticonAttributeString.length)];
    
    return emoticonAttributeString;
}

@end
