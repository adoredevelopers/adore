//
//  ADRecentChatViewController.m
//  Adore
//
//  Created by Wang on 2014-04-22.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADRecentChatViewController.h"
#import "ADRecentChatCell.h"
#import "ADChatManager.h"
#import "ADUserManager.h"
#import "ADManagedChatMessage.h"
#import "ADChatViewController.h"
#import "ADChatBundle.h"
#import "ADUserCoreDataHelper.h"
#import "UIScrollView+EmptyDataSet.h"



@interface ADRecentChatViewController () <UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>

@end

@implementation ADRecentChatViewController

#pragma mark -
#pragma mark - View Lifecycle

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    self.title = [self.chatManager isConnectedToChatService] ? NSLocalizedString(@"Chat", nil) : NSLocalizedString(@"Disconnected", nil); // Initially show disconnected till SocketIO connects to server
    
    self.recentChatsController = [self.chatManager recentChatsFetchedResultsControllerWithDelegate:self];
    self.pendingUsersController = [self.userManager.userCoreDataHelper pendingUsersFetchedResultsControllerWithDelegate:self];
    self.unreadMessagesController = [self.chatManager unreadMessagesFetchedResultsControllerWithDelegate:self];
    self.unreadConnectionsController = [self.userManager.userCoreDataHelper unreadConnectedUsersFetchedResultsControllerWithDelegate:self];
    
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.tableFooterView = [UIView new];
}

- (void)dealloc
{
    self.tableView.emptyDataSetSource = nil;
    self.tableView.emptyDataSetDelegate = nil;
}


#pragma mark -
#pragma mark - Helper Methods

- (void)registerNotificationReceiver
{
    __weak __typeof(self) weakSelf = self;
    [[NSNotificationCenter defaultCenter] addObserverForName:kAD_NOTIFICATION_SOCKET_CONNECTED
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      weakSelf.title = NSLocalizedString(@"Chat", nil);
                                                  }];

    [[NSNotificationCenter defaultCenter] addObserverForName:kAD_NOTIFICATION_SOCKET_DISCONNECTED
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      weakSelf.title = NSLocalizedString(@"Disconnected", nil);
                                                  }];

    [[NSNotificationCenter defaultCenter] addObserverForName:kAD_NOTIFICATION_SOCKET_CONNECTING
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      weakSelf.title = NSLocalizedString(@"Connecting...", nil);
                                                  }];
}

- (void)pushChatViewControllerWithUserEmail:(NSString *)email animated:(BOOL)animated
{
    ADChatViewController *chatVC = [self.chatManager chatViewWithEmail:email fromView:[self class]];
    if (chatVC) {
        [self.navigationController pushViewController:chatVC animated:animated];
    }
}


#pragma mark -
#pragma mark - NSFetchedResultsControllerDelegate methods

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if (controller == self.recentChatsController) {
        [self.tableView reloadData];
    }
    else if (controller == self.unreadMessagesController) {
        [self updateDrawerBadge];
        [self.tableView reloadData];
    }
    else if (controller == self.pendingUsersController
             || controller == self.unreadConnectionsController) {
        [self updateDrawerBadge];
    }
}


#pragma mark -
#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:28.0],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"No Chat Message", nil)
                                           attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = NSLocalizedString(@"Start chatting with your adorer.", nil);
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:14.0],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}


#pragma mark -
#pragma mark - Table View Controller Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.recentChatsController.fetchedObjects.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	ADRecentChatCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RecentChatCell"];
	
	ADManagedConversation *conversation = self.recentChatsController.fetchedObjects[indexPath.row];
    
    NSIndexSet *indexes = [self.unreadMessagesController.fetchedObjects indexesOfObjectsPassingTest:^BOOL(ADManagedChatMessage *obj, NSUInteger idx, BOOL *stop) {
        return [obj.conversation.user_email isEqualToString:conversation.user_email];
    }];
    [cell updateWithConversation:conversation badgeCount:indexes.count];
    
	return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    ADManagedConversation *conversation = self.recentChatsController.fetchedObjects[indexPath.row];
    [self.chatManager deleteAllMessagesByConversation:conversation];
}


#pragma mark -
#pragma mark - Table View Controller Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.selectedConversation = self.recentChatsController.fetchedObjects[indexPath.row];

    [self pushChatViewControllerWithUserEmail:self.selectedConversation.user_email animated:YES];
}


@end
