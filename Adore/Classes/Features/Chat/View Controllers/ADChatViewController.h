//
//  ADChatViewController.h
//  Adore
//
//  Created by Wang on 2014-12-09.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "JSQMessagesViewController.h"
#import "ADChatBundle.h"
#import "ADChatManager.h"


@interface ADChatViewController : JSQMessagesViewController <ADChatBundleDelegate>


@property (nonatomic, strong) Class parentViewClass;
@property (nonatomic, strong) ADChatBundle *chatBundle;
@property (nonatomic, strong) ADUserManager *userManager;
@property (nonatomic, strong) ADChatManager *chatManager;
@property (nonatomic, strong) NSMutableArray *messages;
@property (nonatomic, assign) BOOL notificationRegistered;
@property (nonatomic, strong) NSArray *emoticonGroups;


@end
