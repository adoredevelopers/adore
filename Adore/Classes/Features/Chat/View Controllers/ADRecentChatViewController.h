//
//  ADRecentChatViewController.h
//  Adore
//
//  Created by Wang on 2014-04-22.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADViewController.h"
#import "ADManagedConversation.h"


@interface ADRecentChatViewController : ADViewController


@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) ADManagedConversation *selectedConversation;


- (void)pushChatViewControllerWithUserEmail:(NSString *)email animated:(BOOL)animated;


@end
