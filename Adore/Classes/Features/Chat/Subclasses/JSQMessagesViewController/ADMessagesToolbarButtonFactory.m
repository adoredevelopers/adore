//
//  ADMessagesToolbarButtonFactory.m
//  Adore
//
//  Created by Chao Lu on 2015-07-21.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADMessagesToolbarButtonFactory.h"
#import "UIImage+JSQMessages.h"

@implementation ADMessagesToolbarButtonFactory

+ (UIButton *)defaultEmoticonButtonItem
{
//    UIImage *emoticonImage = [UIImage imageNamed:@"smile_face"];
    UIImage *emoticonImage = [UIImage imageNamed:@"compose_emoticonbutton_background"];
    UIImage *normalImage = [emoticonImage jsq_imageMaskedWithColor:[UIColor lightGrayColor]];
    UIImage *highlightedImage = [emoticonImage jsq_imageMaskedWithColor:[UIColor darkGrayColor]];
    
    UIButton *emoticonButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, emoticonImage.size.width, 32.0f)];
    [emoticonButton setImage:normalImage forState:UIControlStateNormal];
    [emoticonButton setImage:highlightedImage forState:UIControlStateHighlighted];
    
    emoticonButton.contentMode = UIViewContentModeScaleAspectFit;
    emoticonButton.backgroundColor = [UIColor clearColor];
    emoticonButton.tintColor = [UIColor lightGrayColor];
    
    return emoticonButton;
}

+ (UIButton *)defaultKeyboardButtonItem
{
    UIImage *emoticonImage = [UIImage imageNamed:@"compose_keyboardbutton_background"];
    UIImage *normalImage = [emoticonImage jsq_imageMaskedWithColor:[UIColor lightGrayColor]];
    UIImage *highlightedImage = [emoticonImage jsq_imageMaskedWithColor:[UIColor darkGrayColor]];
    
    UIButton *emoticonButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, emoticonImage.size.width, 32.0f)];
    [emoticonButton setImage:normalImage forState:UIControlStateNormal];
    [emoticonButton setImage:highlightedImage forState:UIControlStateHighlighted];
    
    emoticonButton.contentMode = UIViewContentModeScaleAspectFit;
    emoticonButton.backgroundColor = [UIColor clearColor];
    emoticonButton.tintColor = [UIColor lightGrayColor];
    
    return emoticonButton;
}

@end
