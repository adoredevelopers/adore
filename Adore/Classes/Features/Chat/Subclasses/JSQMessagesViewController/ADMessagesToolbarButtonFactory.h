//
//  ADMessagesToolbarButtonFactory.h
//  Adore
//
//  Created by Chao Lu on 2015-07-21.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "JSQMessagesToolbarButtonFactory.h"

@interface ADMessagesToolbarButtonFactory : JSQMessagesToolbarButtonFactory

+ (UIButton *)defaultEmoticonButtonItem;
+ (UIButton *)defaultKeyboardButtonItem;

@end
