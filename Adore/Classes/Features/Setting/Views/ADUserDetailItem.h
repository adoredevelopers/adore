//
//  ADUserDetailItem.h
//  Adore
//
//  Created by Wang on 2014-07-27.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ADUserDetailItemType) {
    ADUserDetailItemTypeHeaderBasicInfo,
    ADUserDetailItemTypeHeaderDetails,
    ADUserDetailItemTypeHeaderWorkEdu,
    
    ADUserDetailItemTypeProfileImage,
    ADUserDetailItemTypeEmail,
	ADUserDetailItemTypeName,
    ADUserDetailItemTypeMotto,
	ADUserDetailItemTypeNickname,
    ADUserDetailItemTypeBirthday,
    ADUserDetailItemTypeWeight,
    ADUserDetailItemTypeHeight,
    ADUserDetailItemTypeMartialStatus,
    ADUserDetailItemTypeBodyShape,
	ADUserDetailItemTypeZodiac,
	ADUserDetailItemTypeC_Zodiac,
	ADUserDetailItemTypeGender,
	ADUserDetailItemTypeReligion,
	ADUserDetailItemTypeDrinking,
	ADUserDetailItemTypeSmoking,
    ADUserDetailItemTypeEducation,
    ADUserDetailItemTypeUndergradEdu,
    ADUserDetailItemTypePostgradEdu,
    ADUserDetailItemTypeEmployer,
    ADUserDetailItemTypeOccupation,
    ADUserDetailItemTypeIncome,
    ADUserDetailItemTypeLanguage,
    ADUserDetailItemTypeInterest,
    ADUserDetailItemTypePersonality,
    ADUserDetailItemTypeRace,
    ADUserDetailItemTypeResidentialStatus,
    ADUserDetailItemTypeCurrentResidence,
    ADUserDetailItemTypeBirthPlace
};



@interface ADUserDetailItem : NSObject


@property (nonatomic, readonly) ADUserDetailItemType type;

+ (instancetype)headerBasicInfo;
+ (instancetype)headerDetails;
+ (instancetype)headerWorkEdu;

+ (instancetype)profileImageType;
+ (instancetype)emailType;
+ (instancetype)nameType;
+ (instancetype)mottoType;
+ (instancetype)nicknameType;
+ (instancetype)birthdayType;
+ (instancetype)weightType;
+ (instancetype)heightType;
+ (instancetype)martialStatusType;
+ (instancetype)bodyShapeType;
+ (instancetype)zodiacType;
+ (instancetype)c_zodiacType;
+ (instancetype)genderType;
+ (instancetype)religionType;
+ (instancetype)drinkingType;
+ (instancetype)smokingType;
+ (instancetype)languageType;
+ (instancetype)interestType;
+ (instancetype)personalityType;
+ (instancetype)educationType;
+ (instancetype)undergradEduType;
+ (instancetype)postgradEduType;
+ (instancetype)employerType;
+ (instancetype)occupationType;
+ (instancetype)incomeLevelType;
+ (instancetype)raceType;
+ (instancetype)residentialStatusType;
+ (instancetype)currentResidenceType;
+ (instancetype)birthPlaceType;

- (BOOL)isHeader;

@end
