//
//  ADUserDetailItem.m
//  Adore
//
//  Created by Wang on 2014-07-27.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADUserDetailItem.h"



@implementation ADUserDetailItem


- (id)initWithType:(ADUserDetailItemType)type
{
	self = [super init];
	if (self) {
		_type = type;
	}
	return self;
}

+ (instancetype)headerBasicInfo
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeHeaderBasicInfo];
}

+ (instancetype)headerDetails
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeHeaderDetails];
}

+ (instancetype)headerWorkEdu
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeHeaderWorkEdu];
}

+ (instancetype)profileImageType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeProfileImage];
}

+ (instancetype)emailType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeEmail];
}

+ (instancetype)nameType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeName];
}

+ (instancetype)mottoType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeMotto];
}

+ (instancetype)nicknameType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeNickname];
}

+ (instancetype)birthdayType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeBirthday];
}

+ (instancetype)weightType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeWeight];
}

+ (instancetype)heightType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeHeight];
}

+ (instancetype)martialStatusType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeMartialStatus];
}

+ (instancetype)bodyShapeType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeBodyShape];
}

+ (instancetype)zodiacType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeZodiac];
}

+ (instancetype)c_zodiacType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeC_Zodiac];
}

+ (instancetype)genderType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeGender];
}

+ (instancetype)religionType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeReligion];
}

+ (instancetype)drinkingType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeDrinking];
}

+ (instancetype)smokingType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeSmoking];
}

+ (instancetype)languageType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeLanguage];
}

+ (instancetype)interestType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeInterest];
}

+ (instancetype)personalityType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypePersonality];
}

+ (instancetype)educationType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeEducation];
}

+ (instancetype)undergradEduType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeUndergradEdu];
}

+ (instancetype)postgradEduType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypePostgradEdu];
}

+ (instancetype)employerType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeEmployer];
}

+ (instancetype)occupationType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeOccupation];
}

+ (instancetype)incomeLevelType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeIncome];
}

+ (instancetype)raceType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeRace];
}

+ (instancetype)residentialStatusType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeResidentialStatus];
}

+ (instancetype)currentResidenceType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeCurrentResidence];
}

+ (instancetype)birthPlaceType
{
    return [[ADUserDetailItem alloc] initWithType:ADUserDetailItemTypeBirthPlace];
}

- (BOOL)isHeader
{
    if (self.type == ADUserDetailItemTypeHeaderBasicInfo
        || self.type == ADUserDetailItemTypeHeaderDetails
        || self.type == ADUserDetailItemTypeHeaderWorkEdu) {
        return YES;
    }
    return NO;
}

@end

