//
//  ADProfileSearchableListViewController.h
//  Adore
//
//  Created by Wang on 2015-04-12.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADViewController.h"
#import "ADUserDetailItem.h"



@interface ADProfileSearchableListViewController : ADViewController


@property (nonatomic, assign) ADUserDetailItemType type;


@end
