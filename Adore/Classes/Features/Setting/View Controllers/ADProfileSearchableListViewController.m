//
//  ADProfileSearchableListViewController.m
//  Adore
//
//  Created by Wang on 2015-04-12.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADProfileSearchableListViewController.h"
#import "ADUserManager.h"
#import "ADManagedUser+Extensions.h"
#import "ADDetailListCoreDataHelper.h"



@interface ADProfileSearchableListViewController () <UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic, assign) BOOL singleSelectionMode;

@property (nonatomic, strong) ADManagedUser *currentUser;
@property (nonatomic, strong) ADDetailListCoreDataHelper *detailListCoreDataHelper;
@property (nonatomic, strong) NSString *languageCode;

@property (nonatomic, strong) NSArray *list;
@property (nonatomic, strong) NSArray *filteredList;
@property (nonatomic, strong) NSMutableArray *selectedArray;

@end



@implementation ADProfileSearchableListViewController


#pragma mark -
#pragma makr - Initializers

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _detailListCoreDataHelper = self.userManager.detailListCoreDataHelper;
        _currentUser = self.userManager.currentUserProfile;
        _languageCode = [_detailListCoreDataHelper currentLanguageCode];
    }
    return self;
}


#pragma mark -
#pragma makr - Setter methods

- (void)setType:(ADUserDetailItemType)type
{
    _type = type;
    self.list = [self.detailListCoreDataHelper loadListForType:type];
    self.filteredList = self.list;
    
    if (self.type == ADUserDetailItemTypeWeight) {
        self.selectedArray = @[].mutableCopy;
        if (self.currentUser.weight) {
            [self.selectedArray addObject:self.currentUser.weight];
        }
        self.title = NSLocalizedString(@"Weight", nil);
        self.singleSelectionMode = YES;
    }
    else if (self.type == ADUserDetailItemTypeHeight) {
        self.selectedArray = @[].mutableCopy;
        if (self.currentUser.height) {
            [self.selectedArray addObject:self.currentUser.height];
        }
        self.title = NSLocalizedString(@"Height", nil);
        self.singleSelectionMode = YES;
    }
    else if (self.type == ADUserDetailItemTypeMartialStatus) {
        self.selectedArray = @[].mutableCopy;
        if (self.currentUser.martial_status) {
            [self.selectedArray addObject:self.currentUser.martial_status];
        }
        self.title = NSLocalizedString(@"Martial Status", nil);
        self.singleSelectionMode = YES;
    }
    else if (self.type == ADUserDetailItemTypeBodyShape) {
        self.selectedArray = @[].mutableCopy;
        if (self.currentUser.body_shape) {
            [self.selectedArray addObject:self.currentUser.body_shape];
        }
        self.title = NSLocalizedString(@"Body Shape", nil);
        self.singleSelectionMode = YES;
    }
    else if (self.type == ADUserDetailItemTypeLanguage) {
        self.selectedArray = [NSMutableArray arrayWithArray:self.currentUser.languageArray];
        self.title = NSLocalizedString(@"Languages", nil);
    }
    else if (self.type == ADUserDetailItemTypeInterest) {
        self.selectedArray = [NSMutableArray arrayWithArray:self.currentUser.interestArray];
        self.title = NSLocalizedString(@"Interests", nil);
    }
    else if (self.type == ADUserDetailItemTypePersonality) {
        self.selectedArray = @[].mutableCopy;
        if (self.currentUser.personality) {
            [self.selectedArray addObject:self.currentUser.personality];
        }
        self.title = NSLocalizedString(@"Personality", nil);
        self.singleSelectionMode = YES;
    }
    else if (self.type == ADUserDetailItemTypeDrinking) {
        self.selectedArray = @[].mutableCopy;
        if (self.currentUser.drinking) {
            [self.selectedArray addObject:self.currentUser.drinking];
        }
        self.title = NSLocalizedString(@"Drinking", nil);
        self.singleSelectionMode = YES;
    }
    else if (self.type == ADUserDetailItemTypeSmoking) {
        self.selectedArray = @[].mutableCopy;
        if (self.currentUser.smoking) {
            [self.selectedArray addObject:self.currentUser.smoking];
        }
        self.title = NSLocalizedString(@"Smoking", nil);
        self.singleSelectionMode = YES;
    }
    else if (self.type == ADUserDetailItemTypeReligion) {
        self.selectedArray = @[].mutableCopy;
        if (self.currentUser.religion) {
            [self.selectedArray addObject:self.currentUser.religion];
        }
        self.title = NSLocalizedString(@"Religion", nil);
        self.singleSelectionMode = YES;
    }
    else if (self.type == ADUserDetailItemTypeEducation) {
        self.selectedArray = @[].mutableCopy;
        if (self.currentUser.education_level) {
            [self.selectedArray addObject:self.currentUser.education_level];
        }
        self.title = NSLocalizedString(@"Education Level", nil);
        self.singleSelectionMode = YES;
    }
    else if (self.type == ADUserDetailItemTypeOccupation) {
        self.selectedArray = @[].mutableCopy;
        if (self.currentUser.occupation) {
            [self.selectedArray addObject:self.currentUser.occupation];
        }
        self.title = NSLocalizedString(@"Occupation", nil);
        self.singleSelectionMode = YES;
    }
    else if (self.type == ADUserDetailItemTypeIncome) {
        self.selectedArray = @[].mutableCopy;
        if (self.currentUser.income) {
            [self.selectedArray addObject:self.currentUser.income];
        }
        self.title = NSLocalizedString(@"Income", nil);
        self.singleSelectionMode = YES;
    }
    else if (self.type == ADUserDetailItemTypeRace) {
        self.selectedArray = @[].mutableCopy;
        if (self.currentUser.race) {
            [self.selectedArray addObject:self.currentUser.race];
        }
        self.title = NSLocalizedString(@"Race", nil);
        self.singleSelectionMode = YES;
    }
    else if (self.type == ADUserDetailItemTypeResidentialStatus) {
        self.selectedArray = @[].mutableCopy;
        if (self.currentUser.residential_status) {
            [self.selectedArray addObject:self.currentUser.residential_status];
        }
        self.title = NSLocalizedString(@"Residential Status", nil);
        self.singleSelectionMode = YES;
    }
    
    // Remove bad data
    NSMutableArray *removeList = @[].mutableCopy;
    for (NSNumber *item in self.selectedArray) {
        NSUInteger index = [self.list indexOfObjectPassingTest:^BOOL(ADManagedListItem *obj, NSUInteger idx, BOOL *stop) {
            return [item isEqualToNumber:obj.lid];
        }];
        if (index == NSNotFound) {
            [removeList addObject:item];
        }
    }
    for (NSString *item in removeList) {
        [self.selectedArray removeObject:item];
    }
    
    [self.tableView reloadData];
}


#pragma mark -
#pragma makr - View lifecycles

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.singleSelectionMode) {
        self.navigationItem.rightBarButtonItem.title = NSLocalizedString(@"Clear", nil);
    }
    else {
        self.navigationItem.rightBarButtonItem.title = NSLocalizedString(@"Clear All", nil);
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    if (self.type == ADUserDetailItemTypeLanguage) {
        [self.currentUser setLanguageArray:self.selectedArray.copy];
    }
    else if (self.type == ADUserDetailItemTypeInterest) {
        [self.currentUser setInterestArray:self.selectedArray.copy];
    }
    else if (self.type == ADUserDetailItemTypePersonality) {
        self.currentUser.personality = [self.selectedArray firstObject];
    }
    else if (self.type == ADUserDetailItemTypeDrinking) {
        self.currentUser.drinking = [self.selectedArray firstObject];
    }
    else if (self.type == ADUserDetailItemTypeSmoking) {
        self.currentUser.smoking = [self.selectedArray firstObject];
    }
    else if (self.type == ADUserDetailItemTypeReligion) {
        self.currentUser.religion = [self.selectedArray firstObject];
    }
    else if (self.type == ADUserDetailItemTypeHeight) {
        self.currentUser.height = [self.selectedArray firstObject];
    }
    else if (self.type == ADUserDetailItemTypeWeight) {
        self.currentUser.weight = [self.selectedArray firstObject];
    }
    else if (self.type == ADUserDetailItemTypeMartialStatus) {
        self.currentUser.martial_status = [self.selectedArray firstObject];
    }
    else if (self.type == ADUserDetailItemTypeBodyShape) {
        self.currentUser.body_shape = [self.selectedArray firstObject];
    }
    else if (self.type == ADUserDetailItemTypeOccupation) {
        self.currentUser.occupation = [self.selectedArray firstObject];
    }
    else if (self.type == ADUserDetailItemTypeIncome) {
        self.currentUser.income = [self.selectedArray firstObject];
    }
    else if (self.type == ADUserDetailItemTypeEducation) {
        self.currentUser.education_level = [self.selectedArray firstObject];
    }
    else if (self.type == ADUserDetailItemTypeRace) {
        self.currentUser.race = [self.selectedArray firstObject];
    }
    else if (self.type == ADUserDetailItemTypeResidentialStatus) {
        self.currentUser.residential_status = [self.selectedArray firstObject];
    }
}


#pragma mark -
#pragma mark Helper methods

- (IBAction)clearButtonClicked:(id)sender
{
    [self.selectedArray removeAllObjects];
    [self.tableView reloadData];
}


#pragma mark -
#pragma makr - UITableView data source methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tableView) {
        return [self.list count];
    }
    else {
        return [self.filteredList count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *array = nil;
    if (tableView == self.tableView) {
        array = self.list;
    }
    else {
        array = self.filteredList;
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    ADManagedListItem *item = array[indexPath.row];
    cell.textLabel.text = [item valueForLanguageCode:self.languageCode];
    
    if ([self.selectedArray containsObject:item.lid]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}


#pragma mark -
#pragma makr - UITableView delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSArray *array = nil;
    if (tableView == self.tableView) {
        array = self.list;
    }
    else {
        array = self.filteredList;
    }

    ADManagedListItem *item = array[indexPath.row];
    NSNumber *key = item.lid;

    if (self.singleSelectionMode) {
        [self.selectedArray removeAllObjects];
        [self.selectedArray addObject:key];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if ([self.selectedArray containsObject:key]) {
            [self.selectedArray removeObject:key];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        else {
            [self.selectedArray addObject:key];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    }
}


#pragma mark -
#pragma makr - UISearchDisplayDelegate

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    NSIndexSet *indexSet = [self.list indexesOfObjectsPassingTest:^BOOL(ADManagedListItem *obj, NSUInteger idx, BOOL *stop) {
        return [[obj valueForLanguageCode:self.languageCode] rangeOfString:searchString options:NSCaseInsensitiveSearch].location != NSNotFound;
    }];
    self.filteredList = [self.list objectsAtIndexes:indexSet];
    
    return YES;
}

@end
