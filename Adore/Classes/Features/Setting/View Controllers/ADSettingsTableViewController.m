//
//  ADSettingsTableViewController.m
//  Adore
//
//  Created by Wang on 2014-06-16.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADSettingsTableViewController.h"
#import "ADUserManager.h"
#import "ADChatManager.h"
#import "ADUserDefaultHelper.h"
#import "AppDelegate.h"
#import "ADImageTextTableViewCell.h"
#import "ADUserCoreDataHelper.h"
#import "SDImageCache.h"
#import "SCLAlertView.h"
#import "ADEULAViewController.h"

typedef NS_ENUM(NSInteger, ADSettingsSectionType)
{
    ADSettingsSectionTypeEULA,
    ADSettingsSectionTypeLogout,
    ADSettingsSectionTypeClearCache,
    
    ADSettingsSectionTypeCount
};



@interface ADSettingsTableViewController () <UITableViewDataSource, UITableViewDelegate>


@property (nonatomic, weak) IBOutlet UITableView *tableView;


@end



@implementation ADSettingsTableViewController

#pragma mark -
#pragma mark - View lifecycles

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Settings", nil);
    
    self.pendingUsersController = [self.userManager.userCoreDataHelper pendingUsersFetchedResultsControllerWithDelegate:self];
    self.unreadMessagesController = [self.chatManager unreadMessagesFetchedResultsControllerWithDelegate:self];
    self.unreadConnectionsController = [self.userManager.userCoreDataHelper unreadConnectedUsersFetchedResultsControllerWithDelegate:self];
}


#pragma mark -
#pragma mark - Helper methods

- (void)logoutCurrentUser
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate logoutCurrentUser];
}

- (void)showLogoutConfirmation
{
    __weak __typeof(self) weakSelf = self;
    
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    alert.showAnimationType = SCLAlertViewShowAnimationFadeIn;
    
    [alert addButton:NSLocalizedString(@"Log off", nil)
         actionBlock:^(void) {
             [weakSelf logoutCurrentUser];
         }];
    
    [alert showWarning:self
                 title:NSLocalizedString(@"Log off", nil)
              subTitle:NSLocalizedString(@"You will lose all your chat history if you logout.", nil)
      closeButtonTitle:NSLocalizedString(@"Cancel", nil)
              duration:0.0f];
}

- (void)clearImageCache
{
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [imageCache clearDisk];
}


#pragma mark -
#pragma mark - NSFetchedResultsControllerDelegate methods

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if (controller == self.pendingUsersController
        || controller == self.unreadMessagesController
        || controller == self.unreadConnectionsController) {
        [self updateDrawerBadge];
    }
}


#pragma mark -
#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ADSettingsSectionTypeCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return tableView.rowHeight;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if (section == ADSettingsSectionTypeLogout) {
        NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
        NSString *version = [infoDict objectForKey:@"CFBundleShortVersionString"];
        NSString *subversion = [infoDict objectForKey:@"CFBundleVersion"];
        NSString *buildTime = [NSString stringWithFormat:@"%s, %s", __DATE__, __TIME__];
        NSString *footerText = [NSString stringWithFormat:@"%@ %@(%@) - %@", NSLocalizedString(@"Version", nil), version, subversion, buildTime];
        return footerText;
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (indexPath.section == ADSettingsSectionTypeLogout) {
        cell.textLabel.text = NSLocalizedString(@"Log off", nil);
        cell.textLabel.textColor = [UIColor redColor];
        return cell;
    }
    else if (indexPath.section == ADSettingsSectionTypeClearCache) {
        cell.textLabel.text = NSLocalizedString(@"Clear Cache", nil);
        cell.textLabel.textColor = [UIColor redColor];
        return cell;
    }
    else if (indexPath.section == ADSettingsSectionTypeEULA) {
        cell.textLabel.text = NSLocalizedString(@"User Agreement", nil);
        cell.textLabel.textColor = [UIColor blackColor];
        return cell;
    }
    return [[UITableViewCell alloc] init];
}


#pragma mark -
#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == ADSettingsSectionTypeLogout) {
        [self showLogoutConfirmation];
    }
    else if (indexPath.section == ADSettingsSectionTypeClearCache) {
        [self clearImageCache];
    }
    else if (indexPath.section == ADSettingsSectionTypeEULA) {
        ADEULAViewController *vc = [[UIStoryboard storyboardWithName:@"ADLoginStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"EULAViewController"];
        vc.viewOnly = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
