//
//  ADProfileEditingTableViewController.m
//  Adore
//
//  Created by Wang on 2015-04-04.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADProfileEditingTableViewController.h"
#import "ADManagedUser+Extensions.h"
#import "ADUserManager.h"
#import "ADUserDetailItem.h"
#import "ADProfileImageCollectionTableViewCell.h"
#import "ADProfileImageCollectionViewCell.h"
#import "ADUserDetailTableViewCell.h"
#import "ADUserObject.h"
#import "UICollectionView+Draggable.h"
#import "TDDatePickerController.h"
#import "UIImage+Resize.h"
#import "SDWebImageManager.h"
#import "ADUpdateUserRequest.h"
#import "NSManagedObject+Extension.h"
#import "ADUserDetailSectionHeaderCell.h"
#import "NSDate+Utils.h"
#import "ADProfileSearchableListViewController.h"
#import "ADTextFieldTableViewCell.h"
#import "SCLAlertView.h"
#import "ADAddressObject.h"

static NSString * const kAD_ADORE_SHOW_SEARCHABLE_LIST_SEGUE = @"ShowSearchableList";


@interface ADProfileEditingTableViewController () <UICollectionViewDelegate, TDDatePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, ADProfileImageCollectionTableViewCellDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *itemArray;
@property (nonatomic, strong) ADManagedUser *userProfile;

@property (nonatomic, strong) TDDatePickerController *datePickerView;

@property (nonatomic, strong) ADUpdateUserRequest *updateUserRequest;
@property (nonatomic, strong) NSMutableDictionary *tempImagesDictionary;
@property (nonatomic, assign) ADUserDetailItemType selectedType;
@property (nonatomic, assign) BOOL hasChanges;

@end



@implementation ADProfileEditingTableViewController

#pragma mark -
#pragma mark - Initializer

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _userProfile = self.userManager.currentUserProfile;
        _itemArray = [_userProfile editableItemArray];
        _tempImagesDictionary = @{}.mutableCopy;
    }
    
    return self;
}

#pragma mark -
#pragma mark - View lifecycles

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_SETTINGS_EDIT_PROFILE];
    self.title = NSLocalizedString(@"Edit Profile", nil);
    
    UINib *collectionNib = [UINib nibWithNibName:@"ADProfileImageCollectionTableViewCell" bundle:nil];
    [self.tableView registerNib:collectionNib forCellReuseIdentifier:kAD_USER_DETAIL_PROFILE_IMAGE_COLLECTION_VIEW_CELL_IDENTIFIER];
    UINib *userDetailNib = [UINib nibWithNibName:@"ADUserDetailTableViewCell" bundle:nil];
    [self.tableView registerNib:userDetailNib forCellReuseIdentifier:kAD_USER_DETAIL_TABLE_VIEW_CELL_IDENTIFIER];
    UINib *sectionHeaderNib = [UINib nibWithNibName:@"ADUserDetailSectionHeaderCell" bundle:nil];
    [self.tableView registerNib:sectionHeaderNib forCellReuseIdentifier:kAD_USER_DETAIL_SECTION_HEADER_IDENTIFIER];
    UINib *textFieldCellNib = [UINib nibWithNibName:@"ADTextFieldTableViewCell" bundle:nil];
    [self.tableView registerNib:textFieldCellNib forCellReuseIdentifier:kAD_TEXT_FIELD_TABLE_VIEW_CELL_IDENTIFIER];
    
    self.datePickerView = [[TDDatePickerController alloc] initWithNibName:@"TDDatePickerController"
                                                                   bundle:nil];
    self.datePickerView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    [self registerKeyboardNotification];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self deregisterKeyboardNotification];
    [super viewWillDisappear:animated];
}

-(void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;

    UIEdgeInsets contentInsets;
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height), 0.0);
    } else {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width), 0.0);
    }
   
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
    [UIView commitAnimations];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.tableView.scrollIndicatorInsets = self.tableView.contentInset;
    
    [UIView commitAnimations];
}


#pragma mark -
#pragma mark - Helper methods

- (void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)registerNotificationReceiver
{
    __weak typeof(self) weakSelf = self;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kAD_NOTIFICATION_USER_PROFILE_IMAGE_UPDATED
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          weakSelf.hasChanges = YES;
                                                          [weakSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
                                                      });
                                                  }];
}

- (IBAction)updateUserProfile:(id)sender
{
    [self dismissKeyboard];
    
    if (self.hasChanges) {
        __weak __typeof(self) weakSelf = self;

        [self showLoadingIndicator:NSLocalizedString(@"Saving...", nil)];
        [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_SETTINGS_SAVE_PROFILE];
        UpdateUserRequestCompletionBlock completionBlock = ^(BOOL response, NSError *error) {
            if (response) {
                [weakSelf dismissLoadingIndicator];
                
                [weakSelf.tempImagesDictionary removeAllObjects];
                [weakSelf.tableView reloadData];
                
                [weakSelf.delegate reloadView];
                
                [weakSelf dismissViewControllerAnimated:YES completion:nil];
            }
            else {
                
            }
        };
        self.updateUserRequest = [[ADUpdateUserRequest alloc] initWithUser:self.userProfile tempImages:self.tempImagesDictionary CompletionHandler:completionBlock];
    }
    else {
        [self cancelButtonClicked:nil];
    }
}

- (IBAction)cancelButtonClicked:(id)sender
{
    [self dismissKeyboard];
    [self.userProfile revertChanges];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)profileImageUploadButtonClicked:(id)sender
{
    [self dismissKeyboard];
    
    BOOL photoLibraryAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
    BOOL cameraAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    if (photoLibraryAvailable && cameraAvailable) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:NSLocalizedString(@"Take Photo", nil), NSLocalizedString(@"Choose Existing Photo", nil), nil];
        actionSheet.tag = ADUserDetailItemTypeProfileImage;
        [actionSheet showInView:self.view];
    }
    else if (photoLibraryAvailable) {
        [self showPhotoLibraryPicker];
    }
    else if (cameraAvailable) {
        [self showCameraPicker];
    }
    else {
        [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_NO_IMAGE_SOURCE];
        [self showErrorMessage:NSLocalizedString(@"No source is available.", nil)];
    }
}

- (void)showBirthdayPicker
{
    [self dismissKeyboard];
    [self presentSemiModalViewController:self.datePickerView];
    self.datePickerView.datePicker.date = self.userProfile.birthday;
    self.datePickerView.datePicker.backgroundColor = [UIColor whiteColor];
}

- (void)showPhotoLibraryPicker
{
    [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_IMAGE_PHOTO_LIBRARY];
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.navigationBar.translucent = NO;
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)showCameraPicker
{
    [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_IMAGE_CAMERA];
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.navigationBar.translucent = NO;
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)showAddressPickerForType:(ADUserDetailItemType)type
{
    __weak __typeof(self) weakSelf = self;
    
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    alert.showAnimationType = SCLAlertViewShowAnimationFadeIn;
    
    UITextField *textField = [alert addTextField:NSLocalizedString(@"", nil)];
    textField.text = (type == ADUserDetailItemTypeCurrentResidence) ? [self.userProfile currentResidenceString] : [self.userProfile birthPlaceString];
    
    [alert addButton:NSLocalizedString(@"Done", nil)
     validationBlock:^BOOL{
         return textField.text.length > 0;
     }
         actionBlock:^(void) {
             [weakSelf showLoadingIndicator:nil];
             
             CLGeocoder *clGeo = [[CLGeocoder alloc] init];
             CLGeocodeCompletionHandler completionBlock = ^(NSArray *placemarks, NSError *error) {
                 [weakSelf dismissLoadingIndicator];
                 CLPlacemark *place = [placemarks firstObject];
                 if (place != nil) {
                     ADAddressObject *address = [ADAddressObject addressObjectWithDictionary:place.addressDictionary];
                     SCLAlertView *confirmAlert = [[SCLAlertView alloc] initWithNewWindow];
                     
                     [confirmAlert addButton:NSLocalizedString(@"OK", nil)
                                 actionBlock:^(void) {
                                     self.hasChanges = YES;
                                     
                                     if (type == ADUserDetailItemTypeCurrentResidence) {
                                         self.userProfile.residence_country = address.country;
                                         self.userProfile.residence_province = address.province;
                                         self.userProfile.residence_city = address.city;
                                     }
                                     else if (type == ADUserDetailItemTypeBirthPlace) {
                                         self.userProfile.birth_country = address.country;
                                         self.userProfile.birth_province = address.province;
                                         self.userProfile.birth_city = address.city;
                                     }
                                     [weakSelf.tableView reloadData];
                                 }];
                     
                     NSString *subtitle = nil;
                     if (type == ADUserDetailItemTypeCurrentResidence) {
                         subtitle = NSLocalizedString(@"Do you want to use this address as your current residence?", nil);
                     }
                     else if (type == ADUserDetailItemTypeBirthPlace) {
                         subtitle = NSLocalizedString(@"Do you want to use this address as your birth place?", nil);
                     }
                     
                     [confirmAlert showInfo:weakSelf
                                      title:[address addressString]
                                   subTitle:subtitle
                           closeButtonTitle:NSLocalizedString(@"Cancel", nil)
                                   duration:0.0f];
                 }
                 else {
                     [self showErrorMessage:NSLocalizedString(@"We couldn't find any address for you. Please try something else.", nil)];
                 }
             };
             [clGeo geocodeAddressString:textField.text completionHandler:completionBlock];
         }];
    
    [alert showEdit:self
              title:(type == ADUserDetailItemTypeCurrentResidence) ? NSLocalizedString(@"Current Residence", nil) : NSLocalizedString(@"Birth Place", nil)
           subTitle:NSLocalizedString(@"Search for an address or a postal code", nil)
   closeButtonTitle:NSLocalizedString(@"Cancel", nil)
           duration:0.0f];
}


#pragma mark -
#pragma mark - Image Picker Helper

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *img = [info objectForKey:UIImagePickerControllerEditedImage];
    if (img == nil) {
        img = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    img = [img scaleToSizeKeepAspect:CGSizeMake(640, 640)];
    img = [img compressedImage];

    NSString *randomImageURL = [NSString stringWithFormat:@"%@%@", KAD_IMAGE_URL_PREFIX, [[NSUUID UUID] UUIDString]];
    [[SDWebImageManager sharedManager] saveImageToCache:img forURL:[NSURL URLWithString:randomImageURL]];
    self.tempImagesDictionary[randomImageURL] = img;
    
    self.hasChanges = YES;
    [self.userProfile addUserPhoto:randomImageURL];
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark - UITextField Methods

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.hasChanges = YES;
    
    ADUserDetailItemType type = textField.tag;
    switch (type) {
        case ADUserDetailItemTypeNickname:
            self.userProfile.nickname = textField.text;
            break;
            
        case ADUserDetailItemTypeName:
            self.userProfile.fullname = textField.text;
            break;
            
        case ADUserDetailItemTypeMotto:
            self.userProfile.motto = textField.text;
            break;
            
        case ADUserDetailItemTypeUndergradEdu:
            self.userProfile.undergrad_edu_institution = textField.text;
            break;
            
        case ADUserDetailItemTypePostgradEdu:
            self.userProfile.postgrad_edu_institution = textField.text;
            break;
            
        case ADUserDetailItemTypeEmployer:
            self.userProfile.employer = textField.text;
            break;
            
        default:
            break;
    }
}


#pragma mark -
#pragma mark - UIActionSheet Methods

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (actionSheet.cancelButtonIndex == buttonIndex) {
        return;
    }
    
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if (actionSheet.tag == ADUserDetailItemTypeProfileImage) {
        if ([buttonTitle isEqualToString:NSLocalizedString(@"Take Photo", nil)]) {
            [self showCameraPicker];
        }
        else if ([buttonTitle isEqualToString:NSLocalizedString(@"Choose Existing Photo", nil)]){
            [self showPhotoLibraryPicker];
        }
        return;
    }
    
    self.hasChanges = YES;
    switch (actionSheet.tag) {
            
        default:
            break;
    }
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma mark -
#pragma mark - TDDatePickerControllerDelegate Methods

-(void)datePickerSetDate:(TDDatePickerController*)viewController
{
    self.hasChanges = YES;
    
    self.userProfile.birthday = viewController.datePicker.date;
    [self dismissSemiModalViewController:viewController];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(void)datePickerCancel:(TDDatePickerController*)viewController
{
    [self dismissSemiModalViewController:viewController];
}


#pragma mark -
#pragma mark - Collection view delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissKeyboard];
    
    // Add button
    if (indexPath.row == [self.userProfile profileImageArray].count) {
        [self profileImageUploadButtonClicked:nil];
    }
}



#pragma mark -
#pragma mark - ADProfileImageCollectionTableViewCellDelegate methods
- (void)setUserProfileImagesOrderingUpdated:(BOOL)isUpdated
{
    _hasChanges = isUpdated;
}


#pragma mark -
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    else if (section == 1) {
        return self.itemArray.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row	== 0) {
        ADProfileImageCollectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_USER_DETAIL_PROFILE_IMAGE_COLLECTION_VIEW_CELL_IDENTIFIER];
        cell.userObject = self.userProfile;
        cell.isEditing = YES;
        return [cell cellHeight];
    }
    else if (indexPath.section == 1) {
        ADUserDetailItem *item = self.itemArray[indexPath.row];
        if ([item isHeader]) {
            ADUserDetailSectionHeaderCell *cell = [ADUserDetailSectionHeaderCell userDetailSectionHeaderCellForTableView:tableView type:item.type];
            return [cell cellHeight];
        }
        else {
            CGFloat height = [self heightOfUserDetailCellForTable:tableView rowAtIndexPath:indexPath];
            return height;
        }
    }
    return tableView.rowHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return CGFLOAT_MIN;
    }
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        ADProfileImageCollectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_USER_DETAIL_PROFILE_IMAGE_COLLECTION_VIEW_CELL_IDENTIFIER];
        cell.delegate = self;
        cell.userObject = self.userProfile;
        cell.isEditing = YES;
        cell.collectionView.delegate = self;
        cell.collectionView.draggable = YES;
        [cell.collectionView reloadData];
        return cell;
    }
    else if (indexPath.section == 1) {
        ADUserDetailItem *item = self.itemArray[indexPath.row];
        if ([item isHeader]) {
            return [ADUserDetailSectionHeaderCell userDetailSectionHeaderCellForTableView:tableView type:item.type];
        }
        else {
            UITableViewCell *cell = [self userDetailCellForTable:tableView rowAtIndexPath:indexPath];
            switch (item.type) {
                case ADUserDetailItemTypeHeaderBasicInfo:
                case ADUserDetailItemTypeHeaderDetails:
                case ADUserDetailItemTypeHeaderWorkEdu:
                case ADUserDetailItemTypeProfileImage:
                case ADUserDetailItemTypeEmail:
                case ADUserDetailItemTypeName:
                case ADUserDetailItemTypeMotto:
                case ADUserDetailItemTypeNickname:
                case ADUserDetailItemTypeBirthday:
                case ADUserDetailItemTypeZodiac:
                case ADUserDetailItemTypeC_Zodiac:
                case ADUserDetailItemTypeGender:
                case ADUserDetailItemTypeUndergradEdu:
                case ADUserDetailItemTypePostgradEdu:
                case ADUserDetailItemTypeEmployer:
                    break;
                    
                case ADUserDetailItemTypeBodyShape:
                case ADUserDetailItemTypeWeight:
                case ADUserDetailItemTypeHeight:
                case ADUserDetailItemTypeMartialStatus:
                case ADUserDetailItemTypeReligion:
                case ADUserDetailItemTypeDrinking:
                case ADUserDetailItemTypeSmoking:
                case ADUserDetailItemTypeEducation:
                case ADUserDetailItemTypeOccupation:
                case ADUserDetailItemTypeIncome:
                case ADUserDetailItemTypeLanguage:
                case ADUserDetailItemTypeInterest:
                case ADUserDetailItemTypePersonality:
                case ADUserDetailItemTypeRace:
                case ADUserDetailItemTypeResidentialStatus:
                case ADUserDetailItemTypeBirthPlace:
                case ADUserDetailItemTypeCurrentResidence:
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            return cell;
        }
    }
    
    return nil;
}

- (UITableViewCell *)userDetailCellForTable:(UITableView *)tableView rowAtIndexPath:(NSIndexPath *)indexPath
{
    ADUserDetailItem *item = self.itemArray[indexPath.row];
    switch (item.type) {
        case ADUserDetailItemTypeHeaderBasicInfo:
        case ADUserDetailItemTypeHeaderDetails:
        case ADUserDetailItemTypeHeaderWorkEdu:
        case ADUserDetailItemTypeProfileImage:
        case ADUserDetailItemTypeEmail:
        case ADUserDetailItemTypeBirthday:
        case ADUserDetailItemTypeWeight:
        case ADUserDetailItemTypeHeight:
        case ADUserDetailItemTypeMartialStatus:
        case ADUserDetailItemTypeBodyShape:
        case ADUserDetailItemTypeLanguage:
        case ADUserDetailItemTypeInterest:
        case ADUserDetailItemTypePersonality:
        case ADUserDetailItemTypeZodiac:
        case ADUserDetailItemTypeC_Zodiac:
        case ADUserDetailItemTypeGender:
        case ADUserDetailItemTypeReligion:
        case ADUserDetailItemTypeDrinking:
        case ADUserDetailItemTypeSmoking:
        case ADUserDetailItemTypeIncome:
        case ADUserDetailItemTypeOccupation:
        case ADUserDetailItemTypeEducation:
        case ADUserDetailItemTypeRace:
        case ADUserDetailItemTypeResidentialStatus:
        case ADUserDetailItemTypeBirthPlace:
        case ADUserDetailItemTypeCurrentResidence:
        {
            ADUserDetailTableViewCell *cell = [ADUserDetailTableViewCell userDetailCellForTableView:tableView type:item.type userData:self.userProfile];
            cell.detailLabel.textColor = [[UIApplication sharedApplication].delegate window].tintColor;
            return cell;
        }
            
        case ADUserDetailItemTypeName:
        case ADUserDetailItemTypeMotto:
        case ADUserDetailItemTypeNickname:
        case ADUserDetailItemTypeUndergradEdu:
        case ADUserDetailItemTypePostgradEdu:
        case ADUserDetailItemTypeEmployer:
        {
            ADTextFieldTableViewCell *cell = [ADTextFieldTableViewCell textFieldCellForTableView:tableView type:item.type userData:self.userProfile delegate:self];
            return cell;
        }
    }
    
    return nil;
}

- (CGFloat)heightOfUserDetailCellForTable:(UITableView *)tableView rowAtIndexPath:(NSIndexPath *)indexPath
{
    ADUserDetailItem *item = self.itemArray[indexPath.row];
    switch (item.type) {
        case ADUserDetailItemTypeHeaderBasicInfo:
        case ADUserDetailItemTypeHeaderDetails:
        case ADUserDetailItemTypeHeaderWorkEdu:
        case ADUserDetailItemTypeProfileImage:
        case ADUserDetailItemTypeEmail:
        case ADUserDetailItemTypeBirthday:
        case ADUserDetailItemTypeWeight:
        case ADUserDetailItemTypeHeight:
        case ADUserDetailItemTypeMartialStatus:
        case ADUserDetailItemTypeBodyShape:
        case ADUserDetailItemTypeLanguage:
        case ADUserDetailItemTypeInterest:
        case ADUserDetailItemTypePersonality:
        case ADUserDetailItemTypeZodiac:
        case ADUserDetailItemTypeC_Zodiac:
        case ADUserDetailItemTypeGender:
        case ADUserDetailItemTypeReligion:
        case ADUserDetailItemTypeDrinking:
        case ADUserDetailItemTypeSmoking:
        case ADUserDetailItemTypeIncome:
        case ADUserDetailItemTypeOccupation:
        case ADUserDetailItemTypeEducation:
        case ADUserDetailItemTypeRace:
        case ADUserDetailItemTypeResidentialStatus:
        case ADUserDetailItemTypeBirthPlace:
        case ADUserDetailItemTypeCurrentResidence:
        {
            ADUserDetailTableViewCell *cell = [ADUserDetailTableViewCell userDetailCellForTableView:tableView type:item.type userData:self.userProfile];
            return [cell cellHeight];
        }
            
        case ADUserDetailItemTypeName:
        case ADUserDetailItemTypeMotto:
        case ADUserDetailItemTypeNickname:
        case ADUserDetailItemTypeUndergradEdu:
        case ADUserDetailItemTypePostgradEdu:
        case ADUserDetailItemTypeEmployer:
        {
            ADTextFieldTableViewCell *cell = [ADTextFieldTableViewCell textFieldCellForTableView:tableView type:item.type userData:self.userProfile delegate:self];
            return [cell cellHeight];
        }
    }
    
}


#pragma mark -
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self dismissKeyboard];
    
    ADUserDetailItem *item = [self.itemArray objectAtIndex:indexPath.row];
    switch (item.type)
    {
        case ADUserDetailItemTypeBirthday:
            [self showBirthdayPicker];
            break;
            
        case ADUserDetailItemTypeLanguage:
        case ADUserDetailItemTypeInterest:
        case ADUserDetailItemTypePersonality:
        case ADUserDetailItemTypeDrinking:
        case ADUserDetailItemTypeSmoking:
        case ADUserDetailItemTypeReligion:
        case ADUserDetailItemTypeWeight:
        case ADUserDetailItemTypeHeight:
        case ADUserDetailItemTypeMartialStatus:
        case ADUserDetailItemTypeBodyShape:
        case ADUserDetailItemTypeIncome:
        case ADUserDetailItemTypeOccupation:
        case ADUserDetailItemTypeEducation:
        case ADUserDetailItemTypeRace:
        case ADUserDetailItemTypeResidentialStatus:
            self.selectedType = item.type;
            [self performSegueWithIdentifier:kAD_ADORE_SHOW_SEARCHABLE_LIST_SEGUE sender:nil];
            break;
            
        case ADUserDetailItemTypeBirthPlace:
        case ADUserDetailItemTypeCurrentResidence:
            [self showAddressPickerForType:item.type];
            break;
            
        default:
            break;
            
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kAD_ADORE_SHOW_SEARCHABLE_LIST_SEGUE]) {
        ADProfileSearchableListViewController *vc = segue.destinationViewController;
        vc.type = self.selectedType;
        self.hasChanges = YES;
    }
}

@end
