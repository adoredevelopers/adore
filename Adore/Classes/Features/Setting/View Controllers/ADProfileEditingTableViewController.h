//
//  ADProfileEditingTableViewController.h
//  Adore
//
//  Created by Wang on 2015-04-04.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADViewController.h"


@protocol ADProfileDetailTableViewControllerDelegate <NSObject>

- (void)reloadView;

@end


@interface ADProfileEditingTableViewController : ADViewController

@property (nonatomic,retain) id delegate;

@end
