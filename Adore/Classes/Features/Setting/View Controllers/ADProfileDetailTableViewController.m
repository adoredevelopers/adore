//
//  ADProfileDetailTableViewController.m
//  Adore
//
//  Created by Wang on 2014-06-22.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADProfileDetailTableViewController.h"
#import "ADManagedUser+Extensions.h"
#import "ADUserManager.h"
#import "ADUserDetailItem.h"
#import "ADProfileImageCollectionTableViewCell.h"
#import "ADProfileImageCollectionViewCell.h"
#import "ADUserDetailTableViewCell.h"
#import "ADUserDetailSectionHeaderCell.h"
#import "ADUserCoreDataHelper.h"
#import "ADChatManager.h"
#import "NYTPhotosViewController.h"
#import "ADNYTPhoto.h"
#import "ADProfileEditingTableViewController.h"

static NSString * const kAD_ADORE_EDIT_PROFILE_DETAILS_SEGUE = @"EditProfileDetails";


@interface ADProfileDetailTableViewController () <UICollectionViewDelegate, ADProfileDetailTableViewControllerDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) ADManagedUser *userProfile;
@property (nonatomic, strong) NSArray *itemArray;
@property (nonatomic, strong) NSArray *photos;


@end


@implementation ADProfileDetailTableViewController

#pragma mark -
#pragma mark - Initializer

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _userProfile = self.userManager.currentUserProfile;
    }
    
    return self;
}


#pragma mark -
#pragma mark - View lifecycles

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_SETTINGS_VIEW_PROFILE];
    self.title = NSLocalizedString(@"My Profile", nil);
    
    self.pendingUsersController = [self.userManager.userCoreDataHelper pendingUsersFetchedResultsControllerWithDelegate:self];
    self.unreadMessagesController = [self.chatManager unreadMessagesFetchedResultsControllerWithDelegate:self];
    self.unreadConnectionsController = [self.userManager.userCoreDataHelper unreadConnectedUsersFetchedResultsControllerWithDelegate:self];
    
    UINib *collectionNib = [UINib nibWithNibName:@"ADProfileImageCollectionTableViewCell" bundle:nil];
    [self.tableView registerNib:collectionNib forCellReuseIdentifier:kAD_USER_DETAIL_PROFILE_IMAGE_COLLECTION_VIEW_CELL_IDENTIFIER];
    UINib *userDetailNib = [UINib nibWithNibName:@"ADUserDetailTableViewCell" bundle:nil];
    [self.tableView registerNib:userDetailNib forCellReuseIdentifier:kAD_USER_DETAIL_TABLE_VIEW_CELL_IDENTIFIER];
    UINib *sectionHeaderNib = [UINib nibWithNibName:@"ADUserDetailSectionHeaderCell" bundle:nil];
    [self.tableView registerNib:sectionHeaderNib forCellReuseIdentifier:kAD_USER_DETAIL_SECTION_HEADER_IDENTIFIER];
    
    [self reloadView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    [self reloadView];
}


#pragma mark -
#pragma mark - Helper methods
#pragma mark - ADProfileDetailTableViewControllerDelegate method

- (void)reloadView
{
    self.itemArray = [self.userProfile profileUserDetailItemArray];
    self.photos = [self.userProfile arrayOfNYTPhotos];
    [self.tableView reloadData];
}

- (void)showPhotoBrowserWithIndex:(NSInteger)index
{
    if (index < 0 || index >= self.photos.count) {
        return;
    }
    
    NYTPhotosViewController *photosViewController = [[NYTPhotosViewController alloc] initWithPhotos:self.photos initialPhoto:self.photos[index]];
    [self presentViewController:photosViewController animated:YES completion:nil];
}

#pragma mark -
#pragma mark - NSFetchedResultsControllerDelegate methods

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if (controller == self.pendingUsersController
        || controller == self.unreadMessagesController
        || controller == self.unreadConnectionsController) {
        [self updateDrawerBadge];
    }
}


#pragma mark -
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    else if (section == 1) {
        return self.itemArray.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row	== 0) {
        ADProfileImageCollectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_USER_DETAIL_PROFILE_IMAGE_COLLECTION_VIEW_CELL_IDENTIFIER];
        cell.userObject = self.userProfile;
        return [cell cellHeight];
    }
    else if (indexPath.section == 1) {
        ADUserDetailItem *item = self.itemArray[indexPath.row];
        if ([item isHeader]) {
            ADUserDetailSectionHeaderCell *cell = [self userDetailSectionHeaderForTable:tableView rowAtIndexPath:indexPath];
            return [cell cellHeight];
        }
        else {
            ADUserDetailTableViewCell *cell = [self userDetailCellForTable:tableView rowAtIndexPath:indexPath];
            return [cell cellHeight];
        }
    }
    return tableView.rowHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return CGFLOAT_MIN;
    }
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        ADProfileImageCollectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_USER_DETAIL_PROFILE_IMAGE_COLLECTION_VIEW_CELL_IDENTIFIER];
        cell.userObject = self.userProfile;
        cell.collectionView.delegate = self;
        [cell.collectionView reloadData];
        return cell;
    }
    else if (indexPath.section == 1) {
        ADUserDetailItem *item = self.itemArray[indexPath.row];
        if ([item isHeader]) {
            return [self userDetailSectionHeaderForTable:tableView rowAtIndexPath:indexPath];
        }
        else {
            return [self userDetailCellForTable:tableView rowAtIndexPath:indexPath];
        }
    }
    
    return [[UITableViewCell alloc] init];
}

- (ADUserDetailSectionHeaderCell *)userDetailSectionHeaderForTable:(UITableView *)tableView rowAtIndexPath:(NSIndexPath *)indexPath
{
    ADUserDetailItem *item = self.itemArray[indexPath.row];
    ADUserDetailSectionHeaderCell *cell = [ADUserDetailSectionHeaderCell userDetailSectionHeaderCellForTableView:tableView type:item.type];
    return cell;
}

- (ADUserDetailTableViewCell *)userDetailCellForTable:(UITableView *)tableView rowAtIndexPath:(NSIndexPath *)indexPath
{
    ADUserDetailItem *item = self.itemArray[indexPath.row];
    ADUserDetailTableViewCell *cell = [ADUserDetailTableViewCell userDetailCellForTableView:tableView type:item.type userData:self.userProfile];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}


#pragma mark -
#pragma mark - Collection View Controller Data Source

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_SETTINGS_VIEW_PROFILE_IMAGE];
    
    [self showPhotoBrowserWithIndex:indexPath.item];
}


#pragma mark -
#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kAD_ADORE_EDIT_PROFILE_DETAILS_SEGUE]) {
        ADProfileEditingTableViewController *vc = segue.destinationViewController.childViewControllers[0];
        vc.delegate = self;
    }
}


@end
