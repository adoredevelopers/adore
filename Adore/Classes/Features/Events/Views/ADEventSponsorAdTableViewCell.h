//
//  ADEventSponsorAdTableViewCell.h
//  Adore
//
//  Created by Chao Lu on 2016-03-16.
//  Copyright © 2016 AdoreMobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADNearbyEventObject.h"
#import "YYKit.h"

@interface ADEventSponsorAdTableViewCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UIImageView *adImageView;
@property (nonatomic, weak) IBOutlet UIImageView *verticalDivider;

@property (nonatomic, weak) IBOutlet YYLabel *adTitleLabel;
@property (nonatomic, weak) IBOutlet YYLabel *adSponsorName;
@property (nonatomic, weak) IBOutlet YYLabel *adDescriptionLabel;
@property (nonatomic, weak) IBOutlet UIImageView *adLocationImageView;
@property (nonatomic, weak) IBOutlet YYLabel *adLocationLabel;


- (void)updateViewWithNearbyEventObject:(ADNearbyEventObject *)nearbyEvent;


@end
