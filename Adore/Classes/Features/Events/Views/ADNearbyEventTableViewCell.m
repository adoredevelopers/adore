//
//  ADNearbyEventTableViewCell.m
//  Adore
//
//  Created by Chao Lu on 2016-03-01.
//  Copyright © 2016 AdoreMobile Inc. All rights reserved.
//

#import "ADNearbyEventTableViewCell.h"
#import "YYKit.h"
#import "SDWebImage/UIImageView+WebCache.h"

@implementation ADNearbyEventTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)updateViewWithNearbyEventObject:(ADNearbyEventObject *)nearbyEvent
{
    [self.eventPosterImageView sd_setImageWithURL:[NSURL URLWithString:nearbyEvent.event_image] placeholderImage:[UIImage imageNamed:@"profile_image_placebolder_150x150"] options:SDWebImageRetryFailed | SDWebImageLowPriority];
    
    self.eventTitleLabel.text = nearbyEvent.event_title;
    
    NSMutableAttributedString *eventTagAttrString = [NSMutableAttributedString new];
    NSAttributedString *eventTagLabelString = [self buildEventTagWithText:nearbyEvent.event_type strokeColor:UIColorHex(fa3f39) fillColor:UIColorHex(fb6560)];
    [eventTagAttrString appendAttributedString:eventTagLabelString];
    for (NSString *eventTag in nearbyEvent.event_tags) {
        eventTagLabelString = [self buildEventTagWithText:eventTag strokeColor:UIColorHex(f48f25) fillColor:UIColorHex(f6a550)];
        [eventTagAttrString appendAttributedString:eventTagLabelString];
    }
    eventTagAttrString.lineSpacing = 5;
    self.eventTagsLabel.numberOfLines = 1;
    self.eventTagsLabel.attributedText = eventTagAttrString;
    
    self.eventDescriptionLabel.text = nearbyEvent.event_description;
    
    self.eventLocationLabel.text = [NSString stringWithFormat:@"%@, %@", nearbyEvent.event_location_city_region, nearbyEvent.event_location_province_state_region];
}

- (NSAttributedString *)buildEventTagWithText:(NSString *)tagText strokeColor:(UIColor *)strokeColor fillColor:(UIColor *)fillColor
{
    UIColor *tagStrokeColor = strokeColor;
    UIColor *tagFillColor = fillColor;
    NSMutableAttributedString *tagAttrText = [[NSMutableAttributedString alloc] initWithString:tagText];
    [tagAttrText insertString:@"  " atIndex:0];
    [tagAttrText appendString:@"  "];
    UIFont *font = [UIFont boldSystemFontOfSize:12];
    tagAttrText.font = font;
    tagAttrText.color = [UIColor whiteColor];
    [tagAttrText setTextBinding:[YYTextBinding bindingWithDeleteConfirm:NO] range:tagText.rangeOfAll];
    
    YYTextBorder *border = [YYTextBorder new];
    border.strokeWidth = 0.0;
    border.strokeColor = tagStrokeColor;
    border.fillColor = tagFillColor;
    border.cornerRadius = 50; // a huge value
    border.insets = UIEdgeInsetsMake(-2, -5.5, -2, -5.5);
    [tagAttrText setTextBackgroundBorder:border range:[tagAttrText.string rangeOfString:tagText]];
    
    return tagAttrText;
}

@end
