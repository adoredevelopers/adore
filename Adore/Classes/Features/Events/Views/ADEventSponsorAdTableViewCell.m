//
//  ADEventSponsorAdTableViewCell.m
//  Adore
//
//  Created by Chao Lu on 2016-03-16.
//  Copyright © 2016 AdoreMobile Inc. All rights reserved.
//

#import "ADEventSponsorAdTableViewCell.h"
#import "SDWebImage/UIImageView+WebCache.h"

@implementation ADEventSponsorAdTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateViewWithNearbyEventObject:(ADNearbyEventObject *)nearbyEvent
{
    [self.adImageView sd_setImageWithURL:[NSURL URLWithString:nearbyEvent.event_image] placeholderImage:[UIImage imageNamed:@"profile_image_placebolder_150x150"] options:SDWebImageRetryFailed];
    
    self.adTitleLabel.text = nearbyEvent.event_title;
    
    self.adSponsorName.attributedText = [[NSAttributedString alloc] initWithString:nearbyEvent.event_organizer];
    
    self.adDescriptionLabel.text = nearbyEvent.event_description;
    
    self.adLocationLabel.text = [NSString stringWithFormat:@"%@, %@", nearbyEvent.event_location_city_region, nearbyEvent.event_location_province_state_region];
}

@end
