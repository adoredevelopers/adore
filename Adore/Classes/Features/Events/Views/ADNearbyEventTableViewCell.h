//
//  ADNearbyEventTableViewCell.h
//  Adore
//
//  Created by Chao Lu on 2016-03-01.
//  Copyright © 2016 AdoreMobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADNearbyEventObject.h"
#import "YYKit.h"

@interface ADNearbyEventTableViewCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UIImageView *eventPosterImageView;
@property (nonatomic, weak) IBOutlet UIImageView *verticalDivider;

@property (nonatomic, weak) IBOutlet YYLabel *eventTitleLabel;
@property (nonatomic, weak) IBOutlet YYLabel *eventTagsLabel;
@property (nonatomic, weak) IBOutlet YYLabel *eventDescriptionLabel;
@property (nonatomic, weak) IBOutlet UIImageView *eventLocationImageView;
@property (nonatomic, weak) IBOutlet YYLabel *eventLocationLabel;


- (void)updateViewWithNearbyEventObject:(ADNearbyEventObject *)nearbyEvent;


@end
