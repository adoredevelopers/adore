//
//  ADEventDetailsViewController.m
//  Adore
//
//  Created by Chao Lu on 2016-03-15.
//  Copyright © 2016 AdoreMobile Inc. All rights reserved.
//

#import "ADEventDetailsViewController.h"

@implementation ADEventDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = self.event.event_title;
    
    [self showLoadingIndicator:@"Loading..."];
    
    NSString *eventDetailsUrlString = [NSString stringWithFormat:@"%@:%@%@%@", kAD_SERVER_HOST, kAD_SERVER_PORT, @"/events/event_details/", self.event.event_id];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:eventDetailsUrlString]];
    [self.webView loadRequest:request];
    
    [self.webView setDelegate:self];
    
    [self.webView.scrollView setDelegate:self];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self dismissLoadingIndicator];
}

- (IBAction)back:(id)sender {
    [self.webView goBack];
}

- (IBAction)forward:(id)sender {
    [self.webView goForward];
}

- (IBAction)refresh:(id)sender {
    [self.webView reload];
}

@end