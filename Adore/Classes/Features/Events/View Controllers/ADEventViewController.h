//
//  ADEventViewController.h
//  Adore
//
//  Created by Chao Lu on 2016-03-05.
//  Copyright © 2016 AdoreMobile Inc. All rights reserved.
//

#import "ADViewController.h"

@interface ADEventViewController : ADViewController

- (IBAction)refreshContent:(id)sender;

@end
