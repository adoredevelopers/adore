//
//  ADEventViewController.m
//  Adore
//
//  Created by Chao Lu on 2016-03-05.
//  Copyright © 2016 AdoreMobile Inc. All rights reserved.
//

#import "ADEventViewController.h"
#import "CarbonKit.h"
//#import "ViewControllerOne.h"
//#import "ViewControllerTwo.h"
//#import "ViewControllerThree.h"
#import "ADNearbyEventViewController.h"

@interface ADEventViewController () <CarbonTabSwipeNavigationDelegate>
{
    NSArray *items;
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    NSNumber *currentIndex;
}

@end

@implementation ADEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Events";
    
    items = @[@"Events", @"Past Events", @"My Events"];
    
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
    
    [self style];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)style {
    
    UIColor *color = [UIColor colorWithRed:24.0/255 green:75.0/255 blue:152.0/255 alpha:1];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = color;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    [carbonTabSwipeNavigation setIndicatorColor:color];
    //	[carbonTabSwipeNavigation setTabExtraWidth:30];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat tabWidth = screenWidth / 3;
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:tabWidth forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:tabWidth forSegmentAtIndex:1];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:tabWidth forSegmentAtIndex:2];
    
    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:[color colorWithAlphaComponent:0.6]
                                        font:[UIFont boldSystemFontOfSize:14]];
    [carbonTabSwipeNavigation setSelectedColor:color
                                          font:[UIFont boldSystemFontOfSize:14]];
}

# pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    switch (index) {
        case 0:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"ADNearbyEventViewController"];
            
        case 1:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"ADNearbyEventViewController"];
            
        default:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"ADNearbyEventViewController"];
    }
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
    switch(index) {
        case 0:
            self.title = @"Events";
            currentIndex = @0;
            break;
        case 1:
            self.title = @"Events";
            currentIndex = @1;
            break;
        case 2:
            self.title = @"Events";
            currentIndex = @2;
            break;
        default:
            self.title = items[index];
            break;
    }
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
    NSLog(@"Did move at index: %ld", index);
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}

- (void)refreshContent:(id)sender {
    ADViewController *vc = (ADViewController *)[carbonTabSwipeNavigation.viewControllers objectForKey:currentIndex];
    
    [vc refreshContent];
}

@end
