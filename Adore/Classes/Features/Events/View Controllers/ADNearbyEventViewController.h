//
//  ADNearbyEventViewController.h
//  Adore
//
//  Created by Chao Lu on 2016-02-28.
//  Copyright © 2016 AdoreMobile Inc. All rights reserved.
//

#import "ADViewController.h"
#import "ADNearbyEventRequest.h"
#import "ADNearbyEventObject.h"


@interface ADNearbyEventViewController : ADViewController


@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) ADNearbyEventRequest *nearbyEventRequest;
@property (nonatomic, strong) NSMutableArray *nearbyEventList;
@property (nonatomic, strong) ADNearbyEventObject *selectedEvent;
@property (nonatomic, assign) BOOL hasMoreNearbyEvents;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, strong) UIRefreshControl *tableRefreshControl;
@property (nonatomic, strong) NSError *error;


@end
