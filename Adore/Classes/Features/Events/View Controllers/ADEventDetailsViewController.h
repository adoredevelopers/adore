//
//  ADEventDetailsViewController.h
//  Adore
//
//  Created by Chao Lu on 2016-03-15.
//  Copyright © 2016 AdoreMobile Inc. All rights reserved.
//

#import "ADViewController.h"
#import "ADNearbyEventObject.h"

@interface ADEventDetailsViewController : ADViewController <UIWebViewDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) ADNearbyEventObject *event;

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
