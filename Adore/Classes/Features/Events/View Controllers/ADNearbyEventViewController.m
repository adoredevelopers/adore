//
//  ADNearbyEventViewController.m
//  Adore
//
//  Created by Chao Lu on 2016-02-28.
//  Copyright © 2016 AdoreMobile Inc. All rights reserved.
//

#import "ADNearbyEventViewController.h"
#import "ADNearbyEventTableViewCell.h"
#import "ADEventSponsorAdTableViewCell.h"
#import "ADNearbyEventObject.h"
#import "ADUserDefaultHelper.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ADEventDetailsViewController.h"


@interface ADNearbyEventViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation ADNearbyEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _nearbyEventList = @[].mutableCopy;
    _hasMoreNearbyEvents = YES;
    
    [self.navigationController.navigationBar setTranslucent:NO];
    [self loadNearbyEvent:NO];
    
    _tableRefreshControl = [[UIRefreshControl alloc] init];
    [self.tableRefreshControl addTarget:self action:@selector(refreshContent)
                      forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.tableRefreshControl];
    self.tableView.scrollsToTop = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadMoreNearbyEventList
{
//    self.isLoading = YES;
    [self loadNearbyEvent:YES];
}

- (void)loadNearbyEvent:(BOOL)isLoadingMore
{
    // Cancel existing request
    if (self.nearbyEventRequest) {
        [self.nearbyEventRequest cancel];
        self.nearbyEventRequest = nil;
    }
    
    if (isLoadingMore == NO) {
        [self showLoadingIndicator:NSLocalizedString(@"Loading...", nil)];
    }
    
    __weak typeof(self) weakSelf = self;
    id completionBlock = ^(NSArray *array, NSError *error) {
        if (error.code != NSURLErrorCancelled) {
//            self.isLoading = NO;
            [self dismissLoadingIndicator];
        }
        // Don't show CANCELLED error
        if (error && error.code != NSURLErrorCancelled) {
            self.error = error;
        }
        else if (error == nil) {
            self.error = nil;
        }
        
        if (isLoadingMore == NO) {
            [self.nearbyEventList removeAllObjects];
            [self.tableView reloadData];
        }
        
        for (ADNearbyEventObject *event in array)
        {
            [weakSelf.nearbyEventList addObject:event];
        }
        if (array.count == 0) {
            weakSelf.hasMoreNearbyEvents = NO;
        }
        else {
            weakSelf.hasMoreNearbyEvents = YES;
        }
        
        // Release Pull Lock
        weakSelf.nearbyEventRequest = nil;
        [weakSelf.tableView reloadData];
        
        if (isLoadingMore == NO) {
            [self.tableRefreshControl endRefreshing];
        }
    };
    
    NSInteger maximumDistance = [ADUserDefaultHelper maximumDistance];
    
    self.nearbyEventRequest = [[ADNearbyEventRequest alloc] initWithMode:isLoadingMore ? @"paging" : @"normal"
                                                               BatchSize:10
                                                             MaxDistance:maximumDistance
                                                       CompletionHandler:completionBlock];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma -
#pragma - UITableVIewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.nearbyEventList.count == 0) {
        return 0;
    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.nearbyEventList.count + (self.hasMoreNearbyEvents ? 1 : 0);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.nearbyEventList.count) {
        ADNearbyEventObject *nearbyEvent = self.nearbyEventList[indexPath.item];
        
        if ([nearbyEvent.event_type isEqualToString:@"Advertisement"]) {
//            ADEventSponsorAdTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SponsorAdTableViewCell" forIndexPath:indexPath];
            ADEventSponsorAdTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SponsorAdTableViewCell"];
            return cell.bounds.size.height;
        } else {
//            ADNearbyEventTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventTableViewCell" forIndexPath:indexPath];
            ADNearbyEventTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventTableViewCell"];
            return cell.bounds.size.height;
        }
    }
    if (indexPath.row == self.nearbyEventList.count) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LoadingCell"];
        return cell.bounds.size.height;
    }
    
    return tableView.rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.nearbyEventList.count) {
        ADNearbyEventObject *nearbyEvent = self.nearbyEventList[indexPath.item];
        
        if ([nearbyEvent.event_type isEqualToString:@"Advertisement"]) {
            ADEventSponsorAdTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SponsorAdTableViewCell" forIndexPath:indexPath];
            return cell;
        } else {
            ADNearbyEventTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventTableViewCell" forIndexPath:indexPath];
            return cell;
        }
    }
    if (indexPath.row == self.nearbyEventList.count) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LoadingCell"];
        return cell;
    }
    
    return nil;
}


#pragma -
#pragma - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedEvent = self.nearbyEventList[indexPath.item];
    [self performSegueWithIdentifier:@"ShowEventDetailsSegue" sender:self];
    
    return;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == self.nearbyEventList.count) {
        [self loadMoreNearbyEventList];
    }
    
    if (indexPath.row < self.nearbyEventList.count) {
        ADNearbyEventObject *nearbyEvent = self.nearbyEventList[indexPath.item];
        ADNearbyEventTableViewCell *eventViewCell = (ADNearbyEventTableViewCell *)cell;
        [eventViewCell updateViewWithNearbyEventObject:nearbyEvent];
    }
    
    return;
}


#pragma -
#pragma - Custom segue methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowEventDetailsSegue"]) {
        ADEventDetailsViewController *eventDetailsVC = (ADEventDetailsViewController *)segue.destinationViewController;
        eventDetailsVC.event = self.selectedEvent;
    }
}


- (void)refreshContent {
    [self loadNearbyEvent:NO];
}

@end
