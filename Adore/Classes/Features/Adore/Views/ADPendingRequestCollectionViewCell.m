//
//  ADPendingRequestCollectionViewCell.m
//  Adore
//
//  Created by Wang on 2014-07-09.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADPendingRequestCollectionViewCell.h"
#import "SDWebImage/UIImageView+WebCache.h"



@implementation ADPendingRequestCollectionViewCell

- (void)updateViewWithImageUrl:(NSString *)imageUrl AtIndex:(NSUInteger)index
{
	[self.photoImageView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"profile_image_placeholder_150x150"] options:SDWebImageRetryFailed];
	
	self.photoImageView.layer.cornerRadius = 4;
	self.photoImageView.clipsToBounds = YES;
	self.photoImageView.tag = index;
}

- (void)select
{
    self.layer.borderWidth = 5.0;
    self.layer.borderColor = [UIColor colorWithRed:231/255.0 green:170/255.0 blue:169/255.0 alpha:1].CGColor;
    self.layer.cornerRadius = 5.0;
}

- (void)deselect
{
    self.layer.borderWidth = 1.0;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.cornerRadius = 5.0;
}


@end
