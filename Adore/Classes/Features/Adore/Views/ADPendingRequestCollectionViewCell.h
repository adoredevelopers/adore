//
//  ADPendingRequestCollectionViewCell.h
//  Adore
//
//  Created by Wang on 2014-07-09.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ADPendingRequestCollectionViewCell : UICollectionViewCell


@property (nonatomic, weak) IBOutlet UIImageView *photoImageView;
@property (nonatomic, weak) IBOutlet UIButton *adoreButton;


- (void)updateViewWithImageUrl:(NSString *)imageUrl AtIndex:(NSUInteger)index;

- (void)select;
- (void)deselect;


@end
