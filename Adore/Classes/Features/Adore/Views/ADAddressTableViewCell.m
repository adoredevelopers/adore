//
//  ADAddressTableViewCell.m
//  Adore
//
//  Created by Wang on 2014-12-14.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADAddressTableViewCell.h"
#import "ADManagedCoupon+Extensions.h"
#import "ADManagedPartner+Extensions.h"
#import "ADUserManager.h"
#import "ADUserCoreDataHelper.h"

@implementation ADAddressTableViewCell


- (void)updateWithCoupon:(ADManagedCoupon *)coupon
{
    self.iconView.image = [[UIImage imageNamed:@"map"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    NSMutableString *addressString = [NSMutableString string];
    if (coupon.partner.address) {
        [addressString appendFormat:@"\n%@", coupon.partner.address];
    }
    if (coupon.partner.postal_code) {
        [addressString appendFormat:@"\n%@", coupon.partner.postal_code];
    }
    self.addressLabel.text = [addressString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

@end
