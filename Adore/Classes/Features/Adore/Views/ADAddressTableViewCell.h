//
//  ADAddressTableViewCell.h
//  Adore
//
//  Created by Wang on 2014-12-14.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>



@class ADManagedCoupon;

@interface ADAddressTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *iconView;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;


- (void)updateWithCoupon:(ADManagedCoupon *)coupon;


@end
