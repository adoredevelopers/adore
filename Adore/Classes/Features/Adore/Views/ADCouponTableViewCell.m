//
//  ADCouponTableViewCell.m
//  Adore
//
//  Created by Wang on 2014-09-27.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADCouponTableViewCell.h"
#import "ADManagedCoupon+Extensions.h"
#import "ADManagedPartner+Extensions.h"
#import "ADUserManager.h"
#import "ADUserCoreDataHelper.h"



@implementation ADCouponTableViewCell

#pragma mark -
#pragma mark - Update methods

- (void)updateWithCoupon:(ADManagedCoupon *)coupon
{
    self.activityImageView.image = [[UIImage imageNamed:@"coupon_dinner"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.partnerNameLabel.text = coupon.partner.name;
    
    if (coupon.redeemed || coupon.expired) {
        NSString *text = nil;
        if (coupon.redeemed) {
            text = NSLocalizedString(@"Redeemed", nil);
        }
        else {
            text = NSLocalizedString(@"Expired", nil);
        }
        self.expireDateLabel.text = text;
        self.expireDateLabel.textColor = [UIColor grayColor];
        self.partnerNameLabel.textColor = [UIColor grayColor];
    }
    else {
        self.partnerNameLabel.textColor = [UIColor blackColor];
        self.expireDateLabel.attributedText = [coupon expireAttributedString];
    }
}


@end
