//
//  ADCouponTableViewCell.h
//  Adore
//
//  Created by Wang on 2014-09-27.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ADManagedCoupon;



@interface ADCouponTableViewCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UIImageView *activityImageView;
@property (nonatomic, weak) IBOutlet UILabel *partnerNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *expireDateLabel;

- (void)updateWithCoupon:(ADManagedCoupon *)coupon;


@end
