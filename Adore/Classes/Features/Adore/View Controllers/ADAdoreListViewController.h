//
//  ADAdoreListViewController.h
//  Adore
//
//  Created by Wang on 2014-05-26.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADViewController.h"
#import "ADUserManager.h"


@interface ADAdoreListViewController : ADViewController


@property (nonatomic, strong) NSArray *adores;
@property (nonatomic, strong) NSArray *filteredAdoresArray;
@property (nonatomic, strong) NSArray *sortedAdoresArrayOfArrays;

@property (nonatomic, assign) SEL selectorToReturnDisplayName;
@property (nonatomic, strong) NSPredicate *filteringPredicate;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) ADManagedUser *selectedUserObject;


- (void)pushUserConnectionViewControllerWithUserEmail:(NSString *)email;


@end
