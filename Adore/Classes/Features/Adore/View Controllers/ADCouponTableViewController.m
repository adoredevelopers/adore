//
//  ADCouponTableViewController.m
//  Adore
//
//  Created by Kevin Wang on 2014-09-30.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADCouponTableViewController.h"
#import "ADMapViewController.h"
#import "ADManagedCoupon+Extensions.h"
#import "NSDate+Utils.h"
#import "NSDictionary+NonNilObject.h"
#import "ADManagedPartner+Extensions.h"
#import "ADImageTextTableViewCell.h"
#import "ADAddressTableViewCell.h"
#import "ADUserManager.h"
#import "ADUserCoreDataHelper.h"
#import "ADChatManager.h"
#import "ADProfileImageScrollableCollectionTableViewCell.h"
#import "NYTPhotosViewController.h"



@interface ADCouponTableViewController () <UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate>

@end



@implementation ADCouponTableViewController


#pragma mark -
#pragma mark - Initializer

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        _userCoreDataHelper = self.userManager.userCoreDataHelper;
    }
    return self;
}

#pragma mark -
#pragma mark - View lifecycles

- (void)viewDidLoad {
    [super viewDidLoad];

    UINib *cellNib = [UINib nibWithNibName:@"ADImageTextTableViewCell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"ImageTextCell"];
    UINib *collectionNib = [UINib nibWithNibName:@"ADProfileImageScrollableCollectionTableViewCell" bundle:nil];
    [self.tableView registerNib:collectionNib forCellReuseIdentifier:kAD_USER_DETAIL_PROFILE_IMAGE_SCROLLABLE_COLLECTION_VIEW_CELL_IDENTIFIER];
    UINib *addressNib = [UINib nibWithNibName:@"ADAddressTableViewCell" bundle:nil];
    [self.tableView registerNib:addressNib forCellReuseIdentifier:@"AddressCell"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupToolbar];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.toolbarHidden = YES;
}

- (void)dealloc
{
    [_coupon removeObserver:self forKeyPath:NSStringFromSelector(@selector(coupon_status))];
}


#pragma mark -
#pragma mark - Custom setter methods

- (void)setCoupon:(ADManagedCoupon *)coupon
{
    [_coupon removeObserver:self forKeyPath:NSStringFromSelector(@selector(coupon_status))];
    
    _coupon = coupon;
    self.title = coupon.partner.name;
    self.photos = [coupon.partner arrayOfNYTPhotos];

    [_coupon addObserver:self
                  forKeyPath:NSStringFromSelector(@selector(coupon_status))
                     options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld)
                     context:nil];
}


#pragma mark -
#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    NSString *old = change[NSKeyValueChangeOldKey];
    NSString *new = change[NSKeyValueChangeNewKey];
    
    if ([keyPath isEqual:NSStringFromSelector(@selector(coupon_status))]) {
        [self setupToolbar];
        [self.tableView reloadData];
        if ([old isEqualToString:kAD_COUPON_STATUS_UNUSED] && [new isEqualToString:kAD_COUPON_STATUS_ACTIVATED]) {
            [self showSuccessWithTitle:NSLocalizedString(@"Coupon Activated", nil) message:NSLocalizedString(@"Show this coupon at checkout to get the discount.", nil)];
        }
        else if ([old isEqualToString:kAD_COUPON_STATUS_ACTIVATED] && [new isEqualToString:kAD_COUPON_STATUS_REDEEMED]) {
            [self showSuccessWithTitle:NSLocalizedString(@"Coupon Redeemed", nil) message:NSLocalizedString(@"You have successfully redeemed the coupon.", nil)];
        }
    }
}


#pragma mark -
#pragma mark - Helper methods

- (void)setupToolbar
{
    if ([self.coupon activated] == NO) {
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Activate Coupon", nil) style:UIBarButtonItemStyleDone target:nil action:nil];
        
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(activateCoupon:)];
        longPress.minimumPressDuration = 0.1f;
        [self.navigationController.toolbar addGestureRecognizer:longPress];
        
        self.toolbarItems = @[flexibleSpace, item, flexibleSpace];
        self.navigationController.toolbarHidden = NO;
    }
    else {
        self.navigationController.toolbarHidden = YES;
    }
}

- (void)showPhotoBrowserWithIndex:(NSInteger)index
{
    if (index < 0 || index >= self.photos.count) {
        return;
    }
    NYTPhotosViewController *photosViewController = [[NYTPhotosViewController alloc] initWithPhotos:self.photos initialPhoto:self.photos[index]];
    [self presentViewController:photosViewController animated:YES completion:nil];
}

- (void)activateCoupon:(UILongPressGestureRecognizer *)longPress
{
    if (longPress.state == UIGestureRecognizerStateBegan) {
        [self showLoadingIndicator:NSLocalizedString(@"Waiting for activation...", nil)];
        [self.chatManager sendEnableActivateCouponThroughSocket:self.coupon.coupon_code];
    }
    else if (longPress.state == UIGestureRecognizerStateChanged) {
        
    }
    else {
        [self dismissLoadingIndicator];
        [self.chatManager sendDisableActivateCouponThroughSocket:self.coupon.coupon_code];
    }
}


#pragma mark -
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        // Discount
        // Expiration date
        // View coupon
        NSInteger couponRow = [self.coupon activated] == YES;
        return 2 + couponRow;
    }
    else if (section == 1) {
        // Partner name
        // Phone number
        // Business hours
        // Map
        // Images
        NSInteger imageRow = self.photos.count > 0 ? 1 : 0;
        return 3 + imageRow;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ((indexPath.section == 0 && indexPath.row == 0)
        || (indexPath.section == 0 && indexPath.row == 1)
        || (indexPath.section == 1 && indexPath.row == 0)
        || (indexPath.section == 1 && indexPath.row == 1)) {
        ADImageTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ImageTextCell"];
        return cell.bounds.size.height;
    }
    else if (indexPath.section == 1 && indexPath.row == 2) {
        ADAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddressCell"];
        [cell updateWithCoupon:self.coupon];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        
        CGSize size = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return MAX(size.height + 1, 44);
    }
    else if (indexPath.section == 1 && indexPath.row == 3) {
        ADProfileImageScrollableCollectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_USER_DETAIL_PROFILE_IMAGE_SCROLLABLE_COLLECTION_VIEW_CELL_IDENTIFIER];
        return [cell cellHeight];
    }
    else if (indexPath.section == 2 && indexPath.row == 0) {
        return 280;
    }
    return tableView.rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == 0) {
        ADImageTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ImageTextCell"];
        [cell updateWithImage:[[UIImage imageNamed:@"coupon_drink"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                         text:self.coupon.partner.coupon_discount_message];
        return cell;
    }
    if (indexPath.section == 0 && indexPath.row == 1) {
        ADImageTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ImageTextCell"];
        NSString *expireString = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Expiry date:", nil), [self.coupon.expiration_date toDateStringFormattedWith_MMM_dd_yyyy]];
        [cell updateWithImage:[[UIImage imageNamed:@"coupons"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                         text:expireString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    if (indexPath.section == 0 && indexPath.row == 2) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ViewCoupon"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ViewCoupon"];
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
        }
        if ([self.coupon redeemed]) {
            cell.textLabel.textColor = [UIColor darkGrayColor];
            cell.textLabel.text = NSLocalizedString(@"Coupon is redeemed.", nil);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        else {
            cell.textLabel.textColor = [Constants femaleTintColor];
            cell.textLabel.text = NSLocalizedString(@"View Coupon", nil);
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        }
        return cell;
    }
    if (indexPath.section == 1 && indexPath.row == 0) {
        ADImageTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ImageTextCell"];
        [cell updateWithImage:[[UIImage imageNamed:@"coupon_drink"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                         text:self.coupon.partner.name];
        return cell;
    }
    if (indexPath.section == 1 && indexPath.row == 1) {
        ADImageTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ImageTextCell"];
        [cell updateWithImage:[[UIImage imageNamed:@"phone"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                         text:self.coupon.partner.phone_number];
        return cell;
    }
    if (indexPath.section == 1 && indexPath.row == 2) {
        ADAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddressCell"];
        [cell updateWithCoupon:self.coupon];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        return cell;
    }
    if (indexPath.section == 1 && indexPath.row == 3) {
        ADProfileImageScrollableCollectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_USER_DETAIL_PROFILE_IMAGE_SCROLLABLE_COLLECTION_VIEW_CELL_IDENTIFIER];
        cell.partner = self.coupon.partner;
        cell.collectionView.delegate = self;
        cell.collectionView.backgroundColor = [UIColor clearColor];
        return cell;
    }
    return [[UITableViewCell alloc] init];
}


#pragma mark -
#pragma mark - Table view delegate

- (void)tableView:tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0 && indexPath.row == 2 && [self.coupon redeemed] == NO) {
        NYTPhotosViewController *photosViewController = [[NYTPhotosViewController alloc] initWithPhotos:@[self.coupon.couponNYTPhoto]];
        [self presentViewController:photosViewController animated:YES completion:nil];
    }
    if (indexPath.section == 1 && indexPath.row == 2) {
        [self performSegueWithIdentifier:@"ShowOnMap" sender:nil];
    }
}


#pragma mark -
#pragma mark - UICollectionView delegate methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_SETTINGS_VIEW_PROFILE_IMAGE];
    [self showPhotoBrowserWithIndex:indexPath.item];
}


#pragma mark -
#pragma mark - Segue Actions

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowOnMap"]) {
        ADMapViewController *mapView = segue.destinationViewController;
        mapView.coupon = self.coupon;
    }
}


@end
