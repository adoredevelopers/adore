//
//  ADUserConnectionTableViewController.h
//  Adore
//
//  Created by Wang on 2014-08-15.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADViewController.h"

@class ADManagedUser;



@interface ADUserConnectionTableViewController : ADViewController


@property (nonatomic, strong) ADManagedUser *userObject;
@property (nonatomic, strong) Class parentViewClass;


@end
