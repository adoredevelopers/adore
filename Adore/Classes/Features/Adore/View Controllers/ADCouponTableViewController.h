//
//  ADCouponTableViewController.h
//  Adore
//
//  Created by Kevin Wang on 2014-09-30.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADViewController.h"
#import "ADChatManager.h"
#import "ADUserCoreDataHelper.h"


@class ADManagedCoupon;


@interface ADCouponTableViewController : ADViewController


@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, strong) ADUserCoreDataHelper *userCoreDataHelper;
@property (nonatomic, strong) ADManagedCoupon *coupon;


@end
