//
//  ADPendingRequestViewController.h
//  Adore
//
//  Created by Wang on 2014-07-09.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADViewController.h"
#import "ADAdoreConnectionNetworkingEngine.h"


@interface ADPendingRequestViewController : ADViewController


@property (nonatomic, strong) ADAdoreConnectionNetworkingEngine *networkingEngine;
@property (nonatomic, strong) ADManagedUser *pendingUser;
@property (nonatomic, strong) NSArray *requestsArray;
@property (nonatomic, strong) NSArray *photosArray;
@property (nonatomic, assign) NSInteger selected;
@property (nonatomic, assign) BOOL sendingRequest;
@property (nonatomic, strong) SCLAlertView *alertView;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;


@end
