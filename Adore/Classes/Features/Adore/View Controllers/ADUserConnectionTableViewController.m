//
//  ADUserConnectionTableViewController.m
//  Adore
//
//  Created by Wang on 2014-08-15.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADUserConnectionTableViewController.h"
#import "ADUserDetailItem.h"
#import "ADCouponTableViewCell.h"
#import "ADCouponTableViewController.h"
#import "ADManagedUser+Extensions.h"
#import "ADUserDetailTableViewCell.h"
#import "ADManagedPartner+Extensions.h"
#import "ADManagedCoupon+Extensions.h"
#import "ADChatViewController.h"
#import "ADUserCoreDataHelper.h"
#import "ADImageTextTableViewCell.h"
#import "ADUserManager.h"
#import "ADProfileImageScrollableCollectionTableViewCell.h"
#import "ADAdoreManageViewController.h"
#import "NSObject+Notification.h"
#import "ADUserDetailSectionHeaderCell.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ADProfileCoverTableViewCell.h"
#import "NYTPhotosViewController.h"



typedef NS_ENUM(NSInteger, ADUserConnectionTableViewSection) {
    ADUserConnectionTableViewSectionProfileCover,
    ADUserConnectionTableViewSectionImages,
    ADUserConnectionTableViewSectionModeSwitch,
    ADUserConnectionTableViewSectionDetails,
};

typedef NS_ENUM(NSInteger, ADUserConnectionMode) {
    ADUserConnectionModeDetails = 1,
    ADUserConnectionModeCoupon = 2
};



@interface ADUserConnectionTableViewController () <UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) UIImageView *profileCoverImageView;

@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, strong) NSArray *sectionArray;
@property (nonatomic, strong) NSArray *detailsItemArray;
@property (nonatomic, strong) NSArray *couponsArray;

@property (nonatomic, assign) ADUserConnectionMode mode;
@property (nonatomic, strong) ADManagedCoupon *selectedCoupon;

@end


@implementation ADUserConnectionTableViewController

#pragma mark -
#pragma mark - Initializer

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _mode = ADUserConnectionModeDetails;
    }
    
    return self;
}


#pragma mark -
#pragma mark - Custom Setter Methods

- (void)setUserObject:(ADManagedUser *)userObject
{
    _userObject = userObject;
    
    self.title = userObject.nickname;
    self.photos = [userObject arrayOfNYTPhotos];
    self.detailsItemArray = [userObject userDetailItemArray];
    [self buildSectionItems];
    
    self.couponsController = [self.userManager.userCoreDataHelper couponsFetchedResultsControllerForEmail:self.userObject.email withDelegate:self];
    [self sortCoupons:self.couponsController.fetchedObjects];
}


#pragma mark -
#pragma mark - View lifecycle

- (void)viewDidLoad
{
	[super viewDidLoad];
        
    UINib *profileCoverNib = [UINib nibWithNibName:@"ADProfileCoverTableViewCell" bundle:nil];
    [self.tableView registerNib:profileCoverNib forCellReuseIdentifier:kAD_USER_DETAIL_PROFILE_COVER_IDENTIFIER];
    UINib *collectionNib = [UINib nibWithNibName:@"ADProfileImageScrollableCollectionTableViewCell" bundle:nil];
    [self.tableView registerNib:collectionNib forCellReuseIdentifier:kAD_USER_DETAIL_PROFILE_IMAGE_SCROLLABLE_COLLECTION_VIEW_CELL_IDENTIFIER];
    UINib *userDetailNib = [UINib nibWithNibName:@"ADUserDetailTableViewCell" bundle:nil];
    [self.tableView registerNib:userDetailNib forCellReuseIdentifier:kAD_USER_DETAIL_TABLE_VIEW_CELL_IDENTIFIER];
    UINib *sectionHeaderNib = [UINib nibWithNibName:@"ADUserDetailSectionHeaderCell" bundle:nil];
    [self.tableView registerNib:sectionHeaderNib forCellReuseIdentifier:kAD_USER_DETAIL_SECTION_HEADER_IDENTIFIER];
}


#pragma mark -
#pragma mark - Helper methods

- (void)buildSectionItems
{
    NSMutableArray *tempArray = @[].mutableCopy;
    [tempArray addObject:@(ADUserConnectionTableViewSectionProfileCover)];
    
    if (self.userObject.profileImageArray.count > 1) {
        [tempArray addObject:@(ADUserConnectionTableViewSectionImages)];
    }
    
    [tempArray addObject:@(ADUserConnectionTableViewSectionModeSwitch)];
    [tempArray addObject:@(ADUserConnectionTableViewSectionDetails)];
    self.sectionArray = tempArray.copy;
}

- (void)reloadSection:(NSInteger)section
{
    NSInteger index = [self.sectionArray indexOfObjectPassingTest:^BOOL(NSNumber *obj, NSUInteger idx, BOOL *stop) {
        return section == [obj integerValue];
    }];
    if (index != NSNotFound) {
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:index]
                      withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)segmentedControlValueChanged:(id)sender
{
    UISegmentedControl *seg = sender;
    if (seg.selectedSegmentIndex == 0) {
        self.mode = ADUserConnectionModeDetails;
    }
    else if (seg.selectedSegmentIndex == 1) {
        self.mode = ADUserConnectionModeCoupon;
    }
    [self reloadSection:ADUserConnectionTableViewSectionDetails];
}

- (IBAction)showChatView:(id)sender
{
    if (self.parentViewClass == [ADChatViewController class]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        ADChatViewController *chatVC = [self.chatManager chatViewWithEmail:self.userObject.email fromView:[self class]];
        [self.navigationController pushViewController:chatVC animated:YES];
    }
}

- (void)showPhotoBrowserWithIndex:(NSInteger)index
{
    if (index < 0 || index >= self.photos.count) {
        return;
    }
    NYTPhotosViewController *photosViewController = [[NYTPhotosViewController alloc] initWithPhotos:self.photos initialPhoto:self.photos[index] showRightButton:YES];
    [self presentViewController:photosViewController animated:YES completion:nil];
}

- (void)sortCoupons:(NSArray *)array
{
    NSArray *sortedArray = [array sortedArrayUsingComparator: ^(ADManagedCoupon *coupon1, ADManagedCoupon *coupon2) {
        NSComparisonResult result;
        // Expired or redeemed coupon goes to the end of the list
        result = [[NSNumber numberWithBool:coupon1.redeemed || coupon1.expired] compare:[NSNumber numberWithBool:coupon2.redeemed || coupon2.expired]];
        if (result != NSOrderedSame) {
            return result;
        }
        // Order by expiration date
        return [coupon2.expiration_date compare:coupon1.expiration_date];
    }];
    self.couponsArray = [sortedArray copy];
    [self.tableView reloadData];
}


#pragma mark -
#pragma mark - NSFetchedResultsControllerDelegate methods

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if (controller == self.couponsController) {
        [self sortCoupons:self.couponsController.fetchedObjects];
    }
}


#pragma mark -
#pragma mark - Table view datat source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return self.sectionArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    ADUserConnectionTableViewSection sectionNumber = [self.sectionArray[section] integerValue];
	switch (sectionNumber)
	{
        case ADUserConnectionTableViewSectionProfileCover:
        case ADUserConnectionTableViewSectionImages:
        case ADUserConnectionTableViewSectionModeSwitch:
            return 1;
            
		case ADUserConnectionTableViewSectionDetails:
            if (self.mode == ADUserConnectionModeDetails) {
                return self.detailsItemArray.count;
            }
            else if (self.mode == ADUserConnectionModeCoupon) {
                return self.couponsArray.count;
            }
    }
	return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    ADUserConnectionTableViewSection sectionNumber = [self.sectionArray[section] integerValue];
	if (sectionNumber == ADUserConnectionTableViewSectionProfileCover
        || sectionNumber == ADUserConnectionTableViewSectionDetails) {
		return CGFLOAT_MIN;
	}
	return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    ADUserConnectionTableViewSection sectionNumber = [self.sectionArray[section] integerValue];
    if (sectionNumber == ADUserConnectionTableViewSectionModeSwitch) {
        return CGFLOAT_MIN;
    }
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ADUserConnectionTableViewSection sectionNumber = [self.sectionArray[indexPath.section] integerValue];
    if (sectionNumber == ADUserConnectionTableViewSectionProfileCover) {
        ADProfileCoverTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_USER_DETAIL_PROFILE_COVER_IDENTIFIER];
        return [cell cellHeight];
    }
	else if (sectionNumber == ADUserConnectionTableViewSectionImages && indexPath.row == 0) {
        ADProfileImageScrollableCollectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_USER_DETAIL_PROFILE_IMAGE_SCROLLABLE_COLLECTION_VIEW_CELL_IDENTIFIER];
        cell.userObject = self.userObject;
        return [cell cellHeight];
	}
    else if (sectionNumber == ADUserConnectionTableViewSectionModeSwitch) {
        return 50;
    }
    else if (sectionNumber == ADUserConnectionTableViewSectionDetails && self.mode == ADUserConnectionModeDetails) {
        ADUserDetailItem *item = self.detailsItemArray[indexPath.row];
        if ([item isHeader]) {
            ADUserDetailSectionHeaderCell *cell = [ADUserDetailSectionHeaderCell userDetailSectionHeaderCellForTableView:tableView type:item.type];
            return [cell cellHeight];
        }
        else {
            ADUserDetailTableViewCell *cell = [ADUserDetailTableViewCell userDetailCellForTableView:tableView type:item.type userData:self.userObject];
            return [cell cellHeight];
        }
    }
    else if (indexPath.section == ADUserConnectionTableViewSectionDetails && self.mode == ADUserConnectionModeCoupon) {
        return 50;
    }
	return tableView.rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ADUserConnectionTableViewSection sectionNumber = [self.sectionArray[indexPath.section] integerValue];
    if (sectionNumber == ADUserConnectionTableViewSectionProfileCover) {
        ADProfileCoverTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_USER_DETAIL_PROFILE_COVER_IDENTIFIER];
        self.profileCoverImageView = cell.profileImageView;
        [cell.profileImageView sd_setImageWithURL:[NSURL URLWithString:self.userObject.profilePictureUrl] placeholderImage:[UIImage imageNamed:@"profile_image_placeholder_150x150"] options:SDWebImageRetryFailed];
        cell.userObject = self.userObject;
        [cell.button addTarget:self action:@selector(showChatView:) forControlEvents:UIControlEventTouchUpInside];
        [cell setupButton];
        return cell;
    }
    else if (sectionNumber == ADUserConnectionTableViewSectionImages) {
        ADProfileImageScrollableCollectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_USER_DETAIL_PROFILE_IMAGE_SCROLLABLE_COLLECTION_VIEW_CELL_IDENTIFIER];
        cell.userObject = self.userObject;
        cell.collectionView.delegate = self;
        [cell.collectionView reloadData];
        return cell;
	}
    else if (sectionNumber == ADUserConnectionTableViewSectionModeSwitch) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SegmentedCell" forIndexPath:indexPath];
        
        UISegmentedControl *seg = (UISegmentedControl *)[cell viewWithTag:50];
        if (self.mode == ADUserConnectionModeDetails) {
            seg.selectedSegmentIndex = 0;
        }
        else if (self.mode == ADUserConnectionModeCoupon) {
            seg.selectedSegmentIndex = 1;
        }
        
        [seg addTarget:self action:@selector(segmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
        
        return cell;
    }
	else if (sectionNumber == ADUserConnectionTableViewSectionDetails && self.mode == ADUserConnectionModeDetails) {
        ADUserDetailItem *item = self.detailsItemArray[indexPath.row];
        if ([item isHeader]) {
            return [ADUserDetailSectionHeaderCell userDetailSectionHeaderCellForTableView:tableView type:item.type];
        }
        else {
            return [ADUserDetailTableViewCell userDetailCellForTableView:tableView type:item.type userData:self.userObject];
        }
    }
    else if (sectionNumber == ADUserConnectionTableViewSectionDetails && self.mode == ADUserConnectionModeCoupon) {
        ADCouponTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CouponCell"];
        ADManagedCoupon *coupon = [self.couponsArray objectAtIndex:indexPath.row];
        [cell updateWithCoupon:coupon];
        return cell;
    }
	return nil;
}


#pragma mark -
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger sectionNumber = [self.sectionArray[indexPath.section] integerValue];
    if (sectionNumber == ADUserConnectionTableViewSectionProfileCover) {
        [self showPhotoBrowserWithIndex:0];
    }
    else if (sectionNumber == ADUserConnectionTableViewSectionDetails && self.mode == ADUserConnectionModeCoupon) {
        self.selectedCoupon = [self.couponsArray objectAtIndex:indexPath.row];
        [self performSegueWithIdentifier:@"ShowCoupon" sender:nil];
    }
}


#pragma mark -
#pragma mark - UIScrollView delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat yOffset  = scrollView.contentOffset.y;
    CGFloat x = MAX(-yOffset / 200, 0);
    self.profileCoverImageView.transform = CGAffineTransformIdentity;
    self.profileCoverImageView.transform = CGAffineTransformScale(self.profileCoverImageView.transform, 1+x, 1+x);
}


#pragma mark -
#pragma mark - Collection View Controller Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self showPhotoBrowserWithIndex:indexPath.item+1];
}


#pragma mark -
#pragma mark - Segue Actions

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowCoupon"]) {
        [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_ADORE_VIEW_COUPON];
        ADCouponTableViewController *couponView = segue.destinationViewController;
        couponView.coupon = self.selectedCoupon;
    }
    else if ([segue.identifier isEqualToString:@"Manage User"]) {
        ADAdoreManageViewController *manageView = segue.destinationViewController;
        manageView.userObject = self.userObject;
    }
}


@end
