//
//  ADMapViewController.m
//  Adore
//
//  Created by Kevin Wang on 2014-09-30.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADMapViewController.h"
#import <CoreLocation/CLLocation.h>
#import "ADAnnotation.h"
#import "ADManagedCoupon+Extensions.h"
#import "ADManagedPartner+Extensions.h"
#import "ADUserManager.h"
#import "ADUserCoreDataHelper.h"



@implementation ADMapViewController

#pragma mark
#pragma mark - View lifecycles

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mapView.showsUserLocation = YES;
    
    CLGeocoder *clGeo = [[CLGeocoder alloc] init];
    CLGeocodeCompletionHandler completionBlock = ^(NSArray *placemarks, NSError *error) {
        CLPlacemark *place = [placemarks firstObject];
        if (place != nil) {
            self.mapView.centerCoordinate = place.location.coordinate;
            
            ADAnnotation *annotation = [[ADAnnotation alloc] initWithCoordinate:place.location.coordinate
                                                                          title:self.coupon.partner.name
                                                                       subtitle:nil];
            [self.mapView addAnnotation:annotation];
            [self.mapView selectAnnotation:annotation animated:YES];
            
            MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.mapView.centerCoordinate, 1500, 1500);
            [self.mapView setRegion:region animated:YES];
        }
    };
    NSString *addressString = [NSString stringWithFormat:@"%@, %@", self.coupon.partner.address, self.coupon.partner.postal_code];
    [clGeo geocodeAddressString:addressString completionHandler:completionBlock];
}

@end
