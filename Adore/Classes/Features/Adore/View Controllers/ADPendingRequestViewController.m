//
//  ADPendingRequestViewController.m
//  Adore
//
//  Created by Wang on 2014-07-09.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADPendingRequestViewController.h"
#import "ADUserManager.h"
#import "ADPendingRequestObject.h"
#import "ADPendingRequestCollectionViewCell.h"
#import "ADAdoreConnectionNetworkingEngine.h"
#import "ADManagedUser+Extensions.h"
#import "ADUserCoreDataHelper.h"
#import "NSMutableArray+Shuffling.h"
#import "NSObject+Notification.h"
#import "LessBoringFlowLayout.h"
#import "NYTPhotosViewController.h"
#import "ADNYTPhoto.h"
#import "SCLAlertView.h"



@interface ADPendingRequestViewController () <UICollectionViewDataSource, UICollectionViewDelegate, NYTPhotosViewControllerDelegate>

@end



@implementation ADPendingRequestViewController

#pragma mark -
#pragma mark - Initalizer

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _networkingEngine = [[ADAdoreConnectionNetworkingEngine alloc] init];
        _sendingRequest = NO;
        self.pendingUsersController = [self.userManager.userCoreDataHelper pendingUsersFetchedResultsControllerWithDelegate:self];
    }
    return self;
}


#pragma mark -
#pragma mark - View cycles

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    LessBoringFlowLayout *layout = [[LessBoringFlowLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10);
    layout.minimumInteritemSpacing = 10;
    layout.minimumLineSpacing = 10;
    layout.itemSize = CGSizeMake(150, 150);
    self.collectionView.collectionViewLayout = layout;
    
    [self getFirstAvailableRequest];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setToolbarHidden:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController setToolbarHidden:YES];
}


#pragma mark -
#pragma mark - Helper methods

- (IBAction)cancelButtonClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)adoreButtonClicked:(id)sender
{
    [self selectUserAtIndex:((UIButton *)sender).tag];
}

- (IBAction)sendRequestButtonClicked:(id)sender
{
    // Avoid fat finger
    if (self.sendingRequest || _alertView.isVisible) {
        return;
    }
    
    // None is selected
    if (self.selected == NSNotFound)
    {
        _alertView = [self showToastMessage:NSLocalizedString(@"Please pick your favourite.", nil)];
        return;
    }
    
    ADManagedUser *user = self.requestsArray[self.selected];
    [self showLoadingIndicator:nil];
    
    // Picked the right one
    if (user == self.pendingUser) {
        [self addUser:self.pendingUser];
    }
    // Picked the wrong one
    else {
        // We want to reject the original request
        [self rejectUser:self.pendingUser];
        
        // Add the one picked
        [self addUser:user];
    }
}

- (IBAction)nextRequestButtonClicked:(id)sender
{
    // avoid fat finger
    if (self.sendingRequest) {
        return;
    }
    
    [self rejectUser:self.pendingUser];
}

- (void)addUser:(ADManagedUser *)user
{
    self.sendingRequest = YES;
    __weak __typeof(self) weakSelf = self;
    ADConnectionNetworkCompletionBlock completionBlock = ^(BOOL success, BOOL approved, NSError *error) {
        weakSelf.sendingRequest = NO;
        [self dismissLoadingIndicator];
        if (success) {
            if (approved) {
                [weakSelf showSuccessWithTitle:NSLocalizedString(@"The right one!", nil) message:NSLocalizedString(@"Start chatting with your adorer.", nil)];
            }
            else {
                [weakSelf showErrorMessage:NSLocalizedString(@"That is not the person who liked you. Don't be discouraged, better luck next time.", nil)];
            }
        }
        else {
            [weakSelf showErrorMessage:error.localizedDescription];
        }
    };
    [self.networkingEngine startAddingUserRequestToUser:user completionBlock:completionBlock];
}

- (void)rejectUser:(ADManagedUser *)user
{
    self.sendingRequest = YES;
    __weak __typeof(self) weakSelf = self;
    ADConnectionNetworkCompletionBlock completionBlock = ^(BOOL success, BOOL approved, NSError *error) {
        weakSelf.sendingRequest = NO;
        if (success) {
        }
        else {
            [weakSelf showErrorMessage:error.localizedDescription];
        }
    };
    
    [self.networkingEngine startRejectingUserRequestWithUser:user completionBlock:completionBlock];
}

- (void)setupRequestsArray
{
    self.selected = NSNotFound;
    
    NSArray *pendingArray = self.pendingUsersController.fetchedObjects;
    if (pendingArray.count >= 1) {
        self.pendingUser = [pendingArray firstObject];
        
        NSArray *randomUsers = [self.userManager.userCoreDataHelper loadUserFromCoreDataRelatedToPendingUser:self.pendingUser.email];
        // randomUsers already have 4 candidates that include the pendingUser as well as the randomUsers
        NSMutableArray *tempRequestsArray = [NSMutableArray arrayWithArray:randomUsers];
        [tempRequestsArray shuffle];
        self.requestsArray = tempRequestsArray.copy;
        DDLogDebug(@"%ld", (long)[self.requestsArray indexOfObject:self.pendingUser]);
        
        NSMutableArray *photoArray = @[].mutableCopy;
        for (ADManagedUser *user in self.requestsArray) {
            [photoArray addObject:[user NYTPhotoForUserProfile]];
        }
        self.photosArray = photoArray.copy;
    }
}

- (void)getFirstAvailableRequest
{
    [self.collectionView performBatchUpdates:^{
        if (self.requestsArray) {
            self.requestsArray = nil;
            [self.collectionView deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0],
                                                           [NSIndexPath indexPathForRow:1 inSection:0],
                                                           [NSIndexPath indexPathForRow:2 inSection:0],
                                                           [NSIndexPath indexPathForRow:3 inSection:0]]];
            [self.collectionView deleteSections:[NSIndexSet indexSetWithIndex:0]];
        }
    }
                                  completion:^(BOOL finished) {
                                      [self.collectionView performBatchUpdates:^{
                                          [self setupRequestsArray];
                                          
                                          [self.collectionView insertSections:[NSIndexSet indexSetWithIndex:0]];
                                          [self.collectionView insertItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0],
                                                                                         [NSIndexPath indexPathForRow:1 inSection:0],
                                                                                         [NSIndexPath indexPathForRow:2 inSection:0],
                                                                                         [NSIndexPath indexPathForRow:3 inSection:0]]];
                                      }
                                                                    completion:^(BOOL finished) {
                                                                    }];
                                      
                                  }];
}

- (void)selectUserAtIndex:(NSInteger)index
{
    self.selected = index;
    [self.collectionView reloadData];
}

- (void)showPhotoBrowserWithIndex:(NSInteger)index
{
    if (index < 0 || index >= self.photosArray.count) {
        return;
    }
    
    for (NSInteger i = 0; i < self.photosArray.count; i++) {
        ADNYTPhoto *photo = self.photosArray[i];
        if (i == self.selected) {
            photo.selected = YES;
        }
        else {
            photo.selected = NO;
        }
    }
    
    NYTPhotosViewController *photosViewController = [[NYTPhotosViewController alloc] initWithPhotos:self.photosArray initialPhoto:self.photosArray[index] showRightButton:YES showHeartButton:YES];
    photosViewController.delegate = self;
    [self presentViewController:photosViewController animated:YES completion:nil];
}


#pragma mark -
#pragma mark - NSFetchedResultsControllerDelegate methods

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if (controller == self.pendingUsersController) {
        // If you have more requests, get the next one
        if (controller.fetchedObjects.count >= 1) {
            [self getFirstAvailableRequest];
        }
        // No more request
        else {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}


#pragma mark -
#pragma mark - Collection view data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (self.requestsArray.count == 4) {
        return 1;
    }
    
    return 0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.requestsArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ADPendingRequestCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoCell" forIndexPath:indexPath];
    ADManagedUser *user = [self.requestsArray objectAtIndex:indexPath.row];
    [cell updateViewWithImageUrl:user.profilePictureUrl AtIndex:indexPath.row];
    
    if (indexPath.row == self.selected) {
        [cell select];
        [cell.adoreButton setImage:[UIImage imageNamed:@"red_heart_with_white_border"] forState:UIControlStateNormal];
        [cell.adoreButton setImage:[UIImage imageNamed:@"red_heart_with_white_border"] forState:UIControlStateHighlighted];
    }
    else {
        [cell deselect];
        [cell.adoreButton setImage:[UIImage imageNamed:@"grey_heart_with_white_border"] forState:UIControlStateNormal];
        [cell.adoreButton setImage:[UIImage imageNamed:@"red_heart_with_white_border"] forState:UIControlStateHighlighted];
    }
    cell.adoreButton.tag = indexPath.row;
    [cell.adoreButton addTarget:self action:@selector(adoreButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}


#pragma mark -
#pragma mark - Collection view delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self showPhotoBrowserWithIndex:indexPath.item];
}


#pragma mark -
#pragma mark - UICollectionViewDelegateFlowLayout Methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = (collectionView.frame.size.width - 30) / 2;
    return CGSizeMake(width, width);
}


#pragma mark -
#pragma mark - NYTPhotosViewControllerDelegate Methods

- (void)photosViewController:(NYTPhotosViewController *)photosViewController didSelectPhoto:(ADNYTPhoto *)photo
{
    NSInteger index = [self.photosArray indexOfObject:photo];
    if (index != NSNotFound) {
        [self selectUserAtIndex:index];
    }
}

@end
