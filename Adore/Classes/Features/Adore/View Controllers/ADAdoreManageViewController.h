//
//  ADAdoreManageViewController.h
//  Adore
//
//  Created by Wang on 2015-01-20.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADViewController.h"
#import "ADAdoreConnectionNetworkingEngine.h"
#import "ADUserCoreDataHelper.h"


@class ADManagedUser;


@interface ADAdoreManageViewController : ADViewController


@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) ADAdoreConnectionNetworkingEngine *networkingEngine;
@property (nonatomic, strong) ADUserCoreDataHelper *userCoreDataHelper;
@property (nonatomic, strong) ADManagedUser *userObject;


@end
