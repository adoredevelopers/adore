//
//  ADAdoreManageViewController.m
//  Adore
//
//  Created by Wang on 2015-01-20.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADAdoreManageViewController.h"
#import "ADButtonTableViewCell.h"
#import "ADSwitchTableViewCell.h"
#import "ADManagedUser+Extensions.h"
#import "ADUserManager.h"
#import "NSObject+Notification.h"
#import "ADReportViewController.h"
#import "ADNavigationController.h"



@interface ADAdoreManageViewController () <UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate>

@end



@implementation ADAdoreManageViewController

#pragma mark -
#pragma mark - Lazy instantiation

- (ADAdoreConnectionNetworkingEngine *)networkingEngine
{
    if (_networkingEngine == nil) {
        _networkingEngine = [[ADAdoreConnectionNetworkingEngine alloc] init];
    }
    return _networkingEngine;
}

- (ADUserCoreDataHelper *)userCoreDataHelper
{
    if (_userCoreDataHelper == nil) {
        _userCoreDataHelper = self.userManager.userCoreDataHelper;
    }
    return _userCoreDataHelper;
}


#pragma mark -
#pragma mark - View lifecycles

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UINib *buttonNib = [UINib nibWithNibName:@"ADButtonTableViewCell" bundle:nil];
    [self.tableView registerNib:buttonNib forCellReuseIdentifier:@"ButtonCell"];

    UINib *switchNib = [UINib nibWithNibName:@"ADSwitchTableViewCell" bundle:nil];
    [self.tableView registerNib:switchNib forCellReuseIdentifier:@"SwitchCell"];
}


#pragma mark -
#pragma mark - Helper methods

- (BOOL)connectedUser
{
    ADUserRelationshipType type = self.userObject.relationType;
    if (type == ADUserRelationshipTypeConnected
        || type == ADUserRelationshipTypeBlocked) {
        return YES;
    }
    return NO;
}
   
- (void)blockUser:(id)sender
{
    [self showLoadingIndicator:nil];
    UISwitch *switchControl = (UISwitch *)sender;

    __block BOOL switchOn = switchControl.on;
    __weak __typeof(self) weakSelf = self;
    
    ADConnectionNetworkCompletionBlock completionBlock = ^(BOOL response, BOOL approved, NSError *error) {
        [self dismissLoadingIndicator];
        if (response) {
            ADUserRelationshipType type = ADUserRelationshipTypeInvalid;
            if (switchOn) {
                type = ADUserRelationshipTypeBlocked;
            }
            else {
                type = ADUserRelationshipTypeConnected;
            }

            if (type != ADUserRelationshipTypeInvalid) {
                [weakSelf.userCoreDataHelper saveUserWithEmail:weakSelf.userObject.email withUserObject:nil withRelationshipType:type updateProfileOnly:NO isRead:NO save:YES];
            }
        }
    };
    
    // If the switch is switched to ON, then block the user
    if (switchOn) {
        [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_ADORE_BLOCK_USER];
        [self.networkingEngine startBlockingUser:self.userObject.email completionBlock:completionBlock];
    }
    else {
        [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_ADORE_UNBLOCK_USER];
        [self.networkingEngine startUnblockingUser:self.userObject.email completionBlock:completionBlock];
    }
}

- (void)deleteUser
{
    __weak __typeof(self) weakSelf = self;

    [self showLoadingIndicator:nil];
    [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_ADORE_DELETE_USER];
    ADConnectionNetworkCompletionBlock completionBlock = ^(BOOL response, BOOL approved, NSError *error) {
        [weakSelf dismissLoadingIndicator];
        if (response) {
            [weakSelf.navigationController popToRootViewControllerAnimated:YES];
        }
        else {
            [weakSelf showErrorMessage:error.localizedDescription];
        }
    };
    
    [self.networkingEngine startDeletingUser:self.userObject completionBlock:completionBlock];
}

- (void)deleteConfirmation
{
    UIActionSheet *delete = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Delete", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:NSLocalizedString(@"Delete", nil) otherButtonTitles:nil];
    [delete showInView:self.view];
}

- (void)showReportView
{
    ADReportViewController *report = [[ADReportViewController alloc] initWithNibName:@"ADReportViewController" bundle:nil];
    [report setReportedUserId:self.userObject.email];
    ADNavigationController *navi = [[ADNavigationController alloc] initWithRootViewController:report];
    [self presentViewController:navi animated:YES completion:nil];
}


#pragma mark -
#pragma mark - UIActionSheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self deleteUser];
    }
}


#pragma mark -
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        ADSwitchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SwitchCell"];
        BOOL blocked = NO;
        ADUserRelationshipType type = self.userObject.relationType;
        if (type == ADUserRelationshipTypeBlocked) {
            blocked = YES;
        }
        [cell updateWithText:NSLocalizedString(@"Block", nil) switchOn:blocked];
        [cell.switchControl addTarget:self action:@selector(blockUser:) forControlEvents:UIControlEventValueChanged];
        return cell;
    }
    if (indexPath.section == 1) {
        ADButtonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ButtonCell"];
        [cell updateWithImage:nil text:NSLocalizedString(@"Report", nil)];
        cell.label.textColor = [Constants femaleTintColor];
        return cell;
    }

    if (indexPath.section == 2) {
        ADButtonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ButtonCell"];
        [cell updateWithImage:nil text:NSLocalizedString(@"Delete", nil)];
        cell.label.textColor = [UIColor redColor];
        return cell;
    }
    return [[UITableViewCell alloc] init];
}


#pragma mark -
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 2) {
        [self deleteConfirmation];
    }
    else if (indexPath.section == 1) {
        [self showReportView];
    }
}

@end
