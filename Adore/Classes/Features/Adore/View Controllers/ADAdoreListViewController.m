//
//  ADAdoreListViewController.m
//  Adore
//
//  Created by Wang on 2014-05-26.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADAdoreListViewController.h"
#import "ADUserObject.h"
#import "ADImageTextTableViewCell.h"
#import "ADUserConnectionTableViewController.h"
#import "ADManagedUser+Extensions.h"
#import "ADUserCoreDataHelper.h"
#import "ADChatManager.h"
#import "UIScrollView+EmptyDataSet.h"

// Cells
static NSString * const kAD_ADORE_ADORE_TABLE_VIEW_CELL_IDENTIFIER = @"AdoreCell";

// Segues
static NSString * const kAD_ADORE_SHOW_PENDING_REQUESTS_SEGUE = @"ShowPendingRequest";
static NSString * const kAD_ADORE_SHOW_USER_DETAILS_SEGUE = @"ShowUserDetails";



@interface ADAdoreListViewController () <UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>

@end


@implementation ADAdoreListViewController

#pragma mark -
#pragma mark - Initializers

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _selectorToReturnDisplayName = @selector(canonicalSearchTerm);
        _filteringPredicate = [NSPredicate predicateWithFormat:@"canonicalSearchTerm CONTAINS $searchTerm"];
        
        self.connectedUsersController = [self.userManager.userCoreDataHelper connectedUsersFetchedResultsControllerWithDelegate:self];
        self.pendingUsersController = [self.userManager.userCoreDataHelper pendingUsersFetchedResultsControllerWithDelegate:self];
        self.unreadConnectionsController = [self.userManager.userCoreDataHelper unreadConnectedUsersFetchedResultsControllerWithDelegate:self];
        self.unreadMessagesController = [self.chatManager unreadMessagesFetchedResultsControllerWithDelegate:self];
    }
    return self;
}


#pragma mark -
#pragma mark - View lifecycles

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Adore", nil);
    
    UINib *nib = [UINib nibWithNibName:@"ADImageTextTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kAD_ADORE_ADORE_TABLE_VIEW_CELL_IDENTIFIER];
    [self.searchDisplayController.searchResultsTableView registerNib:nib forCellReuseIdentifier:kAD_ADORE_ADORE_TABLE_VIEW_CELL_IDENTIFIER];
    
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.tableFooterView = [UIView new];
    
    [self reloadTableView];
}

- (void)dealloc
{
    self.tableView.emptyDataSetSource = nil;
    self.tableView.emptyDataSetDelegate = nil;
}


#pragma mark -
#pragma mark - Helper methods

- (void)reloadTableView
{
    self.adores = self.connectedUsersController.fetchedObjects;
    self.sortedAdoresArrayOfArrays = [self partitionObjects:self.adores usingCollationStringSelector:self.selectorToReturnDisplayName];
    [self.tableView reloadData];
}

- (NSArray *)partitionObjects:(NSArray *)arrayOfObjects usingCollationStringSelector:(SEL)selector
{
    UILocalizedIndexedCollation *collation = [UILocalizedIndexedCollation currentCollation];
    
    NSInteger sectionCount = [[collation sectionTitles] count];
    NSMutableArray *unsortedSections = [NSMutableArray arrayWithCapacity:sectionCount];
    
    for (NSInteger i = 0; i < sectionCount; i++)
    {
        [unsortedSections addObject:@[].mutableCopy];
    }
    
    for (id object in arrayOfObjects)
    {
        NSInteger index = [collation sectionForObject:object collationStringSelector:selector];
        [unsortedSections[index] addObject:object];
    }
    
    NSMutableArray *sections = [NSMutableArray arrayWithCapacity:sectionCount];
    for (NSMutableArray *section in unsortedSections)
    {
        [sections addObject:[collation sortedArrayFromArray:section collationStringSelector:selector]];
    }
    return sections.copy;
}

- (void)filterAdoresArrayForString:(NSString *)searchString
{
    if (searchString.length > 0) {
        NSDictionary *searchDict = @{@"searchTerm" : [searchString lowercaseString]};
        self.filteredAdoresArray = [self.adores filteredArrayUsingPredicate:[self.filteringPredicate predicateWithSubstitutionVariables:searchDict]];
    }
    else {
        self.filteredAdoresArray = nil;
    }
}

- (void)pushUserConnectionViewControllerWithUserEmail:(NSString *)email
{
    self.selectedUserObject = [self.userManager.userCoreDataHelper loadUserObjectFromCoreDataByEmail:email];
    if (self.selectedUserObject) {
        [self performSegueWithIdentifier:kAD_ADORE_SHOW_USER_DETAILS_SEGUE sender:nil];
    }
}


#pragma mark -
#pragma mark - NSFetchedResultsControllerDelegate methods

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if (controller == self.connectedUsersController) {
        [self reloadTableView];
    }
    else if (controller == self.pendingUsersController
             || controller == self.unreadMessagesController
             || controller == self.unreadConnectionsController) {
        [self updateDrawerBadge];
    }
}


#pragma mark -
#pragma mark - Search display delegate

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterAdoresArrayForString:searchString];
    
    return YES;
}


#pragma mark -
#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:28.0],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"No Adorer Found", nil)
                                           attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = NSLocalizedString(@"Go to Nearby User and pick the one you like.", nil);
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:14.0],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}


#pragma mark -
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.tableView) {
        if (self.sortedAdoresArrayOfArrays.count > 0) {
            return [[UILocalizedIndexedCollation currentCollation] sectionTitles].count;
        }
    }
    else {
        return 1;
    }
    return 0;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (tableView == self.tableView) {
        if (self.adores.count > 0 && self.sortedAdoresArrayOfArrays.count > 0) {
            return [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles];
        }
    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView == self.tableView) {
        if (section < [self.sortedAdoresArrayOfArrays count]) {
            if ([self.sortedAdoresArrayOfArrays[section] count] > 0) {
                return [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section];
            }
        }
    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tableView) {
        NSArray *sectionArray = [self.sortedAdoresArrayOfArrays objectAtIndex:section];
        return sectionArray.count;
    }
    else {
        return self.filteredAdoresArray.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ADImageTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_ADORE_ADORE_TABLE_VIEW_CELL_IDENTIFIER];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    
    if (tableView == self.tableView) {
        NSArray *sectionArray = self.sortedAdoresArrayOfArrays[indexPath.section];
        ADManagedUser *user = (ADManagedUser *)[sectionArray objectAtIndex:indexPath.row];
        [cell updateViewWithUserObject:user showBadge:NO];
        return cell;
    }
    else {
        ADManagedUser *user = (ADManagedUser *)[self.filteredAdoresArray objectAtIndex:indexPath.row];
        [cell updateViewWithUserObject:user showBadge:NO];
        return cell;
    }
}


#pragma mark -
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ADImageTextTableViewCell *cell = (ADImageTextTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    self.selectedUserObject = cell.userObject;
    [self.userManager.userCoreDataHelper markUserAsRead:self.selectedUserObject];
    [self performSegueWithIdentifier:kAD_ADORE_SHOW_USER_DETAILS_SEGUE sender:nil];
}


#pragma mark -
#pragma mark - Segue Actions

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kAD_ADORE_SHOW_USER_DETAILS_SEGUE]) {
        [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_ADORE_VIEW_USER];
        ADUserConnectionTableViewController *userConnectionView = segue.destinationViewController;
        userConnectionView.userObject = self.selectedUserObject;
    }
}

@end
