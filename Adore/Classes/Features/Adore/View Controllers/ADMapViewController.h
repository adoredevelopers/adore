//
//  ADMapViewController.h
//  Adore
//
//  Created by Kevin Wang on 2014-09-30.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADViewController.h"
#import <MapKit/MapKit.h>


@class ADManagedCoupon;


@interface ADMapViewController : ADViewController


@property (nonatomic, weak) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) ADManagedCoupon *coupon;


@end
