//
//  ADNearbyUserListViewCell.m
//  Adore
//
//  Created by Chao Lu on 2016-02-24.
//  Copyright © 2016 AdoreMobile Inc. All rights reserved.
//

#import "ADNearbyUserListViewCell.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ADNearbyUserObject.h"
#import "NSDate+Utils.h"

@implementation ADNearbyUserListViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateViewWithNearbyUserObject:(ADNearbyUserObject *)nearbyUser
{
    self.nearbyUserObject = nearbyUser;
    BOOL isMale;
    if ([nearbyUser.gender isEqualToString:@"Male"]) {
        isMale = YES;
    } else {
        isMale = NO;
    }
    
    // ===========================
    // Nearby user profile images
    // ===========================
    self.profileImageView.layer.cornerRadius = 4;
    self.profileImageView.clipsToBounds = YES;
    [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:[nearbyUser profilePictureThumbnailUrlWithSize:@160]] placeholderImage:[UIImage imageNamed:@"profile_image_placeholder_150x150"] options:SDWebImageRetryFailed | SDWebImageLowPriority];
    
    
    // =======================
    // Nearby user basic info
    // =======================
    self.nameLabel.text = nearbyUser.nickname;
    self.mottoLabel.text = nearbyUser.motto;
    if ([[self.mottoLabel.text stringByTrim] isEqualToString:@""]) {
        self.mottoLabel.text = NSLocalizedString(@"I am too shy to leave anything about myself.", nil);
    }
    
    
    // ===================
    // Nearby user zodiac
    // ===================
//    if (isMale) {
//        self.zodiacImage.tintColor = [Constants maleTintColor];
//    } else {
//        self.zodiacImage.tintColor = [Constants femaleTintColor];
//    }
    NSDictionary *zodiac_map = @{
                                 @"1": @"aries",
                                 @"2": @"taurus",
                                 @"3": @"gemini",
                                 @"4": @"cancer",
                                 @"5": @"leo",
                                 @"6": @"virgo",
                                 @"7": @"libra",
                                 @"8": @"scorpio",
                                 @"9": @"sagittarius",
                                 @"10": @"capricorn",
                                 @"11": @"aquarius",
                                 @"12": @"pisces"
                                 };
    NSString *zodiac_string = [zodiac_map valueForKey:[NSString stringWithFormat:@"%@", nearbyUser.zodiac]];
    self.zodiacImage.image = [UIImage imageNamed:zodiac_string];
    self.zodiacImage.hidden = YES;
    
    
    // ===================
    // Nearby user gender
    // ===================
    if (isMale) {
        self.genderImage.image = [[UIImage imageNamed:@"list_male"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.genderImage.tintColor = [Constants maleTintColor];
    }
    else {
        self.genderImage.image = [[UIImage imageNamed:@"list_female"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.genderImage.tintColor = [Constants femaleTintColor];
    }
    self.genderImage.layer.cornerRadius = 2;
    self.genderImage.clipsToBounds = YES;
    
    if (isMale) {
        self.ageLabel.backgroundColor = [Constants maleTintColor];
    } else {
        self.ageLabel.backgroundColor = [Constants femaleTintColor];
    }
    self.ageLabel.text = [NSString stringWithFormat:@"%ld", (long)nearbyUser.birthday.age];
    self.ageLabel.textColor = [UIColor whiteColor];
    self.ageLabel.layer.cornerRadius = 2;
    self.ageLabel.clipsToBounds = YES;
    
    
    // =========================
    // Nearby user photo counts
    // =========================
//    self.photoNumberImage.image = [[UIImage imageNamed:@"photos"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    if (isMale) {
//        self.photoNumberImage.tintColor = [Constants maleTintColor];
//    } else {
//        self.photoNumberImage.tintColor = [Constants femaleTintColor];
//    }
//    
//    long badgeCount = nearbyUser.displayImageUrlArray.count;
//    self.photoNumLabel.text = [NSString stringWithFormat:@"%ld", badgeCount];
    
    
    // ========================
    // Nearby user impressions
    // ========================
    if (isMale) {
//        self.impressionImage.tintColor = [Constants maleTintColor];
        self.impressionImage.tintColor = [UIColor purpleColor];
//        self.impressionLabel.backgroundColor = [Constants maleTintColor];
    } else {
//        self.impressionImage.tintColor = [Constants femaleTintColor];
        self.impressionImage.tintColor = [UIColor purpleColor];
//        self.impressionLabel.backgroundColor = [Constants femaleTintColor];
    }
    self.impressionImage.image = [[UIImage imageNamed:@"list_flame"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.impressionImage.layer.cornerRadius = 2;
    self.impressionImage.clipsToBounds = YES;
    
    NSNumber *impressionCount = nearbyUser.impression_count;
    self.impressionLabel.text = [impressionCount stringValue];
    self.impressionLabel.textColor = [UIColor whiteColor];
    self.impressionLabel.layer.cornerRadius = 2;
    self.impressionLabel.clipsToBounds = YES;
    self.impressionLabel.backgroundColor = [UIColor purpleColor];
    
    
    // ==================
    // Nearby user likes
    // ==================
    if (isMale) {
//        self.likesImage.tintColor = [Constants maleTintColor];
        self.likesImage.tintColor = [UIColor orangeColor];
//        self.likesLabel.backgroundColor = [Constants maleTintColor];
    } else {
//        self.likesImage.tintColor = [Constants femaleTintColor];
        self.likesImage.tintColor = [UIColor orangeColor];
//        self.likesLabel.backgroundColor = [Constants femaleTintColor];
    }
    self.likesImage.image = [[UIImage imageNamed:@"list_heart"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.likesImage.layer.cornerRadius = 2;
    self.likesImage.clipsToBounds = YES;
    
    NSNumber *likesCount = nearbyUser.likes_count;
    self.likesLabel.text = [likesCount stringValue];
    self.likesLabel.textColor = [UIColor whiteColor];
    self.likesLabel.layer.cornerRadius = 2;
    self.likesLabel.clipsToBounds = YES;
    self.likesLabel.backgroundColor = [UIColor orangeColor];
    
    // ======================
    // Distance & Last Visit
    // ======================
    self.lastActiveLabel.text = [nearbyUser lastActivitySinceString];
    self.lastActiveLabel.hidden = @YES;
    NSString *distanceLocalizedString = NSLocalizedString(@"%.2f km", nil);
    self.distanceLabel.text = [NSString stringWithFormat:distanceLocalizedString, [nearbyUser.distance floatValue]/1000];
}

@end
