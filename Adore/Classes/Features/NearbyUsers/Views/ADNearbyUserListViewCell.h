//
//  ADNearbyUserListViewCell.h
//  Adore
//
//  Created by Chao Lu on 2016-02-24.
//  Copyright © 2016 AdoreMobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADNearbyUserObject.h"
#import "YYKit.h"

@interface ADNearbyUserListViewCell : UITableViewCell



@property (nonatomic, strong) ADNearbyUserObject *nearbyUserObject;

@property (nonatomic, weak) IBOutlet UIImageView *profileImageView;

@property (nonatomic, weak) IBOutlet YYLabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIImageView *zodiacImage;
@property (weak, nonatomic) IBOutlet UIImageView *genderImage;
@property (weak, nonatomic) IBOutlet YYLabel *ageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *photoNumberImage;
@property (weak, nonatomic) IBOutlet YYLabel *photoNumberLabel;

@property (weak, nonatomic) IBOutlet UIImageView *impressionImage;
@property (weak, nonatomic) IBOutlet YYLabel *impressionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *likesImage;
@property (weak, nonatomic) IBOutlet YYLabel *likesLabel;

@property (nonatomic, weak) IBOutlet YYLabel *lastActiveLabel;
@property (nonatomic, weak) IBOutlet YYLabel *distanceLabel;

@property (nonatomic, weak) IBOutlet YYLabel *mottoLabel;


- (void)updateViewWithNearbyUserObject:(ADNearbyUserObject *)nearbyUser;



@end
