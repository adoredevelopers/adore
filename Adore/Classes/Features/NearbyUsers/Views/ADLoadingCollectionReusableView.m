//
//  ADLoadingCollectionReusableView.m
//  Adore
//
//  Created by Kevin Wang on 2015-06-11.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADLoadingCollectionReusableView.h"
#import "M13ProgressViewRing.h"



@interface ADLoadingCollectionReusableView ()


@property (nonatomic, weak) IBOutlet M13ProgressViewRing *loadingRing;
@property (nonatomic, weak) IBOutlet UILabel *textLabel;


@end



@implementation ADLoadingCollectionReusableView


- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UIColor *tintColor = [Constants femaleTintColor];
    self.loadingRing.primaryColor = tintColor;
    self.loadingRing.secondaryColor = tintColor;
    self.textLabel.textColor = tintColor;
    
    self.loadingRing.showPercentage = NO;
    self.loadingRing.indeterminate = YES;
    self.textLabel.text = NSLocalizedString(@"Loading...", nil);
}

- (void)show
{
    self.loadingRing.hidden = NO;
    self.textLabel.hidden = NO;
}

- (void)hide
{
    self.loadingRing.hidden = YES;
    self.textLabel.hidden = YES;
}


@end
