//
//  ADNearbyUserCollectionViewCell.m
//  Adore
//
//  Created by Wang on 2015-04-15.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADNearbyUserCollectionViewCell.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ADNearbyUserObject.h"
#import "NSDate+Utils.h"
#import "CustomBadge.h"



@interface ADNearbyUserCollectionViewCell ()

@property (nonatomic, strong) CustomBadge *badgeView;

@end



@implementation ADNearbyUserCollectionViewCell

#pragma mark -
#pragma mark - Helper methods

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.heartButton setImage:[[UIImage imageNamed:@"heart"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.nearbyUserDividerView.backgroundColor = [Constants femaleTintColor];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.clipsToBounds = YES;
    self.backgroundColor = [UIColor whiteColor];
    self.heartButton.layer.cornerRadius = self.heartButton.frame.size.height/2;
}

- (void)updateViewWithNearbyUserObject:(ADNearbyUserObject *)nearbyUser
{
    // ===========================
    // Nearby user profile images
    // ===========================
    self.nearbyUserObject = nearbyUser;
    
    [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:[nearbyUser profilePictureUrl]] placeholderImage:[UIImage imageNamed:@"profile_image_placeholder_150x150"] options:SDWebImageRetryFailed | SDWebImageLowPriority];

    if ([nearbyUser.connection_status isEqualToString:kAD_CONNECTION_STATUS_PENDING]
         || [nearbyUser.connection_status length] == 0) {
        self.heartButton.hidden = NO;
    }
    else {
        self.heartButton.hidden = YES;
    }
    
    
    // =======================
    // Nearby user basic info
    // =======================
    self.profileImageView.clipsToBounds = YES;
    
    self.nameLabel.text = nearbyUser.nickname;
    self.mottoLabel.text = nearbyUser.motto;
    if ([[self.mottoLabel.text stringByTrim] isEqualToString:@""]) {
        self.mottoLabel.text = NSLocalizedString(@"I am too shy to leave anything about myself.", nil);
    }
    self.lastActiveLabel.text = [nearbyUser lastActivitySinceString];
    self.lastActiveLabel.hidden = @YES;
    NSString *distanceLocalizedString = NSLocalizedString(@"%.2f km", nil);
    self.distanceLabel.text = [NSString stringWithFormat:distanceLocalizedString, [nearbyUser.distance floatValue]/1000];
    
    
    // ===================
    // Nearby user gender
    // ===================
    BOOL isMale;
    if ([nearbyUser.gender isEqualToString:@"Male"]) {
        self.sexImage.image = [[UIImage imageNamed:@"male"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        isMale = YES;
        self.sexImage.tintColor = [Constants maleTintColor];
    }
    else {
        self.sexImage.image = [[UIImage imageNamed:@"female"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        isMale = NO;
        self.sexImage.tintColor = [Constants femaleTintColor];
    }
    
    self.ageLabel.text = [NSString stringWithFormat:@"%ld", (long)nearbyUser.birthday.age];
    
    
    // ===================
    // Nearby user zodiac
    // ===================
    NSDictionary *zodiac_map = @{
                                 @"1": @"aries",
                                 @"2": @"taurus",
                                 @"3": @"gemini",
                                 @"4": @"cancer",
                                 @"5": @"leo",
                                 @"6": @"virgo",
                                 @"7": @"libra",
                                 @"8": @"scorpio",
                                 @"9": @"sagittarius",
                                 @"10": @"capricorn",
                                 @"11": @"aquarius",
                                 @"12": @"pisces"
                                 };
    NSString *zodiac_string = [zodiac_map valueForKey:[NSString stringWithFormat:@"%@", nearbyUser.zodiac]];
    self.zodiacImage.image = [[UIImage imageNamed:zodiac_string] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    if (isMale) {
        self.zodiacImage.tintColor = [Constants maleTintColor];
    } else {
        self.zodiacImage.tintColor = [Constants femaleTintColor];
    }
    
    
    // =========================
    // Nearby user photo counts
    // =========================
    long photoCount = nearbyUser.displayImageUrlArray.count;
    
    if (photoCount > 1) {
        NSTextAttachment *pictureIconAttachment = [[NSTextAttachment alloc] init];
        pictureIconAttachment.bounds = CGRectMake(0, [UIFont systemFontOfSize:24.0].descender, 20.0, 20.0);
        pictureIconAttachment.image = [UIImage imageNamed:@"photo_image"];
        NSAttributedString *pictureIconAttachmentString = [NSAttributedString attributedStringWithAttachment:pictureIconAttachment];
        NSString *photoCountString = [NSString stringWithFormat:@" %ld", photoCount];
        NSMutableAttributedString *photoCountAttriString = [[NSMutableAttributedString alloc] initWithAttributedString:pictureIconAttachmentString];
        [photoCountAttriString appendString:photoCountString];
        self.photoCountLabel.attributedText = photoCountAttriString;
        self.photoCountLabel.backgroundColor = [UIColor darkGrayColor];
        self.photoCountLabel.alpha = 0.7;
        self.photoCountLabel.textColor = [UIColor whiteColor];
        self.photoCountLabel.layer.cornerRadius = 4;
        self.photoCountLabel.clipsToBounds = YES;
        self.photoCountLabel.hidden = false;
    } else {
        self.photoCountLabel.hidden = true;
    }
    
    
    // ========================
    // Nearby user impressions
    // ========================
    self.impressionImage.image = [[UIImage imageNamed:@"flame"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    if (isMale) {
//        self.impressionImage.tintColor = [Constants maleTintColor];
//    } else {
//        self.impressionImage.tintColor = [Constants femaleTintColor];
//    }
    self.impressionImage.tintColor = [UIColor purpleColor];
    
    NSNumber *impressionCount = nearbyUser.impression_count;
    self.impressionNumLabel.text = [impressionCount stringValue];
    
    
    // ==================
    // Nearby user likes
    // ==================
    self.likedImage.image = [[UIImage imageNamed:@"ios7-heart"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    if (isMale) {
//        self.likedImage.tintColor = [Constants maleTintColor];
//    } else {
//        self.likedImage.tintColor = [Constants femaleTintColor];
//    }
    self.likedImage.tintColor = [UIColor orangeColor];
    
    NSNumber *likesCount = nearbyUser.likes_count;
    self.likedNumLabel.text = [likesCount stringValue];
}

@end
