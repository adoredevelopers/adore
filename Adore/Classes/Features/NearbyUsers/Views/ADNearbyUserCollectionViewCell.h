//
//  ADNearbyUserCollectionViewCell.h
//  Adore
//
//  Created by Wang on 2015-04-15.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADNearbyUserObject.h"
#import "YYKit.h"


@interface ADNearbyUserCollectionViewCell : UICollectionViewCell


@property (nonatomic, strong) ADNearbyUserObject *nearbyUserObject;

@property (weak, nonatomic) IBOutlet UIView *nearbyUserDividerView;

@property (nonatomic, weak) IBOutlet UIImageView *profileImageView;
@property (nonatomic, weak) IBOutlet YYLabel *nameLabel;
@property (nonatomic, weak) IBOutlet YYLabel *mottoLabel;
@property (nonatomic, weak) IBOutlet YYLabel *lastActiveLabel;
@property (nonatomic, weak) IBOutlet YYLabel *distanceLabel;

@property (weak, nonatomic) IBOutlet YYLabel *photoCountLabel;

@property (weak, nonatomic) IBOutlet UIView *nearbyUserRightSideBarView;
@property (weak, nonatomic) IBOutlet UIImageView *sexImage;
@property (weak, nonatomic) IBOutlet YYLabel *ageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *zodiacImage;

@property (weak, nonatomic) IBOutlet UIImageView *impressionImage;
@property (weak, nonatomic) IBOutlet YYLabel *impressionNumLabel;
@property (weak, nonatomic) IBOutlet UIImageView *likedImage;
@property (weak, nonatomic) IBOutlet YYLabel *likedNumLabel;

@property (nonatomic, weak) IBOutlet UIButton *heartButton;


- (void)updateViewWithNearbyUserObject:(ADNearbyUserObject *)nearbyUser;


@end
