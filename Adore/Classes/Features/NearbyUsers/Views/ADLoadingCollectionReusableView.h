//
//  ADLoadingCollectionReusableView.h
//  Adore
//
//  Created by Kevin Wang on 2015-06-11.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ADLoadingCollectionReusableView : UICollectionReusableView


- (void)show;
- (void)hide;


@end
