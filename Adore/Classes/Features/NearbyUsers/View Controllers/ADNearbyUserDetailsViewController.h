//
//  ADUserDetailsViewController.h
//  Adore
//
//  Created by Chao Lu on 2013-10-15.
//  Copyright (c) 2013 AdoreMobile. All rights reserved.
//

#import "ADViewController.h"

@class ADNearbyUserObject;



@interface ADNearbyUserDetailsViewController : ADViewController


@property (nonatomic, strong) ADNearbyUserObject *nearbyUserObject;


@end
