//
//  ADNearbyUserViewController.m
//  Adore
//
//  Created by Wang on 2015-04-15.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADNearbyUserViewController.h"
#import "ADNearbyUserDetailsViewController.h"
#import "ADUserManager.h"
#import "ADUserDefaultHelper.h"
#import "ADManagedUser+Extensions.h"
#import "ADUserCoreDataHelper.h"
#import "ADNearbyUserCollectionViewCell.h"
#import "ADNearbyUserListViewCell.h"
#import "ADChatManager.h"
#import "UIScrollView+EmptyDataSet.h"
#import "ADLoadingCollectionReusableView.h"



@interface ADNearbyUserViewController () <ADNearbyUserFilterDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>

@end



@implementation ADNearbyUserViewController

#pragma mark -
#pragma mark - Initializer

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        _nearbyUserList = @[].mutableCopy;
        _hasMoreNearbyUsers = YES;
        _networkingEngine = [[ADAdoreConnectionNetworkingEngine alloc] init];
    }
    return self;
}


#pragma mark -
#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Nearby User", nil);
    
    UIImage *switchImage = [UIImage imageNamed:@"grid"];
    self.navigationItem.titleView.backgroundColor = [Constants femaleTintColor];
    self.switcherBtn.titleLabel.text = @"";
    [self.switcherBtn setImage:switchImage forState:UIControlStateNormal];
    
    self.pendingUsersController = [self.userManager.userCoreDataHelper pendingUsersFetchedResultsControllerWithDelegate:self];
    self.unreadMessagesController = [self.chatManager unreadMessagesFetchedResultsControllerWithDelegate:self];
    self.unreadConnectionsController = [self.userManager.userCoreDataHelper unreadConnectedUsersFetchedResultsControllerWithDelegate:self];
    
    self.listRefreshControl = [[UIRefreshControl alloc] init];
    [self.listRefreshControl addTarget:self action:@selector(reloadNearbyUserList)
                      forControlEvents:UIControlEventValueChanged];
    self.collectionRefreshControl = [[UIRefreshControl alloc] init];
    [self.collectionRefreshControl addTarget:self action:@selector(reloadNearbyUserList)
                  forControlEvents:UIControlEventValueChanged];

    [self.tableView addSubview:self.listRefreshControl];
    [self.collectionView addSubview:self.collectionRefreshControl];
    
    self.collectionView.emptyDataSetDelegate = self;
    self.collectionView.emptyDataSetSource = self;
    
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.emptyDataSetSource = self;
    
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
//    width = MIN(width, 320);
    layout.itemSize = CGSizeMake(width, width);
    layout.minimumInteritemSpacing = 0.0;
    layout.minimumLineSpacing = 0.0;
    layout.sectionInset = UIEdgeInsetsZero; // UIEdgeInsetsMake(20, 20, 20, 20);
    self.collectionView.collectionViewLayout = layout;
    
    self.currentPresentationView = @"collection";
    [self switchPresentationViewLayout:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // If the list is empty and we have a current location, load more nearby users
    if (self.nearbyUserList.count == 0 && [self.userManager isLoggedIn] && self.userManager.locationHelper.currentLocation) {
        [self reloadNearbyUserList];
    }
}

- (void)dealloc
{
    self.collectionView.emptyDataSetSource = nil;
    self.collectionView.emptyDataSetDelegate = nil;
}


- (IBAction)switchPresentationViewLayout:(id)sender
{
    if ([self.currentPresentationView isEqualToString:@"list"]) {
        self.tableView.hidden = YES;
        self.collectionView.hidden = NO;
        [self.view addSubview:self.collectionView];
        self.currentPresentationView = @"collection";
//        [self.collectionView setScrollsToTop:YES];
//        [self.tableView setScrollsToTop:NO];
        
        [self.collectionView reloadData];
    } else if ([self.currentPresentationView isEqualToString:@"collection"]) {
        self.collectionView.hidden = YES;
        self.tableView.hidden = NO;
        self.currentPresentationView = @"list";
//        [self.tableView setScrollsToTop:YES];
//        [self.collectionView setScrollsToTop:NO];
        
        [self.tableView reloadData];
    }
}


#pragma mark -
#pragma mark - Helper methods

- (void)showFaceNotValidWarning
{
    [self showMessageWithTitle:NSLocalizedString(@"Warning", nil) message:NSLocalizedString(@"Your profile image is not to our standard, please upload a clear photo of yours.", nil)];
}


#pragma mark -
#pragma mark - Web Service Calls

- (void)reloadNearbyUserList
{
    [self.listRefreshControl beginRefreshing];
    [self.collectionRefreshControl beginRefreshing];
    
    self.isLoading = YES;
    [self loadNearbyUsers:NO];
}

- (void)loadMoreNearbyUserList
{
    [self loadNearbyUsers:YES];
}

- (void)loadNearbyUsers:(BOOL)isLoadingMore
{
    // Cancel existing request
    if (self.nearbyUserRequest) {
        [self.nearbyUserRequest cancel];
        self.nearbyUserRequest = nil;
    }
    
    if (isLoadingMore == NO) {
        [self.listRefreshControl endRefreshing];
        [self.collectionRefreshControl endRefreshing];
        
        [self showLoadingIndicator:NSLocalizedString(@"Loading...", nil)];
    }
    
    __weak typeof(self) weakSelf = self;
    id completionBlock = ^(NSArray *array, NSError *error) {
        if (error.code != NSURLErrorCancelled) {
            self.isLoading = NO;
            [self dismissLoadingIndicator];
        }
        // Don't show CANCELLED error
        if (error && error.code != NSURLErrorCancelled) {
            self.error = error;
        }
        else if (error == nil) {
            self.error = nil;
        }
        
        if (isLoadingMore == NO) {
            [self.nearbyUserList removeAllObjects];
            [self.collectionView reloadData];
            [self.tableView reloadData];
        }
        
        NSInteger count = 0;
        for (ADNearbyUserObject *user in array)
        {
            NSUInteger index = [self.nearbyUserList indexOfObjectPassingTest:^BOOL(ADNearbyUserObject *obj, NSUInteger idx, BOOL *stop) {
                return [obj.email isEqualToString:user.email];
            }];
            // Add to the list if new
            if (index == NSNotFound) {
                count++;
                [weakSelf.nearbyUserList addObject:user];
            }
        }
        if (count == 0) {
            weakSelf.hasMoreNearbyUsers = NO;
        }
        else {
            weakSelf.hasMoreNearbyUsers = YES;
        }
        
        // Release Pull Lock
        weakSelf.nearbyUserRequest = nil;
        
        [weakSelf.collectionView reloadData];
        [weakSelf.tableView reloadData];
    };
    
    NSInteger minimumAge = [ADUserDefaultHelper minimumAge];
    NSInteger maximumAge = [ADUserDefaultHelper maximumAge];
    NSInteger maximumDistance = [ADUserDefaultHelper maximumDistance];
    ADGenderFilter genderFilter = [ADUserDefaultHelper gender];
    
    self.nearbyUserRequest = [[ADNearbyUserRequest alloc] initWithMode:isLoadingMore ? @"paging" : @"normal"
                                                             BatchSize:10
                                                                Gender:genderFilter
                                                                MinAge:minimumAge
                                                                MaxAge:maximumAge
                                                           MaxDistance:maximumDistance
                                                     CompletionHandler:completionBlock];
}

- (void)requestConnectionButtonClicked:(UIButton *)button
{
    BOOL faceValidated = [self.userManager.userCoreDataHelper currentUserFaceValidated];
    if (faceValidated == NO) {
        [self showFaceNotValidWarning];
    }
    else {
        ADNearbyUserObject *nearbyUser = self.nearbyUserList[button.tag];
        [self requestConnection:nearbyUser];
        
        [self updateNearbyUserLikes:nearbyUser.email];
    }
}

- (void)requestConnection:(ADNearbyUserObject *)nearbyUserObject
{
    [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_NEARBY_USER_SEND_REQUEST];
    [self showLoadingIndicator:NSLocalizedString(@"Sending request...", nil)];
    
    __weak __typeof(self) weakSelf = self;
    ADConnectionNetworkCompletionBlock completionBlock = ^(BOOL response, BOOL approved, NSError *error) {
        [weakSelf dismissLoadingIndicator];
        
        if (response) {
            // No matter if the connection is created, we hide the heart button
            nearbyUserObject.connection_status = kAD_CONNECTION_STATUS_REQUESTED;
            [weakSelf.collectionView reloadData];
            
            if (approved) {
                [weakSelf showSuccessWithTitle:NSLocalizedString(@"The right one!", nil) message:NSLocalizedString(@"Start chatting with your adorer.", nil)];
            }
            else {
                [weakSelf showSuccessWithTitle:NSLocalizedString(@"Request Sent", nil) message:NSLocalizedString(@"You will be notified when your request is approved.", nil)];
            }
        }
        else {
            [weakSelf showErrorMessage:error.localizedDescription];
        }
    };
    
    // Create a temp ADManagedUser to fit the adore network engine protocol
    ADManagedUser *managedUser = [self.userManager.userCoreDataHelper managedUserFromNearbyUser:nearbyUserObject];
    [self.networkingEngine startAddingUserRequestToUser:managedUser completionBlock:completionBlock];
}


#pragma mark -
#pragma mark - NSFetchedResultsControllerDelegate methods

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if (controller == self.pendingUsersController
        || controller == self.unreadMessagesController
        || controller == self.unreadConnectionsController) {
        [self updateDrawerBadge];
    }
}


#pragma mark -
#pragma mark - Nearby User Delegate methods

- (void)filterCancelled
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)filterUpdated
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
    [self.tableView scrollsToTop];
    [self reloadNearbyUserList];
}


#pragma mark -
#pragma mark - UICollectionViewDataSource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.nearbyUserList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ADNearbyUserCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NearbyUser" forIndexPath:indexPath];
//    ADNearbyUserObject *nearbyUser = self.nearbyUserList[indexPath.item];
//    [cell updateViewWithNearbyUserObject:nearbyUser];
//    cell.heartButton.tag = indexPath.item;
//    [cell.heartButton addTarget:self action:@selector(requestConnectionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == UICollectionElementKindSectionFooter) {
        ADLoadingCollectionReusableView *loadingView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView" forIndexPath:indexPath];
        // Show the loading indicator if the list is not empty and the list has more users
        if (self.hasMoreNearbyUsers && self.nearbyUserList.count > 0) {
            [loadingView show];
        }
        else {
            [loadingView hide];
        }
        return loadingView;
    }
    
    return nil;
}


#pragma mark -
#pragma mark - UICollectionViewDelegate methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.nearbyUserList.count) {
        // TODO: update warning message
        BOOL faceValidated = [self.userManager.userCoreDataHelper currentUserFaceValidated];
        if (faceValidated == NO) {
            [self showFaceNotValidWarning];
        }
        else {
            self.selectedNearbyUser = self.nearbyUserList[indexPath.item];
            [self performSegueWithIdentifier:@"ShowUserDetailsSegue" sender:nil];
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == self.nearbyUserList.count - 1 && self.hasMoreNearbyUsers) {
        [self loadMoreNearbyUserList];
    }
    
    ADNearbyUserObject *nearbyUser = self.nearbyUserList[indexPath.item];
    ADNearbyUserCollectionViewCell *collectionCell = (ADNearbyUserCollectionViewCell *)cell;
    [collectionCell updateViewWithNearbyUserObject:nearbyUser];
    collectionCell.heartButton.tag = indexPath.item;
    [collectionCell.heartButton addTarget:self action:@selector(requestConnectionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
}


#pragma mark -
#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.nearbyUserList.count == 0) {
        return 0;
    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.nearbyUserList.count + (self.hasMoreNearbyUsers ? 1 : 0);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.nearbyUserList.count) {
        ADNearbyUserListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserListCell"];
        return cell.bounds.size.height;
    }
    else if (indexPath.row == self.nearbyUserList.count) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LoadingCell"];
        return cell.bounds.size.height;
    }
    return tableView.rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.nearbyUserList.count) {
        ADNearbyUserListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserListCell"];
//        ADNearbyUserObject *nearbyUser = self.nearbyUserList[indexPath.row];
//        [cell updateViewWithNearbyUserObject:nearbyUser];
        
        cell.layer.shouldRasterize = YES;
        cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
        
        return cell;
    }
    else if (indexPath.row == self.nearbyUserList.count) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LoadingCell"];
        cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
        return cell;
    }
    
    return nil;
}


#pragma mark -
#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
//    if (indexPath.row < self.nearbyUserList.count) {
//        self.selectedNearbyUser = self.nearbyUserList[indexPath.row];
//        [self performSegueWithIdentifier:@"ShowUserDetailsSegue" sender:nil];
//    }
    if (indexPath.row < self.nearbyUserList.count) {
        // TODO: update warning message
        BOOL faceValidated = [self.userManager.userCoreDataHelper currentUserFaceValidated];
        if (faceValidated == NO) {
            [self showFaceNotValidWarning];
        }
        else {
            self.selectedNearbyUser = self.nearbyUserList[indexPath.item];
            [self performSegueWithIdentifier:@"ShowUserDetailsSegue" sender:nil];
        }
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == self.nearbyUserList.count) {
        [self loadMoreNearbyUserList];
    }
    
    if (indexPath.row < self.nearbyUserList.count) {
        ADNearbyUserListViewCell *listCell = (ADNearbyUserListViewCell *)cell;
        ADNearbyUserObject *nearbyUser = self.nearbyUserList[indexPath.row];
        [listCell updateViewWithNearbyUserObject:nearbyUser];
    }
}


#pragma mark -
#pragma mark - UIScrollViewDelegate methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (NSFoundationVersionNumber < NSFoundationVersionNumber_iOS_8_0) {
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
            [self loadMoreNearbyUserList];
        }
    }
}


#pragma mark -
#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    if (self.isLoading) {
        return nil;
    }
    NSString *text = nil;
    if (self.error) {
        text = self.error.localizedFailureReason;
    }
    else {
        text = NSLocalizedString(@"No nearby user found.\nPlease pull down to refresh.", nil);
    }
    if (text == nil) {
        text = @"";
    }
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = nil;
    if (self.error) {
        text = self.error.localizedDescription;
    }
    if (text == nil) {
        text = @"";
    }
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:14.0],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return YES;
}


#pragma mark -
#pragma mark - Update Stats

- (void)updateNearbyUserImpression:(NSString *)userEmail
{
    NSMutableArray *impressionUsers = [[ADUserDefaultHelper nearbyUserImpression] mutableCopy];
    if (![impressionUsers containsObject:userEmail]) {
        [impressionUsers addObject:userEmail];
    }
    [ADUserDefaultHelper setImpressionForNearbyUsers:impressionUsers];
}



- (void)updateNearbyUserLikes:(NSString *)userEmail
{
    NSMutableArray *likesUsers = [[ADUserDefaultHelper nearbyUserLikes] mutableCopy];
    if (![likesUsers containsObject:userEmail]) {
        [likesUsers addObject:userEmail];
    }
    [ADUserDefaultHelper setLikesForNearbyUsers:likesUsers];
}


#pragma mark -
#pragma mark - Segue Actions

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowUserDetailsSegue"]) {
        [self updateNearbyUserImpression:self.selectedNearbyUser.email];
        
        [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_NEARBY_USER_VIEW_USER];
        ADNearbyUserDetailsViewController *userDetailVC = (ADNearbyUserDetailsViewController *)segue.destinationViewController;
        userDetailVC.nearbyUserObject = self.selectedNearbyUser;
    }
    else if ([segue.identifier isEqualToString:@"ShowFilter"]) {
        [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_NEARBY_USER_SHOW_FILTER];
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        ADNearbyUserFilterViewController *filter = (ADNearbyUserFilterViewController *)navController.topViewController;
        filter.delegate = self;
    }
}

@end
