//
//  ADNearbyUserFilterViewController.h
//  Adore
//
//  Created by Wang on 2014-05-24.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADViewController.h"

@class NMRangeSlider;

typedef NS_ENUM(NSInteger, ADGenderFilter) {
    ADGenderFilterAll = 0,
    ADGenderFilterFemale = 1,
    ADGenderFilterMale = 2
};



@protocol ADNearbyUserFilterDelegate <NSObject>

- (void)filterCancelled;
- (void)filterUpdated;

@end



@interface ADNearbyUserFilterViewController : ADViewController


@property (nonatomic, weak) id<ADNearbyUserFilterDelegate> delegate;

@property (nonatomic, weak) IBOutlet UISegmentedControl *genderSegmentedControl;
@property (nonatomic, weak) IBOutlet UILabel *ageSectionHeaderLabel;
@property (nonatomic, weak) IBOutlet NMRangeSlider *ageSlider;
@property (nonatomic, weak) IBOutlet UILabel *distanceSectionHeaderLabel;
@property (nonatomic, weak) IBOutlet UISlider *distanceSlider;


@end
