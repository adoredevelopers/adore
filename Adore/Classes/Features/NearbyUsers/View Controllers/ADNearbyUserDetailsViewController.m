//
//  ADUserDetailsViewController.m
//  Adore
//
//  Created by Chao Lu on 2013-09-19.
//  Copyright (c) 2013 AdoreMobile. All rights reserved.
//

#import "ADNearbyUserDetailsViewController.h"
#import "ADManagedUser+Extensions.h"
#import "ADUserDetailItem.h"
#import "ADUserDetailTableViewCell.h"
#import "ADUserManager.h"
#import "ADConnectionObject.h"
#import "ADUserCoreDataHelper.h"
#import "ADAdoreConnectionNetworkingEngine.h"
#import "ADUserDetailSectionHeaderCell.h"
#import "ADProfileCoverTableViewCell.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "NYTPhotosViewController.h"
#import "ADProfileImageScrollableCollectionTableViewCell.h"
#import "ADNYTPhoto.h"
#import "ADChatManager.h"
#import "ADChatViewController.h"
#import "ADReportViewController.h"
#import "ADNavigationController.h"
#import "ADNearbyUserObject.h"
#import "ADUserDefaultHelper.h"

typedef NS_ENUM(NSInteger, ADNearbyUserDetailsSectionType)
{
    ADNearbyUserDetailsSectionTypeCover,
    ADNearbyUserDetailsSectionTypeImages,
    ADNearbyUserDetailsSectionTypeDetails
};



@interface ADNearbyUserDetailsViewController () <UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate>


@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) UIImageView *profileCoverImageView;

@property (nonatomic, strong) ADManagedUser *userObject;
@property (nonatomic, strong) ADAdoreConnectionNetworkingEngine *networkingEngine;
@property (nonatomic, strong) NSArray *detailItemArray;
@property (nonatomic, strong) NSArray *sectionArray;
@property (nonatomic, strong) NSArray *photos;


@end



@implementation ADNearbyUserDetailsViewController

#pragma mark -
#pragma mark - Lazy instantiation

- (ADAdoreConnectionNetworkingEngine *)networkingEngine
{
    if (_networkingEngine == nil) {
        _networkingEngine = [[ADAdoreConnectionNetworkingEngine alloc] init];
    }
    return _networkingEngine;
}


#pragma mark -
#pragma mark - Custom Setter Methods

- (void)setUserObject:(ADManagedUser *)userObject
{
    _userObject = userObject;
    
    self.title = self.userObject.nickname;
    self.photos = [userObject arrayOfNYTPhotos];
    self.detailItemArray = [self.userObject nearbyUserDetailItemArray];
    [self buildSectionItems];
    [self.tableView reloadData];
}

- (void)setNearbyUserObject:(ADNearbyUserObject *)nearbyUserObject
{
    _nearbyUserObject = nearbyUserObject;
    self.userObject = [self.userManager.userCoreDataHelper managedUserFromNearbyUser:nearbyUserObject];
}


#pragma mark -
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UINib *profileCoverNib = [UINib nibWithNibName:@"ADProfileCoverTableViewCell" bundle:nil];
    [self.tableView registerNib:profileCoverNib forCellReuseIdentifier:kAD_USER_DETAIL_PROFILE_COVER_IDENTIFIER];
    UINib *collectionNib = [UINib nibWithNibName:@"ADProfileImageScrollableCollectionTableViewCell" bundle:nil];
    [self.tableView registerNib:collectionNib forCellReuseIdentifier:kAD_USER_DETAIL_PROFILE_IMAGE_SCROLLABLE_COLLECTION_VIEW_CELL_IDENTIFIER];
    UINib *userDetailNib = [UINib nibWithNibName:@"ADUserDetailTableViewCell" bundle:nil];
    [self.tableView registerNib:userDetailNib forCellReuseIdentifier:kAD_USER_DETAIL_TABLE_VIEW_CELL_IDENTIFIER];
    UINib *sectionHeaderNib = [UINib nibWithNibName:@"ADUserDetailSectionHeaderCell" bundle:nil];
    [self.tableView registerNib:sectionHeaderNib forCellReuseIdentifier:kAD_USER_DETAIL_SECTION_HEADER_IDENTIFIER];
    
    UIBarButtonItem *reportButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Report", nil) style:UIBarButtonItemStyleDone target:self action:@selector(showReportView)];
    self.navigationItem.rightBarButtonItem = reportButton;
}


#pragma mark -
#pragma mark - Helper Methods

- (void)showChatView
{
    ADChatViewController *chatView = [self.chatManager chatViewWithEmail:self.userObject.email fromView:[self class]];
    [self.navigationController pushViewController:chatView animated:YES];
}

- (void)requestConnection
{
    [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_NEARBY_USER_SEND_REQUEST];
    [self showLoadingIndicator:NSLocalizedString(@"Sending request...", nil)];
    
    __weak __typeof(self) weakSelf = self;
    ADConnectionNetworkCompletionBlock completionBlock = ^(BOOL response, BOOL approved, NSError *error) {
        [weakSelf dismissLoadingIndicator];

        if (response) {
            if (approved) {
                self.nearbyUserObject.connection_status = kAD_CONNECTION_STATUS_CONNECTED;
                self.nearbyUserObject = self.nearbyUserObject; // to trigger the custom setter
                [weakSelf showSuccessWithTitle:NSLocalizedString(@"The right one!", nil) message:NSLocalizedString(@"Start chatting with your adorer.", nil)];
            }
            else {
                self.nearbyUserObject.connection_status = kAD_CONNECTION_STATUS_REQUESTED;
                self.nearbyUserObject = self.nearbyUserObject; // to trigger the custom setter
                [weakSelf showSuccessWithTitle:NSLocalizedString(@"Request Sent", nil) message:NSLocalizedString(@"You will be notified when your request is approved.", nil)];
            }
            [weakSelf.tableView reloadData];
        }
        else {
            [weakSelf showErrorMessage:error.localizedDescription];
        }
    };
    
    [self.networkingEngine startAddingUserRequestToUser:self.userObject completionBlock:completionBlock];
}


#pragma mark -
#pragma mark - Helper Methods

- (void)buildSectionItems
{
    NSMutableArray *tempArray = @[].mutableCopy;
    [tempArray addObject:@(ADNearbyUserDetailsSectionTypeCover)];
    
    if (self.userObject.profileImageArray.count > 1) {
        [tempArray addObject:@(ADNearbyUserDetailsSectionTypeImages)];
    }
    
    [tempArray addObject:@(ADNearbyUserDetailsSectionTypeDetails)];
    self.sectionArray = tempArray.copy;
}

- (void)buttonClicked:(UIButton *)sender
{
    ADUserRelationshipType relationship = [self.userObject.relationship integerValue];
    switch (relationship) {
        case ADUserRelationshipTypeInvalid:
        {
            [self requestConnection];
            [self updateNearbyUserLikes:self.userObject.email];
            break;
        }
        case ADUserRelationshipTypeConnected:
        case ADUserRelationshipTypeBlocked:
        {
            [self showChatView];
            break;
        }
        case ADUserRelationshipTypeSelf:
        case ADUserRelationshipTypeRequested:
        case ADUserRelationshipTypeRejected:
        {
            break;
        }
    }
}

- (void)showPhotoBrowserWithIndex:(NSInteger)index
{
    if (index < 0 || index >= self.photos.count) {
        return;
    }
    NYTPhotosViewController *photosViewController = [[NYTPhotosViewController alloc] initWithPhotos:self.photos initialPhoto:self.photos[index] showRightButton:YES];
    [self presentViewController:photosViewController animated:YES completion:nil];
}

- (void)showReportView
{
    ADReportViewController *report = [[ADReportViewController alloc] initWithNibName:@"ADReportViewController" bundle:nil];
    [report setReportedUserId:self.userObject.email];
    ADNavigationController *navi = [[ADNavigationController alloc] initWithRootViewController:report];
    [self presentViewController:navi animated:YES completion:nil];
}


#pragma mark -
#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sectionArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    ADNearbyUserDetailsSectionType sectionNumber = [self.sectionArray[section] integerValue];
    switch (sectionNumber)
    {
        case ADNearbyUserDetailsSectionTypeCover:
        case ADNearbyUserDetailsSectionTypeImages:
            return 1;
            
        case ADNearbyUserDetailsSectionTypeDetails:
            return self.detailItemArray.count;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    ADNearbyUserDetailsSectionType sectionNumber = [self.sectionArray[section] integerValue];
    if (sectionNumber == ADNearbyUserDetailsSectionTypeCover) {
        return CGFLOAT_MIN;
    }
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ADNearbyUserDetailsSectionType sectionNumber = [self.sectionArray[indexPath.section] integerValue];
    if (sectionNumber == ADNearbyUserDetailsSectionTypeCover) {
        ADProfileCoverTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_USER_DETAIL_PROFILE_COVER_IDENTIFIER];
        return [cell cellHeight];
    }
    else if (sectionNumber == ADNearbyUserDetailsSectionTypeImages) {
        ADProfileImageScrollableCollectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_USER_DETAIL_PROFILE_IMAGE_SCROLLABLE_COLLECTION_VIEW_CELL_IDENTIFIER];
        cell.userObject = self.userObject;
        return [cell cellHeight];
    }
    else if (sectionNumber == ADNearbyUserDetailsSectionTypeDetails) {
        ADUserDetailItem *item = self.detailItemArray[indexPath.row];
        if ([item isHeader]) {
            ADUserDetailSectionHeaderCell *cell = [ADUserDetailSectionHeaderCell userDetailSectionHeaderCellForTableView:tableView type:item.type];
            return [cell cellHeight];
        }
        else {
            ADUserDetailTableViewCell *cell = [ADUserDetailTableViewCell userDetailCellForTableView:tableView type:item.type userData:self.userObject];
            return [cell cellHeight];
        }
    }
    return tableView.rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger sectionNumber = [self.sectionArray[indexPath.section] integerValue];
    if (sectionNumber == ADNearbyUserDetailsSectionTypeCover) {
        ADProfileCoverTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_USER_DETAIL_PROFILE_COVER_IDENTIFIER];
        self.profileCoverImageView = cell.profileImageView;
        cell.userObject = self.userObject;
        [cell.button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell setupButton];
        return cell;
    }
    else if (sectionNumber == ADNearbyUserDetailsSectionTypeImages) {
        ADProfileImageScrollableCollectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAD_USER_DETAIL_PROFILE_IMAGE_SCROLLABLE_COLLECTION_VIEW_CELL_IDENTIFIER];
        cell.userObject = self.userObject;
        cell.collectionView.delegate = self;
        return cell;
    }
    else if (sectionNumber == ADNearbyUserDetailsSectionTypeDetails) {
        ADUserDetailItem *item = self.detailItemArray[indexPath.row];
        if ([item isHeader]) {
            return [ADUserDetailSectionHeaderCell userDetailSectionHeaderCellForTableView:tableView type:item.type];
        }
        else {
            return [ADUserDetailTableViewCell userDetailCellForTableView:tableView type:item.type userData:self.userObject];
        }
    }
    
    return [[UITableViewCell alloc] init];
}


#pragma mark -
#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_NEARBY_USER_VIEW_PROFILE_IMAGE];
    
    NSInteger sectionNumber = [self.sectionArray[indexPath.section] integerValue];
    if (sectionNumber == ADNearbyUserDetailsSectionTypeCover) {
        [self showPhotoBrowserWithIndex:0];
    }
}


#pragma mark -
#pragma mark - UIScrollView delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat yOffset  = scrollView.contentOffset.y;
    CGFloat x = MAX(-yOffset / 200, 0);
    self.profileCoverImageView.transform = CGAffineTransformIdentity;
    self.profileCoverImageView.transform = CGAffineTransformScale(self.profileCoverImageView.transform, 1+x, 1+x);
}


#pragma mark -
#pragma mark - Collection View Controller Data Source

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [ADFlurryHelper logEvent:kAD_FLURRY_EVENT_NEARBY_USER_VIEW_PROFILE_IMAGE];
    
    [self showPhotoBrowserWithIndex:indexPath.item+1];
}



- (void)updateNearbyUserLikes:(NSString *)userEmail
{
    NSMutableArray *likesUsers = [[ADUserDefaultHelper nearbyUserLikes] mutableCopy];
    if (![likesUsers containsObject:userEmail]) {
        [likesUsers addObject:userEmail];
    }
    [ADUserDefaultHelper setLikesForNearbyUsers:likesUsers];
}

@end
