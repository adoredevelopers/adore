//
//  ADNearbyUserViewController.h
//  Adore
//
//  Created by Wang on 2015-04-15.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADViewController.h"
#import "ADNearbyUserObject.h"
#import "ADNearbyUserRequest.h"
#import "ADAdoreConnectionNetworkingEngine.h"


@interface ADNearbyUserViewController : ADViewController


@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSString *currentPresentationView;
@property (nonatomic, weak) IBOutlet UIButton *switcherBtn;

@property (nonatomic, strong) ADAdoreConnectionNetworkingEngine *networkingEngine;
@property (nonatomic, strong) NSMutableArray *nearbyUserList;
@property (nonatomic, strong) ADNearbyUserRequest *nearbyUserRequest;
@property (nonatomic, strong) ADNearbyUserObject *selectedNearbyUser;
@property (nonatomic, strong) NSError *error;

@property (nonatomic, assign) BOOL hasMoreNearbyUsers;
@property (nonatomic, assign) BOOL isLoading;

@property (nonatomic, strong) UIRefreshControl *listRefreshControl;
@property (nonatomic, strong) UIRefreshControl *collectionRefreshControl;


- (IBAction)switchPresentationViewLayout:(id)sender;


@end
