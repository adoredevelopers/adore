//
//  ADNearbyUserFilterViewController.m
//  Adore
//
//  Created by Wang on 2014-05-24.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADNearbyUserFilterViewController.h"
#import "NMRangeSlider.h"
#import "ADUserDefaultHelper.h"



@implementation ADNearbyUserFilterViewController


#pragma mark -
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Filter", nil);

    [self.genderSegmentedControl setTitle:NSLocalizedString(@"All", nil) forSegmentAtIndex:0];
    [self.genderSegmentedControl setTitle:NSLocalizedString(@"Female", nil) forSegmentAtIndex:1];
    [self.genderSegmentedControl setTitle:NSLocalizedString(@"Male", nil) forSegmentAtIndex:2];
    self.genderSegmentedControl.selectedSegmentIndex = [ADUserDefaultHelper gender];

	self.ageSlider.maximumValue = 34;
	self.ageSlider.minimumValue = 18;
    self.ageSlider.upperValue = [self ageSliderValueReverseMapping:[ADUserDefaultHelper maximumAge]];
    self.ageSlider.lowerValue = [self ageSliderValueReverseMapping:[ADUserDefaultHelper minimumAge]];
    self.ageSlider.minimumRange = 0;
    [self.ageSlider addTarget:self action:@selector(ageSliderValueChanged:) forControlEvents:UIControlEventValueChanged];

    self.distanceSlider.value = [ADUserDefaultHelper maximumDistance];

    // We need to call this method to set the label text
    [self ageSliderValueChanged:nil];
	[self distanceSliderValueChanged:nil];
}


#pragma mark -
#pragma mark - Helper methods

- (IBAction)doneButtonClicked:(id)sender
{
    [ADUserDefaultHelper setMinimumAge:[self ageSliderValueMapping:(NSInteger)self.ageSlider.lowerValue]];
    [ADUserDefaultHelper setMaximumAge:[self ageSliderValueMapping:(NSInteger)self.ageSlider.upperValue]];
    [ADUserDefaultHelper setGender:self.genderSegmentedControl.selectedSegmentIndex];
    [ADUserDefaultHelper setMaximumDistance:(NSInteger)self.distanceSlider.value];
    
	[self.delegate filterUpdated];
}

- (IBAction)cancelButtonClicked:(id)sender
{
	[self.delegate filterCancelled];
}

- (void)ageSliderValueChanged:(id)sender
{
	NSAttributedString *age = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"   %@: ", NSLocalizedString(@"Age", nil)] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:16]}];
	NSString *lowerValue = @([self ageSliderValueMapping:(NSInteger)self.ageSlider.lowerValue]).stringValue;
	NSString *upperValue = @([self ageSliderValueMapping:(NSInteger)self.ageSlider.upperValue]).stringValue;
	if ([self ageSliderValueMapping:(NSInteger)self.ageSlider.upperValue] == 40) {
		upperValue = [NSString stringWithFormat:@"%@+", upperValue];
	}
	NSAttributedString *rangeString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ - %@", lowerValue, upperValue]
																		  attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:14], NSForegroundColorAttributeName:[UIColor redColor]}];
	NSMutableAttributedString *ageText = [[NSMutableAttributedString alloc] init];
	[ageText appendAttributedString:age];
	[ageText appendAttributedString:rangeString];
	self.ageSectionHeaderLabel.attributedText = ageText;
}

- (NSInteger)ageSliderValueMapping:(NSInteger)value
{
	switch (value){
		case 31:
			return 32;
		case 32:
			return 34;
		case 33:
			return 37;
		case 34:
			return 40;
	}
	if (value >= 34) {
		return 40;
	}
	return value;
}

- (NSInteger)ageSliderValueReverseMapping:(NSInteger)value
{
	switch (value){
		case 32:
			return 31;
		case 34:
			return 32;
		case 37:
			return 33;
		case 40:
			return 34;
	}
	return value;
}

- (IBAction)distanceSliderValueChanged:(id)sender
{
	NSAttributedString *distance = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"   %@: ", NSLocalizedString(@"Distance", nil)] attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:16]}];
    NSInteger distanceValue = (NSInteger)self.distanceSlider.value;
    NSString *value;
    if (distanceValue < 100) {
        value = @(distanceValue).stringValue;
    }
    else {
        value = [NSString stringWithFormat:@"%ld+", (long)distanceValue];
    }
	NSAttributedString *valueString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ km", value]
																	  attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:14], NSForegroundColorAttributeName:[UIColor redColor]}];
	NSMutableAttributedString *distanceText = [[NSMutableAttributedString alloc] init];
	[distanceText appendAttributedString:distance];
	[distanceText appendAttributedString:valueString];
	self.distanceSectionHeaderLabel.attributedText = distanceText;
}


@end
