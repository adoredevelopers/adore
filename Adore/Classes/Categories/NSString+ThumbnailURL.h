//
//  NSString+ThumbnailURL.h
//  Adore
//
//  Created by Wang on 2015-01-05.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface NSString (ThumbnailURL)


- (NSString *)thumbnailImageUrl;
- (NSString *)thumbnailImageUrl160;
- (NSString *)thumbnailImageUrl320;


@end
