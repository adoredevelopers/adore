//
//  UIImage+APResize.h
//  Adore
//
//  Created by Chao Lu on 2016-02-07.
//  Copyright © 2016 AdoreMobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (APResize)

+ (UIImage *)imageWithImage:(UIImage *)image
          scaledToFitToSize:(CGSize)newSize;

+ (UIImage *)imageWithImage:(UIImage *)image
         scaledToFillToSize:(CGSize)newSize;

@end
