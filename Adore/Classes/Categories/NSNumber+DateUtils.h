//
//  NSNumber+DateUtils.h
//  Adore
//
//  Created by Wang on 2014-06-07.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface NSNumber (DateUtils)


- (NSString *)durationString;
- (NSDate *)dateObject;


@end
