//
//  NSString+Security.m
//  Adore
//
//  Created by Wang on 2014-05-10.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "NSString+Security.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (Security)

#pragma mark -
#pragma mark - Private methods

- (NSString*)toHexString:(unsigned char*) data length: (unsigned int) length {
	NSMutableString* hash = [NSMutableString stringWithCapacity:length * 2];
	for (unsigned int i = 0; i < length; i++) {
		[hash appendFormat:@"%02x", data[i]];
		data[i] = 0;
	}
	return hash;
}


#pragma mark -
#pragma mark - Public methods

- (NSString *)SHA256 {
	unsigned int outputLength = CC_SHA256_DIGEST_LENGTH;
	unsigned char output[outputLength];
	CC_SHA256(self.UTF8String, (CC_LONG)[self lengthOfBytesUsingEncoding:NSUTF8StringEncoding], output);
	return [self toHexString:output length:outputLength];;
}


@end
