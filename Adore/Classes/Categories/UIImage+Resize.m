//
//  UIImage+Resize.m
//  Adore
//
//  Created by Wang on 2015-03-23.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "UIImage+Resize.h"



@implementation UIImage (Resize)


- (UIImage *)scaleToSizeKeepAspect:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    
    CGFloat ws = size.width/self.size.width;
    CGFloat hs = size.height/self.size.height;
    
    if (ws > hs) {
        ws = hs/ws;
        hs = 1.0;
    } else {
        hs = ws/hs;
        ws = 1.0;
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0.0, size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextDrawImage(context, CGRectMake(size.width/2-(size.width*ws)/2,
                                           size.height/2-(size.height*hs)/2, size.width*ws,
                                           size.height*hs), self.CGImage);
    
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

- (UIImage *)compressedImage
{
    UIImage *image = [[UIImage alloc] initWithData:[self compressedData]];
    return image;
}

- (NSData *)compressedData
{
    CGFloat compressionRate = 1;
    NSData *compressedData = nil;
    
    do {
        compressedData = UIImageJPEGRepresentation(self, compressionRate);
        compressionRate *= 0.8;
    } while (compressedData.length > 100000);
    
    return compressedData;
}

//- (UIImage *)compressedImage
//{
//    float actualHeight = self.size.height;
//    float actualWidth = self.size.width;
//    float maxHeight = 640.0;
//    float maxWidth = 640.0;
//    float imgRatio = actualWidth/actualHeight;
//    float maxRatio = maxWidth/maxHeight;
//    float compressionQuality = 0.4f; //60 percent compression
//    
//    if (actualHeight > maxHeight || actualWidth > maxWidth) {
//        if(imgRatio < maxRatio){
//            //adjust width according to maxHeight
//            imgRatio = maxHeight / actualHeight;
//            actualWidth = imgRatio * actualWidth;
//            actualHeight = maxHeight;
//        }
//        else if(imgRatio > maxRatio){
//            //adjust height according to maxWidth
//            imgRatio = maxWidth / actualWidth;
//            actualHeight = imgRatio * actualHeight;
//            actualWidth = maxWidth;
//        }else{
//            actualHeight = maxHeight;
//            actualWidth = maxWidth;
//        }
//    }
//    
//    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
//    UIGraphicsBeginImageContext(rect.size);
//    [self drawInRect:rect];
//    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
//    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
//    UIGraphicsEndImageContext();
//    
//    return [UIImage imageWithData:imageData];
//}

@end