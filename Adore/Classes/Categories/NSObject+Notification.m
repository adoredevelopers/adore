//
//  NSObject+Notification.m
//  Adore
//
//  Created by Wang on 2015-03-30.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "NSObject+Notification.h"



@implementation NSObject (Notification)


- (void)postNotificationOnMainThread:(NSString *)aName object:(id)anObject userInfo:(NSDictionary *)aUserInfo
{
//    dispatch_async(dispatch_get_main_queue(),^{
        [[NSNotificationCenter defaultCenter] postNotificationName:aName object:anObject userInfo:aUserInfo];
//    });
}

- (void)postNotificationOnMainThread:(NSString *)aName
{
    [self postNotificationOnMainThread:aName object:nil userInfo:nil];
}


@end
