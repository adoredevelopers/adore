//
//  NSManagedObject+Extension.h
//  Adore
//
//  Created by Wang on 2015-04-20.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <CoreData/CoreData.h>



@interface NSManagedObject (Extension)


+ (void)deleteAllFromEntity:(NSString *)entityName managedObjectContext:(NSManagedObjectContext *)managedObjectContext;

- (void)revertChanges;


@end
