//
//  NSDictionary+JSONObject.h
//  Adore
//
//  Created by Wang on 2014-06-16.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (JSONObject)

- (NSData *)JSONDataObject;
- (NSString *)JSONString;

- (NSData *)queryDataObject;
- (NSData *)multiUploadsDataObject;

@end
