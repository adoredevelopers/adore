//
//  NSDictionary+NonNilObject.h
//  Adore
//
//  Created by Wang on 2014-04-24.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NonNilObject)


- (id)notNilObjectForKey:(NSString *)key;

- (NSNumber *)numberObjectForKey:(NSString *)key;

- (NSString *)stringObjectForKey:(NSString *)key;

- (NSArray *)arrayObjectForKey:(NSString *)key;

- (NSDictionary *)dictionaryObjectForKey:(NSString *)key;

- (NSDate *)dateObjectForKey:(NSString *)key;

- (NSInteger)integerForKey:(NSString *)key;

- (BOOL)boolForKey:(NSString *)key;


@end
