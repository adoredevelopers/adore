//
//  NSNumber+DateUtils.m
//  Adore
//
//  Created by Wang on 2014-06-07.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "NSNumber+DateUtils.h"

typedef NS_ENUM(NSInteger, ADDurationUnitTitle) {
    ADDurationUnitTitleYear,
    ADDurationUnitTitleMonth,
    ADDurationUnitTitleDay,
    ADDurationUnitTitleHour,
    ADDurationUnitTitleMinute,
    ADDurationUnitTitleSecond
};



@implementation NSNumber (DateUtils)

#pragma mark -
#pragma mark - Public methods

- (NSString *)durationString
{
    NSInteger seconds = self.integerValue;
    if (seconds < 0) {
        return @"ERROR";
    }
    else if (seconds / 31536000 > 0) {
        return [self durationStringWithNumber:seconds / 31536000 Unit:ADDurationUnitTitleYear];
    }
    else if (seconds / 2592000 > 0) {
        return [self durationStringWithNumber:seconds / 2592000 Unit:ADDurationUnitTitleMonth];
    }
    else if (seconds / 86400 > 0) {
        return [self durationStringWithNumber:seconds / 86400 Unit:ADDurationUnitTitleDay];
    }
    else if (seconds / 3600 > 0) {
        return [self durationStringWithNumber:seconds / 3600 Unit:ADDurationUnitTitleHour];
    }
    else if (seconds / 60 > 0) {
        return [self durationStringWithNumber:seconds / 60 Unit:ADDurationUnitTitleMinute];
    }
    else {
        return [self durationStringWithNumber:seconds Unit:ADDurationUnitTitleSecond];
    }
    return @"";
}

- (NSDate *)dateObject
{
    CGFloat timeInterval = [self longLongValue] / 1000.0;
	return [NSDate dateWithTimeIntervalSince1970:timeInterval];
}


#pragma mark -
#pragma mark - Private methods

- (NSString *)durationStringWithNumber:(NSInteger)number Unit:(ADDurationUnitTitle)unit
{
    BOOL singular = (number == 1);
    return [NSString stringWithFormat:@"%ld %@", (long)number, [self unitStringWithUnit:unit Form:singular]];
}

- (NSString *)unitStringWithUnit:(ADDurationUnitTitle)unit Form:(BOOL)singular
{
    switch (unit) {
        case ADDurationUnitTitleYear:
            return singular ? NSLocalizedString(@"year", @"year") : NSLocalizedString(@"years", @"years");
        case ADDurationUnitTitleMonth:
            return singular ? NSLocalizedString(@"month", @"month") : NSLocalizedString(@"months", @"months");
        case ADDurationUnitTitleDay:
            return singular ? NSLocalizedString(@"day", @"day") : NSLocalizedString(@"days", @"days");
        case ADDurationUnitTitleHour:
            return singular ? NSLocalizedString(@"hour", @"hour") : NSLocalizedString(@"hours", @"hours");
        case ADDurationUnitTitleMinute:
            return singular ? NSLocalizedString(@"minute", @"minute") : NSLocalizedString(@"minutes", @"minutes");
        case ADDurationUnitTitleSecond:
            return singular ? NSLocalizedString(@"second", @"second") : NSLocalizedString(@"seconds", @"seconds");
    }
    return @"";
}


@end
