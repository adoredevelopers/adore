//
//  NSDateFormatter+Utils.m
//  Adore
//
//  Created by Kevin Wang on 2015-06-17.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "NSDateFormatter+Utils.h"



@implementation NSDateFormatter (Utils)


+ (instancetype)ADDefaultFormatter
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:kAD_DATE_FORMATTER_DEFAULT_FORMAT];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    return dateFormatter;
}


@end
