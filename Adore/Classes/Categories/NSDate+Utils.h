//
//  NSDate+Utils.h
//  Adore
//
//  Created by Wang on 2014-09-27.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utils)

- (NSDate *)dateByAddingDays:(NSInteger)days;
- (NSDate *)dateComponet;
- (NSDate *)timeComponet;

- (NSString *)toDateStringFormattedWith_MMM_dd_yyyy;
- (NSString *)toTimeStringFormattedWith_hh_mm_tt;

- (NSString *)toShortFormatString;

- (NSInteger)age;

+ (NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day;
+ (NSDate *)appUserMinimumBirthday;
+ (NSDate *)appUserMaximumBirthday;

@end
