//
//  NSDateFormatter+Utils.h
//  Adore
//
//  Created by Kevin Wang on 2015-06-17.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface NSDateFormatter (Utils)


+ (instancetype)ADDefaultFormatter;


@end
