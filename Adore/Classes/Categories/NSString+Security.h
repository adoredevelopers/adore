//
//  NSString+Security.h
//  Adore
//
//  Created by Wang on 2014-05-10.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Security)

- (NSString *)SHA256;

@end
