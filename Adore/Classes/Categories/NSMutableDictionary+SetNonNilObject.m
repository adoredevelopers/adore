//
//  NSMutableDictionary+SetNonNilObject.m
//  Adore
//
//  Created by Wang on 2015-03-23.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "NSMutableDictionary+SetNonNilObject.h"



@implementation NSMutableDictionary (SetNonNilObject)


- (void)setObject:(id)anObject forKey:(id<NSCopying>)aKey defaultValue:(id)defaultValue
{
    if (anObject) {
        [self setObject:anObject forKey:aKey];
    }
    else {
        if (defaultValue) {
            [self setObject:defaultValue forKey:aKey];
        }
    }
}


@end
