//
//  NSMutableArray+Shuffling.h
//  Adore
//
//  Created by Chao Lu on 2015-01-23.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Shuffling)

- (void)shuffle;

@end
