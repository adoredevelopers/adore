//
//  NSDate+Utils.m
//  Adore
//
//  Created by Wang on 2014-09-27.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "NSDate+Utils.h"

@implementation NSDate (Utils)

- (NSDate *)dateByAddingDays:(NSInteger)days
{
    NSCalendar *currentCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = days;
    
    return [currentCalendar dateByAddingComponents:dayComponent toDate:self options:0];
}

- (NSDate *)dateComponet {
    NSCalendar *currentCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComponents = [currentCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:self];
    return [currentCalendar dateFromComponents:dateComponents];
}

- (NSDate *)timeComponet {
    NSCalendar *currentCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComponents = [currentCalendar components:NSCalendarUnitHour | NSCalendarUnitMinute fromDate:self];
    return [currentCalendar dateFromComponents:dateComponents];
}

// MMM dd, yyyy - Example: Dec 20, 2013
- (NSString *)toDateStringFormattedWith_MMM_dd_yyyy
{
    return [self toStringWithFormat:@"MMM dd, yyyy"];
}

// hh:mm tt - Example: 5:00 AM
- (NSString *)toTimeStringFormattedWith_hh_mm_tt
{
    return [self toStringWithFormat:@"hh:mm tt"];
}

// formats the date in with the given format
- (NSString *)toStringWithFormat:(NSString *)format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = [NSDateFormatter dateFormatFromTemplate:format options:0 locale:[NSLocale currentLocale]];
    
    return [formatter stringFromDate:self];
}

/* 
 * Show time if it is today
 * Show yesterday if it is yesterday
 * Show date if it is before yesterday
 */
- (NSString *)toShortFormatString
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSDate *today = [[NSDate date] dateComponet];
    
    // today
    if ([[self dateComponet] compare:today] == NSOrderedSame) {
        formatter.dateStyle = NSDateFormatterNoStyle;
        formatter.timeStyle = NSDateFormatterShortStyle;
        return [formatter stringFromDate:self];
    }
    // Yesterday
    else if ([[self dateComponet] compare:[today dateByAddingDays:-1]] == NSOrderedSame) {
        return NSLocalizedString(@"Yesterday", nil);
    }
    else {
        formatter.dateStyle = NSDateFormatterShortStyle;
        formatter.timeStyle = NSDateFormatterNoStyle;
        return [formatter stringFromDate:self];
    }
}

- (NSInteger)age
{
    NSTimeInterval timeDiff = [[NSDate date] timeIntervalSinceDate:self];
    NSDate *ageDate = [NSDate dateWithTimeIntervalSince1970:timeDiff];
    NSDateComponents *dc = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:ageDate];
    return [dc year] - 1970;
}

+ (NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day
{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.year = year;
    components.month = month;
    components.day = day;
    
    return [gregorianCalendar dateFromComponents:components];
}

+ (NSDate *)appUserMinimumBirthday
{
    return [self dateWithYear:1900 month:1 day:1];
}

+ (NSDate *)appUserMaximumBirthday
{
    // User has to be at least 18
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:[NSDate date]];
    components.year = components.year - 18;
    return [gregorianCalendar dateFromComponents:components];
}

@end
