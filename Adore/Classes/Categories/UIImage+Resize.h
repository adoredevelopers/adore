//
//  UIImage+Resize.h
//  Adore
//
//  Created by Wang on 2015-03-23.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface UIImage (Resize)


- (UIImage*)scaleToSizeKeepAspect:(CGSize)size;
- (UIImage *)compressedImage;
- (NSData *)compressedData;


@end
