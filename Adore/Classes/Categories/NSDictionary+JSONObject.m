//
//  NSDictionary+JSONObject.m
//  Adore
//
//  Created by Wang on 2014-06-16.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "NSDictionary+JSONObject.h"
#import "UIImage+Resize.h"

@implementation NSDictionary (JSONObject)

- (NSData *)JSONDataObject
{
	NSError *error;
	NSData *data = [NSJSONSerialization dataWithJSONObject:self options:0 error:&error];
	if (data == nil) {
		DDLogError(@"ERROR: %@", error);
		return nil;
	}
	else {
		NSString *JSONString = [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
		DDLogDebug(@"JSON OUTPUT: %@", JSONString);
		
		return data;
	}
}

- (NSString *)JSONString
{
    NSString *json = [[NSString alloc] initWithData:[self JSONDataObject] encoding:NSUTF8StringEncoding];
    return json;
}

- (NSData *)queryDataObject
{
	NSMutableArray *array = @[].mutableCopy;
	for (NSString *key in self.allKeys) {
		[array addObject:[NSString stringWithFormat:@"%@=%@", key, [self objectForKey:key]]];
	}
	NSString *str = [array componentsJoinedByString:@"&"];
	DDLogDebug(@"QUERY OUTPUT: %@", str);
	return [str dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSData *)multiUploadsDataObject
{
	NSMutableData *body = [NSMutableData data];

	NSData *boundaryData = [[NSString stringWithFormat:@"--%@\r\n", kAD_HTTP_MULTIPART_BOUNDARY] dataUsingEncoding:NSUTF8StringEncoding];
	
	[self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
		[body appendData:boundaryData];
		// I use special simple class to distinguish file params
		if( [obj isKindOfClass:[UIImage class]] ) {
			// File upload
			[body appendData: [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n\r\n", key, key] dataUsingEncoding:NSUTF8StringEncoding]];
            NSData *data = [obj compressedData];
			[body appendData: data]; // It just return NSData with loaded file in it
			[body appendData: [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
		}
		else {
			// Regular param
			[body appendData: [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@\r\n", key, obj] dataUsingEncoding:NSUTF8StringEncoding]];
		}
	}];
	
	[body appendData:[[NSString stringWithFormat:@"--%@--\r\n", kAD_HTTP_MULTIPART_BOUNDARY] dataUsingEncoding:NSUTF8StringEncoding]];
	return body;
}

@end
