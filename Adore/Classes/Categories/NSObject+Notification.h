//
//  NSObject+Notification.h
//  Adore
//
//  Created by Wang on 2015-03-30.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface NSObject (Notification)


- (void)postNotificationOnMainThread:(NSString *)aName object:(id)anObject userInfo:(NSDictionary *)aUserInfo;
- (void)postNotificationOnMainThread:(NSString *)aName;


@end
