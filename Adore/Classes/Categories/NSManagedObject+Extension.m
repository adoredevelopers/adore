//
//  NSManagedObject+Extension.m
//  Adore
//
//  Created by Wang on 2015-04-20.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "NSManagedObject+Extension.h"



@implementation NSManagedObject (Extension)

+ (void)deleteAllFromEntity:(NSString *)entityName managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    NSFetchRequest *allRecords = [[NSFetchRequest alloc] init];
    allRecords.entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
    allRecords.includesPropertyValues = NO;

    NSError *error = nil;
    NSArray *result = [managedObjectContext executeFetchRequest:allRecords error:&error];
    for (NSManagedObject *obj in result) {
        [managedObjectContext deleteObject:obj];
    }
    NSError *saveError = nil;
    [managedObjectContext save:&saveError];
}

- (void)revertChanges {
    NSDictionary *changedValues = [self changedValues];
    NSDictionary *committedValues = [self committedValuesForKeys:[changedValues allKeys]];
    NSEnumerator *enumerator;
    id key;
    enumerator = [changedValues keyEnumerator];
    
    while ((key = [enumerator nextObject])) {
        id value = [committedValues objectForKey:key];
        
        NSLog(@"Reverting field ""%@"" from ""%@"" to ""%@""", key, [changedValues objectForKey:key], value);
        if ([value isKindOfClass:[NSNull class]]) {
            [self setValue:nil forKey:key];
        }
        else {
            [self setValue:value forKey:key];
        }
    }
}
@end
