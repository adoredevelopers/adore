//
//  NSMutableDictionary+SetNonNilObject.h
//  Adore
//
//  Created by Wang on 2015-03-23.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface NSMutableDictionary (SetNonNilObject)


- (void)setObject:(id)anObject forKey:(id<NSCopying>)aKey defaultValue:(id)defaultValue;


@end
