//
//  NSString+ThumbnailURL.m
//  Adore
//
//  Created by Wang on 2015-01-05.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "NSString+ThumbnailURL.h"



@implementation NSString (ThumbnailURL)


- (NSString *)thumbnailImageUrl
{
    if ([((NSString *)self) rangeOfString:kAD_SERVER_HOST].location != NSNotFound) {
        return [self thumbnailImageUrl160];
    }
    return self;
}

- (NSString *)thumbnailImageUrl320
{
    NSMutableString *thumbnailImageUrlString = [NSMutableString stringWithString:self];
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\b(.*)(\\..*)\\b" options:NSRegularExpressionCaseInsensitive error:&error];
    [regex replaceMatchesInString:thumbnailImageUrlString options:0 range:NSMakeRange(0, [thumbnailImageUrlString length]) withTemplate:@"$1_thumb_320x320$2"];
    
    return [NSString stringWithString:thumbnailImageUrlString];
}

- (NSString *)thumbnailImageUrl160
{
    NSMutableString *thumbnailImageUrlString = [NSMutableString stringWithString:self];
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\b(.*)(\\..*)\\b" options:NSRegularExpressionCaseInsensitive error:&error];
    [regex replaceMatchesInString:thumbnailImageUrlString options:0 range:NSMakeRange(0, [thumbnailImageUrlString length]) withTemplate:@"$1_thumb_160x160$2"];
    
    return [NSString stringWithString:thumbnailImageUrlString];
}


@end
