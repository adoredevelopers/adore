//
//  ADUserManager.m
//  Adore
//
//  Created by Wang on 2014-03-12.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADUserManager.h"
#import "ADUserObject.h"
#import "ADUserConnectionObject.h"
#import "ADConnectionObject.h"
#import "ADPendingRequestObject.h"
#import "ADCouponObject.h"
#import "ADManagedUser+Extensions.h"
#import "ADManagedPartner+Extensions.h"
#import "ADManagedCoupon+Extensions.h"
#import "ADUserConnectionRequest.h"
#import "ADPendingConnectionsRequest.h"
#import "ADUserCouponRequest.h"
#import "ADUpdateLastActivityRequest.h"
#import "ADCheckSessionTicketRequest.h"
#import "ADUserCoreDataHelper.h"
#import "ADUserDefaultHelper.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "NSObject+Notification.h"
#import "ADDetailListCoreDataHelper.h"
#import "ADChatManager.h"
#import "AppDelegate.h"



@interface ADUserManager ()


@property (nonatomic, strong) ADUserConnectionRequest *connectionRequest;
@property (nonatomic, strong) ADUserConnectionRequest *autoUpdateRequest;
@property (nonatomic, strong) ADPendingConnectionsRequest *pendingConnectionsRequest;
@property (nonatomic, strong) ADUserCouponRequest *couponRequest;
@property (nonatomic, strong) ADUpdateLastActivityRequest *updateLastActivityRequest;
@property (nonatomic, strong) ADCheckSessionTicketRequest *checkSessionTicketRequest;


@end



@implementation ADUserManager

#pragma mark -
#pragma mark - Initialization

+ (ADUserManager *)sharedInstance
{
	static dispatch_once_t pred;
	static ADUserManager *sharedInstance = nil;
	dispatch_once(&pred, ^{
		sharedInstance = [[ADUserManager alloc] init];
	});
	return sharedInstance;
}

- (void)initializeUserManager
{
    if (_userCoreDataHelper == nil) {
        _userCoreDataHelper = [[ADUserCoreDataHelper alloc] init];
    }
    _detailListCoreDataHelper = [[ADDetailListCoreDataHelper alloc] init];
    _locationHelper = [[ADLocationHelper alloc] initWithDelegate:self];
    [_locationHelper startTracking];
    
    _currentUserProfile = [self.userCoreDataHelper loadCurrentUserFromCoreData];
}

- (void)clearUserManager
{
    [self.userCoreDataHelper removeCoreDataDatabase];
    
    _locationHelper = nil;
    _sessionTicket = nil;
	
    _currentUserProfile = nil;
    
    _connectionRequest = nil;
    _couponRequest = nil;
    _pendingConnectionsRequest = nil;
    _updateLastActivityRequest = nil;
}


#pragma mark -
#pragma mark - Helper methods

- (void)updateCurrentUser:(ADUserObject *)userObject sessionTicket:(NSString *)ticket needsFetching:(BOOL)fetch
{
    [self updateSessionTicket:ticket];
    [self saveUserObjectIntoCoreData:userObject];
    
    [ADUserDefaultHelper setUsername:userObject.email];
    [ADUserDefaultHelper setToken:ticket];
    [ADUserDefaultHelper setUserProfileImageUrl:userObject.profilePictureThumbnailUrl];
    
    // Set filter gender to the opposite sex
    if ([userObject.gender isEqualToString:@"Female"]) {
        [ADUserDefaultHelper setGender:ADGenderFilterMale];
    }
    else {
        [ADUserDefaultHelper setGender:ADGenderFilterFemale];
    }
    [ADUserDefaultHelper setGender:ADGenderFilterAll];
    
    // Save the url into user defaults and cache the image
    UIImageView *imageView = [[UIImageView alloc] init];
//    [imageView sd_setImageWithURL:[NSURL URLWithString:userObject.profilePictureThumbnailUrl]];
    [imageView sd_setImageWithURL:[NSURL URLWithString:userObject.profilePictureThumbnailUrl] placeholderImage:nil options:SDWebImageRetryFailed progress:nil completed:nil];
    
    if (fetch) {
        [self fetchUserCoupons];
        [self fetchUserConnections];
        [self fetchPendingConnections];
    }
}

- (void)updateSessionTicket:(NSString *)ticket
{
    _sessionTicket = ticket;
    [self updateLastActivity];
    [self autoUpdateConnections];
}

- (void)checkSessionTicket
{
    __weak __typeof(self) weakSelf = self;
    CheckSessionTicketRequestCompletionBlock completionBlock = ^(ADJSONResponse* resp, NSError *error) {
        weakSelf.checkSessionTicketRequest = nil;
        
        if (resp == nil) {
            if (error != nil) {
                AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [appDelegate logoutMultiDeviceUser];
            }
            return;
        }
        if (resp.success == NO && [self isLoggedIn]) {
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate logoutMultiDeviceUser];
        }
    };
    self.checkSessionTicketRequest = [[ADCheckSessionTicketRequest alloc] initWithCompletionHandler:completionBlock];
}

- (BOOL)isLoggedIn
{
    return (self.sessionTicket.length > 0);
}

- (NSInteger)badgeNumberCount
{
    NSFetchedResultsController *pendingUsersController = [self.userCoreDataHelper pendingUsersFetchedResultsControllerWithDelegate:self];
    NSFetchedResultsController *unreadMessagesController = [[ADChatManager sharedInstance] unreadMessagesFetchedResultsControllerWithDelegate:self];
    NSFetchedResultsController *unreadConnectionsController = [self.userCoreDataHelper unreadConnectedUsersFetchedResultsControllerWithDelegate:self];

    NSUInteger countOfUnreadMessage = unreadMessagesController.fetchedObjects.count;
    NSUInteger countOfPendingUsers = pendingUsersController.fetchedObjects.count;
    NSUInteger countOfUnreadConnections = unreadConnectionsController.fetchedObjects.count;

    return countOfPendingUsers + countOfUnreadConnections + countOfUnreadMessage;
}


#pragma mark -
#pragma mark - Networking

- (void)fetchUserConnections
{
	UserConnectionRequestCompletionBlock completionBlock = ^(NSArray *array, NSError *error) {
        self.connectionRequest = nil;
        [self.userCoreDataHelper saveUserConnections:array removeAllConnections:YES updateProfileOnly:NO isRead:YES];
	};
	self.connectionRequest = [[ADUserConnectionRequest alloc] initWithCompletionHandler:completionBlock];
}

- (void)autoUpdateConnections
{
    UserConnectionRequestCompletionBlock completionBlock = ^(NSArray *array, NSError *error) {
        self.autoUpdateRequest = nil;
        [self.userCoreDataHelper saveUserConnections:array removeAllConnections:NO updateProfileOnly:YES isRead:NO];
    };

    NSFetchedResultsController *fetchedResult = [self.userCoreDataHelper connectedUsersFetchedResultsControllerWithDelegate:self];
    self.autoUpdateRequest = [[ADUserConnectionRequest alloc] initWithList:fetchedResult.fetchedObjects completionHandler:completionBlock];
}

- (void)fetchPendingConnections
{
	PendingConnectionsRequestCompletionBlock completionBlock = ^(NSArray *array, NSError *error) {
        self.pendingConnectionsRequest = nil;
        [self.userCoreDataHelper savePendingRequests:array clearAll:YES];
	};
	self.pendingConnectionsRequest = [[ADPendingConnectionsRequest alloc] initWithCompletionHandler:completionBlock];
}

- (void)fetchUserCoupons
{
    UserCouponRequestCompletionBlock completionBlock = ^(NSArray *couponArray, NSArray *partnerArray, NSError *error) {
        self.couponRequest = nil;
        [self.userCoreDataHelper saveCoupons:couponArray partners:partnerArray clearAll:YES];
    };
    self.couponRequest = [[ADUserCouponRequest alloc] initWithCompletionHandler:completionBlock];
}

- (void)updateLastActivity
{
    if ([self isLoggedIn]) {
        UpdateLastActivityCompletionBlock completionBlock = ^(BOOL success, NSError *error) {
            self.updateLastActivityRequest = nil;
        };
        self.updateLastActivityRequest = [[ADUpdateLastActivityRequest alloc] initWithCompletionHandler:completionBlock];
    }
}


#pragma mark -
#pragma mark - Core Data Helper Methods

- (void)saveUserObjectIntoCoreData:(ADUserObject *)userObject
{
    // remove the old current user
    ADManagedUser *currentUser = [self.userCoreDataHelper loadCurrentUserFromCoreData];
    if (currentUser) {
        [self.userCoreDataHelper deleteObjectsFromArray:@[currentUser] save:YES];
    }
    
    // add the new current user
    [self.userCoreDataHelper saveUserWithEmail:userObject.email withUserObject:userObject withRelationshipType:ADUserRelationshipTypeSelf updateProfileOnly:NO isRead:NO save:YES];
    
    [self.userCoreDataHelper updateFaceValidationResult:userObject.display_image_verified];
    
    _currentUserProfile = [self.userCoreDataHelper loadCurrentUserFromCoreData];
}

    
#pragma mark -
#pragma mark - LocationHelperDelegate methods

- (void)locationHelperUpdated:(CLLocation *)newLocation
{
    [self postNotificationOnMainThread:kAD_NOTIFICATION_LOCATION_UPDATED];
}

- (void)locationHelperUpdateError:(NSError *)error
{
    
}

- (void)locationHelperAuthorizationStatusDenied
{
    
}

- (void)locationHelperAuthorizationStatusRestricted
{
    
}

@end
