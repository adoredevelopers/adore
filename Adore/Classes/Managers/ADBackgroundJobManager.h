//
//  ADBackgroundJobManager.h
//  Adore
//
//  Created by Kevin Wang on 2015-07-23.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ADBackgroundJobManager : NSObject


+ (ADBackgroundJobManager *)sharedInstance;

- (void)initializeBackgroundJobManager;
- (void)clearBackgroundJobManager;

@end
