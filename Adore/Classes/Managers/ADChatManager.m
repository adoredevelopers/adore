//
//  ADChatManager.m
//  Adore
//
//  Created by Wang on 2014-04-22.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADChatManager.h"
#import "ADUserManager.h"
#import "ADLoginObject.h"
#import "ADManagedUser+Extensions.h"
#import "NSDictionary+NonNilObject.h"
#import "ADManagedChatMessage.h"
#import "NSNumber+DateUtils.h"
#import "ADUserCoreDataHelper.h"
#import "ADPendingRequestObject.h"
#import "ADUserCoreDataHelper.h"
#import "ADUserObject.h"
#import "ADCouponObject.h"
#import "ADBusinessPartnerObject.h"
#import "NSObject+Notification.h"
#import "ADUserDefaultHelper.h"
#import "NSManagedObject+Extension.h"
#import "ADChatViewController.h"
#import "ADManagedConversation.h"

//=====================
// Message Identifiers
//=====================

// build-ins identifiers
static NSString * const kAD_CHAT_SERVICE_MESSAGE_BUILDIN_CONNECT = @"connect";
static NSString * const kAD_CHAT_SERVICE_MESSAGE_BUILDIN_ERROR = @"error";
static NSString * const kAD_CHAT_SERVICE_MESSAGE_BUILDIN_DISCONNECT = @"disconnect";;
static NSString * const kAD_CHAT_SERVICE_MESSAGE_BUILDIN_RECONNECTING = @"reconnecting";
static NSString * const kAD_CHAT_SERVICE_MESSAGE_BUILDIN_RECONNECT = @"reconnect";
static NSString * const kAD_CHAT_SERVICE_MESSAGE_BUILDIN_RECONNECT_ATTEMPT = @"reconnectAttempt";
static NSString * const kAD_CHAT_SERVICE_MESSAGE_BUILDIN_RECONNECT_ERROR = @"reconnectError";
static NSString * const kAD_CHAT_SERVICE_MESSAGE_BUILDIN_RECONNECT_FAILED = @"reconnectFailed";

// custom identifiers
static NSString * const kAD_CHAT_SERVICE_MESSAGE_WELCOME = @"welcome message";

static NSString * const kAD_CHAT_SERVICE_MESSAGE_READY = @"ready message";

static NSString * const kAD_CHAT_SERVICE_MESSAGE_CONNECTED_USER_LIST = @"connected user_id list";

static NSString * const kAD_CHAT_SERVICE_MESSAGE_NEW_USER_CONNECTED = @"new user connected";

static NSString * const kAD_CHAT_SERVICE_MESSAGE_USER_DISCONNECTED = @"user disconnected";

static NSString * const kAD_CHAT_SERVICE_MESSAGE_PUBLIC_MESSAGE = @"public message";

static NSString * const kAD_CHAT_SERVICE_MESSAGE_PRIVATE_MESSAGE = @"private message";

static NSString * const kAD_CHAT_SERVICE_MESSAGE_OFFLINE_PRIVATE_MESSAGE = @"offline private messages";

static NSString * const kAD_CHAT_SERVICE_MESSAGE_COUPON_ACTIVATION_CONFIRMATION_MESSAGE = @"coupon activation confirmation message";

static NSString * const kAD_CHAT_SERVICE_MESSAGE_COUPON_ACTIVATION_REJECTION_MESSAGE = @"coupon activation rejection message";

static NSString * const kAD_CHAT_SERVICE_MESSAGE_NEW_CONNECTION_REQUEST = @"new connection request";

static NSString * const kAD_CHAT_SERVICE_MESSAGE_APPROVE_USER_CONFIRMATION = @"approve user confirmation";

static NSString * const kAD_CHAT_SERVICE_MESSAGE_NEW_COUPON_GENERATED_AFTER_APPROVAL = @"approve user new coupon";

static NSString * const kAD_CHAT_SERVICE_MESSAGE_COUPON_REDEEMED = @"redeem coupon confirmation";

static NSString * const kAD_CHAT_SERVICE_MESSAGE_NEW_COUPON_GENERATED_AFTER_REDEMPTION = @"redeem coupon new coupon";

static NSString * const kAD_CHAT_SERVICE_MESSAGE_USER_PROFILE_IMAGE_FACE_VALIDATION = @"user profile image face validation";


// Event Identifiers
static NSString * const kAD_CHAT_SERVICE_EVENT_REGISTER = @"register user";

static NSString * const kAD_CHAT_SERVICE_EVENT_ENABLE_COUPON_ACTIVATION = @"enable coupon activation message";

static NSString * const kAD_CHAT_SERVICE_EVENT_DISENABLE_COUPON_ACTIVATION = @"disable coupon activation message";

static NSString * const kAD_CHAT_SERVICE_EVENT_REQUEST_OFFLINE_PRIVATE_MESSAGES = @"request offline private messages";

static NSString * const kAD_CHAT_SERVICE_EVENT_GET_OFFLINE_SOCKET_PAYLOAD_CACHE = @"get offline socket payload cache";

static NSString * const kAD_CHAT_SERVICE_EVENT_SYNC_STATS = @"sync stats";

static NSString * const kAD_USER_LOGGED_ON_FROM_ANOTHER_DEVICE = @"user logged on from another device";



@interface ADChatManager ()


@property (nonatomic, strong) ADUserManager *userManager;

@property (nonatomic, strong) NSMutableArray *chatCache;
@property (nonatomic, strong) SocketIOClient* socketIOClient;
@property (nonatomic, assign) BOOL readyToConnect;
@property (nonatomic, strong) NSTimer *reconnectTimer;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;


@end



@implementation ADChatManager

+ (ADChatManager *)sharedInstance
{
    static dispatch_once_t pred;
    static ADChatManager *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[ADChatManager alloc] init];
    });
    return sharedInstance;
}

- (void)initializeChatManager
{
    self.readyToConnect = YES;
    if (_socketIOClient == nil) {
        _socketIOClient = [SocketIOClient alloc];
    }
}

- (void)clearChatManager
{
    //[self removeCoreDataDatabase];

    self.readyToConnect = NO;
    [self disconnectFromChatService];
}

- (ADUserManager *)userManager
{
    if (_userManager == nil) {
        _userManager = [ADUserManager sharedInstance];
    }
    return _userManager;
}

- (NSMutableArray *)chatCache
{
    if (_chatCache == nil) {
        _chatCache = @[].mutableCopy;
    }
    return _chatCache;
}


#pragma mark -
#pragma mark - Public methods

- (void)connectToChatService
{
    if (![self isConnectedToChatService]) {
        if (self.userManager.sessionTicket && self.userManager.currentUserProfile.email) {
            [self postNotificationOnMainThread:kAD_NOTIFICATION_SOCKET_CONNECTING];
            
            NSString *chatUrlString = [NSString stringWithFormat:@"%@:%@", kAD_SERVER_HOST, kAD_SERVER_PORT];
            NSURL *chatURL = [[NSURL alloc] initWithString:chatUrlString];
            self.socketIOClient = [[SocketIOClient alloc] initWithSocketURL:chatURL config:@{@"log": @NO, @"connectParams": @{@"user_id": self.userManager.currentUserProfile.email, @"token": self.userManager.sessionTicket}}];
            
            DDLogDebug(@"chatURL: %@", chatURL);
            
            [self attachSocketIOHandlers];
            
            [self.socketIOClient connect];
            DDLogDebug(@"### SocketIOClient ### - connecting");
        }
    }
}

- (BOOL)isConnectedToChatService
{
    return self.socketIOClient.status == SocketIOClientStatusConnected;
}

- (BOOL)isConnectingToChatService
{
    return self.socketIOClient.status == SocketIOClientStatusConnecting;
}

- (void)disconnectFromChatService
{
    [self.socketIOClient disconnect];
    DDLogDebug(@"### SocketIOClient ### - disconnecting");
}

- (void)reconnectToChatService
{
    if (![self isConnectedToChatService]) {
        [self connectToChatService];
        DDLogDebug(@"### SocketIOClient ### - reconnecting");
    } else {
        DDLogDebug(@"### SocketIOClient ### - reconnecting abort: already connected");
    }
}

- (void)checkSessionTicket
{
    [self.userManager checkSessionTicket];
}

- (SocketIOClientStatus)chatSocketConnectionStatus
{
    return self.socketIOClient.status;
}

- (void)attachSocketIOHandlers
{
    //=====================
    // SocketIO Connection
    //=====================
    //---------------------------
    // socket connection success
    //===========================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_BUILDIN_CONNECT callback:^(NSArray* data, SocketAckEmitter* ack) {
        [self postNotificationOnMainThread:kAD_NOTIFICATION_SOCKET_CONNECTED];
        // get offline private messages
        [self sendGetOfflinePrivateMessages];
        // get offline socket payloads
        [self sendGetOfflineSocketPayloadCache];
        
        DDLogInfo(@"######################");
        DDLogInfo(@"### SocketIOClient ### - connected: %@", data);
        DDLogInfo(@"######################");
    }];
    [self.socketIOClient on:kAD_USER_LOGGED_ON_FROM_ANOTHER_DEVICE callback:^(NSArray* data, SocketAckEmitter* ack) {
        [self postNotificationOnMainThread:kAD_NOTIFICATION_SOCKET_CONNECTED];
        // get offline private messages
        [self sendGetOfflinePrivateMessages];
        // get offline socket payloads
        [self sendGetOfflineSocketPayloadCache];
        
        DDLogInfo(@"######################");
        DDLogInfo(@"### SocketIOClient ### - connected: %@", data);
        DDLogInfo(@"######################");
    }];
    //-------------------------
    // socket connection error
    //=========================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_BUILDIN_ERROR callback:^(NSArray* data, SocketAckEmitter* ack) {
        DDLogInfo(@"socket connection error: %@", data);
    }];
    
    
    
    //=======================
    // SocketIO Reconnection
    //=======================
    //---------------------
    // socket reconnecting
    //=====================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_BUILDIN_RECONNECTING callback:^(NSArray* data, SocketAckEmitter* ack) {
        DDLogInfo(@"socket reconnecting attempt: #%@", data);
    }];
    //--------------------
    // socket reconnected
    //====================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_BUILDIN_RECONNECT callback:^(NSArray* data, SocketAckEmitter* ack) {
        DDLogInfo(@"socket reconnected on attempt: #%@", data);
    }];
    //----------------------------
    // socket reconnecion attempt
    //============================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_BUILDIN_RECONNECT_ATTEMPT callback:^(NSArray* data, SocketAckEmitter* ack) {
        DDLogInfo(@"socket attempt reconnecting: %@", data);
    }];
    //---------------------------
    // socket reconnection error
    //===========================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_BUILDIN_RECONNECT_FAILED callback:^(NSArray* data, SocketAckEmitter* ack) {
        DDLogInfo(@"socket reconnection failed: %@", data);
    }];
    //---------------------------
    // socket reconnection error
    //===========================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_BUILDIN_RECONNECT_ERROR callback:^(NSArray* data, SocketAckEmitter* ack) {
        DDLogInfo(@"socket reconnection error: %@", data);
    }];
    
    
    
    //========================
    // SocketIO Disconnection
    //========================
    //---------------------
    // socket disconnected
    //=====================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_BUILDIN_DISCONNECT callback:^(NSArray* data, SocketAckEmitter* ack) {
        [self postNotificationOnMainThread:kAD_NOTIFICATION_SOCKET_DISCONNECTED];
        
        DDLogInfo(@"socket disconnected: %@", data);
    }];
    
    
    
    //===========================================
    // Register User Socket Upon "Welcome" Event
    //===========================================
    //-----------------
    // welcome message
    //=================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_WELCOME callback:^(NSArray* data, SocketAckEmitter* ack) {
        [self registerUser];
        
        DDLogDebug(@"### SocketIOClient ### - welcome message: %@", data);
    }];
    
    
    
    //===============================
    // Ready Event, Socket Is Active
    //===============================
    //---------------
    // ready message
    //===============
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_READY callback:^(NSArray* data, SocketAckEmitter* ack) {
        // get offline private messages
        [self sendGetOfflinePrivateMessages];
        // get offline socket payloads
        [self sendGetOfflineSocketPayloadCache];
        
        DDLogDebug(@"### SocketIOClient ### - ready: %@", data);
    }];
    //---------------------
    // connected user list
    //=====================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_CONNECTED_USER_LIST callback:^(NSArray* data, SocketAckEmitter* ack) {
        DDLogDebug(@"connected user list: %@", data);
    }];
    //----------------------
    // newly connected user
    //======================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_NEW_USER_CONNECTED callback:^(NSArray* data, SocketAckEmitter* ack) {
        DDLogDebug(@"user connected: %@", data);
    }];
    //--------------------
    // user disconnection
    //====================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_USER_DISCONNECTED callback:^(NSArray* data, SocketAckEmitter* ack) {
        DDLogDebug(@"user disconnected: %@", data);
    }];
    
    
    
    //=================
    // Message Related
    //=================
    //-----------------
    // private message
    //=================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_PRIVATE_MESSAGE callback:^(NSArray* data, SocketAckEmitter* ack) {
        ADManagedChatMessage *message = [self handleReceivedPrivateMessage:[data firstObject]];
        if (message) {
            [self postNotificationOnMainThread:kAD_NOTIFICATION_NEW_MESSAGE_RECEIVED
                                        object:self
                                      userInfo:@{@"message" : message}];
        }
        
        // refresh offline last payload timestamp
        DDLogInfo(@"received private message: %@", data);
        [self storeLastSocketPayloadTimestampFromSingleItemPayload:data];
    }];
    //----------------
    // public message
    //================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_PUBLIC_MESSAGE callback:^(NSArray* data, SocketAckEmitter* ack) {
        ADManagedChatMessage *message = [self handleReceivedPrivateMessage:[data firstObject]];
        if (message) {
            [self postNotificationOnMainThread:kAD_NOTIFICATION_NEW_MESSAGE_RECEIVED
                                        object:self
                                      userInfo:@{@"message" : message}];
        }
        
        // refresh offline last payload timestamp
        DDLogInfo(@"received public message: %@", data);
        [self storeLastSocketPayloadTimestampFromSingleItemPayload:data];
    }];
    //--------------------------
    // offline private messages
    //==========================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_OFFLINE_PRIVATE_MESSAGE callback:^(NSArray* data, SocketAckEmitter* ack) {
        // deal with offline private message packet specially
        NSArray *messageItemList = [data firstObject];
        for (id messageItem in messageItemList) {
            ADManagedChatMessage *message = [self handleReceivedPrivateMessage:messageItem];
            DDLogInfo(@"offline message: %@", message);
            if (message) {
                [self postNotificationOnMainThread:kAD_NOTIFICATION_NEW_MESSAGE_RECEIVED
                                            object:self
                                          userInfo:@{@"message" : message}];
            }
        }
        
        // refresh offline last payload timestamp
        DDLogInfo(@"fetched offline messages: %@", data);
        [self storeLastSocketPayloadTimestampFromMultiItemPayload:data];
    }];
    
    
    
    //==============================
    // User Connections and Coupons
    //==============================
    //--------------------
    // connection request
    //====================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_NEW_CONNECTION_REQUEST callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSDictionary *dict = [self dataDictionaryFromSocketIOPayload:data];
        if (dict) {
            ADPendingRequestObject *pendingUser = [[ADPendingRequestObject alloc] initWithDictionary:dict];
            [self.userManager.userCoreDataHelper savePendingRequests:@[pendingUser] clearAll:NO];
        }
        
        // refresh offline last payload timestamp
        DDLogInfo(@"user connection request: %@", data);
        [self storeLastSocketPayloadTimestampFromSingleItemPayload:data];
    }];
    //---------------------
    // connection approval
    //=====================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_APPROVE_USER_CONFIRMATION callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSDictionary *dict = [self dataDictionaryFromSocketIOPayload:data];
        if (dict) {
            NSDictionary *connected_user = [dict dictionaryObjectForKey:@"connected_user"];
            ADUserObject *userObject = [[ADUserObject alloc] initWithDictionary:connected_user];
            [self.userManager.userCoreDataHelper saveUserWithEmail:userObject.email withUserObject:userObject withRelationshipType:ADUserRelationshipTypeConnected updateProfileOnly:NO isRead:NO save:YES];
            [self.userManager.userCoreDataHelper deletePendingUser:userObject.email];
        }
        
        // refresh offline last payload timestamp
        DDLogInfo(@"user connected request approved: %@", data);
        [self storeLastSocketPayloadTimestampFromSingleItemPayload:data];
    }];
    //--------------------------------
    // coupon activation confirmation
    //================================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_COUPON_ACTIVATION_CONFIRMATION_MESSAGE callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSDictionary *dict = [self dataDictionaryFromSocketIOPayload:data];
        if (dict) {
            NSString *couponCode = [dict stringObjectForKey:@"coupon"];
            [self.userManager.userCoreDataHelper activateCoupon:couponCode withDate:nil];
        }
     
        // refresh offline last payload timestamp
        DDLogInfo(@"coupon activated confirmed: %@", data);
        [self storeLastSocketPayloadTimestampFromSingleItemPayload:data];
    }];
    //-----------------------------
    // coupon activation rejection
    //=============================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_COUPON_ACTIVATION_REJECTION_MESSAGE callback:^(NSArray* data, SocketAckEmitter* ack) {
        // do nothing
        
        // refresh offline last payload timestamp
        DDLogInfo(@"coupon activation rejected: %@", data);
        [self storeLastSocketPayloadTimestampFromSingleItemPayload:data];
    }];
    //-------------------
    // coupon redemption
    //===================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_COUPON_REDEEMED callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSDictionary *dict = [self dataDictionaryFromSocketIOPayload:data];
        if (dict) {
            NSString *redeemedCouponCode = [dict stringObjectForKey:@"redeemed_coupon_code"];
            NSDate *redeemedDate = [dict dateObjectForKey:@"redeemed_date"];
            [self.userManager.userCoreDataHelper redeemCoupon:redeemedCouponCode withDate:redeemedDate];
        }
     
        // refresh offline last payload timestamp
        DDLogInfo(@"coupon redemption: %@", data);
        [self storeLastSocketPayloadTimestampFromSingleItemPayload:data];
    }];
    //------------------------------------------
    // post coupon redemption coupon generation
    //==========================================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_NEW_COUPON_GENERATED_AFTER_REDEMPTION callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSDictionary *dict = [self dataDictionaryFromSocketIOPayload:data];
        if (dict) {
            ADCouponObject *coupon = [[ADCouponObject alloc] initWithDictionary:[dict dictionaryObjectForKey:@"coupon"]];
            ADBusinessPartnerObject *partner = [[ADBusinessPartnerObject alloc] initWithDictionary:[dict dictionaryObjectForKey:@"partner"]];
            [self.userManager.userCoreDataHelper saveCoupons:@[coupon] partners:@[partner] clearAll:NO];
        }
        
        // refresh offline last payload timestamp
        DDLogInfo(@"post coupon redemption coupon: %@", data);
        [self storeLastSocketPayloadTimestampFromSingleItemPayload:data];
    }];
    //--------------------------------------------
    // post connection approval coupon generation
    //============================================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_NEW_COUPON_GENERATED_AFTER_APPROVAL callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSDictionary *dict = [self dataDictionaryFromSocketIOPayload:data];
        if (dict) {
            ADCouponObject *coupon = [[ADCouponObject alloc] initWithDictionary:[dict dictionaryObjectForKey:@"coupon"]];
            ADBusinessPartnerObject *partner = [[ADBusinessPartnerObject alloc] initWithDictionary:[dict dictionaryObjectForKey:@"partner"]];
            [self.userManager.userCoreDataHelper saveCoupons:@[coupon] partners:@[partner] clearAll:NO];
        }
        
        // refresh offline last payload timestamp
        DDLogInfo(@"post connection approval coupon: %@", data);
        [self storeLastSocketPayloadTimestampFromSingleItemPayload:data];
    }];
    //--------------------------------------
    // profile image face validation result
    //======================================
    [self.socketIOClient on:kAD_CHAT_SERVICE_MESSAGE_USER_PROFILE_IMAGE_FACE_VALIDATION callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSDictionary *dict = [self dataDictionaryFromSocketIOPayload:data];
        if (dict) {
            BOOL face_validated = [dict boolForKey:@"user_profile_image_face_validation_result"];
            [self.userManager.userCoreDataHelper updateFaceValidationResult:face_validated];
        }
     
        // refresh offline last payload timestamp
        DDLogInfo(@"profile image fail validation result: %@", data);
        [self storeLastSocketPayloadTimestampFromSingleItemPayload:data];
    }];
    
    
    
    // global catch (note* only the predefined events are catched! useless)
    [self.socketIOClient onAny:^(SocketAnyEvent* e) {
        DDLogInfo(@"Event: %@", e.event);
        DDLogInfo(@"Items: %@", e.items);
    }];
}

- (ADChatViewController *)chatViewWithEmail:(NSString *)email fromView:(Class)viewClass
{
    ADChatViewController *chatVC = [[[ADChatViewController class] alloc] initWithNibName:NSStringFromClass([JSQMessagesViewController class]) bundle:[NSBundle bundleForClass:[JSQMessagesViewController class]]];
    ADManagedUser *user = [self.userManager.userCoreDataHelper loadUserObjectFromCoreDataByEmail:email];
    if (user) {
        ADManagedUser *currentUser = self.userManager.currentUserProfile;
        ADChatBundle *chatBundle = [[ADChatBundle alloc] initWithUsers:@[user, currentUser]];
        chatVC.chatBundle = chatBundle;
        chatVC.parentViewClass  = viewClass;
        chatBundle.delegate = chatVC;
        return chatVC;
    }
    else {
        return nil;
    }
}

#pragma mark -
#pragma mark - Socket IO methods

- (void)registerUser
{
    [self.socketIOClient emitWithAck:kAD_CHAT_SERVICE_EVENT_REGISTER with:@[@{@"user_id": self.userManager.currentUserProfile.email}]](10, ^(NSArray* data) {
        if ([data[0] isKindOfClass:[NSString class]] && [data[0] isEqualToString:@"NO ACK"]) {
            DDLogInfo(@"acknowledgement timeout");
        } else {
            if ([data[0][@"success"] isEqual: @YES]) {
                DDLogInfo(@"registered");
            } else {
                DDLogInfo(@"failed to register socket");
            }
        }
    });
}

- (ADManagedChatMessage *)sendMessageThroughSocket:(NSString *)message toUser:(NSString *)email
{
    __block ADManagedChatMessage *managedMessage = [self sendChatMessage:message toUser:email date:[NSDate date]];
    [self.chatCache addObject:managedMessage];
    
    NSDictionary *dict = @{
                           @"message": message,
                           @"recipient_id": email,
                           @"mode": @"PRIVATE_MESSAGE"
                           };
    NSInteger timeout = [self isConnectedToChatService] ? 10 : 1; // set timeout to be close to 0 if chat server is not connected, 0 means no timeout, wait forever, that's why we use 1, not significant to user experience
    [self.socketIOClient emitWithAck:kAD_CHAT_SERVICE_MESSAGE_PRIVATE_MESSAGE with:@[dict]](timeout, ^(NSArray* data) {
        if ([data[0] isKindOfClass:[NSString class]] && [data[0] isEqualToString:@"NO ACK"]) {
            DDLogInfo(@"acknowledgement timeout");
            [self.chatCache removeObject:managedMessage];
            [self postNotificationOnMainThread:kAD_NOTIFICATION_MESSAGE_SENT
                                        object:self
                                      userInfo:nil];
        } else {
            [self.chatCache removeObject:managedMessage];
            NSDictionary *payloadDict = [data[0] dictionaryObjectForKey:@"payload"];
            NSDate *timestamp = [[payloadDict numberObjectForKey:@"timestamp"] dateObject];
            if (!timestamp) {
                timestamp = [NSDate new];
            }
            managedMessage.server_date = timestamp;
            [self saveContext];
            
            [self postNotificationOnMainThread:kAD_NOTIFICATION_MESSAGE_SENT
                                        object:self
                                      userInfo:@{@"message" : message}];
        }
        DDLogDebug(@"%@ -> ack: %@", kAD_CHAT_SERVICE_MESSAGE_PRIVATE_MESSAGE, data);
    });
    
    return managedMessage;
}

- (void)sendEnableActivateCouponThroughSocket:(NSString *)couponCode
{
    NSMutableDictionary *dict = @{}.mutableCopy;
    dict[@"coupon_code"] = couponCode;
    
    CLLocation *currentLocation = self.userManager.locationHelper.currentLocation;
    if (currentLocation == nil) {
        // TODO: show location not enabled error
        return;
    }
    dict[@"loc"] = @[@(currentLocation.coordinate.longitude), @(currentLocation.coordinate.latitude)];
    
    [self.socketIOClient emitWithAck:kAD_CHAT_SERVICE_EVENT_ENABLE_COUPON_ACTIVATION with:@[dict]](10, ^(NSArray* data) {
        DDLogDebug(@"%@ -> ack: %@", kAD_CHAT_SERVICE_EVENT_ENABLE_COUPON_ACTIVATION, data);
    });
}

- (void)sendDisableActivateCouponThroughSocket:(NSString *)couponCode
{
    NSMutableDictionary *dict = @{}.mutableCopy;
    dict[@"coupon_code"] = couponCode;
    
    [self.socketIOClient emitWithAck:kAD_CHAT_SERVICE_EVENT_DISENABLE_COUPON_ACTIVATION with:@[dict]](10, ^(NSArray* data) {
        DDLogDebug(@"%@ -> ack: %@", kAD_CHAT_SERVICE_EVENT_DISENABLE_COUPON_ACTIVATION, data);
    });
}

- (void)sendGetOfflinePrivateMessages
{
    NSString *dateString = [ADUserDefaultHelper lastSocketPayloadTimestamp];
    DDLogDebug(@"last socket payload timestamp is: %@", dateString);
    if (dateString == nil) {
        dateString = @"0";
    }
    NSDictionary *dict = @{ @"last_socket_payload_timestamp" : dateString };
    
    [self.socketIOClient emitWithAck:kAD_CHAT_SERVICE_EVENT_REQUEST_OFFLINE_PRIVATE_MESSAGES with:@[dict]](10, ^(NSArray* data) {
        DDLogDebug(@"%@ -> ack: %@", kAD_CHAT_SERVICE_EVENT_REQUEST_OFFLINE_PRIVATE_MESSAGES, data);
        
        
        
        
        
        // deal with offline private message packet specially
        if ([data[0] isKindOfClass:[NSString class]] && [data[0] isEqualToString:@"NO ACK"]) {
            DDLogInfo(@"%@ acknowledgement timeout", kAD_CHAT_SERVICE_EVENT_REQUEST_OFFLINE_PRIVATE_MESSAGES);
        } else {
            NSArray *messageItemList = [[data firstObject] objectForKey:@"payload"];
            for (id messageItem in messageItemList) {
                ADManagedChatMessage *message = [self handleReceivedPrivateMessage:messageItem];
                DDLogInfo(@"offline message: %@", message);
                if (message) {
                    [self postNotificationOnMainThread:kAD_NOTIFICATION_NEW_MESSAGE_RECEIVED
                                                object:self
                                              userInfo:@{@"message" : message}];
                }
            }
            
            // refresh offline last payload timestamp
            DDLogInfo(@"fetched offline messages: %@", data);
            for (NSDictionary *payloadItem in [[data firstObject] objectForKey:@"payload"]) {
                [self storeLastSocketPayloadTimestampFromPayloadItem:payloadItem];
            }
        }
        
        
        
        
        
    });
}

- (void)sendGetOfflineSocketPayloadCache
{
    NSString *dateString = [ADUserDefaultHelper lastSocketPayloadTimestamp];
    if (dateString == nil) {
        dateString = @"0";
    }
    NSDictionary *dict = @{ @"last_socket_payload_timestamp" : dateString};
    
    [self.socketIOClient emitWithAck:kAD_CHAT_SERVICE_EVENT_GET_OFFLINE_SOCKET_PAYLOAD_CACHE with:@[dict]](10, ^(NSArray* data) {
        DDLogDebug(@"%@ -> ack: %@", kAD_CHAT_SERVICE_EVENT_GET_OFFLINE_SOCKET_PAYLOAD_CACHE, data);
    });
}

- (void)sendSyncStats
{
    NSInteger badgeCount = [self.userManager badgeNumberCount];
    [UIApplication sharedApplication].applicationIconBadgeNumber = badgeCount;
    
    NSString *countString = [NSString stringWithFormat:@"%ld", (long)badgeCount];
    NSArray *userImpression = [ADUserDefaultHelper nearbyUserImpression];
    NSArray *userLikes = [ADUserDefaultHelper nearbyUserLikes];
    NSDictionary *dict = @{ @"badge_count" : countString, @"user_impression" : userImpression, @"user_likes" : userLikes };
    DDLogDebug(@"%@", dict);
    
    [ADUserDefaultHelper setImpressionForNearbyUsers:[NSArray new]];
    [ADUserDefaultHelper setLikesForNearbyUsers:[NSArray new]];
    
    [self.socketIOClient emitWithAck:kAD_CHAT_SERVICE_EVENT_SYNC_STATS with:@[dict]](10, ^(NSArray* data) {
        DDLogInfo(@"%@ -> ack: %@", kAD_CHAT_SERVICE_EVENT_SYNC_STATS, data);
    });
}


#pragma mark -
#pragma mark - Helper methods

- (BOOL)isSendingMessage:(ADManagedChatMessage *)chatMessage
{
    return [self.chatCache containsObject:chatMessage];
}

- (void)reviewChatCache
{
    NSMutableArray *chatsToRemove = @[].mutableCopy;
    NSDate *now = [NSDate date];
    for (ADManagedChatMessage *chat in self.chatCache) {
        if ([now timeIntervalSinceDate:chat.sent_date] >= kAD_CHAT_TIMEOUT_INTERVAL) {
            [chatsToRemove addObject:chat];
        }
    }

    if (chatsToRemove.count > 0) {
        [self.chatCache removeObjectsInArray:chatsToRemove];
        [self postNotificationOnMainThread:kAD_NOTIFICATION_MESSAGE_SENT
                                    object:self
                                  userInfo:nil];
    }
}

- (void)storeLastSocketPayloadTimestampFromMultiItemPayload:(NSArray *)payload
{
    for (NSDictionary *payloadItem in [payload firstObject]) {
        [self storeLastSocketPayloadTimestampFromPayloadItem:payloadItem];
    }
}
- (void)storeLastSocketPayloadTimestampFromSingleItemPayload:(NSArray *)payload
{
    [self storeLastSocketPayloadTimestampFromPayloadItem:[payload firstObject]];
}
- (void)storeLastSocketPayloadTimestampFromPayloadItem:(NSDictionary *)payloadItem
{
    NSString *dateString = [payloadItem objectForKey:@"timestamp"];
    if (dateString != nil) {
        DDLogDebug(@"last load timestamp: %@", dateString);
        [ADUserDefaultHelper setLastSocketPayloadTimestamp:dateString];
    }
}

- (NSDictionary *)dataDictionaryFromSocketIOPayload:(NSArray *)payload
{
    NSDictionary *payloadItem = [payload firstObject];
    // If the sender is not server or the recipient is not me
    if ([[payloadItem stringObjectForKey:@"sender_id"] isEqualToString:@"SERVER"] == NO) { // || [[payloadItem stringObjectForKey:@"recipient_id"] isEqualToString:self.userManager.currentUserProfile.email] == NO
        return nil;
    }
    
    NSDictionary *data = [payloadItem dictionaryObjectForKey:@"data"];
    return data;
}


#pragma mark -
#pragma mark - Core Data

#pragma mark - Fetched Requests Controller

- (NSFetchedResultsController *)unreadMessagesFetchedResultsControllerWithEmail:(NSString *)email delegate:(id<NSFetchedResultsControllerDelegate>)delegate
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kAD_ENTITY_CHAT_MESSAGE];
    if (email) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(read_date = nil) AND (message_owner_id = %@) AND (conversation.user_email = %@)", [ADUserDefaultHelper username], email];
    }
    else {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(read_date = nil) AND (message_owner_id = %@) AND (conversation.user_email != %@)", [ADUserDefaultHelper username], [ADUserDefaultHelper username]];
    }
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"sent_date" ascending:NO]];
    
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    fetchedResultsController.delegate = delegate;
    
    NSError *error;
    [fetchedResultsController performFetch:&error];
    return fetchedResultsController;
}

- (NSFetchedResultsController *)unreadMessagesFetchedResultsControllerWithDelegate:(id<NSFetchedResultsControllerDelegate>)delegate
{
    return [self unreadMessagesFetchedResultsControllerWithEmail:nil delegate:delegate];
}

- (NSFetchedResultsController *)recentChatsFetchedResultsControllerWithDelegate:(id<NSFetchedResultsControllerDelegate>)delegate
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kAD_ENTITY_CONVERSATION];
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(message_owner_id = %@) AND (user_email != %@)", [ADUserDefaultHelper username], [ADUserDefaultHelper username]];

    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"last_message.sent_date" ascending:NO];
    fetchRequest.sortDescriptors = @[sortDescriptor];
    
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    fetchedResultsController.delegate = delegate;
    
    NSError *error;
    [fetchedResultsController performFetch:&error];
    return fetchedResultsController;
}

#pragma mark - Chat

- (void)markMessagesRead:(NSArray *)array withDate:(NSDate *)date
{
    for (ADManagedChatMessage *chat in array) {
        [self markMessageRead:chat withDate:date];
    }
}

- (void)markMessageRead:(ADManagedChatMessage *)message withDate:(NSDate *)date
{
    if (message.read_date) {
        return;
    }
    
    message.read_date = date;
    if (message.read_date == nil) {
        message.read_date = [NSDate date];
    }
    [self saveContext];
}

- (ADManagedChatMessage *)sendChatMessage:(NSString *)text toUser:(NSString *)email date:(NSDate *)date
{
    ADManagedChatMessage *message = [self newManagedChatMessage];
    ADManagedConversation *conversation = [self conversationWithEmail:email];
    conversation.last_message = message;
    
    message.conversation = conversation;
    message.message = text;
    message.sent_date = date;
    message.read_date = date;
    message.isIncoming = NO;
    message.mode = @"PRIVATE_MESSAGE";
    if (message.sent_date == nil) {
        message.sent_date = [NSDate date];
    }
    if (message.read_date == nil) {
        message.read_date = [NSDate date];
    }
    
    [self saveContext];
    return message;
}

- (ADManagedChatMessage *)handleReceivedPrivateMessage:(NSDictionary *)dict
{
    if ([dict isKindOfClass:[NSDictionary class]]) {
        ADManagedChatMessage *message = [self newManagedChatMessage];
        NSString *messageOwner = [dict stringObjectForKey:@"recipient_id"];
        NSString *email = [dict stringObjectForKey:@"sender_id"];
        NSString *messageBody = [dict stringObjectForKey:@"message"];
        NSDate *sentDate = [[dict numberObjectForKey:@"timestamp"] dateObject];
        
        ADManagedConversation *conversation = [self conversationWithEmail:email];
        conversation.last_message = message;

        message.message_owner_id = messageOwner;
        message.conversation = conversation;
        message.message = messageBody;
        message.sent_date = sentDate;
        message.mode = [dict stringObjectForKey:@"mode"];
        message.isIncoming = @YES;
        
        [self saveContext];
        return message;
    }
    
    return nil;
}

- (ADManagedChatMessage *)newManagedChatMessage
{
    NSEntityDescription *userEntity = [NSEntityDescription entityForName:kAD_ENTITY_CHAT_MESSAGE inManagedObjectContext:[self managedObjectContext]];
    ADManagedChatMessage *message = [[ADManagedChatMessage alloc] initWithEntity:userEntity insertIntoManagedObjectContext:[self managedObjectContext]];
    message.message_owner_id = [ADUserDefaultHelper username];
    return message;
}

- (ADManagedConversation *)conversationWithEmail:(NSString *)email
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kAD_ENTITY_CONVERSATION];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(message_owner_id = %@) AND (user_email = %@)", [ADUserDefaultHelper username], email];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"user_email" ascending:NO]];
    fetchRequest.fetchLimit = 1;
    
    NSError *error;
    NSArray *fetchResults = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    ADManagedConversation *managedConversation = [fetchResults firstObject];
    if (managedConversation == nil) {
        NSEntityDescription *conversation = [NSEntityDescription entityForName:kAD_ENTITY_CONVERSATION inManagedObjectContext:[self managedObjectContext]];
        managedConversation = [[ADManagedConversation alloc] initWithEntity:conversation insertIntoManagedObjectContext:[self managedObjectContext]];
        managedConversation.message_owner_id = [ADUserDefaultHelper username];
        managedConversation.user_email = email;
    }
    return managedConversation;
}

- (NSArray *)loadChatMessageByEmail:(NSString *)email beforeDate:(NSDate *)date
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kAD_ENTITY_CHAT_MESSAGE];
    if (date) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(conversation.message_owner_id = %@) AND (conversation.user_email = %@) AND (sent_date < %@)", [ADUserDefaultHelper username], email, date];
    }
    else {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(conversation.message_owner_id = %@) AND (conversation.user_email = %@)", [ADUserDefaultHelper username], email];
    }
    // When retrieve the messages, we should fetch the most recent messages.
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"sent_date" ascending:NO];
    fetchRequest.sortDescriptors = @[sortDescriptor];
    fetchRequest.fetchLimit = kAD_CHAT_SERVICE_BATCH_SIZE;
    
    NSArray *messages = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    NSArray *sortedmessages =[messages sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"sent_date" ascending:YES]]];
    return sortedmessages;
}

- (void)deleteAllMessagesByConversation:(ADManagedConversation *)conversation
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    [self deleteObjectsFromArray:[conversation.messages allObjects] save:NO];
    [conversation removeMessages:conversation.messages];
    [managedObjectContext deleteObject:conversation];
    [self saveContext];
}

- (void)deleteAllMessagesByEmail:(NSString *)email
{
    ADManagedConversation *conversation = [self conversationWithEmail:email];
    [self deleteAllMessagesByConversation:conversation];
}


@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Adore" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Adore.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];

    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (void)deleteObjectsFromArray:(NSArray *)array save:(BOOL)save
{
    for (id item in array) {
        [[self managedObjectContext] deleteObject:item];
    }
    if (save) {
        [self saveContext];
    }
}

- (void)removeCoreDataDatabase
{
    [NSManagedObject deleteAllFromEntity:kAD_ENTITY_CHAT_MESSAGE managedObjectContext:[self managedObjectContext]];
    [NSManagedObject deleteAllFromEntity:kAD_ENTITY_CONVERSATION managedObjectContext:[self managedObjectContext]];
}


@end
