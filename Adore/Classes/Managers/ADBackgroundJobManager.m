//
//  ADBackgroundJobManager.m
//  Adore
//
//  Created by Kevin Wang on 2015-07-23.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADBackgroundJobManager.h"
#import "ADUserManager.h"
#import "ADChatManager.h"



@interface ADBackgroundJobManager ()


@property (nonatomic, strong) NSTimer *updateLastActivityTimer;
@property (nonatomic, strong) NSTimer *socketIOCheckReconnectTimer;
@property (nonatomic, strong) NSTimer *chatTimeoutTimer;


@end



@implementation ADBackgroundJobManager

#pragma mark -
#pragma mark - Initialization

+ (ADBackgroundJobManager *)sharedInstance
{
    static dispatch_once_t pred;
    static ADBackgroundJobManager *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[ADBackgroundJobManager alloc] init];
    });
    return sharedInstance;
}


#pragma mark -
#pragma mark - Helper methods

- (void)initializeBackgroundJobManager
{
    [self setupUpdateLastActivityTimer];
    [self setupSocketIOCheckReconnectTimer];
    [self setupChatTimeoutTimer];
}

- (void)clearBackgroundJobManager
{
    [self invalidateUpdateLastActivityTimer];
    [self invalidateSocketIOCheckReconnectTimer];
    [self invalidateChatTimeoutTimer];
}


#pragma mark - Update last activity

- (void)setupUpdateLastActivityTimer
{
    self.updateLastActivityTimer = [NSTimer scheduledTimerWithTimeInterval:kAD_UPDATE_LAST_ACTIVITY_TIME_INTERVAL target:self selector:@selector(updateLastActivity) userInfo:nil repeats:YES];
}

- (void)invalidateUpdateLastActivityTimer
{
    [self.updateLastActivityTimer invalidate];
    self.updateLastActivityTimer = nil;
}

- (void)updateLastActivity
{
    ADUserManager *userManager = [ADUserManager sharedInstance];
    [userManager updateLastActivity];
}

#pragma mark - SocketIO check reconnect
- (void)setupSocketIOCheckReconnectTimer
{
    self.socketIOCheckReconnectTimer = [NSTimer scheduledTimerWithTimeInterval:kAD_SOCKETIO_CHECK_RECONNECT_INTERVAL target:self selector:@selector(socketIOCheckReconnect) userInfo:nil repeats:YES];
}

- (void)invalidateSocketIOCheckReconnectTimer
{
    [self.socketIOCheckReconnectTimer invalidate];
    self.socketIOCheckReconnectTimer = nil;
}

- (void)socketIOCheckReconnect
{
    ADChatManager *chatManager = [ADChatManager sharedInstance];
    [chatManager reconnectToChatService];
}

#pragma mark - Chat timeout

- (void)setupChatTimeoutTimer
{
    self.chatTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:kAD_CHAT_TIMEOUT_INTERVAL target:self selector:@selector(chatTimeout) userInfo:nil repeats:YES];
}

- (void)invalidateChatTimeoutTimer
{
    [self.chatTimeoutTimer invalidate];
    self.chatTimeoutTimer = nil;
}

- (void)chatTimeout
{
    ADChatManager *chatManager = [ADChatManager sharedInstance];
    [chatManager reviewChatCache];
}

@end
