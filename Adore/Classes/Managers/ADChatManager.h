//
//  ADChatManager.h
//  Adore
//
//  Created by Wang on 2014-04-22.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef DEV
#import "Adore_DEV-Swift.h"
#else
#import "Adore-Swift.h"
#endif

@class ADManagedChatMessage;
@class ADManagedConversation;
@class ADChatManager;
@class ADChatViewController;

// Constants
static NSInteger const kAD_CHAT_SERVICE_BATCH_SIZE = 10;
static NSInteger const kAD_CHAT_SERVICE_RECONNECT_TIME_INTERVAL = 10;



@interface ADChatManager : NSObject


+ (ADChatManager *)sharedInstance;
- (void)initializeChatManager;
- (void)clearChatManager;

- (void)connectToChatService;
- (BOOL)isConnectedToChatService;
- (void)disconnectFromChatService;
- (void)reconnectToChatService;
- (void)checkSessionTicket;
- (SocketIOClientStatus)chatSocketConnectionStatus;
- (BOOL)isSendingMessage:(ADManagedChatMessage *)message;
- (void)reviewChatCache;
- (ADChatViewController *)chatViewWithEmail:(NSString *)email fromView:(Class)viewClass;

- (NSFetchedResultsController *)unreadMessagesFetchedResultsControllerWithEmail:(NSString *)email delegate:(id<NSFetchedResultsControllerDelegate>)delegate;
- (NSFetchedResultsController *)unreadMessagesFetchedResultsControllerWithDelegate:(id<NSFetchedResultsControllerDelegate>)delegate;
- (NSFetchedResultsController *)recentChatsFetchedResultsControllerWithDelegate:(id<NSFetchedResultsControllerDelegate>)delegate;
- (NSArray *)loadChatMessageByEmail:(NSString *)email beforeDate:(NSDate *)date;

- (ADManagedChatMessage *)sendMessageThroughSocket:(NSString *)message toUser:(NSString *)email;
- (void)sendEnableActivateCouponThroughSocket:(NSString *)couponCode;
- (void)sendDisableActivateCouponThroughSocket:(NSString *)couponCode;
- (void)sendSyncStats;

- (void)markMessagesRead:(NSArray *)array withDate:(NSDate *)date;
- (void)markMessageRead:(ADManagedChatMessage *)message withDate:(NSDate *)date;
- (void)deleteAllMessagesByConversation:(ADManagedConversation *)conversation;
- (void)deleteAllMessagesByEmail:(NSString *)email;


@end
