//
//  ADUserManager.h
//  Adore
//
//  Created by Wang on 2014-03-12.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADLocationHelper.h"

@class ADConnectionObject;
@class ADUserObject;
@class ADUserConnectionRequest;
@class ADPendingConnectionsRequest;
@class ADUserCouponRequest;
@class ADUpdateLastActivityRequest;
@class ADManagedUser;
@class ADManagedPartner;
@class ADManagedCoupon;
@class ADUserCoreDataHelper;
@class ADDetailListCoreDataHelper;



@interface ADUserManager : NSObject <ADLocationHelperDelegate, NSFetchedResultsControllerDelegate>

@property (nonatomic, strong, readonly) ADLocationHelper *locationHelper;
@property (nonatomic, strong, readonly) ADUserCoreDataHelper *userCoreDataHelper;
@property (nonatomic, strong, readonly) ADDetailListCoreDataHelper *detailListCoreDataHelper;

@property (nonatomic, strong, readonly) NSString *sessionTicket;

@property (nonatomic, strong, readonly) ADManagedUser *currentUserProfile;


+ (ADUserManager *)sharedInstance;
- (void)initializeUserManager;
- (void)clearUserManager;

- (void)autoUpdateConnections;
- (void)updateLastActivity;

- (void)updateCurrentUser:(ADUserObject *)userObject sessionTicket:(NSString *)ticket needsFetching:(BOOL)fetch;
- (void)updateSessionTicket:(NSString *)ticket;
- (void)checkSessionTicket;
- (BOOL)isLoggedIn;
- (NSInteger)badgeNumberCount;


@end
