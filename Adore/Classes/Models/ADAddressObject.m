//
//  ADAddressObject.m
//  Adore
//
//  Created by Wang on 2015-08-20.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADAddressObject.h"
#import <AddressBook/AddressBook.h>



@implementation ADAddressObject

+ (instancetype)addressObjectWithCountry:(NSString *)country province:(NSString *)province city:(NSString *)city
{
    ADAddressObject *address = [[ADAddressObject alloc] init];
    address.country = country;
    address.province = province;
    address.city = city;
    
    return address;
}

+ (instancetype)addressObjectWithDictionary:(NSDictionary *)dict
{
    ADAddressObject *address = [ADAddressObject addressObjectWithCountry:[dict objectForKey:(NSString *)kABPersonAddressCountryKey]
                                                                province:[dict objectForKey:(NSString *)kABPersonAddressStateKey]
                                                                    city:[dict objectForKey:(NSString *)kABPersonAddressCityKey]];
    return address;
}

+ (NSString *)addressStringForCountry:(NSString *)country province:(NSString *)province city:(NSString *)city
{
    NSMutableString *address = [NSMutableString string];
    if (city && [city length] > 0) {
        [address appendString:city];
    }
    if (province && [province length] > 0) {
        [address appendFormat:@" %@,", province];
    }
    if (country && [country length] > 0) {
        [address appendFormat:@" %@", country];
    }
    
    return [address stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *)addressString
{
    return [ADAddressObject addressStringForCountry:self.country province:self.province city:self.city];
}

@end