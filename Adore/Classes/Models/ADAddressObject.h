//
//  ADAddressObject.h
//  Adore
//
//  Created by Wang on 2015-08-20.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ADAddressObject : NSObject


@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *province;
@property (nonatomic, strong) NSString *city;


+ (instancetype)addressObjectWithCountry:(NSString *)country province:(NSString *)province city:(NSString *)city;
+ (instancetype)addressObjectWithDictionary:(NSDictionary *)dict;

+ (NSString *)addressStringForCountry:(NSString *)country province:(NSString *)province city:(NSString *)city;

- (NSString *)addressString;

@end
