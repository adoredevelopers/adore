//
//  ADBusinessPartnerObject.h
//  Adore
//
//  Created by Chao Lu on 2015-01-09.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ADBusinessPartnerObject : NSObject

@property (nonatomic, strong) NSString *partner_id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *legal_name;

@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *postal_code;
@property (nonatomic, strong) NSString *phone_number;
@property (nonatomic, strong) NSString *homepage;

@property (nonatomic, assign) NSInteger number_of_recommendations;
@property (nonatomic, assign) NSInteger popularity;
@property (nonatomic, assign) CGFloat coupon_discount;
@property (nonatomic, strong) NSString *coupon_discount_message;

@property (nonatomic, strong) NSArray *activity_tags;
@property (nonatomic, strong) NSMutableArray *partner_images;

@property (nonatomic, strong) NSString *enterprise_id;
@property (nonatomic, strong) NSString *partner_level;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
