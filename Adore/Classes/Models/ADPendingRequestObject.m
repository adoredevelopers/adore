//
//  ADPendingRequestObject.m
//  Adore
//
//  Created by Wang on 2014-07-09.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADPendingRequestObject.h"
#import "NSDictionary+NonNilObject.h"
#import "ADUserObject.h"

@implementation ADPendingRequestObject

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
	self = [super init];
	
	if (self)
	{
		NSDictionary *pendingUserDict = [dict dictionaryObjectForKey:@"pending_user"];
		_requestedUser = [[ADUserObject alloc] initWithDictionary:pendingUserDict];
		
		_randomUsersArray = @[].mutableCopy;
		NSArray *randomUserArray = [dict arrayObjectForKey:@"random_pending_users"];
		for (NSDictionary *randomUserDict in randomUserArray) {
            if ([randomUserDict isKindOfClass:[NSNull class]] == NO) {
                ADUserObject *random = [[ADUserObject alloc] initWithDictionary:randomUserDict];
                [_randomUsersArray addObject:random];
            }
		}
	}
	
	return self;
}

@end
