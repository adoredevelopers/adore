//
//  ADNearbyUserObject.h
//  Adore
//
//  Created by Wang on 2014-03-09.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ADUserObject : NSObject

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *motto;

@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSString *fullname;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSDate *birthday;
@property (nonatomic, strong) NSNumber *weight;
@property (nonatomic, strong) NSNumber *height;
@property (nonatomic, strong) NSNumber *martial_status;
@property (nonatomic, strong) NSNumber *body_shape;
@property (nonatomic, strong) NSNumber *race;
@property (nonatomic, strong) NSNumber *residential_status;
@property (nonatomic, strong) NSString *residence_country;
@property (nonatomic, strong) NSString *residence_province;
@property (nonatomic, strong) NSString *residence_city;
@property (nonatomic, strong) NSString *birth_country;
@property (nonatomic, strong) NSString *birth_province;
@property (nonatomic, strong) NSString *birth_city;
@property (nonatomic, strong) NSNumber *c_zodiac;
@property (nonatomic, strong) NSNumber *zodiac;
@property (nonatomic, strong) NSNumber *drinking;
@property (nonatomic, strong) NSNumber *smoking;
@property (nonatomic, strong) NSNumber *religion;
@property (nonatomic, strong) NSNumber *personality;
@property (nonatomic, strong) NSArray *languageArray;
@property (nonatomic, strong) NSArray *interestArray;
@property (nonatomic, strong) NSNumber *education_level;
@property (nonatomic, strong) NSString *undergrad_edu_institution;
@property (nonatomic, strong) NSString *postgrad_edu_institution;
@property (nonatomic, strong) NSString *employer;
@property (nonatomic, strong) NSNumber *occupation;
@property (nonatomic, strong) NSNumber *income;

@property (nonatomic, strong) NSNumber *impression_count;
@property (nonatomic, strong) NSNumber *likes_count;

//@property (nonatomic, strong) NSMutableArray *once_connected_up_for_activities;
@property (nonatomic, strong) NSNumber *last_updated_timestamp;

@property (nonatomic, assign) BOOL display_image_verified;
@property (nonatomic, strong) NSMutableArray *displayImageUrlArray;


- (instancetype)initWithDictionary:(NSDictionary *)dict;

- (NSString *)profilePictureThumbnailUrl;
- (NSString *)profilePictureThumbnailUrlWithSize:(NSNumber *)size;
- (NSString *)profilePictureUrl;

@end
