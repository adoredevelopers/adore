//
//  ADBusinessPartnerObject.m
//  Adore
//
//  Created by Chao Lu on 2015-01-09.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADBusinessPartnerObject.h"
#import "NSDictionary+NonNilObject.h"
#import "ADNSURLCreator.h"



@implementation ADBusinessPartnerObject

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        _partner_id = [dict stringObjectForKey:@"_id"];
        _name = [dict stringObjectForKey:@"name"];
        _legal_name = [dict stringObjectForKey:@"legal_name"];
        
        _address = [dict stringObjectForKey:@"address"];
        _postal_code = [dict stringObjectForKey:@"postal_code"];
        _phone_number = [dict stringObjectForKey:@"phone_number"];
        _homepage = [dict stringObjectForKey:@"homepage"];
        
        _number_of_recommendations = [dict integerForKey:@"number_of_recommendations"];
        _popularity = [dict integerForKey:@"popularity"];
        _coupon_discount = [[dict numberObjectForKey:@"coupon_discount"] floatValue];
        _coupon_discount_message = [dict valueForKey:@"coupon_discount_message"];
        
        _activity_tags = [dict arrayObjectForKey:@"activity_tags"];
        _partner_images = [dict arrayObjectForKey:@"partner_images"].mutableCopy;
        for (int i = 0; i < _partner_images.count; i++) {
            if ([((NSString *)_partner_images[i]) rangeOfString:@"://"].location == NSNotFound) {
                _partner_images[i] = [[ADNSURLCreator partnerImageBaseUrl] stringByAppendingString:_partner_images[i]];
            }
        }
    }
    return self;
}

@end
