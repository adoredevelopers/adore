//
//  ADLoginObject.h
//  Adore
//
//  Created by Wang on 2014-04-19.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADUserObject;

@interface ADLoginObject : NSObject

@property (nonatomic, strong) NSString *token;

@property (nonatomic, strong) ADUserObject *profile;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
