//
//  ADRegisterObject.h
//  Adore
//
//  Created by Wang on 2014-06-05.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADAddressObject;



@interface ADRegisterObject : NSObject

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSString *sex;
@property (nonatomic, strong) UIImage *photo;
@property (nonatomic, strong) NSDate *birthday;
@property (nonatomic, strong) NSString *residence_country;
@property (nonatomic, strong) NSString *residence_province;
@property (nonatomic, strong) NSString *residence_city;

@property (nonatomic, strong) NSString *notification_token;

- (NSData *)JSONData;
- (NSString *)validationResult;
- (NSString *)residentialAddress;
- (void)updateWithAddressObject:(ADAddressObject *)address;

@end
