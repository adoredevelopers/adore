//
//  ADCouponObject.h
//  Adore
//
//  Created by Kevin Wang on 2014-09-23.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ADCouponObject : NSObject

@property (nonatomic, strong) NSString *coupon_code;
@property (nonatomic, strong) NSString *coupon_status;
@property (nonatomic, strong) NSDate *expiration_date;
@property (nonatomic, strong) NSDate *creation_date;
@property (nonatomic, strong) NSDate *redeem_date;

@property (nonatomic, strong) NSString *coupon_activity_tag;
@property (nonatomic, strong) NSString *connection_user;

@property (nonatomic, strong) NSString *partner_id;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
