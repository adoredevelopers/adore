//
//  ADUserConnectionObject.m
//  Adore
//
//  Created by Wang on 2014-08-14.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADUserConnectionObject.h"
#import "NSDictionary+NonNilObject.h"

@implementation ADUserConnectionObject

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
	self = [super initWithDictionary:dict];
    
	_connection_status = [dict stringObjectForKey:@"connection_status"];
	return self;
}

@end
