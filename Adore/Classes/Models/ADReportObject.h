//
//  ADReportObject.h
//  Adore
//
//  Created by Wang on 2015-07-07.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ADReportObject : NSObject


@property (nonatomic, strong) NSString *user_id;
@property (nonatomic, strong) NSString *image_url;
@property (nonatomic, strong) NSString *reason;
@property (nonatomic, strong) NSString *comments;


- (NSData *)JSONData;


@end
