//
//  ADNearbyEventObject.h
//  Adore
//
//  Created by Chao Lu on 2016-03-05.
//  Copyright © 2016 AdoreMobile Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ADNearbyEventObject : NSObject

@property (nonatomic, strong) NSString *event_id;
@property (nonatomic, strong) NSString *event_title;
@property (nonatomic, strong) NSString *event_description;
@property (nonatomic, strong) NSString *event_type;
@property (nonatomic, strong) NSString *event_status;
@property (nonatomic, strong) NSString *event_owner;
@property (nonatomic, strong) NSString *event_organizer;
@property (nonatomic, strong) NSString *event_image;
@property (nonatomic, strong) NSArray *event_tags;
@property (nonatomic, strong) NSNumber *maximum_participants;
@property (nonatomic, strong) NSString *activity_tag;
@property (nonatomic, strong) NSDate *creation_date;
@property (nonatomic, strong) NSDate *deadline_date;
@property (nonatomic, strong) NSDate *start_date;
@property (nonatomic, strong) NSNumber *duration_in_hours;
@property (nonatomic, strong) NSArray *participate_users;
@property (nonatomic, strong) NSArray *participate_users_waiting_list;
@property (nonatomic, strong) NSNumber *distance;

@property (nonatomic, strong) NSDictionary *event_location;
@property (nonatomic, strong) NSString *event_location_street;
@property (nonatomic, strong) NSString *event_location_city_region;
@property (nonatomic, strong) NSString *event_location_province_state_region;
@property (nonatomic, strong) NSString *event_location_postal_zip_code;
@property (nonatomic, strong) NSString *event_location_country_region;


- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
