//
//  ADConnectionObject.m
//  Adore
//
//  Created by Kevin Wang on 2014-11-16.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADConnectionObject.h"
#import "NSDictionary+NonNilObject.h"
#import "NSDate+Utils.h"

@implementation ADConnectionObject

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    if (self) {
        _email = [dict stringObjectForKey:@"email"];
        _status = [dict stringObjectForKey:@"status"];
        _creation_date = [dict dateObjectForKey:@"creation_date"];
    }
    return self;
}

@end
