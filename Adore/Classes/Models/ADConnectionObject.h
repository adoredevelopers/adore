//
//  ADConnectionObject.h
//  Adore
//
//  Created by Kevin Wang on 2014-11-16.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ADConnectionObject : NSObject

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSDate *creation_date;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
