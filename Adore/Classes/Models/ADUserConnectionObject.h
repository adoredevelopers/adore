//
//  ADUserConnectionObject.h
//  Adore
//
//  Created by Wang on 2014-08-14.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADUserObject.h"

@interface ADUserConnectionObject : ADUserObject

@property (nonatomic, strong) NSString *connection_status;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
