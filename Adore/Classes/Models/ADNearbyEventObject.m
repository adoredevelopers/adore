//
//  ADNearbyEventObject.m
//  Adore
//
//  Created by Chao Lu on 2016-03-05.
//  Copyright © 2016 AdoreMobile Inc. All rights reserved.
//

#import "ADNearbyEventObject.h"
#import "NSNumber+DateUtils.h"
#import "NSDictionary+NonNilObject.h"

@implementation ADNearbyEventObject

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    if (self) {
        _event_id = [dict stringObjectForKey:@"event_id"];
        _event_title = [dict stringObjectForKey:@"event_title"];
        _event_description = [dict stringObjectForKey:@"event_description"];
        _event_image = [dict stringObjectForKey:@"event_image"];
        _event_type = [dict stringObjectForKey:@"event_type"];
        _event_owner = [dict stringObjectForKey:@"event_owner"];
        _event_organizer = [dict stringObjectForKey:@"event_organizer"];
        _event_status = [dict stringObjectForKey:@"event_status"];
        _activity_tag = [dict stringObjectForKey:@"activity_tag"];
        _event_tags = [dict arrayObjectForKey:@"event_tags"];
        _maximum_participants = [dict numberObjectForKey:@"maximum_participants"];
        _distance = [dict numberObjectForKey:@"distance"];
        _creation_date = [dict dateObjectForKey:@"creation_date"];
        _deadline_date = [dict dateObjectForKey:@"deadline_date"];
        _start_date = [dict dateObjectForKey:@"start_date"];
        _duration_in_hours = [dict numberObjectForKey:@"duration_in_hours"];
        _participate_users = [dict arrayObjectForKey:@"participate_users"];
        _participate_users_waiting_list = [dict arrayObjectForKey:@"participate_users_waiting_list"];
        
        _event_location = [dict dictionaryObjectForKey:@"event_location"];
        _event_location_street = [_event_location stringObjectForKey:@"street"];
        _event_location_city_region = [_event_location stringObjectForKey:@"city_region"];
        _event_location_province_state_region = [_event_location stringObjectForKey:@"province_state_region"];
        _event_location_postal_zip_code = [_event_location stringObjectForKey:@"postal_zip_code"];
        _event_location_country_region = [_event_location stringObjectForKey:@"country_region"];
    }
    
    return self;
}

@end
