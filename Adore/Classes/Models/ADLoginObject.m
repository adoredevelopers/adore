//
//  ADLoginObject.m
//  Adore
//
//  Created by Wang on 2014-04-19.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADLoginObject.h"
#import "ADUserObject.h"
#import "NSDictionary+NonNilObject.h"

@implementation ADLoginObject

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
	self = [super init];
    
	if (self) {
		_token = [dict stringObjectForKey:@"token"];
		
		NSDictionary *profileDict = [dict dictionaryObjectForKey:@"user_record"];
		_profile = [[ADUserObject alloc] initWithDictionary:profileDict];
	}
	
    return self;
}

@end
