//
//  ADRegisterObject.m
//  Adore
//
//  Created by Wang on 2014-06-05.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADRegisterObject.h"
#import "NSString+Security.h"
#import "NSDictionary+JSONObject.h"
#import "ADUserManager.h"
#import "NSDate+Utils.h"
#import "ADUserManager.h"
#import "NSDateFormatter+Utils.h"
#import "ADAddressObject.h"
#import "NSMutableDictionary+SetNonNilObject.h"



@implementation ADRegisterObject

- (NSData *)JSONData
{
    NSString *birthdayString = [[NSDateFormatter ADDefaultFormatter] stringFromDate:[self.birthday dateComponet]];
    
    ADUserManager *userManager = [ADUserManager sharedInstance];
    CLLocation *currentLocation = userManager.locationHelper.currentLocation;
    NSNumber *latitude;
    NSNumber *longitude;
    if (currentLocation == nil) {
        latitude = [NSNumber numberWithDouble:0.0];
        longitude = [NSNumber numberWithDouble:0.0];
    } else {
        latitude = [NSNumber numberWithDouble:currentLocation.coordinate.latitude];
        longitude = [NSNumber numberWithDouble:currentLocation.coordinate.longitude];
    }
    
    NSMutableDictionary *dict = @{}.mutableCopy;
    [dict setValue:self.email forKey:@"email"];
    [dict setValue:[self.password SHA256] forKey:@"password_hash"];
    [dict setValue:self.nickname forKey:@"nickname"];
    [dict setValue:self.sex forKey:@"gender"];
    [dict setValue:birthdayString forKey:@"birthday"];
    [dict setValue:self.photo forKey:@"photo"];
    [dict setValue:latitude forKey:@"latitude"];
    [dict setValue:longitude forKey:@"longitude"];
    [dict setObject:self.residence_country forKey:@"residence_country" defaultValue:@""];
    [dict setObject:self.residence_province forKey:@"residence_province" defaultValue:@""];
    [dict setObject:self.residence_city forKey:@"residence_city" defaultValue:@""];
    
    [dict setValue:self.notification_token forKey:@"notification_token"];

    return [dict multiUploadsDataObject];
}

- (NSString *)validationResult
{
    NSString *errorMessage = nil;
    
    if ([self.email length] == 0) {
        errorMessage = NSLocalizedString(@"Please enter your email.", nil);
    }
    else if ([self.password length] < 6) {
        errorMessage = NSLocalizedString(@"Please enter a password with more than 6 characters.", nil);
    }
    else if ([self.nickname length] == 0) {
        errorMessage = NSLocalizedString(@"Please enter a nickname.", nil);
    }
    else if (self.photo == nil) {
        errorMessage = NSLocalizedString(@"Please select your profile image.", nil);
    }
    else if (self.sex == nil) {
        errorMessage = NSLocalizedString(@"Please select your gender.", nil);
    }
    else if (self.residence_country == nil && self.residence_province == nil && self.residence_city == nil) {
        errorMessage = NSLocalizedString(@"Please select your current residence place.", nil);
    }
    else {
        NSDate *birthday18 = [NSDate appUserMaximumBirthday];
        NSComparisonResult compareResult = [birthday18 compare:self.birthday];
        if (self.birthday == nil || compareResult == NSOrderedAscending) {
            // too young
            errorMessage = NSLocalizedString(@"You have to be at least 18 years old to use our service.", nil);
        }
    }
    return errorMessage;
}

- (NSString *)residentialAddress
{
    ADAddressObject *address = [ADAddressObject addressObjectWithCountry:self.residence_country
                                                                province:self.residence_province
                                                                    city:self.residence_city];
    return [address addressString];
}

- (void)updateWithAddressObject:(ADAddressObject *)address
{
    self.residence_country = address.country;
    self.residence_province = address.province;
    self.residence_city = address.city;
}


@end
