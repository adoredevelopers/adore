//
//  ADNearbyUserObject.m
//  Adore
//
//  Created by Wang on 2014-08-13.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADNearbyUserObject.h"
#import "NSDictionary+NonNilObject.h"
#import "NSNumber+DateUtils.h"



@implementation ADNearbyUserObject

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
	self = [super initWithDictionary:dict];

	_connection_status = [dict stringObjectForKey:@"connection_status"];
    _last_activity = [dict dateObjectForKey:@"last_activity"];
    _distance = [dict numberObjectForKey:@"distance"];
	return self;
}

- (NSString *)lastActivitySinceString
{
    NSNumber *duration = @((NSInteger)[[NSDate date] timeIntervalSinceDate:self.last_activity]);
    NSString *durationString = [duration durationString];
    return [[NSString stringWithFormat:@"%@", durationString] stringByAppendingString:NSLocalizedString(@"ago", nil)];
}

@end
