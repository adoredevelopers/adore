//
//  ADCouponObject.m
//  Adore
//
//  Created by Kevin Wang on 2014-09-23.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADCouponObject.h"
#import "NSDictionary+NonNilObject.h"
#import "NSDate+Utils.h"
#import "ADUserManager.h"
#import "ADManagedUser+Extensions.h"

@implementation ADCouponObject

#pragma mark -
#pragma mark - Initialization

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        _coupon_code = [dict stringObjectForKey:@"coupon_code"];
        
        ADUserManager *userManager = [ADUserManager sharedInstance];
        NSString *current_user = userManager.currentUserProfile.email;
        NSArray *coupon_owners = [dict arrayObjectForKey:@"coupon_owners"];
        if ([[coupon_owners objectAtIndex:0] isEqualToString:current_user]) {
            _connection_user = [coupon_owners objectAtIndex:1];
        } else {
            _connection_user = [coupon_owners objectAtIndex:0];
        }
        
        _coupon_activity_tag = [dict stringObjectForKey:@"activity_tag"];
        _coupon_status = [dict stringObjectForKey:@"coupon_status"];
        _creation_date = [dict dateObjectForKey:@"creation_date"];
        _expiration_date = [dict dateObjectForKey:@"expiration_date"];
        _redeem_date = [dict dateObjectForKey:@"redeem_date"];
        
        _partner_id = [dict stringObjectForKey:@"partner_id"];
    }
    return self;
}




@end
