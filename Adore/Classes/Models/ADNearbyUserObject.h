//
//  ADNearbyUserObject.h
//  Adore
//
//  Created by Wang on 2014-08-13.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADUserObject.h"

@interface ADNearbyUserObject : ADUserObject

@property (nonatomic, strong) NSString *connection_status;
@property (nonatomic, strong) NSNumber *distance;
@property (nonatomic, strong) NSDate *last_activity;

- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSString *)lastActivitySinceString;

@end
