//
//  ADReportObject.m
//  Adore
//
//  Created by Wang on 2015-07-07.
//  Copyright (c) 2015 AdoreMobile Inc. All rights reserved.
//

#import "ADReportObject.h"
#import "NSDictionary+JSONObject.h"
#import "NSMutableDictionary+SetNonNilObject.h"



@implementation ADReportObject


- (NSData *)JSONData
{
    NSMutableDictionary *dict = @{}.mutableCopy;
    [dict setObject:self.user_id forKey:@"user_id" defaultValue:@""];
    [dict setObject:self.reason forKey:@"reason" defaultValue:@""];
    [dict setObject:self.image_url forKey:@"image_url" defaultValue:@""];
    [dict setObject:self.comments forKey:@"comments" defaultValue:@""];
    
    return [dict JSONDataObject];
}


@end
