//
//  ADNearbyUserObject.m
//  Adore
//
//  Created by Wang on 2014-03-09.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADUserObject.h"
#import "NSNumber+DateUtils.h"
#import "NSDictionary+NonNilObject.h"
#import "ADUserManager.h"
#import "ADCouponObject.h"
#import "ADConnectionObject.h"
#import "NSString+ThumbnailURL.h"
#import "ADNSUrlCreator.h"



@implementation ADUserObject

#pragma mark -
#pragma mark - Initialization

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
	self = [super init];
    
    if (self) {
        _userId = [dict stringObjectForKey:@"_id"];
		_email = [dict stringObjectForKey:@"email"];

		NSDictionary *user_profile_dict = [dict dictionaryObjectForKey:@"profile"];
		if (user_profile_dict != nil) {
			_nickname = [user_profile_dict stringObjectForKey:@"nickname"];
            _fullname = [user_profile_dict stringObjectForKey:@"full_name"];
            _motto = [user_profile_dict stringObjectForKey:@"motto"];
			_gender = [user_profile_dict stringObjectForKey:@"gender"];
			_birthday = [user_profile_dict dateObjectForKey:@"birthday"];
            _c_zodiac = [user_profile_dict numberObjectForKey:@"c_zodiac"];
            _zodiac = [user_profile_dict numberObjectForKey:@"zodiac"];
            _weight = [user_profile_dict numberObjectForKey:@"weight"];
            _height = [user_profile_dict numberObjectForKey:@"height"];
            _martial_status = [user_profile_dict numberObjectForKey:@"martial_status"];
            _body_shape = [user_profile_dict numberObjectForKey:@"body_shape"];
            _race = [user_profile_dict numberObjectForKey:@"race"];
            _residential_status = [user_profile_dict numberObjectForKey:@"residential_status"];
            NSDictionary *residence_place = [user_profile_dict objectForKey:@"residence_place"];
            _residence_country = [residence_place stringObjectForKey:@"residence_country"];
            _residence_province = [residence_place stringObjectForKey:@"residence_province"];
            _residence_city = [residence_place stringObjectForKey:@"residence_city"];
            NSDictionary *birth_place = [user_profile_dict objectForKey:@"birth_place"];
            _birth_country = [birth_place stringObjectForKey:@"birth_country"];
            _birth_province = [birth_place stringObjectForKey:@"birth_province"];
            _birth_city = [birth_place stringObjectForKey:@"birth_city"];
            
            _drinking = [user_profile_dict numberObjectForKey:@"drinking"];
            _smoking = [user_profile_dict numberObjectForKey:@"smoking"];
            _religion = [user_profile_dict numberObjectForKey:@"religion"];
            _personality = [user_profile_dict numberObjectForKey:@"personality"];

			_languageArray = [user_profile_dict arrayObjectForKey:@"language"];
            _interestArray = [user_profile_dict arrayObjectForKey:@"interest"];

            _education_level = [user_profile_dict numberObjectForKey:@"education_level"];
            _undergrad_edu_institution = [user_profile_dict stringObjectForKey:@"undergrad_edu_institutions"];
            _postgrad_edu_institution = [user_profile_dict stringObjectForKey:@"postgrad_edu_institutions"];
            _employer = [user_profile_dict stringObjectForKey:@"employer"];
            _occupation = [user_profile_dict numberObjectForKey:@"occupation"];
            _income = [user_profile_dict numberObjectForKey:@"income"];
        }
        
        NSDictionary *user_stats_dict = [dict dictionaryObjectForKey:@"stats"];
        if (user_stats_dict != nil) {
            _impression_count = [user_stats_dict numberObjectForKey:@"number_of_visits"];
            _likes_count = [user_stats_dict numberObjectForKey:@"number_of_admirers"];
        }
        
        _last_updated_timestamp = [dict numberObjectForKey:@"last_updated_timestamp"];
        _display_image_verified = [dict boolForKey:@"display_image_verified"];
		_displayImageUrlArray = [dict arrayObjectForKey:@"display_image_urls"].mutableCopy;
        for (int i = 0; i < _displayImageUrlArray.count; i++) {
            if ([((NSString *)_displayImageUrlArray[i]) rangeOfString:@"://"].location == NSNotFound) {
                _displayImageUrlArray[i] = [[ADNSURLCreator profileImageBaseUrl] stringByAppendingString:_displayImageUrlArray[i]];
            }
        }
    }
	
    return self;
}


#pragma mark -
#pragma mark - Helper methods

- (NSString *)profilePictureThumbnailUrl
{
    NSString *profile_picture_url = [self.displayImageUrlArray firstObject];
	return [profile_picture_url thumbnailImageUrl];
}

- (NSString *)profilePictureThumbnailUrlWithSize:(NSNumber *)size
{
    NSString *profile_picture_url = [self.displayImageUrlArray firstObject];
    if (size == nil) {
        profile_picture_url = [profile_picture_url thumbnailImageUrl160];
    } else if ([size intValue] == 320) {
        profile_picture_url = [profile_picture_url thumbnailImageUrl320];
    } else if ([size intValue] == 160) {
        profile_picture_url = [profile_picture_url thumbnailImageUrl160];
    } else {
        profile_picture_url = [profile_picture_url thumbnailImageUrl160];
    }
    return profile_picture_url;
}

- (NSString *)profilePictureUrl
{
    NSString *profile_picture_url = [self.displayImageUrlArray firstObject];
    return profile_picture_url;
}

@end
