//
//  ADPendingRequestObject.h
//  Adore
//
//  Created by Wang on 2014-07-09.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADUserObject;

@interface ADPendingRequestObject : NSObject

@property (nonatomic, strong) ADUserObject *requestedUser;
@property (nonatomic, strong) NSMutableArray *randomUsersArray;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end
