//
//  ADUserCoreDataHelper.m
//  Adore
//
//  Created by Wang on 2014-12-10.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADUserCoreDataHelper.h"
#import "ADManagedUser+Extensions.h"
#import "ADManagedCoupon+Extensions.h"
#import "ADManagedPartner+Extensions.h"
#import "ADConnectionObject.h"
#import "ADCouponObject.h"
#import "ADPendingRequestObject.h"
#import "ADUserObject.h"
#import "ADBusinessPartnerObject.h"
#import "NSManagedObject+Extension.h"
#import "ADNearbyUserObject.h"
#import "ADUserConnectionObject.h"



@interface ADUserCoreDataHelper ()

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end



@implementation ADUserCoreDataHelper

#pragma mark -
#pragma mark - Fetched Controller Creators

- (NSFetchedResultsController *)pendingUsersFetchedResultsControllerWithDelegate:(id<NSFetchedResultsControllerDelegate>)delegate
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kAD_ENTITY_USER];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(isPendingUser == YES) AND (related_to_pending_user_email = email)"];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"email" ascending:YES];
    fetchRequest.sortDescriptors = @[sortDescriptor];

    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    fetchedResultsController.delegate = delegate;
    
    NSError *error;
    [fetchedResultsController performFetch:&error];
    return fetchedResultsController;
}

- (NSFetchedResultsController *)connectedUsersFetchedResultsControllerWithDelegate:(id<NSFetchedResultsControllerDelegate>)delegate
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSArray *relationshipArray = @[@(ADUserRelationshipTypeConnected),
                                   @(ADUserRelationshipTypeBlocked)];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kAD_ENTITY_USER];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(isPendingUser == NO) AND (relationship IN %@)", relationshipArray];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"email" ascending:YES];
    fetchRequest.sortDescriptors = @[sortDescriptor];

    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    fetchedResultsController.delegate = delegate;
    
    NSError *error;
    [fetchedResultsController performFetch:&error];
    return fetchedResultsController;
}

- (NSFetchedResultsController *)unreadConnectedUsersFetchedResultsControllerWithDelegate:(id<NSFetchedResultsControllerDelegate>)delegate
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSArray *relationshipArray = @[@(ADUserRelationshipTypeConnected),
                                   @(ADUserRelationshipTypeBlocked)];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kAD_ENTITY_USER];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(isPendingUser == NO) AND (relationship IN %@) AND (isRead = NO OR isRead = nil)", relationshipArray];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"email" ascending:YES];
    fetchRequest.sortDescriptors = @[sortDescriptor];
    
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    fetchedResultsController.delegate = delegate;
    
    NSError *error;
    [fetchedResultsController performFetch:&error];
    return fetchedResultsController;
}

- (NSFetchedResultsController *)couponsFetchedResultsControllerForEmail:(NSString *)email withDelegate:(id<NSFetchedResultsControllerDelegate>)delegate
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kAD_ENTITY_COUPON];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"connection_user_email = %@", email];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"connection_user_email" ascending:YES];
    fetchRequest.sortDescriptors = @[sortDescriptor];

    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    fetchedResultsController.delegate = delegate;
    
    NSError *error;
    [fetchedResultsController performFetch:&error];
    return fetchedResultsController;
}


#pragma mark -
#pragma mark - Adore connections related

- (void)clearupManagedUser:(ADManagedUser *)managedUser
{
    ADManagedUser *storedUser = [self loadUserObjectFromCoreDataByEmail:managedUser.email];
    if (storedUser) {
//        NSAssert(NO, @"this shouldn't happen");
//        [self deleteObjectsFromArray:@[storedUser] save:NO];
    }
    NSArray *pendingUserArray = [self loadUserFromCoreDataRelatedToPendingUser:managedUser.email];
    [self deleteObjectsFromArray:pendingUserArray save:NO];
    [self saveContext];
}

- (ADManagedUser *)rejectPendingUser:(ADManagedUser *)managedUser
{
    if (managedUser.managedObjectContext == nil) {
        [[self managedObjectContext] insertObject:managedUser];
    }
    managedUser.relationship = @(ADUserRelationshipTypeRejected);
    managedUser.relationship_created_date = [NSDate date];
    managedUser.isPendingUser = @NO;
    managedUser.related_to_pending_user_email = nil;
    [self saveContext];
    
    [self clearupManagedUser:managedUser];
    return managedUser;
}

- (void)deleteUser:(ADManagedUser *)managedUser
{
    ADManagedUser *storedUser = [self loadUserObjectFromCoreDataByEmail:managedUser.email];
    if (storedUser) {
        [self deleteObjectsFromArray:@[storedUser] save:YES];
    }
}


#pragma mark -
#pragma mark - Users Methods

#pragma mark - Creation

- (ADManagedUser *)managedUserFromNearbyUser:(ADNearbyUserObject *)nearbyUser
{
    // Create a temporary managed user
    NSEntityDescription *userEntity = [NSEntityDescription entityForName:kAD_ENTITY_USER inManagedObjectContext:[self managedObjectContext]];
    ADManagedUser *managedUser = [[ADManagedUser alloc] initWithEntity:userEntity insertIntoManagedObjectContext:nil];
    [managedUser updateWithUserObject:nearbyUser];
    managedUser.relationship = @([Constants userRelationshipFromString:nearbyUser.connection_status]);
    return managedUser;
}

#pragma mark - Load Users Methods

- (ADManagedUser *)loadCurrentUserFromCoreData
{
    NSArray *array = [self loadUserFromCoreDataWithTypes:@[@(ADUserRelationshipTypeSelf)]];
    return [array firstObject];
}

- (BOOL)currentUserFaceValidated
{
    ADManagedUser *user = [self loadCurrentUserFromCoreData];
    if (user.face_validation_result == nil) {
        return YES;
    }
    return [user.face_validation_result boolValue];
}

- (ADManagedUser *)loadUserObjectFromCoreDataByEmail:(NSString *)email
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kAD_ENTITY_USER];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(email = %@) AND (isPendingUser == NO)", email];
    NSArray *users = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    return [users firstObject];
}

- (NSArray *)loadUserFromCoreDataWithTypes:(NSArray *)typeArray
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kAD_ENTITY_USER];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(isPendingUser == NO) AND (relationship IN %@)", typeArray];
    NSArray *users = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    return users;
}


#pragma mark -
#pragma mark - Save Users Methods

- (void)saveUserConnections:(NSArray *)array removeAllConnections:(BOOL)removeAll updateProfileOnly:(BOOL)updateProfileOnly isRead:(BOOL)isRead
{
    if (removeAll) {
        // Remove all stored connections
        NSFetchedResultsController *currentConnectionController = [self connectedUsersFetchedResultsControllerWithDelegate:nil];
        [self deleteObjectsFromArray:currentConnectionController.fetchedObjects save:YES];
    }
    
    // Add connections into core data
    for (ADUserConnectionObject *user in array) {
        ADUserRelationshipType type = [Constants userRelationshipFromString:user.connection_status];
        if (type == ADUserRelationshipTypeConnected
            || type == ADUserRelationshipTypeBlocked
            || updateProfileOnly) {
            [self saveUserWithEmail:user.email withUserObject:user withRelationshipType:type updateProfileOnly:updateProfileOnly isRead:isRead save:NO];
        }
    }
    
    [self saveContext];
}

// Use the email to find the user record in core data, and update the profile with given user object.
// Override the relationship if necessary
- (ADManagedUser *)saveUserWithEmail:(NSString *)email withUserObject:(ADUserObject *)userObject withRelationshipType:(ADUserRelationshipType)type updateProfileOnly:(BOOL)updateProfileOnly isRead:(BOOL)isRead save:(BOOL)save
{
    ADManagedUser *managedUser = [self loadUserObjectFromCoreDataByEmail:email];
    if (managedUser == nil) {
        NSEntityDescription *userEntity = [NSEntityDescription entityForName:kAD_ENTITY_USER inManagedObjectContext:[self managedObjectContext]];
        managedUser = [[ADManagedUser alloc] initWithEntity:userEntity insertIntoManagedObjectContext:[self managedObjectContext]];
    }
    if (userObject != nil) {
        [managedUser updateWithUserObject:userObject];
    }
    if (updateProfileOnly == NO) {
        managedUser.relationship = @(type);
    }
    if (isRead) {
        managedUser.isRead = @(isRead);
    }
    managedUser.isPendingUser = @NO;
    
    if (save) {
        [self saveContext];
    }
    return managedUser;
}

- (void)markUserAsRead:(ADManagedUser *)user
{
    if ([user.isRead boolValue] == NO) {
        user.isRead = @YES;
        [self saveContext];
    }
}

- (void)updateFaceValidationResult:(BOOL)validation
{
    ADManagedUser *currentUser = [self loadCurrentUserFromCoreData];
    currentUser.face_validation_result = @(validation);
    [self saveContext];
}


#pragma mark -
#pragma mark - Pending Users Methods

#pragma mark - Load Pending Users Methods

- (NSArray *)loadUserFromCoreDataRelatedToPendingUser:(NSString *)email
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kAD_ENTITY_USER];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(isPendingUser == YES) AND (related_to_pending_user_email = %@)", email];
    NSArray *users = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    return users;
}

#pragma mark - Save Pending Users Methods

- (void)savePendingRequests:(NSArray *)pendingRequests clearAll:(BOOL)clearAll
{
    if (clearAll) {
        [self deleteAllPendingUsers];
    }
    
    for (ADPendingRequestObject *request in pendingRequests) {
        [self savePendingRequestObject:request save:NO];
    }
    [self saveContext];
}

- (void)savePendingRequestObject:(ADPendingRequestObject *)pendingUser save:(BOOL)save
{
    // Error handling
    if (pendingUser.randomUsersArray.count != 3) {
        return;
    }
    
    NSString *pendingUserEmail = pendingUser.requestedUser.email;
    
    // Remove all current user info related to the pending user
    [self deletePendingUser:pendingUserEmail];
    
    [self savePendingUser:pendingUser.requestedUser relatedToUser:pendingUserEmail save:NO];
    for (ADUserObject *randomUser in pendingUser.randomUsersArray) {
        [self savePendingUser:randomUser relatedToUser:pendingUserEmail save:NO];
    }
    
    if (save) {
        [self saveContext];
    }
}

- (ADManagedUser *)savePendingUser:(ADUserObject *)userObject relatedToUser:(NSString *)email save:(BOOL)save
{
    NSEntityDescription *pendingUserEntity = [NSEntityDescription entityForName:kAD_ENTITY_USER inManagedObjectContext:[self managedObjectContext]];
    ADManagedUser *pendingUser = [[ADManagedUser alloc] initWithEntity:pendingUserEntity insertIntoManagedObjectContext:[self managedObjectContext]];
    
    [pendingUser updateWithUserObject:userObject];
    pendingUser.isPendingUser = @YES;
    pendingUser.related_to_pending_user_email = email;

    if (save) {
        [self saveContext];
    }
    return pendingUser;
}

#pragma mark - Delete Pending Users Methods

- (void)deletePendingUser:(NSString *)email
{
    NSArray *userArray = [self loadUserFromCoreDataRelatedToPendingUser:email];
    [self deleteObjectsFromArray:userArray save:YES];
}

- (void)deleteAllPendingUsers
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kAD_ENTITY_USER];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"isPendingUser == YES"];
    fetchRequest.includesPropertyValues = NO;
    NSArray *users = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    [self deleteObjectsFromArray:users save:YES];
}


#pragma mark -
#pragma mark - Coupon & Partner Methods

#pragma mark - Load Methods

- (ADManagedCoupon *)loadCouponFromCoreDataByCode:(NSString *)couponCode
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kAD_ENTITY_COUPON];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"coupon_code = %@", couponCode];
    NSArray *coupons = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    return [coupons firstObject];
}

- (ADManagedPartner *)loadBusinessPartnerFromCoreDataByPartnerId:(NSString *)partnerId
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kAD_ENTITY_PARTNER];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"partner_id = %@", partnerId];
    NSArray *partners = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    return [partners firstObject];
}

#pragma mark - Save Methods

- (void)saveCoupons:(NSArray *)coupons partners:(NSArray *)partners clearAll:(BOOL)clearAll
{
    if (clearAll) {
        [NSManagedObject deleteAllFromEntity:kAD_ENTITY_PARTNER managedObjectContext:[self managedObjectContext]];
        [NSManagedObject deleteAllFromEntity:kAD_ENTITY_COUPON managedObjectContext:[self managedObjectContext]];
    }
    
    for (ADBusinessPartnerObject *partner in partners) {
        [self saveBusinessPartner:partner save:NO];
    }
    for (ADCouponObject *coupon in coupons) {
        [self saveCoupon:coupon save:NO];
    }
    [self saveContext];
}

- (void)activateCoupon:(NSString *)couponCode withDate:(NSDate *)date;
{
    ADManagedCoupon *coupon = [self loadCouponFromCoreDataByCode:couponCode];
    coupon.coupon_status = kAD_COUPON_STATUS_ACTIVATED;
    [self saveContext];
}

- (void)redeemCoupon:(NSString *)couponCode withDate:(NSDate *)date;
{
    ADManagedCoupon *coupon = [self loadCouponFromCoreDataByCode:couponCode];
    coupon.coupon_status = kAD_COUPON_STATUS_REDEEMED;
    coupon.redeem_date = date;
    [self saveContext];
}

- (ADManagedPartner *)saveBusinessPartner:(ADBusinessPartnerObject *)partner save:(BOOL)save
{
    ADManagedPartner *managedPartner = [self loadBusinessPartnerFromCoreDataByPartnerId:partner.partner_id];
    if (managedPartner == nil) {
        NSEntityDescription *partnerEntity = [NSEntityDescription entityForName:kAD_ENTITY_PARTNER inManagedObjectContext:[self managedObjectContext]];
        managedPartner = [[ADManagedPartner alloc] initWithEntity:partnerEntity insertIntoManagedObjectContext:[self managedObjectContext]];
    }
    
    [managedPartner updateWithBusinessPartnerObject:partner];
    if (save) {
        [self saveContext];
    }
    return managedPartner;
}

- (ADManagedCoupon *)saveCoupon:(ADCouponObject *)coupon save:(BOOL)save
{
    ADManagedCoupon *managedCoupon = [self loadCouponFromCoreDataByCode:coupon.coupon_code];
    if (managedCoupon == nil) {
        NSEntityDescription *couponEntity = [NSEntityDescription entityForName:kAD_ENTITY_COUPON inManagedObjectContext:[self managedObjectContext]];
        managedCoupon = [[ADManagedCoupon alloc] initWithEntity:couponEntity insertIntoManagedObjectContext:[self managedObjectContext]];
    }
    
    ADManagedPartner *managedPartner = [self loadBusinessPartnerFromCoreDataByPartnerId:coupon.partner_id];
    [managedCoupon updateWithCouponObject:coupon parnter:managedPartner];
    if (save) {
        [self saveContext];
    }
    return managedCoupon;
}


#pragma mark -
#pragma mark - Core Data

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel
{
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Adore" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Adore.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];

    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext
{
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

- (void)saveContext
{
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            //abort();
        }
    }
}

- (void)deleteObjectsFromArray:(NSArray *)array save:(BOOL)save
{
    for (id item in array) {
        [[self managedObjectContext] deleteObject:item];
    }
    if (save) {
        [self saveContext];
    }
}

- (void)removeCoreDataDatabase
{
    [NSManagedObject deleteAllFromEntity:kAD_ENTITY_USER managedObjectContext:[self managedObjectContext]];
    [NSManagedObject deleteAllFromEntity:kAD_ENTITY_COUPON managedObjectContext:[self managedObjectContext]];
    [NSManagedObject deleteAllFromEntity:kAD_ENTITY_PARTNER managedObjectContext:[self managedObjectContext]];
}

@end
