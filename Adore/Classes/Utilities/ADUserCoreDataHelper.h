//
//  ADUserCoreDataHelper.h
//  Adore
//
//  Created by Wang on 2014-12-10.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADManagedUser;
@class ADManagedCoupon;
@class ADManagedPartner;
@class ADConnectionObject;
@class ADPendingRequestObject;
@class ADBusinessPartnerObject;
@class ADCouponObject;
@class ADUserObject;
@class ADNearbyUserObject;



@interface ADUserCoreDataHelper : NSObject

#pragma mark -
#pragma mark - Fetched Controller Creators

- (NSFetchedResultsController *)pendingUsersFetchedResultsControllerWithDelegate:(id<NSFetchedResultsControllerDelegate>)delegate;
- (NSFetchedResultsController *)connectedUsersFetchedResultsControllerWithDelegate:(id<NSFetchedResultsControllerDelegate>)delegate;
- (NSFetchedResultsController *)unreadConnectedUsersFetchedResultsControllerWithDelegate:(id<NSFetchedResultsControllerDelegate>)delegate;
- (NSFetchedResultsController *)couponsFetchedResultsControllerForEmail:(NSString *)email withDelegate:(id<NSFetchedResultsControllerDelegate>)delegate;


#pragma mark - Adore connections related

- (ADManagedUser *)rejectPendingUser:(ADManagedUser *)managedUser;
- (void)deleteUser:(ADManagedUser *)managedUser;


#pragma mark -
#pragma mark - Users Methods

#pragma mark - Creation Methods

- (ADManagedUser *)managedUserFromNearbyUser:(ADNearbyUserObject *)nearbyUser;

#pragma mark - Load Users Methods

- (ADManagedUser *)loadCurrentUserFromCoreData;
- (BOOL)currentUserFaceValidated;
- (ADManagedUser *)loadUserObjectFromCoreDataByEmail:(NSString *)email;

#pragma mark - Save Users Methods

- (void)saveUserConnections:(NSArray *)array removeAllConnections:(BOOL)removeAll updateProfileOnly:(BOOL)updateProfileOnly isRead:(BOOL)isRead;
- (ADManagedUser *)saveUserWithEmail:(NSString *)email withUserObject:(ADUserObject *)userObject withRelationshipType:(ADUserRelationshipType)type updateProfileOnly:(BOOL)updateProfileOnly isRead:(BOOL)isRead save:(BOOL)save;
- (void)markUserAsRead:(ADManagedUser *)user;
- (void)updateFaceValidationResult:(BOOL)validation;


#pragma mark -
#pragma mark - Pending Users Methods

- (NSArray *)loadUserFromCoreDataRelatedToPendingUser:(NSString *)email;
- (void)savePendingRequests:(NSArray *)pendingRequests clearAll:(BOOL)clearAll;
- (void)deletePendingUser:(NSString *)email;


#pragma mark -
#pragma mark - Coupon & Partner Methods

- (void)saveCoupons:(NSArray *)coupons partners:(NSArray *)partners clearAll:(BOOL)clearAll;
- (void)activateCoupon:(NSString *)couponCode withDate:(NSDate *)date;
- (void)redeemCoupon:(NSString *)couponCode withDate:(NSDate *)date;


#pragma mark -
#pragma mark - Core Data

- (void)deleteObjectsFromArray:(NSArray *)array save:(BOOL)save;
- (void)removeCoreDataDatabase;

@end
