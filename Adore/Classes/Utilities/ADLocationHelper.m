//
//  LocationHelper.m
//  Adore
//
//  Created by Wang on 2014-03-10.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADLocationHelper.h"



@interface ADLocationHelper () <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, weak) id<ADLocationHelperDelegate> delegate;

@end



@implementation ADLocationHelper

#pragma mark -
#pragma mark - Deallocation

- (void)dealloc
{
	[_locationManager stopUpdatingLocation];
	_locationManager.delegate = nil;
	_locationManager = nil;
}


#pragma mark -
#pragma mark - Initialization

- (instancetype)initWithDelegate:(id<ADLocationHelperDelegate>)delegate
{
	self = [super init];
	
	if (self) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
//        _locationManager.distanceFilter = 800;
        
        _delegate = delegate;
	}
	
	return self;
}

#pragma mark -
#pragma mark - Helper methods

- (void)startTracking
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        
        if (status == kCLAuthorizationStatusNotDetermined) {
            [self.locationManager requestWhenInUseAuthorization];
            return;
        }
        else if (status == kCLAuthorizationStatusDenied) {
            [self.delegate locationHelperAuthorizationStatusDenied];
            return;
        }
        else if (status == kCLAuthorizationStatusRestricted) {
            [self.delegate locationHelperAuthorizationStatusRestricted];
            return;
        }
    }

    [self.locationManager startUpdatingLocation];
}

- (void)endTracking
{
    [self.locationManager stopUpdatingLocation];
}

- (CLLocation *)currentLocation
{
    return self.locationManager.location;
}


#pragma mark -
#pragma mark - CLLocationManagerDelegate methods

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [self.delegate locationHelperUpdated:[locations lastObject]];
    [self endTracking];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    DDLogError(@"location manager failed. %@", error.localizedDescription);
    [self.delegate locationHelperUpdateError:error];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
            [self startTracking];
            return;
        }
    }
    
    if (status == kCLAuthorizationStatusAuthorized) {
        [self startTracking];
    }
    else {
        if (status == kCLAuthorizationStatusDenied) {
            [self.delegate locationHelperAuthorizationStatusDenied];
        }
        else if (status == kCLAuthorizationStatusRestricted) {
            [self.delegate locationHelperAuthorizationStatusRestricted];
        }
        
        [self endTracking];
    }
}


@end
