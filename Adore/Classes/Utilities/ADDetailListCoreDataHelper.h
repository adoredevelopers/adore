//
//  ADDetailListCoreDataHelper.h
//  Adore
//
//  Created by Wang on 2015-04-12.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADUserDetailItem.h"
#import "ADManagedListItem.h"



@interface ADDetailListCoreDataHelper : NSObject

- (NSDictionary *)loadListFromFile:(NSString *)filename;
- (NSArray *)loadListForType:(ADUserDetailItemType)type;
- (NSString *)stringValueOfArray:(NSArray *)array type:(ADUserDetailItemType)type;
- (ADManagedListItem *)itemWithId:(NSNumber *)listId type:(ADUserDetailItemType)type;
- (NSString *)stringNonLocalizedValueOfId:(NSNumber *)lid type:(ADUserDetailItemType)type;
- (NSString *)stringValueOfId:(NSNumber *)lid type:(ADUserDetailItemType)type;
- (NSString *)currentLanguageCode;


@end
