//
//  ADUserDefaultHelper.h
//  Adore
//
//  Created by Wang on 2014-06-10.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADNearbyUserFilterViewController.h"

@interface ADUserDefaultHelper : NSObject

+ (BOOL)isLoggedIn;
+ (void)setLoggedIn:(BOOL)logInStatus;

+ (NSString *)username;
+ (void)setUsername:(NSString *)username;
+ (NSInteger)minimumAge;
+ (void)setMinimumAge:(NSInteger)minAge;
+ (NSInteger)maximumAge;
+ (void)setMaximumAge:(NSInteger)maxAge;
+ (NSInteger)maximumDistance;
+ (void)setMaximumDistance:(NSInteger)maxDistance;
+ (ADGenderFilter)gender;
+ (void)setGender:(ADGenderFilter)gender;

+ (NSArray *)nearbyUserImpression;
+ (void)setImpressionForNearbyUsers:(NSArray *)userImpression;
+ (NSArray *)nearbyUserLikes;
+ (void)setLikesForNearbyUsers:(NSArray *)userLikes;

+ (NSString *)notificationToken;
+ (void)setNotificationToken:(NSString *)notificationToken;

+ (NSString *)token;
+ (void)setToken:(NSString *)token;

+ (NSString *)userProfileImageUrl;
+ (void)setUserProfileImageUrl:(NSString *)url;

+ (NSString *)lastSocketPayloadTimestamp;
+ (void)setLastSocketPayloadTimestamp:(NSString *)timestamp;

+ (NSString *)lastSocketPayloadTimestampForUser:(NSString *)email;
+ (void)setLastSocketPayloadTimestamp:(NSString *)timestamp forUser:(NSString *)email;

@end
