//
//  LocationHelper.h
//  Adore
//
//  Created by Wang on 2014-03-10.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>



@protocol ADLocationHelperDelegate <NSObject>

@required

- (void)locationHelperUpdated:(CLLocation *)newLocation;
- (void)locationHelperUpdateError:(NSError *)error;
- (void)locationHelperAuthorizationStatusDenied;
- (void)locationHelperAuthorizationStatusRestricted;

@end



@interface ADLocationHelper : NSObject


@property (nonatomic, strong) CLLocation *currentLocation;


- (instancetype)initWithDelegate:(id<ADLocationHelperDelegate>)delegate;

- (void)startTracking;
- (void)endTracking;


@end
