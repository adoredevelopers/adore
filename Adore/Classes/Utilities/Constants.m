//
//  Constants.m
//  Adore
//
//  Created by Wang on 2014-08-10.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

@implementation Constants

+ (ADUserRelationshipType)userRelationshipFromString:(NSString *)string
{
    if ([string isEqualToString:kAD_CONNECTION_STATUS_CONNECTED]) {
        return ADUserRelationshipTypeConnected;
    }
    else if ([string isEqualToString:kAD_CONNECTION_STATUS_REQUESTED]) {
        return ADUserRelationshipTypeRequested;
    }
    else if ([string isEqualToString:kAD_CONNECTION_STATUS_PENDING]) {
        return ADUserRelationshipTypeInvalid;
    }
    else if ([string isEqualToString:kAD_CONNECTION_STATUS_CONNECTED]
             || [string isEqualToString:kAD_CONNECTION_STATUS_APPROVED]) {
        return ADUserRelationshipTypeConnected;
    }
    else if ([string isEqualToString:kAD_CONNECTION_STATUS_REJECTED]) {
        return ADUserRelationshipTypeRejected;
    }
    else if ([string isEqualToString:kAD_CONNECTION_STATUS_BLOCKED_AFTER_CONNECTED]
             || [string isEqualToString:kAD_CONNECTION_STATUS_BLOCKED_AFTER_APPROVED]) {
        return ADUserRelationshipTypeBlocked;
    }
    
    return ADUserRelationshipTypeInvalid;
}

+ (UIColor *)maleTintColor
{
    return [UIColor colorWithRed:35/255.0 green:160/255.0 blue:224/255.0 alpha:1];
}

+ (UIColor *)femaleTintColor
{
    return [UIColor colorWithRed:211/255.0 green:74/255.0 blue:74/255.0 alpha:1];
}

+ (UIColor *)spanishColor
{
    return SPANISH_WHITE;
}

@end
