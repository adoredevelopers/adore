//
//  ADFlurryHelper.h
//  Adore
//
//  Created by Wang on 2015-03-21.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

// Flurry
static NSString * const kAD_FLURRY_APP_KEY = @"YBKTY3R487PZXXH2RNK4";


#pragma mark - Flurry Events

#pragma mark - App Loaded
static NSString * const kAD_FLURRY_EVENT_APP_LOADED = @"App Loaded";

#pragma mark - Image Source
static NSString * const kAD_FLURRY_EVENT_NO_IMAGE_SOURCE = @"No Image Source Found";
static NSString * const kAD_FLURRY_EVENT_IMAGE_PHOTO_LIBRARY = @"Image From Photo Library";
static NSString * const kAD_FLURRY_EVENT_IMAGE_CAMERA = @"Image From Camera";

#pragma mark - Register


#pragma mark - Nearby User
static NSString * const kAD_FLURRY_EVENT_NEARBY_USER_VIEW_USER = @"View Nearby User Profile";
static NSString * const kAD_FLURRY_EVENT_NEARBY_USER_SHOW_FILTER = @"Nearby User Show Filter";
static NSString * const kAD_FLURRY_EVENT_NEARBY_USER_SEND_REQUEST = @"Nearby User Send Request";
static NSString * const kAD_FLURRY_EVENT_NEARBY_USER_VIEW_PROFILE_IMAGE = @"Nearby User View Profile Image";

#pragma mark - Adore
static NSString * const kAD_FLURRY_EVENT_ADORE_VIEW_USER = @"View Adore Profile";
static NSString * const kAD_FLURRY_EVENT_ADORE_BLOCK_USER = @"Block Adore";
static NSString * const kAD_FLURRY_EVENT_ADORE_UNBLOCK_USER = @"Unblock Adore";
static NSString * const kAD_FLURRY_EVENT_ADORE_DELETE_USER = @"Delete Adore";
static NSString * const kAD_FLURRY_EVENT_ADORE_VIEW_COUPON = @"View Coupon";
static NSString * const kAD_FLURRY_EVENT_ADORE_ACTIVATE_COUPON = @"Activate Coupon";
static NSString * const kAD_FLURRY_EVENT_ADORE_REDEEM_COUPON = @"Redeem Coupon";
static NSString * const kAD_FLURRY_EVENT_ADORE_VIEW_PARTNER_MAP = @"Locate Partner On Map";

#pragma mark - Chat


#pragma mark - Settings
static NSString * const kAD_FLURRY_EVENT_SETTINGS_VIEW_PROFILE = @"View My Profile";
static NSString * const kAD_FLURRY_EVENT_SETTINGS_EDIT_PROFILE = @"Edit My Profile";
static NSString * const kAD_FLURRY_EVENT_SETTINGS_SAVE_PROFILE = @"Save My Profile";
static NSString * const kAD_FLURRY_EVENT_SETTINGS_VIEW_PROFILE_IMAGE = @"View Profile Image";




@interface ADFlurryHelper : NSObject


+ (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;
+ (void)logEvent:(NSString *)eventName;

+ (void)logError:(NSString *)error message:(NSString *)message exception:(NSException *)exception;
+ (void)logError:(NSString *)error message:(NSString *)message;


@end
