//
//  ADFlurryHelper.m
//  Adore
//
//  Created by Wang on 2015-03-21.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADFlurryHelper.h"
#import "Flurry.h"



@implementation ADFlurryHelper


+ (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters
{
    [Flurry logEvent:eventName withParameters:parameters];
}

+ (void)logEvent:(NSString *)eventName
{
    [Flurry logEvent:eventName withParameters:nil];
}

+ (void)logError:(NSString *)error message:(NSString *)message exception:(NSException *)exception
{
    [Flurry logError:error message:message exception:exception];
}

+ (void)logError:(NSString *)error message:(NSString *)message
{
    [Flurry logError:error message:message exception:nil];
}


@end
