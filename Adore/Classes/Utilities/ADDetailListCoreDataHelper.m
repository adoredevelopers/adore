//
//  ADDetailListCoreDataHelper.m
//  Adore
//
//  Created by Wang on 2015-04-12.
//  Copyright (c) 2015 AdoreMobile. All rights reserved.
//

#import "ADDetailListCoreDataHelper.h"
#import "ADManagedListItem.h"
#import "NSString+JSONParser.h"
#import "ADUserDetailItem.h"
#import "NSDictionary+NonNilObject.h"

// Message Identifiers

static NSString * const kAD_CORE_DATA_DETAIL_LIST_ENTITY_BODY_SHAPE = @"Body_shape";

static NSString * const kAD_CORE_DATA_DETAIL_LIST_ENTITY_CHINESE_ZODIAC = @"Chinese_zodiac";

static NSString * const kAD_CORE_DATA_DETAIL_LIST_ENTITY_DRINKING = @"Drinking";

static NSString * const kAD_CORE_DATA_DETAIL_LIST_ENTITY_EDUCATION_LEVELS = @"Education_levels";

static NSString * const kAD_CORE_DATA_DETAIL_LIST_ENTITY_HEIGHT = @"Height";

static NSString * const kAD_CORE_DATA_DETAIL_LIST_ENTITY_MARTIAL_STATUS = @"Martial_status";

static NSString * const kAD_CORE_DATA_DETAIL_LIST_ENTITY_INCOME_LEVELS = @"Income_levels";

static NSString * const kAD_CORE_DATA_DETAIL_LIST_ENTITY_INTERESTS = @"Interests";

static NSString * const kAD_CORE_DATA_DETAIL_LIST_ENTITY_LANGUAGES = @"Languages";

static NSString * const kAD_CORE_DATA_DETAIL_LIST_ENTITY_OCCUPATIONS = @"Occupations";

static NSString * const kAD_CORE_DATA_DETAIL_LIST_ENTITY_PERSONALITY = @"Personalities";

static NSString * const kAD_CORE_DATA_DETAIL_LIST_ENTITY_RELIGION = @"Religions";

static NSString * const kAD_CORE_DATA_DETAIL_LIST_ENTITY_SMOKING = @"Smoking";

static NSString * const kAD_CORE_DATA_DETAIL_LIST_ENTITY_WEIGHT = @"Weight";

static NSString * const kAD_CORE_DATA_DETAIL_LIST_ENTITY_ZODIAC = @"Zodiac";

static NSString * const kAD_CORE_DATA_DETAIL_LIST_ENTITY_RACE = @"Race";

static NSString * const kAD_CORE_DATA_DETAIL_LIST_ENTITY_RESIDENTIAL_STATUS = @"Residential_status";



@interface ADDetailListCoreDataHelper ()

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) NSMutableDictionary *cache;


@end



@implementation ADDetailListCoreDataHelper


#pragma mark -
#pragma mark - Initializer

- (id)init
{
    self = [super init];
    if (self) {
        _cache = @{}.mutableCopy;
        [self initilizeAllLists];
    }
    return self;
}


#pragma mark -
#pragma mark - Helper

- (void)initilizeAllLists
{
    [self loadListForUserDetailItemType:ADUserDetailItemTypeBodyShape];
    [self loadListForUserDetailItemType:ADUserDetailItemTypeC_Zodiac];
    [self loadListForUserDetailItemType:ADUserDetailItemTypeEducation];
    [self loadListForUserDetailItemType:ADUserDetailItemTypeDrinking];
    [self loadListForUserDetailItemType:ADUserDetailItemTypeHeight];
    [self loadListForUserDetailItemType:ADUserDetailItemTypeMartialStatus];
    [self loadListForUserDetailItemType:ADUserDetailItemTypeIncome];
    [self loadListForUserDetailItemType:ADUserDetailItemTypeInterest];
    [self loadListForUserDetailItemType:ADUserDetailItemTypeLanguage];
    [self loadListForUserDetailItemType:ADUserDetailItemTypeOccupation];
    [self loadListForUserDetailItemType:ADUserDetailItemTypePersonality];
    [self loadListForUserDetailItemType:ADUserDetailItemTypeSmoking];
    [self loadListForUserDetailItemType:ADUserDetailItemTypeReligion];
    [self loadListForUserDetailItemType:ADUserDetailItemTypeWeight];
    [self loadListForUserDetailItemType:ADUserDetailItemTypeZodiac];
    [self loadListForUserDetailItemType:ADUserDetailItemTypeRace];
    [self loadListForUserDetailItemType:ADUserDetailItemTypeResidentialStatus];
}

- (NSDictionary *)loadListFromFile:(NSString *)filename
{
    NSDictionary *dict = self.cache[filename];
    if (dict == nil) {
        NSDictionary *tempDict = [self processedStringArrayOfFile:filename];
        if (tempDict == nil) {
            DDLogError(@"CANNOT parse json file: %@", filename);
            NSAssert(NO, @"CANNOT parse json file");
        }
        dict = tempDict;
        self.cache[filename] = tempDict;
    }
    
    return dict;
}

- (void)loadListForUserDetailItemType:(ADUserDetailItemType)type
{
    NSString *entityName = [self entityForUserDetailItemType:type];
    NSDictionary *dict = [self processedStringArrayOfFile:entityName];
    if (dict == nil) {
        DDLogError(@"CANNOT parse json file: %@", entityName);
        NSAssert(NO, @"CANNOT parse json file");
    }
    [self saveDictionary:dict type:type];
}

- (NSString *)stringValueOfArray:(NSArray *)array type:(ADUserDetailItemType)type
{
    NSMutableArray *tempArray = @[].mutableCopy;
    for (NSNumber *lid in array) {
        NSString *value = [self stringValueOfId:lid type:type];
        if (value) {
            [tempArray addObject:value];
        }
    }
    return [tempArray componentsJoinedByString:@", "];
}

- (NSString *)stringNonLocalizedValueOfId:(NSNumber *)lid type:(ADUserDetailItemType)type
{
    ADManagedListItem *item = [self itemWithId:lid type:type];
    if (item == nil) {
        return nil;
    }
    
    return item.en_string;
}

- (NSString *)stringValueOfId:(NSNumber *)lid type:(ADUserDetailItemType)type
{
    ADManagedListItem *item = [self itemWithId:lid type:type];
    if (item == nil) {
        return nil;
    }
    NSString *languageCode = [self currentLanguageCode];
    
    if ([languageCode isEqualToString:@"zh"]) {
        return item.cn_string;
    }
    if ([languageCode isEqualToString:@"cn"]) {
        return item.cn_string;
    }
    else {
        return item.en_string;
    }
}

- (NSString *)entityForUserDetailItemType:(ADUserDetailItemType)type
{
    switch (type) {
        case ADUserDetailItemTypeEducation:
            return kAD_CORE_DATA_DETAIL_LIST_ENTITY_EDUCATION_LEVELS;
            
        case ADUserDetailItemTypeBodyShape:
            return kAD_CORE_DATA_DETAIL_LIST_ENTITY_BODY_SHAPE;
            
        case ADUserDetailItemTypeZodiac:
            return kAD_CORE_DATA_DETAIL_LIST_ENTITY_ZODIAC;
            
        case ADUserDetailItemTypeC_Zodiac:
            return kAD_CORE_DATA_DETAIL_LIST_ENTITY_CHINESE_ZODIAC;
            
        case ADUserDetailItemTypeReligion:
            return kAD_CORE_DATA_DETAIL_LIST_ENTITY_RELIGION;
            
        case ADUserDetailItemTypeDrinking:
            return kAD_CORE_DATA_DETAIL_LIST_ENTITY_DRINKING;
            
        case ADUserDetailItemTypeSmoking:
            return kAD_CORE_DATA_DETAIL_LIST_ENTITY_SMOKING;
            
        case ADUserDetailItemTypeWeight:
            return kAD_CORE_DATA_DETAIL_LIST_ENTITY_WEIGHT;
            
        case ADUserDetailItemTypeHeight:
            return kAD_CORE_DATA_DETAIL_LIST_ENTITY_HEIGHT;
            
        case ADUserDetailItemTypeMartialStatus:
            return kAD_CORE_DATA_DETAIL_LIST_ENTITY_MARTIAL_STATUS;
            
        case ADUserDetailItemTypeLanguage:
            return kAD_CORE_DATA_DETAIL_LIST_ENTITY_LANGUAGES;
            
        case ADUserDetailItemTypeInterest:
            return kAD_CORE_DATA_DETAIL_LIST_ENTITY_INTERESTS;
            
        case ADUserDetailItemTypePersonality:
            return kAD_CORE_DATA_DETAIL_LIST_ENTITY_PERSONALITY;
            
        case ADUserDetailItemTypeIncome:
            return kAD_CORE_DATA_DETAIL_LIST_ENTITY_INCOME_LEVELS;
            
        case ADUserDetailItemTypeOccupation:
            return kAD_CORE_DATA_DETAIL_LIST_ENTITY_OCCUPATIONS;
            
        case ADUserDetailItemTypeRace:
            return kAD_CORE_DATA_DETAIL_LIST_ENTITY_RACE;
            
        case ADUserDetailItemTypeResidentialStatus:
            return kAD_CORE_DATA_DETAIL_LIST_ENTITY_RESIDENTIAL_STATUS;
            
        default:
            return nil;
    }
}

- (NSString *)currentLanguageCode
{
    NSString *language = [[[NSBundle mainBundle] preferredLocalizations] firstObject];
    return language;
}


#pragma mark -
#pragma mark - JSON File

- (NSString *)stringContentOfFile:(NSString *)filename
{
    NSStringEncoding encoding;
    NSString* path = [[NSBundle mainBundle] pathForResource:filename ofType:@"js"];
    if(path)
    {
        NSString *content = [NSString stringWithContentsOfFile:path usedEncoding:&encoding error:NULL];
        return content;
    }
    return nil;
}

- (NSDictionary *)processedStringArrayOfFile:(NSString *)filename
{
    NSString *fileString = [self stringContentOfFile:filename];
    if (fileString == nil) {
        DDLogError(@"CANNOT find json file: %@", filename);
    }
    NSDictionary *jsonDict = [fileString dictionaryObject];

    return jsonDict;
}


#pragma mark -
#pragma mark - Load

- (NSArray *)loadListForType:(ADUserDetailItemType)type
{
    NSString *entity = [self entityForUserDetailItemType:type];
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entity];
    NSSortDescriptor *sortDesc = [[NSSortDescriptor alloc] initWithKey:@"lid" ascending:YES];
    fetchRequest.sortDescriptors = @[sortDesc];
    NSArray *list = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    return list;
}

- (ADManagedListItem *)itemWithId:(NSNumber *)listId type:(ADUserDetailItemType)type
{
    NSString *entity = [self entityForUserDetailItemType:type];
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entity];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"lid = %@", listId];
    NSArray *list = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    return [list firstObject];
}


#pragma mark -
#pragma mark - Save

- (void)saveDictionary:(NSDictionary *)list type:(ADUserDetailItemType)type
{
    NSString *entity = [self entityForUserDetailItemType:type];
    [self deleteListForType:type];
    
    for (NSString *key in list) {
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entity inManagedObjectContext:[self managedObjectContext]];
        ADManagedListItem *item = [[ADManagedListItem alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:[self managedObjectContext]];
        NSDictionary *dict = list[key];
        item.lid = @([key integerValue]);
        item.cn_string = dict[@"zh"];
        item.en_string = dict[@"en"];
    }
    
    [self saveContext];
}


#pragma mark -
#pragma mark - Delete

- (void)deleteListForType:(ADUserDetailItemType)type
{
    NSArray *list = [self loadListForType:type];
    [self deleteObjectsFromArray:list save:YES];
}

- (void)deleteObjectsFromArray:(NSArray *)array save:(BOOL)save
{
    for (id item in array) {
        [[self managedObjectContext] deleteObject:item];
    }
    if (save) {
        [self saveContext];
    }
}


#pragma mark -
#pragma mark - Core Data

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel
{
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"UserDetailList" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"UserDetailList.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];

    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext
{
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

- (void)saveContext
{
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (void)removeCoreDataDatabase
{
    NSPersistentStoreCoordinator *storeCoordinator = [self persistentStoreCoordinator];
    
    for(NSPersistentStore *store in [storeCoordinator persistentStores]) {
        [storeCoordinator removePersistentStore:store error:nil];
        [[NSFileManager defaultManager] removeItemAtPath:store.URL.path error:nil];
    }
    _managedObjectModel = nil;
    _persistentStoreCoordinator = nil;
    _managedObjectContext = nil;
}

@end
