//
//  ADUserDefaultHelper.m
//  Adore
//
//  Created by Wang on 2014-06-10.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import "ADUserDefaultHelper.h"

NSString * const AD_USER_LOGGED_IN = @"UserLoggedIn";

NSString * const AD_USERNAME = @"Username";

NSString * const AD_MINAGE = @"MinimumAge";
NSString * const AD_MAXAGE = @"MaximumAge";
NSString * const AD_MAXDISTANCE = @"MaximumDistance";
NSString * const AD_GENDER = @"Gender";
NSString * const AD_NEARBYUSER_IMPRESSION = @"NearbyUserImpression";
NSString * const AD_NEARBYUSER_LIKES = @"NearbyUserLikes";

@implementation ADUserDefaultHelper

+ (void)storeObject:(id)value ForKey:(NSString *)key
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	[prefs setObject:value forKey:key];
}

+ (id)retrieveValueForKey:(NSString *)key
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	return [prefs objectForKey:key];
}

+ (BOOL)isLoggedIn
{
    return [[self retrieveValueForKey:AD_USER_LOGGED_IN] boolValue];
}

+ (void)setLoggedIn:(BOOL)logInStatus
{
    [self storeObject:[NSNumber numberWithBool:logInStatus] ForKey:AD_USER_LOGGED_IN];
}

+ (NSString *)username
{
	return [self retrieveValueForKey:AD_USERNAME]; // username is an email address
}

+ (void)setUsername:(NSString *)username
{
	[self storeObject:username ForKey:AD_USERNAME]; // username is an email address
}

+ (NSInteger)minimumAge
{
	if ([self retrieveValueForKey:AD_MINAGE] == nil) {
		return 18;
	}
	return [[self retrieveValueForKey:AD_MINAGE] integerValue];
}

+ (void)setMinimumAge:(NSInteger)minAge
{
	[self storeObject:@(minAge) ForKey:AD_MINAGE];
}

+ (NSInteger)maximumAge
{
	if ([self retrieveValueForKey:AD_MAXAGE] == nil) {
		return 40;
	}
	return [[self retrieveValueForKey:AD_MAXAGE] integerValue];
}

+ (void)setMaximumAge:(NSInteger)maxAge
{
	[self storeObject:@(maxAge) ForKey:AD_MAXAGE];
}

+ (NSInteger)maximumDistance
{
	if ([self retrieveValueForKey:AD_MAXDISTANCE] == nil) {
		return 100;
	}
	return [[self retrieveValueForKey:AD_MAXDISTANCE] integerValue];
}

+ (void)setMaximumDistance:(NSInteger)maxDistance
{
	[self storeObject:@(maxDistance) ForKey:AD_MAXDISTANCE];
}

+ (ADGenderFilter)gender
{
	if ([self retrieveValueForKey:AD_GENDER] == nil) {
		return ADGenderFilterAll;
	}
	return [[self retrieveValueForKey:AD_GENDER] integerValue];
}

+ (void)setGender:(ADGenderFilter)gender
{
	[self storeObject:@(gender) ForKey:AD_GENDER];
}

+ (NSArray *)nearbyUserImpression
{
    NSArray *userImpression = [self retrieveValueForKey:AD_NEARBYUSER_IMPRESSION];
    if (userImpression == nil) {
        return [NSArray new];
    } else {
        return userImpression;
    }
}

+ (void)setImpressionForNearbyUsers:(NSArray *)userImpression
{
    [self storeObject:userImpression ForKey:AD_NEARBYUSER_IMPRESSION];
}

+ (NSArray *)nearbyUserLikes
{
    NSArray *userLikes = [self retrieveValueForKey:AD_NEARBYUSER_LIKES];
    if (userLikes == nil) {
        return [NSArray new];
    } else {
        return userLikes;
    }
}

+ (void)setLikesForNearbyUsers:(NSArray *)userLikes
{
    [self storeObject:userLikes ForKey:AD_NEARBYUSER_LIKES];
}

+ (NSString *)notificationToken
{
    NSString *notificationToken = [self retrieveValueForKey:@"NotificationToken"];
    if (notificationToken == nil) {
        notificationToken = @"";
    }
    return notificationToken;
}

+ (void)setNotificationToken:(NSString *)notificationToken
{
    if (notificationToken == nil) {
        notificationToken = @"";
    }
    [self storeObject:notificationToken ForKey:@"NotificationToken"];
}

+ (NSString *)token
{
	return [self retrieveValueForKey:@"Token"];
}

+ (void)setToken:(NSString *)token
{
	[self storeObject:token	ForKey:@"Token"];
}

+ (NSString *)userProfileImageUrl
{
    return [self retrieveValueForKey:@"UserProfileImageUrl"];
}

+ (void)setUserProfileImageUrl:(NSString *)url
{
    [self storeObject:url ForKey:@"UserProfileImageUrl"];
}

+ (NSString *)lastSocketPayloadTimestamp
{
    NSString *userEmail = [self username];
    return [self lastSocketPayloadTimestampForUser:userEmail];
}

+ (void)setLastSocketPayloadTimestamp:(NSString *)timestamp
{
    NSString *userEmail = [self username];
    [self setLastSocketPayloadTimestamp:timestamp forUser:userEmail];
}

+ (NSString *)lastSocketPayloadTimestampForUser:(NSString *)email
{
    NSString *lastSocketPayloadTimestampKey = [NSString stringWithFormat:@"%@_%@", @"LastSocketPayloadTimestamp", email];
    return [self retrieveValueForKey:lastSocketPayloadTimestampKey];
}

+ (void)setLastSocketPayloadTimestamp:(NSString *)timestamp forUser:(NSString *)email
{
    long long current_timestamp_llong_value = [[self lastSocketPayloadTimestampForUser:email] longLongValue];
    long long incoming_timestamp_llong_value = [timestamp longLongValue];
    if (incoming_timestamp_llong_value > current_timestamp_llong_value) {
        NSString *lastSocketPayloadTimestampKey = [NSString stringWithFormat:@"%@_%@", @"LastSocketPayloadTimestamp", email];
        [self storeObject:timestamp ForKey:lastSocketPayloadTimestampKey];
    }
}

@end
