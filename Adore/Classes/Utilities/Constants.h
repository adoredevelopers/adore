//
//  Constants.h
//  Adore
//
//  Created by Wang on 2014-08-10.
//  Copyright (c) 2014 AdoreMobile. All rights reserved.
//

#import <Foundation/Foundation.h>



// Marcos
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
// iOS Version Macros
#define IS_OS_6_OR_EARLIER  ([[[UIDevice currentDevice] systemVersion] floatValue] <= 6.0)
#define IS_OS_6_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)
#define IS_OS_7_OR_EARLIER  ([[[UIDevice currentDevice] systemVersion] floatValue] <= 7.0)
#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define IS_OS_8_OR_EARLIER  ([[[UIDevice currentDevice] systemVersion] floatValue] <= 8.0)
#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define SPANISH_WHITE [UIColor colorWithRed:0.996 green:0.992 blue:0.941 alpha:1] /*#fefdf0*/



// Server Hosts
#ifdef DEV
static NSString * const kAD_SERVER_HOST = @"http://dev.adoreapp.love";
static NSString * const kAD_SERVER_PORT = @"15522";
#else
static NSString * const kAD_SERVER_HOST = @"http://api.adoreapp.love";
static NSString * const kAD_SERVER_PORT = @"80";
#endif


// For CocoaLumberJack
static const DDLogLevel ddLogLevel = DDLogLevelVerbose;


// Core Data Entities
static NSString * const kAD_ENTITY_USER = @"User";

static NSString * const kAD_ENTITY_PARTNER = @"Partner";

static NSString * const kAD_ENTITY_COUPON = @"Coupon";

static NSString * const kAD_ENTITY_CHAT_MESSAGE = @"ChatMessage";

static NSString * const kAD_ENTITY_CONVERSATION = @"Conversation";


// Notifications
static NSString * const kAD_NOTIFICATION_USER_LOGOUT = @"User Logout";

static NSString * const kAD_NOTIFICATION_NEW_MESSAGE_RECEIVED = @"New Message Received";

static NSString * const kAD_NOTIFICATION_MESSAGE_SENT = @"New Message Sent";

static NSString * const kAD_NOTIFICATION_LOCATION_UPDATED = @"User Location Updated";

static NSString * const kAD_NOTIFICATION_SOCKET_CONNECTED = @"Socket Connected";

static NSString * const kAD_NOTIFICATION_SOCKET_DISCONNECTED = @"Socket Disconnected";

static NSString * const kAD_NOTIFICATION_SOCKET_CONNECTING = @"Socket Connecting";

static NSString * const kAD_NOTIFICATION_USER_PROFILE_IMAGE_UPDATED = @"User Profile Image Updated";

static NSString * const kAD_NOTIFICATION_SOCKET_CONNECTION_FROM_ANOTHER_DEVICE = @"Socket Connection From Another Device";


// Timeouts
static NSInteger const kAD_UPDATE_LAST_ACTIVITY_TIME_INTERVAL = 900;
static NSInteger const kAD_SOCKETIO_CHECK_RECONNECT_INTERVAL = 15;
static NSInteger const kAD_CHAT_TIMEOUT_INTERVAL = 10;


// Chat View Constants
static NSUInteger const kAD_CHAT_AVATAR_IMAGE_SIZE = 38;


// Connection status

/* For example, A sends B a request. Under A, B is listed as Requested. Under B, A is listed as Pending.
 * If B accepts the request, under A, B is Connected; under B, A is Approved.
 * If B rejects the request, under A, B is still Requested, under B, A is rejected.
 * Users can only block each other when the connection is created.
 * If A blocks B, under A, B is BlockedAfterConnected, under B, A still shows as Approved.
 * If B blocks A, under A, B is Connected, under B, A is BlockedAfterApproved.
 */
static NSString * const kAD_CONNECTION_STATUS_REQUESTED = @"Requested";

static NSString * const kAD_CONNECTION_STATUS_PENDING = @"Pending";

static NSString * const kAD_CONNECTION_STATUS_CONNECTED = @"Connected";

static NSString * const kAD_CONNECTION_STATUS_APPROVED = @"Approved";

static NSString * const kAD_CONNECTION_STATUS_REJECTED = @"Rejected";

static NSString * const kAD_CONNECTION_STATUS_BLOCKED_AFTER_APPROVED = @"BlockedAfterApproved";

static NSString * const kAD_CONNECTION_STATUS_BLOCKED_AFTER_CONNECTED = @"BlockedAfterConnected";


// Other
static NSString * const kAD_HTTP_MULTIPART_BOUNDARY = @"---------------------1234567890987654321";

static NSString * const kAD_DATE_FORMATTER_DEFAULT_FORMAT = @"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'";

static NSString * const KAD_IMAGE_URL_PREFIX = @"ADORE_NEW_IMAGE_";


typedef NS_ENUM(NSInteger, ADUserRelationshipType) {
    ADUserRelationshipTypeInvalid = 0,
    ADUserRelationshipTypeSelf = 1,
    ADUserRelationshipTypeRequested = 2,
    ADUserRelationshipTypeConnected = 4,
    ADUserRelationshipTypeRejected = 6,
    ADUserRelationshipTypeBlocked = 8
};


@interface Constants : NSObject

+ (ADUserRelationshipType)userRelationshipFromString:(NSString *)string;

+ (UIColor *)maleTintColor;
+ (UIColor *)femaleTintColor;
+ (UIColor *)spanishColor;

@end
