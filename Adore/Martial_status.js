{
    "-1": {
        "en": "undisclosed",
        "zh": "未说明"
    },
    "1": {
        "en": "never married",
        "zh": "未婚"
    },
    "2": {
        "en": "divorced",
        "zh": "离异"
    },
    "3": {
        "en": "widowed",
        "zh": "丧偶"
    },
    "4": {
        "en": "separated",
        "zh": "分居/待离"
    }
}
