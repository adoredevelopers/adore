//
//  AppDelegate.m
//  Adore
//
//  Created by Chao Lu on 2013-09-08.
//  Copyright (c) 2013 AdoreMobile. All rights reserved.
//

#import "AppDelegate.h"
#import "ADUserManager.h"
#import "ADChatManager.h"
#import "ADUserDefaultHelper.h"
#import "Flurry.h"
#import "ADFlurryHelper.h"
#import "MMDrawerVisualState.h"
#import "ADAdoreListViewController.h"
#import "ADRecentChatViewController.h"
#import "NSDictionary+NonNilObject.h"
#import "UIImage+ImageEffects.h"
#import "Reachability.h"



@interface AppDelegate ()


@end



@implementation AppDelegate

#pragma mark -
#pragma mark - Lazy initializers

- (ADUserManager *)userManager
{
    if (_userManager == nil) {
        _userManager = [ADUserManager sharedInstance];
    }
    return _userManager;
}

- (ADChatManager *)chatManager
{
    if (_chatManager == nil) {
        _chatManager = [ADChatManager sharedInstance];
    }
    return _chatManager;
}

- (ADBackgroundJobManager *)backgroundJobManager
{
    if (_backgroundJobManager == nil) {
        _backgroundJobManager = [ADBackgroundJobManager sharedInstance];
    }
    return _backgroundJobManager;
}


#pragma mark -
#pragma mark - UIApplicationDelegate methods

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self setupDebugEnvironment];
    [self initializeApplication];
    
    [self setupTheme];

    self.leftDrawerViewController = [[ADLeftDrawerViewController alloc] init];
    ADNavigationController *nc = [[ADNavigationController alloc] initWithRootViewController:self.leftDrawerViewController];
    
    BOOL showLeftDrawerInitially = NO;
    self.centerController = nil;

    MMOpenDrawerGestureMode openDrawerGesutreMode = MMOpenDrawerGestureModeNone;
    MMCloseDrawerGestureMode closeDrawerGestureMode = MMCloseDrawerGestureModeNone;

    // Not logged in
    if ([self.userManager isLoggedIn] == NO)
    {
        self.leftDrawerViewController.selectionMode = ADLeftDrawerSelectionModeNone;
        self.centerDrawerType = ADCenterDrawerViewControllerTypeLogin;
        self.centerController = [self loginNavigationController];
    }
    else {
        // Kick out user if the token has expired, invalidated by another device logon
        [self.userManager checkSessionTicket];
        
        // Handle invocation from remote notification
        NSDictionary *remoteNotificationPayload = [launchOptions dictionaryObjectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (remoteNotificationPayload) {
            NSString *socketMessageId = [remoteNotificationPayload stringObjectForKey:@"socket_message_id"];
            if ([socketMessageId isEqualToString:@"private message"]) {
                self.leftDrawerViewController.selectionMode = ADLeftDrawerSelectionModeChat;
                self.centerDrawerType = ADCenterDrawerViewControllerTypeChat;
                NSString *senderId = [remoteNotificationPayload stringObjectForKey:@"sender_id"];
                self.centerController = [self chatNavigationControllerWithEmail:senderId];
            }
            else if ([socketMessageId isEqualToString:@"connection request"]) {
                self.leftDrawerViewController.selectionMode = ADLeftDrawerSelectionModeProfile;
                self.centerDrawerType = ADCenterDrawerViewControllerTypeViewProfile;
//                NSString *requesterUserId = [[remoteNotificationPayload objectForKey:@"data"] valueForKey:@"requester_user_id"];
                self.centerController = [self viewProfileController];
                showLeftDrawerInitially = YES;
            }
            else if ([socketMessageId isEqualToString:@"connection approval"]) {
                self.leftDrawerViewController.selectionMode = ADLeftDrawerSelectionModeAdore;
                self.centerDrawerType = ADCenterDrawerViewControllerTypeAdore;
                NSString *approverUserId = [[remoteNotificationPayload dictionaryObjectForKey:@"data"] stringObjectForKey:@"approver_user_id"];
                self.centerController = [self adoreNavigationControllerWithEmail:approverUserId];
            }
            else {
                self.leftDrawerViewController.selectionMode = ADLeftDrawerSelectionModeNearbyUsers;
                self.centerDrawerType = ADCenterDrawerViewControllerTypeNearbyUsers;
                self.centerController = [self nearbyUserNavigationController];
            }
            openDrawerGesutreMode = MMOpenDrawerGestureModeBezelPanningCenterView | MMOpenDrawerGestureModePanningNavigationBar;
            closeDrawerGestureMode = MMCloseDrawerGestureModeAll;
        }
        else {
            self.leftDrawerViewController.selectionMode = ADLeftDrawerSelectionModeNearbyUsers;
            self.centerDrawerType = ADCenterDrawerViewControllerTypeNearbyUsers;
            self.centerController = [self nearbyUserNavigationController];
            openDrawerGesutreMode = MMOpenDrawerGestureModeBezelPanningCenterView | MMOpenDrawerGestureModePanningNavigationBar;
            closeDrawerGestureMode = MMCloseDrawerGestureModeAll;
        }
    }
    self.drawerController = [[MMDrawerController alloc] initWithCenterViewController:self.centerController leftDrawerViewController:nc];
    self.drawerController.maximumLeftDrawerWidth = 280;
    self.drawerController.openDrawerGestureModeMask = openDrawerGesutreMode;
    self.drawerController.closeDrawerGestureModeMask = closeDrawerGestureMode;
    self.drawerController.shouldStretchDrawer = NO;
    [self.drawerController setDrawerVisualStateBlock:[MMDrawerVisualState slideAndScaleVisualStateBlock]];

	self.window.rootViewController = self.drawerController;
    if (showLeftDrawerInitially) {
        [self showLeftDrawer];
    }
    
    // output sqlite location
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSLog(@"SQLite File Location: %@", [paths objectAtIndex:0]);
    
	return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // app coming back from the inactive state (resign active)
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    self.blurView = [[UIImageView alloc] initWithImage:[self makeBlurredScreenshot:self.window.rootViewController.view.frame]];
    self.blurView.frame = self.window.rootViewController.view.frame;
    [self.window.rootViewController.view addSubview:self.blurView];
    if (self.userManager.isLoggedIn) {
        [self.chatManager disconnectFromChatService];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    if ([ADUserDefaultHelper isLoggedIn]) {
        // background tasks can still be ran, do not close SocketIO connection
        [self.chatManager sendSyncStats];
        [self.backgroundJobManager clearBackgroundJobManager];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    if (self.blurView) {
        [self.blurView removeFromSuperview];
        self.blurView = nil;
    }
    
    [self.userManager checkSessionTicket];
    
    [self.userManager autoUpdateConnections];
    [self.chatManager reconnectToChatService];
    [self.backgroundJobManager initializeBackgroundJobManager];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [self.chatManager disconnectFromChatService];
}


#pragma mark -

- (UIImage *)makeBlurredScreenshot:(CGRect)frame {
    
    UIGraphicsBeginImageContextWithOptions(frame.size, NO, 0.0);
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(c, CGAffineTransformMakeTranslation(-frame.origin.x, -frame.origin.y));
    [[[[UIApplication sharedApplication] keyWindow] layer] renderInContext:c];
    UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [screenshot applyBlurWithRadius:5
                                 tintColor:[UIColor colorWithWhite:1.0 alpha:0.2]
                     saturationDeltaFactor:1.2
                                 maskImage:nil];
}


#pragma mark -
#pragma mark - View

- (ADNavigationController *)loginNavigationController
{
    return [[UIStoryboard storyboardWithName:@"ADLoginStoryboard" bundle:nil] instantiateInitialViewController];
}

- (ADNavigationController *)nearbyUserNavigationController
{
    if (_nearbyNavigationController == nil) {
        _nearbyNavigationController = [[UIStoryboard storyboardWithName:@"ADNearbyUserStoryboard" bundle:nil] instantiateInitialViewController];
    }
    return _nearbyNavigationController;
}

- (ADNavigationController *)nearbyEventNavigationController
{
    ADNavigationController *nc = [[UIStoryboard storyboardWithName:@"ADNearbyEventStoryboard" bundle:nil] instantiateInitialViewController];
    return nc;
}

- (ADNavigationController *)adoreNavigationController
{
    return [[UIStoryboard storyboardWithName:@"ADAdoreStoryboard" bundle:nil] instantiateInitialViewController];
}

- (ADNavigationController *)adoreNavigationControllerWithEmail:(NSString *)email
{
    ADNavigationController *adoreNC = [[UIStoryboard storyboardWithName:@"ADAdoreStoryboard" bundle:nil] instantiateInitialViewController];
    ADAdoreListViewController *adoreListVC = (ADAdoreListViewController *)adoreNC.topViewController;
    [adoreListVC pushUserConnectionViewControllerWithUserEmail:email];
    return adoreNC;
}

- (ADNavigationController *)pendingUserNavigationController
{
    return [[UIStoryboard storyboardWithName:@"ADAdoreStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"PendingRequestController"];
}

- (ADNavigationController *)chatNavigationController
{
    return [[UIStoryboard storyboardWithName:@"ADChatStoryboard" bundle:nil] instantiateInitialViewController];
}

- (ADNavigationController *)chatNavigationControllerWithEmail:(NSString *)email
{
    ADNavigationController *chatNC = [[UIStoryboard storyboardWithName:@"ADChatStoryboard" bundle:nil] instantiateInitialViewController];
    ADRecentChatViewController *recentChatVC = (ADRecentChatViewController *)chatNC.topViewController;
    [recentChatVC pushChatViewControllerWithUserEmail:email animated:NO];
    return chatNC;
}

- (ADNavigationController *)settingNavigationController
{
    if (_settingNavigationController == nil) {
        _settingNavigationController = [[UIStoryboard storyboardWithName:@"ADSettingStoryboard" bundle:nil] instantiateInitialViewController];
    }
    return _settingNavigationController;
}

- (ADNavigationController *)viewProfileController
{
    if (_viewProfileController == nil) {
        _viewProfileController = [[UIStoryboard storyboardWithName:@"ADSettingStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"ViewProfileController"];
    }
    return _viewProfileController;
}

- (void)showLeftDrawer
{
    [self.leftDrawerViewController reloadProfileCell];
    [self.drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

- (void)showLoginView
{
    [self disableGesturesForDrawer];
    [self.drawerController closeDrawerAnimated:NO completion:nil];
    
    if (self.centerDrawerType != ADCenterDrawerViewControllerTypeLogin) {
        self.centerDrawerType = ADCenterDrawerViewControllerTypeLogin;
        [self.drawerController setCenterViewController:[self loginNavigationController]];
    }
    self.leftDrawerViewController.selectionMode = ADLeftDrawerSelectionModeNone;
}

- (void)showForcedOutLoginView
{
    [self showLoginView];
    
    ADViewController *loginVC = [[[self loginNavigationController] viewControllers] firstObject];
    
    if (_alertView == nil || !_alertView.isVisible) {
        _alertView = [loginVC showMessageWithTitle:@"Device Conflict" message:@"Please login again"];
    }
}

- (void)showPostLoginView
{
    [self enableGesturesForDrawer];
    [self.leftDrawerViewController reloadProfileCell];
    [self.leftDrawerViewController refreshNSFetchedResultsControllers];
    [self showNearbyUsersView];
}

- (void)showNearbyUsersView
{
    [self.drawerController closeDrawerAnimated:YES completion:nil];

    if (self.centerDrawerType != ADCenterDrawerViewControllerTypeNearbyUsers) {
        self.centerDrawerType = ADCenterDrawerViewControllerTypeNearbyUsers;
        [self.drawerController setCenterViewController:[self nearbyUserNavigationController]];
    }
    self.leftDrawerViewController.selectionMode = ADLeftDrawerSelectionModeNearbyUsers;
}

//- (void)showNearbyEventsView
//{
//    [self.drawerController closeDrawerAnimated:YES completion:nil];
//    
//    if (self.centerDrawerType != ADCenterDrawerViewControllerTypeNearbyEvents) {
//        self.centerDrawerType = ADCenterDrawerViewControllerTypeNearbyEvents;
//        [self.drawerController setCenterViewController:[self nearbyEventNavigationController]];
//    }
//    self.leftDrawerViewController.selectionMode = ADLeftDrawerSelectionModeNearbyEvents;
//}

- (void)showAdoreView
{
    [self.drawerController closeDrawerAnimated:YES completion:nil];
    
    if (self.centerDrawerType != ADCenterDrawerViewControllerTypeAdore) {
        self.centerDrawerType = ADCenterDrawerViewControllerTypeAdore;
        [self.drawerController setCenterViewController:[self adoreNavigationController]];
    }
    self.leftDrawerViewController.selectionMode = ADLeftDrawerSelectionModeAdore;
}

- (void)showPendingUserView
{
    [self.drawerController presentViewController:[self pendingUserNavigationController] animated:YES completion:nil];
    self.leftDrawerViewController.selectionMode = ADLeftDrawerSelectionModeNone;
}

- (void)showChatView
{
    [self.drawerController closeDrawerAnimated:YES completion:nil];
    
    if (self.centerDrawerType != ADCenterDrawerViewControllerTypeChat) {
        self.centerDrawerType = ADCenterDrawerViewControllerTypeChat;
        [self.drawerController setCenterViewController:[self chatNavigationController]];
    }
    self.leftDrawerViewController.selectionMode = ADLeftDrawerSelectionModeChat;
}

- (void)showChatViewWithUser:(NSString *)email
{
    [self.drawerController closeDrawerAnimated:YES completion:nil];
    
    if (self.centerDrawerType != ADCenterDrawerViewControllerTypeChat) {
        self.centerDrawerType = ADCenterDrawerViewControllerTypeChat;
        [self.drawerController setCenterViewController:[self chatNavigationControllerWithEmail:email]];
    }
    self.leftDrawerViewController.selectionMode = ADLeftDrawerSelectionModeChat;
}

- (void)showSettingView
{
    [self.drawerController closeDrawerAnimated:YES completion:nil];

    if (self.centerDrawerType != ADCenterDrawerViewControllerTypeSetting) {
        self.centerDrawerType = ADCenterDrawerViewControllerTypeSetting;
        [self.drawerController setCenterViewController:[self settingNavigationController]];
    }
    self.leftDrawerViewController.selectionMode = ADLeftDrawerSelectionModeSettings;
}

- (void)showViewProfileView
{
    [self.drawerController closeDrawerAnimated:YES completion:nil];
    
    if (self.centerDrawerType != ADCenterDrawerViewControllerTypeViewProfile) {
        self.centerDrawerType = ADCenterDrawerViewControllerTypeViewProfile;
        [self.drawerController setCenterViewController:[self viewProfileController]];
    }
    self.leftDrawerViewController.selectionMode = ADLeftDrawerSelectionModeProfile;
}


#pragma mark -
#pragma mark - Helper methods

- (void)enableGesturesForDrawer
{
    self.drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeAll;
    self.drawerController.closeDrawerGestureModeMask = MMCloseDrawerGestureModeAll;
}

- (void)disableGesturesForDrawer
{
    self.drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeNone;
    self.drawerController.closeDrawerGestureModeMask = MMCloseDrawerGestureModeNone;
}

- (void)setupDebugEnvironment
{
    DDTTYLogger *logger = [DDTTYLogger sharedInstance];
    [DDLog addLogger:logger];
    [logger setColorsEnabled:YES];
    [logger setForegroundColor:[UIColor blueColor] backgroundColor:nil forFlag:DDLogFlagInfo];
    [logger setForegroundColor:[UIColor darkGrayColor] backgroundColor:[UIColor lightGrayColor] forFlag:DDLogFlagDebug];
    
    // Flurry
    NSString *versionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    if ([versionString length] == 0) {
        versionString = @"Unknown";
    }
    [Flurry setAppVersion:versionString];
    [Flurry setCrashReportingEnabled:YES];
    
#ifdef DEBUG_ADORE
    // Do nothing
#else
    if ([[NSBundle mainBundle] pathForResource:@"embedded" ofType:@"mobileprovision"]) {
        // DEVELOPMENT AND TESTING
    }
    else {
        // APP STORE
        [Flurry startSession:kAD_FLURRY_APP_KEY]; // Dayforce HCM
    }
#endif
}

- (void)initializeApplication
{
    UIApplication *application = [UIApplication sharedApplication];
    application.applicationIconBadgeNumber = 0;
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge
                                                                                             |UIUserNotificationTypeSound
                                                                                             |UIUserNotificationTypeAlert)
                                                                                 categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    else {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [application registerForRemoteNotificationTypes:myTypes];
    }
    
    // Start reachability monitoring and register for notification center
    Reachability *internetConnectionReach = [Reachability reachabilityForInternetConnection];
    [internetConnectionReach startNotifier];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    // Initialize managers
    [self.userManager initializeUserManager];
    [self.chatManager initializeChatManager];
    [self.backgroundJobManager initializeBackgroundJobManager];

    NSString *sessionTicket = [ADUserDefaultHelper token];
    if ([sessionTicket length] > 0) {
        [self.userManager updateSessionTicket:sessionTicket];
        [self.chatManager connectToChatService];
    }
    // or by checking [ADUserDefaultHelper isLoggedIn]
//    if ([ADUserDefaultHelper isLoggedIn]) {
//        [self.chatManager connectToChatService];
//    }
}

- (void)setupTheme
{
    // Tint color style
    UIColor *tintColor = [Constants femaleTintColor];
    
    [[UINavigationBar appearance] setBarTintColor:tintColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@ { NSForegroundColorAttributeName : [UIColor whiteColor] }];
    
    [[UITabBar appearance] setTintColor:tintColor];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window makeKeyAndVisible];
    self.window.tintColor = tintColor;
}

- (void)logoutCurrentUser
{
    self.logoutRequest = [[ADLogoutRequest alloc] initWithCompletionHandler:^(BOOL success, NSError *error) {
        DDLogInfo(@"Logout success status: %@", success ? @"YES" : @"NO");
    }];
    
    // do not wait for the callback, logout right away
    [ADUserDefaultHelper setLoggedIn:NO];
    
    [ADUserDefaultHelper setToken:@""];
    [self.userManager clearUserManager];
    [self.chatManager clearChatManager];
    
    [self.userManager initializeUserManager];
    [self.chatManager initializeChatManager];
    
    [self showLoginView];
    _nearbyNavigationController = nil;
    _viewProfileController = nil;
}

- (void)logoutMultiDeviceUser
{
    [ADUserDefaultHelper setLoggedIn:NO];
    
    [ADUserDefaultHelper setToken:@""];
    [self.userManager clearUserManager];
    [self.chatManager clearChatManager];
    
    [self.userManager initializeUserManager];
    [self.chatManager initializeChatManager];
    
    [self showForcedOutLoginView];
    _nearbyNavigationController = nil;
}


#pragma mark -
#pragma mark - Remote APNS registration method

// iOS 8
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    // save token to userDefaults
    [ADUserDefaultHelper setNotificationToken:token];
    
    NSLog(@" Registered device for remote notifications: %@", token);
    
    // Send token to server
}

- (void) application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@" Failed to register for remote notifications: %@", error);
}

// For interactive notification only
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}

#pragma mark -
#pragma mark - Receiving remote notifications

- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)notificationDict fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    application.applicationIconBadgeNumber = 0;
    NSLog(@" Received remote notifcation: %@", notificationDict);
    
    completionHandler(0);
}

// Reachability notification
- (void)reachabilityChanged:(NSNotification*)note
{
    Reachability * reach = [note object];
    
    if([reach isReachable])
    {
        [self.userManager checkSessionTicket];
        
//        NSString * temp = [NSString stringWithFormat:@"InternetConnection Notification Says Reachable(%@)", reach.currentReachabilityString];
//        NSLog(@"%@", temp);
        
        [self.chatManager reconnectToChatService];
    }
    else
    {
//        NSString * temp = [NSString stringWithFormat:@"InternetConnection Notification Says Unreachable(%@)", reach.currentReachabilityString];
//        NSLog(@"%@", temp);
        [self.chatManager disconnectFromChatService];
    }
}

@end
